use crate::precompile::{precompile_all, Precompile};
use crate::structure::{Component, ComponentId, Kind, Structure};
use crate::{Target, REPO_DIR};
use cargo_client::http::HttpCargoClient;
use cargo_client::{CargoClient, GetCrateError, GetCrateRequest};
use clap::Parser;
use eternaltwin_core::types::WeakError;
use gitlab_client::http::HttpGitlabClient;
use gitlab_client::{
  CreateReleaseError, CreateReleaseLinkRequestView, CreateReleaseRequestView, GetReleaseError, GetReleaseRequestView,
  GitlabAuth, GitlabClient, InputPackageStatus, InputReleaseAssetsView, InputReleaseLink, ProjectRef, ProjectSlug,
  PublishPackageFileRequestView, Release, ReleaseLinkType,
};
use npm_client::http::HttpNpmClient;
use npm_client::{GetPackageError, GetPackageRequest, NpmClient};
use packagist_client::http::HttpPackagistClient;
use std::collections::BTreeSet;
use std::fs;
use std::io::{ErrorKind, Write};
use std::process::Command;
use std::time::Duration;

/// Arguments to the `publish` task.
#[derive(Debug, Parser)]
pub struct Args {
  #[clap(long)]
  skip_rust: bool,
  #[clap(long)]
  gitlab_job_token: Option<String>,
  #[clap(long)]
  gitlab_private_token: Option<String>,
  /// Packages to publish:
  /// - `eternaltwin-x86_64-apple-darwin`
  /// - `eternaltwin-x86_64-pc-windows-gnu`
  /// - `eternaltwin-x86_64-unknown-linux-gnu`
  /// - `native`
  components: Vec<String>,
  /// Publish dependencies before publishing the supplied components
  #[clap(long)]
  recursive: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("unexpected publish failure")]
  Inner(#[source] WeakError),
}

pub async fn run(args: &Args) -> Result<(), Error> {
  let s = Structure::load();
  // dbg!(&s);

  let mut roots: BTreeSet<ComponentId> = BTreeSet::new();
  if args.components.is_empty() {
    eprintln!(
      "no components provided, publishing all (except exe, which should first be published by the GitLab CI job)"
    );
    // panic!("support for all components is not implemented yet");
    roots.insert(ComponentId::new("rs_lib_eternaltwin_cli"));
    roots.insert(ComponentId::new("ts_lib_eternaltwin_cli"));
    roots.insert(ComponentId::new("php_lib_eternaltwin_cli"));
  } else {
    for c in &args.components {
      match c.as_str() {
        "exe" => roots.extend(
          s.components
            .values()
            .filter(|c| c.kind == Kind::Executable)
            .map(|c| c.id.clone()),
        ),
        id => {
          let id = ComponentId::new(id);
          match s.components.get(&id) {
            Some(_) => roots.insert(id),
            None => panic!("unknown component {}", id.as_str()),
          };
        }
      }
    }
  }
  let wanted: BTreeSet<ComponentId> = if !args.recursive {
    roots.clone()
  } else {
    let mut wanted = BTreeSet::new();
    let mut stack: Vec<ComponentId> = roots.iter().cloned().collect();
    while let Some(id) = stack.pop() {
      let fresh = wanted.insert(id.clone());
      if !fresh {
        continue;
      }
      let c = s.components.get(&id).expect("component exists");
      for d in &c.deps {
        if !wanted.contains(&d.id) {
          stack.push(d.id.clone())
        }
      }
    }
    wanted
  };

  let mut needs_gitlab_auth = false;
  let mut topo: Vec<&Component> = Vec::new();
  for id in &s.topo {
    if !wanted.contains(id) {
      continue;
    }
    let c = s.components.get(id).expect("component exists");
    if c.kind == Kind::Executable {
      needs_gitlab_auth = true;
    }
    topo.push(c);
  }

  let gl_auth = if needs_gitlab_auth {
    let gl_auth = if let Some(token) = args.gitlab_private_token.as_deref() {
      GitlabAuth::PrivateToken(token)
    } else if let Some(token) = args.gitlab_job_token.as_deref() {
      GitlabAuth::JobToken(token)
    } else {
      panic!("missing GitLab authentication");
    };
    Some(gl_auth)
  } else {
    None
  };

  eprintln!("wanted packages");
  let mut missing = Vec::new();
  for c in topo {
    let is_published = match is_published(c, gl_auth).await {
      Ok(is_published) => is_published,
      Err(e) => {
        panic!("failed to retrieve publication status for {}: {e}", c.id.as_str())
      }
    };
    let is_root = roots.contains(&c.id);
    eprintln!(
      "- {}{}{}",
      c.id.as_str(),
      if is_published { " [published]" } else { "" },
      if is_root { " [root]" } else { "" }
    );
    if !is_published {
      missing.push(c);
    }
  }
  eprintln!("applying changes");
  for c in missing {
    eprintln!("- publish_start: {}", c.id.as_str(),);
    publish_one(c, gl_auth, Some(false)).await.expect("publication failed");
    eprintln!("- publish_complete: {}", c.id.as_str(),);
  }
  Ok(())
}

async fn publish_one(
  component: &Component,
  gl_auth: Option<GitlabAuth<&str>>,
  is_published_hint: Option<bool>,
) -> Result<(), WeakError> {
  if component.private {
    eprintln!("skipping publication of private component {:?}", component.id.as_str());
    return Ok(());
  }
  if is_published_hint.unwrap_or(false) {
    eprintln!(
      "skipping publication of already published component {:?}",
      component.id.as_str()
    );
    return Ok(());
  }
  match component.id.as_str() {
    "rs_lib_eternaltwin_app" => publish_rs_lib_eternaltwin_app(component).await?,
    _ => publish_one_inner(component, gl_auth).await?,
  }
  if matches!(component.kind, Kind::RustBin | Kind::RustLib) {
    eprintln!("ok; sleeping for 10s");
    tokio::time::sleep(Duration::from_secs(10)).await;
  }
  Ok(())
}

async fn publish_one_inner(component: &Component, gl_auth: Option<GitlabAuth<&str>>) -> Result<(), WeakError> {
  match component.kind {
    Kind::RustLib | Kind::RustBin => {
      eprintln!("publish rust");
      let crate_dir = REPO_DIR.join(&component.source);
      let crate_dir = crate_dir.as_path();
      eprintln!("publishing crate {}: {}", component.name, crate_dir.display());
      let mut cmd = Command::new("cargo");
      cmd.current_dir(crate_dir);
      cmd.arg("publish");
      if component.id.as_str() == "rs_lib_eternaltwin_app" {
        cmd.arg("--allow-dirty");
      }
      let s = cmd.status().map_err(WeakError::wrap)?;
      assert!(s.success());
      Ok(())
    }
    Kind::Executable => {
      let gl_client = HttpGitlabClient::new();
      publish_exe(component, &gl_client, gl_auth.expect("missing gl_auth")).await?;
      Ok(())
    }
    Kind::TsLib => {
      eprintln!("publish ts");
      let pkg_dir = REPO_DIR.join(&component.source);
      let pkg_dir = pkg_dir.as_path();
      eprintln!("publishing package {}: {}", component.name, pkg_dir.display());
      let mut cmd = Command::new("yarn");
      cmd.current_dir(pkg_dir);
      cmd.args(["npm", "publish", "--tolerate-republish"]);
      let s = cmd.status().map_err(WeakError::wrap)?;
      assert!(s.success());
      Ok(())
    }
    Kind::PhpLib => {
      eprintln!("publish php");
      let pkg_dir = REPO_DIR.join(&component.source);
      let pkg_dir = pkg_dir.as_path();
      eprintln!("publishing package {}: {}", component.name, pkg_dir.display());
      let mut cmd = Command::new("composer");
      cmd.current_dir(pkg_dir);
      cmd.args(["run-script", "publish"]);
      let s = cmd.status().map_err(WeakError::wrap)?;
      assert!(s.success());
      Ok(())
    }
  }
}

async fn publish_rs_lib_eternaltwin_app(component: &Component) -> Result<(), WeakError> {
  let ts_dir = REPO_DIR.join("packages/website");
  let rs_dir = REPO_DIR.join(&component.source);
  let yarn_out = Command::new("yarn")
    .args(["run", "minimal:build"])
    .current_dir(ts_dir.as_path())
    .output();
  let yarn_out = match yarn_out {
    Ok(out) => out,
    Err(e) => {
      panic!("ERR: `yarn run minimal:build` failed: {e:?}");
    }
  };
  let status = yarn_out.status;
  if !status.success() {
    std::io::stderr().write_all("==STDOUT++\n".as_bytes()).unwrap();
    std::io::stderr().write_all(&yarn_out.stdout).unwrap();
    std::io::stderr().write_all("==STDERR++\n".as_bytes()).unwrap();
    std::io::stderr().write_all(&yarn_out.stderr).unwrap();
    panic!("ERR: `yarn run minimal:build` had non-success exit status: {status:?}");
  };
  let browser_source = ts_dir.join("dist/minimal/browser");
  let browser_target = rs_dir.join("browser");
  // ensure the browser dir is fresh
  match fs::remove_dir_all(browser_target.as_path()) {
    Ok(()) => {}
    Err(e) if e.kind() == ErrorKind::NotFound => {}
    Err(e) => {
      panic!("clearing target browser dir failed: {e:?}");
    }
  }
  {
    fs::create_dir_all(browser_target.as_path()).expect("creating the browser target dir succeeds");
    let mut options = fs_extra::dir::CopyOptions::new();
    options.content_only = true;
    fs_extra::dir::copy(browser_source, &browser_target, &options).expect("copy should succeed");
  }
  let rs_manifest = rs_dir.join("Cargo.toml");
  let old_manifest = fs::read_to_string(&rs_manifest).expect("failed to read old manifest");
  let new_manifest = old_manifest.replace("publish = false", "publish = true");
  fs::write(&rs_manifest, new_manifest).expect("failed to write new manifest");
  let res = publish_one_inner(component, None).await;
  fs::write(&rs_manifest, old_manifest).expect("failed to write old manifest");
  res
}

// fn publish_npm_exe(pkg_dir: PathBuf) -> Result<(), Box<dyn Error>> {
//   let dev_toml = fs::read_to_string(pkg_dir.join("Cargo.toml"))?;
//   let dev_package_json = fs::read_to_string(pkg_dir.join("package.json"))?;
//
//   let publish_toml = dev_toml
//     .replace("\n# [workspace]\n", "\n[workspace]\n")
//     .replace(r#", path = "../../crates/cli""#, "");
//   assert_ne!(publish_toml, dev_toml);
//   let publish_package_json = dev_package_json.replace("\"//install\"", "\"install\"");
//   assert_ne!(publish_package_json, dev_package_json);
//
//   fs::write(pkg_dir.join("Cargo.toml"), publish_toml)?;
//   fs::write(pkg_dir.join("package.json"), publish_package_json)?;
//
//   let mut cmd = Command::new("yarn");
//   cmd.current_dir(&pkg_dir);
//   cmd.arg("npm").arg("publish");
//   let s = cmd.status()?;
//   assert!(s.success());
//   sleep(Duration::from_secs(30));
//
//   fs::write(pkg_dir.join("Cargo.toml"), dev_toml)?;
//   fs::write(pkg_dir.join("package.json"), dev_package_json)?;
//   Ok(())
// }

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NativePackage {
  pub name: &'static str,
  pub target: Target,
}

async fn publish_exe<Gl: GitlabClient>(
  component: &Component,
  gl_client: &Gl,
  gl_auth: GitlabAuth<&str>,
) -> Result<(), WeakError> {
  let target = component.target.expect("target exists");
  let package_name = exe_package_name(target);
  let mut result = precompile_all(&Precompile::one(target)).expect("failed to precompile");
  let (_, (path, meta)) = result.pop_first().expect("precompiled result is available");
  if is_published_exe(component, gl_client, gl_auth).await? {
    eprintln!("already published");
    return Ok(());
  }
  let exe_path = path.join(meta.executable);
  let exe_name = exe_path
    .file_name()
    .expect("exe name exists")
    .to_str()
    .expect("exe name is valid");
  let exe = fs::read(exe_path.as_path()).expect("failed to read exe");
  let exe_result = gl_client
    .publish_package_file(PublishPackageFileRequestView {
      auth: Some(gl_auth),
      project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
      package_name: package_name.as_str(),
      package_version: meta.version.as_str(),
      filename: exe_name,
      status: InputPackageStatus::Default,
      data: exe.as_slice(),
    })
    .await
    .expect("failed to upload exe");
  // not `metadata`, `meta_data`...
  let meta_data = fs::read(path.join("meta.json")).expect("failed to read meta");
  let meta_result = gl_client
    .publish_package_file(PublishPackageFileRequestView {
      auth: Some(gl_auth),
      project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
      package_name: package_name.as_str(),
      package_version: meta.version.as_str(),
      filename: "meta.json",
      status: InputPackageStatus::Default,
      data: meta_data.as_slice(),
    })
    .await
    .expect("failed to upload meta");

  assert_eq!(exe_result.package_id, meta_result.package_id, "package id must match");
  eprintln!("published package files for: {}", package_name);
  let package_id = meta_result.package_id;
  let tag_name = format!("v{}", meta.version);
  let url = format!("https://gitlab.com/eternaltwin/eternaltwin/-/packages/{package_id}");
  let direct_asset_path = format!("/{}", package_name);
  let release_result = gl_client
    .create_release(CreateReleaseRequestView {
      auth: Some(gl_auth),
      project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
      tag_name: tag_name.as_str(),
      name: None,
      tag_message: None,
      description: None,
      r#ref: None,
      assets: InputReleaseAssetsView {
        links: &[InputReleaseLink {
          name: package_name.to_string(),
          url: url.clone(),
          direct_asset_path: Some(direct_asset_path.clone()),
          link_type: ReleaseLinkType::Package,
        }],
      },
      released_at: None,
    })
    .await;
  match release_result {
    Ok(_) => Ok(()),
    Err(CreateReleaseError::AlreadyExists) => {
      gl_client
        .create_release_link(CreateReleaseLinkRequestView {
          auth: Some(gl_auth),
          project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
          tag_name: tag_name.as_str(),
          name: package_name.as_str(),
          url: url.as_str(),
          direct_asset_path: Some(direct_asset_path.as_str()),
          link_type: ReleaseLinkType::Package,
        })
        .await
        .expect("failed to create release link");
      Ok(())
    }
    r => {
      r.expect("release creation failed");
      Err(WeakError::new("release creation failed"))
    }
  }
}

async fn is_published(component: &Component, gl_auth: Option<GitlabAuth<&str>>) -> Result<bool, WeakError> {
  match component.kind {
    Kind::Executable => {
      is_published_exe(
        component,
        &HttpGitlabClient::new(),
        gl_auth.expect("gl auth is provided"),
      )
      .await
    }
    Kind::RustLib | Kind::RustBin => {
      let client = HttpCargoClient::new();
      match client
        .get_crate(GetCrateRequest {
          name: component.name.as_str(),
        })
        .await
      {
        Ok(krate) => Ok(krate.versions.iter().any(|v| v.num == component.local_version)),
        Err(GetCrateError::NotFound) => Ok(false),
        Err(e) => Err(WeakError::wrap(e)),
      }
    }
    Kind::TsLib => {
      let client = HttpNpmClient::new();
      match client
        .get_package(GetPackageRequest {
          name: component.name.as_str(),
        })
        .await
      {
        Ok(pkg) => Ok(pkg.versions.contains_key(&component.local_version)),
        Err(GetPackageError::NotFound) => Ok(false),
        Err(e) => Err(WeakError::wrap(e)),
      }
    }
    Kind::PhpLib => {
      use packagist_client::PackagistClient;

      let (vendor, package) = component.name.split_once('/').expect("composer package is well-formed");

      let client = HttpPackagistClient::new();
      match client
        .get_package(packagist_client::GetPackageRequest { vendor, package })
        .await
      {
        Ok(pkg_list) => Ok(pkg_list.iter().any(|pkg| pkg.version == component.local_version)),
        Err(packagist_client::GetPackageError::NotFound) => Ok(false),
        Err(e) => Err(WeakError::wrap(e)),
      }
    }
  }
}

/// Check if this package is already published
async fn is_published_exe<Gl: GitlabClient>(
  component: &Component,
  gl_client: &Gl,
  gl_auth: GitlabAuth<&str>,
) -> Result<bool, WeakError> {
  let target = component.target.expect("exe must have target");
  let name = exe_package_name(target);
  let tag_name = format!("v{}", component.local_version);
  let release: Release = match gl_client
    .get_release(GetReleaseRequestView {
      auth: Some(gl_auth),
      project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
      tag_name: tag_name.as_str(),
      include_html_description: false,
    })
    .await
  {
    Ok(release) => release,
    Err(GetReleaseError::NotFound) => return Ok(false),
    Err(e) => return Err(WeakError::wrap(e)),
  };
  let is_published = release.assets.links.iter().any(|l| l.name == name);
  Ok(is_published)
}

fn exe_package_name(target: Target) -> String {
  format!("eternaltwin-{}", target.name)
}

// https://gitlab.com/api/v4/projects/eternaltwin%2Feternaltwin/packages/generic/eternaltwin-x86_64-apple-darwin/0.12.5/meta.json
