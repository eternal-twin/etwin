use crate::metagen::backend::kotlin::KotlinBackend;
use crate::metagen::backend::php::PhpBackend;
use crate::metagen::core::TypeRegistry;
use crate::metagen::etwin::register_etwin;
use eternaltwin_core::types::WeakError;

pub mod backend;
mod core;
mod etwin;

pub fn kotlin() -> Result<(), WeakError> {
  let mut registry = TypeRegistry::builder();
  register_etwin(&mut registry)?;
  let registry = registry.build()?;

  let working_dir = std::env::current_dir().map_err(WeakError::wrap)?;
  let backend = KotlinBackend::new(
    working_dir.join("clients/kotlin/src/main/kotlin"),
    vec!["net".to_string(), "eternaltwin".to_string()],
  );
  backend.emit(&registry)?;
  Ok(())
}

pub fn php() -> Result<(), WeakError> {
  let mut registry = TypeRegistry::builder();
  register_etwin(&mut registry)?;
  let registry = registry.build()?;

  let working_dir = std::env::current_dir().map_err(WeakError::wrap)?;
  let backend = PhpBackend::new(working_dir.join("clients/php/src"), vec!["Eternaltwin".to_string()]);
  backend.emit(&registry)?;
  Ok(())
}
