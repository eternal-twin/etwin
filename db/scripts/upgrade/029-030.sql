ALTER TABLE users
  ALTER COLUMN period SET NOT NULL,
  DROP COLUMN created_at;

CREATE VIEW user_current AS
SELECT u.user_id, u.period,
       lower(uh.period) AS updated_at, uh.updated_by, u.is_administrator, uh.display_name, uuh.username,
       email_addresses.email_address AS email, uh.email AS _email_hash, password
FROM users AS u
       INNER JOIN users_history AS uh USING (user_id, _is_current)
       LEFT OUTER JOIN user_username_history AS uuh ON (uuh.user_id = u.user_id AND UPPER(uuh.period) IS NULL)
       LEFT OUTER JOIN email_addresses ON uh.email = email_addresses._hash
