alter table twinoid_user_links
  drop constraint twinoid_user_link__twinoid_user__fk,
  alter column twinoid_user_id TYPE twinoid_user_id USING twinoid_user_id::twinoid_user_id;
alter table twinoid_access_tokens
  drop constraint twinoid_access_token__twinoid_user__fk,
  alter column twinoid_user_id TYPE twinoid_user_id USING twinoid_user_id::twinoid_user_id;
alter table old_twinoid_access_tokens
  drop constraint twinoid_access_token__twinoid_user__fk,
  alter column twinoid_user_id TYPE twinoid_user_id USING twinoid_user_id::twinoid_user_id;
alter table twinoid_refresh_tokens
  drop constraint twinoid_refresh_token__twinoid_user__fk,
  alter column twinoid_user_id TYPE twinoid_user_id USING twinoid_user_id::twinoid_user_id;
alter table old_twinoid_refresh_tokens
  drop constraint old_twinoid_refresh_token__twinoid_user__fk,
  alter column twinoid_user_id TYPE twinoid_user_id USING twinoid_user_id::twinoid_user_id;

alter table twinoid_user_links
  add constraint twinoid_user_link__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES twinoid_user(twinoid_user_id) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table twinoid_access_tokens
  add constraint twinoid_access_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES twinoid_user(twinoid_user_id) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table old_twinoid_access_tokens
  add constraint old_twinoid_access_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES twinoid_user(twinoid_user_id) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table twinoid_refresh_tokens
  add constraint twinoid_refresh_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES twinoid_user(twinoid_user_id) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table old_twinoid_refresh_tokens
  add constraint old_twinoid_refresh_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES twinoid_user(twinoid_user_id) ON DELETE RESTRICT ON UPDATE CASCADE;
