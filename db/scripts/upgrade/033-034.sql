CREATE TABLE hammerfest_session
(
  period                       PERIOD_LOWER       NOT NULL,
  retrieved_at                 INSTANT_SET        NOT NULL,
  hammerfest_server            HAMMERFEST_SERVER  NOT NULL,
  _hammerfest_session_key_hash BYTEA              NOT NULL,
--
  hammerfest_user_id           HAMMERFEST_USER_ID NULL,
--
  hammerfest_session_key       BYTEA              NOT NULL,
  PRIMARY KEY (period, hammerfest_server, _hammerfest_session_key_hash),
  EXCLUDE USING gist (hammerfest_server WITH =, _hammerfest_session_key_hash WITH =, period WITH &&),
  EXCLUDE USING gist (hammerfest_server WITH =, hammerfest_user_id WITH =, period WITH &&),
  CONSTRAINT hammerfest_session__user__fk FOREIGN KEY (hammerfest_server, hammerfest_user_id) REFERENCES hammerfest_users (hammerfest_server, hammerfest_user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
