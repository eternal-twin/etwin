export enum RequestType {
  GetAuthSelf,
  GetClock,
  GetUserById,
}
