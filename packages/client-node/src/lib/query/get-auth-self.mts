import {$AuthContext,AuthContext} from "@eternaltwin/core/auth/auth-context";
import {RfcOauthAccessTokenKey} from "@eternaltwin/core/oauth/rfc-oauth-access-token-key";
import {Observable} from "rxjs";

import {HttpHandler} from "../http-handler.mjs";
import {RequestType} from "../request-type.mjs";

export const TYPE: RequestType.GetAuthSelf = RequestType.GetAuthSelf as const;

export interface Request {
  readonly type: typeof TYPE;
  readonly auth: RfcOauthAccessTokenKey;
}

export type Response = AuthContext;

export function handle(handler: HttpHandler, query: Request): Observable<Response> {
  return handler("get", ["auth", "self"], {auth: query.auth, resType: $AuthContext});
}
