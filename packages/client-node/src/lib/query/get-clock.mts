import {$ClockState, ClockState} from "@eternaltwin/core/clock/clock-state";
import {Observable} from "rxjs";

import {HttpHandler} from "../http-handler.mjs";
import {RequestType} from "../request-type.mjs";

export const TYPE: RequestType.GetClock = RequestType.GetClock as const;

export interface Request {
  readonly type: typeof TYPE;
}

export type Response = ClockState;

export function handle(handler: HttpHandler, _query: Request): Observable<Response> {
  return handler("get", ["dev", "clock"], {resType: $ClockState});
}
