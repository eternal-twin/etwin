import { Environment } from "./environment-type.mjs";

export const environment: Environment = {
  production: false,
  apiBase: "http://localhost:50320/api/v1",
};
