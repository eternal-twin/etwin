import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DonateComponent } from "./donate.component.mjs";

const routes: Routes = [
  {path: "", component: DonateComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DonateRoutingModule {
}
