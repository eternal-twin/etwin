import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { SharedModule } from "../../shared/shared.module.mjs";
import { LoginComponent } from "./login.component.mjs";
import { LoginEtwinComponent } from "./login-etwin.component.mjs";
import { LoginRoutingModule } from "./login-routing.module.mjs";

@NgModule({
  declarations: [
    LoginComponent,
    LoginEtwinComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    SharedModule,
  ],
})
export class LoginModule {
}
