import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { RegisterComponent } from "./register.component.mjs";
import { RegisterEmailComponent } from "./register-email.component.mjs";
import { RegisterUsernameComponent } from "./register-username.component.mjs";

const routes: Routes = [
  {path: "", component: RegisterComponent},
  {path: "email", component: RegisterEmailComponent},
  {path: "username", component: RegisterUsernameComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterRoutingModule {
}
