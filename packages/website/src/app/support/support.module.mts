import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module.mjs";
import { SupportRoutingModule } from "./support-routing.module.mjs";
import { SupportViewComponent } from "./support-view.component.mjs";

@NgModule({
  declarations: [SupportViewComponent],
  imports: [
    SupportRoutingModule,
    SharedModule,
  ],
})
export class SupportModule {
}
