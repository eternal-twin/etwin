import {APP_BASE_HREF} from "@angular/common";
import {NgModule} from "@angular/core";

import {BrowserAuthModule} from "../modules/auth/auth.module.browser.mjs";
import {BrowserConfigModule} from "../modules/config/config.module.browser.mjs";
import {DinoparcModule} from "../modules/dinoparc/dinoparc.module.mjs";
import {ForumModule} from "../modules/forum/forum.module.mjs";
import {HammerfestModule} from "../modules/hammerfest/hammerfest.module.mjs";
import {BrowserRestModule} from "../modules/rest/rest.module.browser.mjs";
import {TwinoidModule} from "../modules/twinoid/twinoid.module.mjs";
import {UserModule} from "../modules/user/user.module.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppModule} from "./app.module.mjs";
import {AppRoutingModule} from "./app-routing.module.mjs";

@NgModule({
  imports: [
    AppModule,
    AppRoutingModule,
    BrowserAuthModule,
    BrowserConfigModule,
    DinoparcModule,
    ForumModule,
    HammerfestModule,
    TwinoidModule,
    UserModule,
    BrowserRestModule,
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: "/"},
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {
}
