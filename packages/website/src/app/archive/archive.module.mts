import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module.mjs";
import { ArchiveHomeView } from "./archive-home.view.mjs";
import { ArchiveRoutingModule } from "./archive-routing.module.mjs";

@NgModule({
  declarations: [ArchiveHomeView],
  imports: [
    CommonModule,
    ArchiveRoutingModule,
    SharedModule,
  ],
})
export class ArchiveModule {
}
