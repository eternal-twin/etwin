import {Injectable, NgModule} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes} from "@angular/router";
import {$HammerfestServer, HammerfestServer} from "@eternaltwin/core/hammerfest/hammerfest-server";
import {HammerfestUser} from "@eternaltwin/core/hammerfest/hammerfest-user";
import {$HammerfestUserId, HammerfestUserId} from "@eternaltwin/core/hammerfest/hammerfest-user-id";
import {CheckId, NOOP_CONTEXT, Result} from "kryo";
import {$Date} from "kryo/date";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";
import {Observable, of as rxOf} from "rxjs";

import {HammerfestService} from "../../../modules/hammerfest/hammerfest.service.mjs";
import {HammerfestHomeView} from "./hammerfest-home.view.mjs";
import {HammerfestUserView} from "./hammerfest-user.view.mjs";

@Injectable()
export class HammerfestUserResolverService implements Resolve<HammerfestUser | null> {
  readonly #router: Router;
  readonly #hammerfest: HammerfestService;

  constructor(router: Router, hammerfest: HammerfestService) {
    this.#router = router;
    this.#hammerfest = hammerfest;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<HammerfestUser | null> {
    const {
      ok: okServer,
      value: server
    }: Result<HammerfestServer, CheckId> = $HammerfestServer.test(NOOP_CONTEXT, route.paramMap.get("server"));
    const {
      ok: okUserId,
      value: userId
    }: Result<HammerfestUserId, CheckId> = $HammerfestUserId.test(NOOP_CONTEXT, route.paramMap.get("user_id"));
    const {
      ok: okTime,
      value: rawTime
    }: Result<Date, CheckId> = $Date.read(NOOP_CONTEXT, SEARCH_PARAMS_VALUE_READER, route.paramMap.get("time"));
    if (!okServer || !okUserId) {
      return rxOf(null);
    }
    const time: Date | undefined = okTime ? rawTime : undefined;
    return this.#hammerfest.getUser({server, id: userId, time});
  }
}

const routes: Routes = [
  {
    path: "",
    component: HammerfestHomeView,
    resolve: {},
  },
  {
    path: ":server/users/:user_id",
    component: HammerfestUserView,
    resolve: {
      user: HammerfestUserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [HammerfestUserResolverService],
})
export class HammerfestRoutingModule {
}
