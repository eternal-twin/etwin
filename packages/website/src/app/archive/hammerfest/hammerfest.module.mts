import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HammerfestModule as HammerfestServiceModule } from "../../../modules/hammerfest/hammerfest.module.mjs";
import { SharedModule } from "../../shared/shared.module.mjs";
import { HammerfestHomeView } from "./hammerfest-home.view.mjs";
import {HammerfestItemComponent} from "./hammerfest-item.component.mjs";
import { HammerfestRoutingModule } from "./hammerfest-routing.module.mjs";
import { HammerfestUserComponent } from "./hammerfest-user.component.mjs";
import { HammerfestUserView } from "./hammerfest-user.view.mjs";

@NgModule({
  declarations: [HammerfestHomeView, HammerfestItemComponent, HammerfestUserComponent, HammerfestUserView],
  imports: [
    CommonModule,
    HammerfestRoutingModule,
    HammerfestServiceModule,
    SharedModule,
  ],
})
export class HammerfestModule {
}
