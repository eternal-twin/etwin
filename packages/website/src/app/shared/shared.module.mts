import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import {DeleteUserButtonComponent} from "./delete-user-button.component.mjs";
import { EtwinBarComponent } from "./etwin-bar.component.mjs";
import { EtwinFooterComponent } from "./etwin-footer.component.mjs";
import { LanguagePickerComponent } from "./language-picker.component.mjs";
import { MainLayoutComponent } from "./main-layout.component.mjs";
import { PaginationComponent } from "./pagination.component.mjs";
import { SmallLayoutComponent } from "./small-layout.component.mjs";
import { TextEditorComponent } from "./text-editor/text-editor.component.mjs";
import { UnlinkDinoparcButtonComponent } from "./unlink-dinoparc-button.component.mjs";
import { UnlinkHammerfestButtonComponent } from "./unlink-hammerfest-button.component.mjs";
import { UnlinkTwinoidButtonComponent } from "./unlink-twinoid-button.component.mjs";
import { UserLinkComponent } from "./user-link.component.mjs";

@NgModule({
  declarations: [
    DeleteUserButtonComponent,
    EtwinBarComponent,
    EtwinFooterComponent,
    LanguagePickerComponent,
    MainLayoutComponent,
    PaginationComponent,
    SmallLayoutComponent,
    UnlinkDinoparcButtonComponent,
    UnlinkHammerfestButtonComponent,
    UnlinkTwinoidButtonComponent,
    UserLinkComponent,
    TextEditorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([]),
  ],
  exports: [MainLayoutComponent, SmallLayoutComponent, RouterModule, PaginationComponent, UserLinkComponent, UnlinkHammerfestButtonComponent, UnlinkDinoparcButtonComponent, UnlinkTwinoidButtonComponent, DeleteUserButtonComponent, TextEditorComponent],
})
export class SharedModule {
}
