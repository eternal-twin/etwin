import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "player-tag",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./player-tag.component.html",
  styleUrls: ["./player-tag.component.scss"],
})
export class PlayerTagComponent {
  @Input() username!: string;
  @Input() isFriend: boolean = false;
}