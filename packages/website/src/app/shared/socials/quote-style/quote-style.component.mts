import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "quote-style",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./quote-style.component.html",
  styleUrls: ["./quote-style.component.scss"],
})
export class QuoteStyleComponent {
  @Input() author: string = "";
}