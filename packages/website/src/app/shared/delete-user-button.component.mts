import { DOCUMENT } from "@angular/common";
import { Component, Inject, Input } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { UserId } from "@eternaltwin/core/user/user-id";
import { Subscription } from "rxjs";

import { UserService } from "../../modules/user/user.service.mjs";

@Component({
  selector: "etwin-delete-user-button",
  templateUrl: "./delete-user-button.component.html",
  styleUrls: [],
})
export class DeleteUserButtonComponent {
  @Input()
  public set userId(value: UserId | undefined) {
    this.userIdControl.setValue(value ?? "");
  }

  public get userId(): UserId | undefined {
    const value: UserId | "" = this.userIdControl.value;
    return value === "" ? undefined : value;
  }

  public readonly deleteUserForm: FormGroup;
  public readonly userIdControl: FormControl;
  public subscription: Subscription | null = null;
  public serverError: Error | null = null;

  private readonly document: Document;
  private readonly route: ActivatedRoute;
  private readonly user: UserService;

  constructor(
    route: ActivatedRoute,
    user: UserService,
    @Inject(DOCUMENT) document: Document,
  ) {
    this.document = document;
    this.route = route;
    this.user = user;

    this.userIdControl = new FormControl(
      "",
      [Validators.required],
    );
    this.deleteUserForm = new FormGroup({
      userId: this.userIdControl,
    });
  }

  ngOnInit(): void {}

  public onSubmitDeleteUser(event: Event) {
    event.preventDefault();
    if (this.subscription !== null) {
      return;
    }
    const model: any = this.deleteUserForm.getRawValue();
    const userId: UserId = model.userId;
    const updateResult$ = this.user.deleteUser(userId);
    this.serverError = null;
    const subscription: Subscription = updateResult$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.subscription = null;
        this.document.location.reload();
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.subscription = null;
        this.serverError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.subscription = null;
      },
    });
    this.subscription = subscription;
  }
}
