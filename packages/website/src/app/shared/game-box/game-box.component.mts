import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "app-game-box",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./game-box.component.html",
  styleUrls: ["./game-box.component.scss"],
})
export class GameBoxComponent {
  @Input() cover!: string;
  @Input() color!: string;
}
