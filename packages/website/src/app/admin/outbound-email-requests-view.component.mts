import {Component} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {OutboundEmailListing} from "@eternaltwin/core/mailer/outbound-email-listing";
import {OutboundEmailRequestListing} from "@eternaltwin/core/mailer/outbound-email-request-listing";
import {NEVER, Observable} from "rxjs";

import {MailerService} from "../../modules/mailer/mailer.service.mjs";

@Component({
  selector: "et-admin-outbound-email-requests-view",
  template:
`
  <etwin-main-layout>
    <h1>Outbound email requests</h1>
    <form
      method="POST"
      enctype="application/x-www-form-urlencoded"
      action="#"
      [formGroup]="form"
      (ngSubmit)="onSubmit($event)"
    >
      <input type="submit" class="btn primary" name="query"
             value="Query"/>
    </form>


    @if (result$ | async; as result) {
      <pre>{{result | json}}</pre>
    }
    @if (error$ | async; as error) {
      <pre>{{error | json}}</pre>
    }

  </etwin-main-layout>
`,
  styleUrls: [],
})
export class OutboundEmailRequestsViewComponent {
  public readonly form: FormGroup;
  public error$: Observable<string>;
  public result$: Observable<OutboundEmailRequestListing>;
  #mailerService: MailerService;

  constructor(mailerService: MailerService) {
    this.form = new FormGroup({});
    this.result$ = NEVER;
    this.error$ = NEVER;
    this.#mailerService = mailerService;
  }

  onSubmit(ev: SubmitEvent): void {
    ev.preventDefault();
    this.result$ = NEVER;
    this.error$ = NEVER;
    const $req = this.#mailerService.getOutboundEmailRequests({limit: 10});
    this.result$ = $req;
  }
}
