import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { GameBoxComponent } from "../shared/game-box/game-box.component.mjs";
import { SharedModule } from "../shared/shared.module.mjs";
import { GamesComponent } from "./games.component.mjs";
import { GamesRoutingModule } from "./games-routing.module.mjs";

@NgModule({
  declarations: [GamesComponent],
  imports: [
    GamesRoutingModule,
    SharedModule,
    GameBoxComponent,
    CommonModule
  ],
})
export class GamesModule {
}
