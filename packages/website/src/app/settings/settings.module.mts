import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {JobModule} from "../../modules/job/job.module.mjs";
import {SharedModule} from "../shared/shared.module.mjs";
import {CreateJobComponent} from "./create-job.component.mjs";
import {LinkedDinoparcSettingsComponent} from "./linked-dinoparc-settings.component.mjs";
import {LinkedHammerfestSettingsComponent} from "./linked-hammerfest-settings.component.mjs";
import {LinkedTwinoidSettingsComponent} from "./linked-twinoid-settings.component.mjs";
import {LinkedUsersSettingsComponent} from "./linked-users-settings.component.mjs";
import {SettingsRoutingModule} from "./settings-routing.module.mjs";
import {SettingsViewComponent} from "./settings-view.component.mjs";

@NgModule({
  declarations: [
    CreateJobComponent,
    LinkedDinoparcSettingsComponent,
    LinkedHammerfestSettingsComponent,
    LinkedUsersSettingsComponent,
    LinkedTwinoidSettingsComponent,
    SettingsViewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    JobModule,
    ReactiveFormsModule,
    SettingsRoutingModule,
    SharedModule,
  ],
})
export class SettingsModule {
}
