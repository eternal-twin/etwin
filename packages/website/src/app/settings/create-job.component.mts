import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {$CreateJob, CreateJob} from "@eternaltwin/core/job/create-job";
import {NOOP_CONTEXT} from "kryo";
import { Subscription } from "rxjs";

import {JobService} from "../../modules/job/job.service.mjs";

@Component({
  selector: "etwin-create-job",
  templateUrl: "./create-job.component.html",
  styleUrls: [],
})
export class CreateJobComponent {
  readonly #job: JobService;

  public readonly form: FormGroup;
  public readonly kind: FormControl;
  public readonly state: FormControl;

  public pendingSubscription: Subscription | null;
  public serverError: Error | null;

  constructor(job: JobService) {
    this.#job = job;

    this.kind = new FormControl(
      "",
      [Validators.required],
    );
    this.state = new FormControl(
      "",
      [Validators.required],
    );
    this.form = new FormGroup({
      kind: this.kind,
      state: this.state,
    });

    this.pendingSubscription = null;
    this.serverError = null;
  }

  public onSubmit(event: Event) {
    event.preventDefault();
    if (this.pendingSubscription !== null) {
      return;
    }
    const model: any = this.form.getRawValue();
    let state: unknown;
    try {
      state = JSON.parse(model.state);
    } catch (e) {
      this.serverError = new Error("Aborting request, invalid JSON");
      return;
    }
    const model2 = {kind: model.kind, state};
    const {ok: okCmd, value: cmd} = $CreateJob.test(NOOP_CONTEXT, model2);
    if (!okCmd) {
      this.serverError = new Error("Aborting request, invalid command");
      return;
    }
    const result$ = this.#job.createJob(cmd satisfies CreateJob);
    this.serverError = null;
    const subscription: Subscription = result$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
        this.serverError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
      }
    });
    this.pendingSubscription = subscription;
  }
}
