import {FrontendConfig, frontendConfigFromProcess} from "./config.mjs";
import {main} from "./index.mjs";

async function realMain(): Promise<void> {
  const config: FrontendConfig = await frontendConfigFromProcess();
  // Create a never-resolving promise so the API is never closed
  return new Promise<never>(() => {
    main(config);
  });
}

await realMain();
