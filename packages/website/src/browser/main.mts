import { enableProdMode } from "@angular/core";
import { platformBrowser } from "@angular/platform-browser";

import { AppBrowserModule } from "../app/app.module.browser.mjs";
import { environment } from "../environments/environment.mjs";

if (environment.production) {
  enableProdMode();
}

document.addEventListener("DOMContentLoaded", () => {
  platformBrowser().bootstrapModule(AppBrowserModule)
    .catch(err => console.error(err));
});
