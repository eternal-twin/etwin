import { InjectionToken } from "@angular/core";
import { Config } from "@eternaltwin/core/config/config";
import type { Request } from "koa";

export const CONFIG: InjectionToken<Config> = new InjectionToken("Config");

export const BACKEND_URI: InjectionToken<string> = new InjectionToken("BACKEND_URI");

export const INTERNAL_AUTH_KEY: InjectionToken<string> = new InjectionToken("INTERNAL_AUTH_KEY");

export const REQUEST: InjectionToken<Request> = new InjectionToken("REQUEST");
