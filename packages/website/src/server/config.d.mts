import { Url } from "@eternaltwin/core/core/url";
import { ForumConfig } from "@eternaltwin/core/forum/forum-config";

export interface ServerAppConfig {
  externalUri?: Url;
  isProduction: boolean;
  backendPort: number;
  internalAuthKey: Uint8Array;
  forum: ForumConfig;
}
