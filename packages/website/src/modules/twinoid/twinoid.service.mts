import { Injectable } from "@angular/core";
import { $TimeQuery } from "@eternaltwin/core/core/time-query";
import { GetTwinoidUserOptions } from "@eternaltwin/core/twinoid/get-twinoid-user-options";
import { $TwinoidUser, TwinoidUser } from "@eternaltwin/core/twinoid/twinoid-user";
import { Observable, of as rxOf } from "rxjs";
import { catchError as rxCatchError } from "rxjs/operators";

import { RestService } from "../rest/rest.service.mjs";

@Injectable()
export class TwinoidService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUser(options: Readonly<GetTwinoidUserOptions>): Observable<TwinoidUser | null> {
    return this.#rest
      .get(
        ["archive", "twinoid", "users", options.id],
        {
          queryType: $TimeQuery,
          query: {time: options.time},
          resType: $TwinoidUser,
        },
      )
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
