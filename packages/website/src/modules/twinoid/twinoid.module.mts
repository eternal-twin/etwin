import { NgModule } from "@angular/core";

import { TwinoidService } from "./twinoid.service.mjs";

@NgModule({
  providers: [
    TwinoidService,
  ],
  imports: [],
})
export class TwinoidModule {
}
