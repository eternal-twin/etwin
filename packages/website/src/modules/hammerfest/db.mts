export const HAMMERFEST_DB: any[] = [
  {
    "id": "0",
    "name": {
      "fr-FR": "Alphabet Cristallin",
      "en-US": "Cristal Alphabet",
      "es-SP": "Alfabeto Cristalino"
    },
    "unlock": 10
  },
  {
    "id": "3",
    "name": {
      "fr-FR": "Ballon de banquise",
      "en-US": "Beach Ball",
      "es-SP": "Balón de playa"
    },
    "unlock": 10
  },
  {
    "id": "4",
    "name": {
      "fr-FR": "Lampe Fétvoveu",
      "en-US": "Magic Lamp",
      "es-SP": "Lámpara Pidedeseo"
    },
    "unlock": 10
  },
  {
    "id": "7",
    "name": {
      "fr-FR": "Basket IcePump",
      "en-US": "Running Shoes",
      "es-SP": "Botín IcePump"
    },
    "unlock": 10
  },
  {
    "id": "13",
    "name": {
      "fr-FR": "Cass-Tet",
      "en-US": "Biker Helmet",
      "es-SP": "Casco"
    },
    "unlock": 10
  },
  {
    "id": "21",
    "name": {
      "fr-FR": "Enceinte Bessel-Son",
      "en-US": "Jukebox Igor",
      "es-SP": "Altavoz Bájaloya"
    },
    "unlock": 10
  },
  {
    "id": "24",
    "name": {
      "fr-FR": "Hippo-flocon",
      "en-US": "Iceberg Shower",
      "es-SP": "Pedrusco de los montes"
    },
    "unlock": 10
  },
  {
    "id": "64",
    "name": {
      "fr-FR": "Arc-en-miel",
      "en-US": "Honey Rainbow",
      "es-SP": "Arcomiel"
    },
    "unlock": 10
  },
  {
    "id": "74",
    "name": {
      "fr-FR": "Esprit de l'orange",
      "en-US": "Spirit of Orange",
      "es-SP": "Espíritu de la naranja"
    },
    "unlock": 10
  },
  {
    "id": "77",
    "name": {
      "fr-FR": "Lucidjané à crête bleue",
      "en-US": "Blue Crested Fish",
      "es-SP": "Lucidjané con cresta azul"
    },
    "unlock": 10
  },
  {
    "id": "84",
    "name": {
      "fr-FR": "Talisman scorpide",
      "en-US": "Scorpion Talisman",
      "es-SP": "Talismán escorpión"
    },
    "unlock": 10
  },
  {
    "id": "102",
    "name": {
      "fr-FR": "La Carotte d'Igor",
      "en-US": "Igor's Carrot",
      "es-SP": "La Zanahoria de Igor"
    },
    "unlock": 1
  },
  {
    "id": "113",
    "name": {
      "fr-FR": "Cape de Tuberculoz",
      "en-US": "The Robe of Tuber",
      "es-SP": "Capa de Tubérculo"
    },
    "unlock": 10
  },
  {
    "id": "115",
    "name": {
      "fr-FR": "Casque de Volleyfest",
      "en-US": "The Helmet of Volleyfest",
      "es-SP": "Casco de Volleyfest"
    },
    "unlock": 10
  },
  {
    "id": "116",
    "name": {
      "fr-FR": "Joyau d'Ankhel",
      "en-US": "The Ankhel Jewel",
      "es-SP": "Joya de Ankhel"
    },
    "unlock": 10
  },
  {
    "id": "117",
    "name": {
      "fr-FR": "Clé de Gordon",
      "en-US": "Gordon's Key",
      "es-SP": "Llave de Gordon"
    },
    "unlock": 10
  },
  {
    "id": "1",
    "name": {
      "fr-FR": "Bouclidur en or",
      "en-US": "Golden Shield",
      "es-SP": "Escudo de madera y oro"
    },
    "unlock": 10
  },
  {
    "id": "5",
    "name": {
      "fr-FR": "Lampe Léveussonfé",
      "en-US": "Black Magic Lam",
      "es-SP": "Lámpara Negra"
    },
    "unlock": 10
  },
  {
    "id": "8",
    "name": {
      "fr-FR": "Etoile des neiges",
      "en-US": "Gold Star",
      "es-SP": "Estrella de las nieves"
    },
    "unlock": 10
  },
  {
    "id": "11",
    "name": {
      "fr-FR": "Parapluie rouge",
      "en-US": "Red Umbrella",
      "es-SP": "Paraguas rojo"
    },
    "unlock": 10
  },
  {
    "id": "18",
    "name": {
      "fr-FR": "Pissenlit tropical",
      "en-US": "Tropical Dandelion",
      "es-SP": "Flor tropical"
    },
    "unlock": 10
  },
  {
    "id": "22",
    "name": {
      "fr-FR": "Vieille chaussure trouée",
      "en-US": "Heavy Weight Boots",
      "es-SP": "Zapato viejo con agujeros"
    },
    "unlock": 10
  },
  {
    "id": "23",
    "name": {
      "fr-FR": "Boule cristalline",
      "en-US": "Orb of Ice",
      "es-SP": "Bola cristalina"
    },
    "unlock": 10
  },
  {
    "id": "25",
    "name": {
      "fr-FR": "Flamme froide",
      "en-US": "Snow Flame",
      "es-SP": "Llama fría"
    },
    "unlock": 10
  },
  {
    "id": "27",
    "name": {
      "fr-FR": "Porte-grenouilles",
      "en-US": "Special Lilypad",
      "es-SP": "Posa-ranas"
    },
    "unlock": 10
  },
  {
    "id": "28",
    "name": {
      "fr-FR": "Bibelot en argent",
      "en-US": "Silver Trophy",
      "es-SP": "Baratija de plata"
    },
    "unlock": 10
  },
  {
    "id": "38",
    "name": {
      "fr-FR": "Totem des dinoz",
      "en-US": "Dino Totem Pole",
      "es-SP": "Tótem de los Dinos"
    },
    "unlock": 10
  },
  {
    "id": "39",
    "name": {
      "fr-FR": "Tête de granit lestée de plomb",
      "en-US": "Granite Easter Island",
      "es-SP": "Cabeza de granito"
    },
    "unlock": 10
  },
  {
    "id": "65",
    "name": {
      "fr-FR": "Bouée canard",
      "en-US": "Rubber Ducky",
      "es-SP": "Flotador de pato"
    },
    "unlock": 10
  },
  {
    "id": "66",
    "name": {
      "fr-FR": "Branche de Kipik",
      "en-US": "Magical Cactus",
      "es-SP": "Rama de Kipik"
    },
    "unlock": 10
  },
  {
    "id": "69",
    "name": {
      "fr-FR": "Koulraoule des îles",
      "en-US": "Turtle Islands",
      "es-SP": "Tortuga de las islas"
    },
    "unlock": 10
  },
  {
    "id": "71",
    "name": {
      "fr-FR": "Chaud devant !",
      "en-US": "Dragon's Breath",
      "es-SP": "¡Emana calor!"
    },
    "unlock": 10
  },
  {
    "id": "82",
    "name": {
      "fr-FR": "Jugement avant-dernier",
      "en-US": "Eye of Providence",
      "es-SP": "Juicio penúltimo"
    },
    "unlock": 10
  },
  {
    "id": "106",
    "name": {
      "fr-FR": "Livre des champignons",
      "en-US": "Book of Mushrooms",
      "es-SP": "Libro de los hongos"
    },
    "unlock": 10
  },
  {
    "id": "107",
    "name": {
      "fr-FR": "Livre des étoiles",
      "en-US": "Book of Stars",
      "es-SP": "Libro de las estrellas"
    },
    "unlock": 10
  },
  {
    "id": "2",
    "name": {
      "fr-FR": "Bouclidur argenté",
      "en-US": "Silver Shield",
      "es-SP": "Escudo plateado"
    },
    "unlock": 10
  },
  {
    "id": "6",
    "name": {
      "fr-FR": "Paix intérieure",
      "en-US": "Inner Peace",
      "es-SP": "Paz interior"
    },
    "unlock": 10
  },
  {
    "id": "9",
    "name": {
      "fr-FR": "Mauvais-oeil",
      "en-US": "Bad eye",
      "es-SP": "Ojo malo"
    },
    "unlock": 10
  },
  {
    "id": "12",
    "name": {
      "fr-FR": "Parapluie bleu",
      "en-US": "Blue Umbrella",
      "es-SP": "Paraguas azul"
    },
    "unlock": 10
  },
  {
    "id": "19",
    "name": {
      "fr-FR": "Tournelune",
      "en-US": "Explosive Sunflower",
      "es-SP": "Giraluna"
    },
    "unlock": 10
  },
  {
    "id": "30",
    "name": {
      "fr-FR": "Lunettes tournantes bleues",
      "en-US": "Blue Shades",
      "es-SP": "Gafas volteadoras azules"
    },
    "unlock": 10
  },
  {
    "id": "31",
    "name": {
      "fr-FR": "Lunettes renversantes rouges",
      "en-US": "Red Shades",
      "es-SP": "Gafas revolteadoras rojas"
    },
    "unlock": 10
  },
  {
    "id": "36",
    "name": {
      "fr-FR": "Ig'or",
      "en-US": "Golden Igor",
      "es-SP": "Igoro"
    },
    "unlock": 10
  },
  {
    "id": "70",
    "name": {
      "fr-FR": "Trêfle commun",
      "en-US": "Four-Leaf Clover",
      "es-SP": "Trébol común"
    },
    "unlock": 10
  },
  {
    "id": "72",
    "name": {
      "fr-FR": "Chapeau de Mage-Gris",
      "en-US": "Magician's Hat",
      "es-SP": "Sombrero de Mago Gris"
    },
    "unlock": 10
  },
  {
    "id": "73",
    "name": {
      "fr-FR": "Feuille de sinopée",
      "en-US": "Leaf",
      "es-SP": "Hoja de árbol"
    },
    "unlock": 10
  },
  {
    "id": "75",
    "name": {
      "fr-FR": "Esprit de la pluie",
      "en-US": "Spirit of Rain",
      "es-SP": "Espíritu de la lluvia"
    },
    "unlock": 10
  },
  {
    "id": "76",
    "name": {
      "fr-FR": "Esprit des arbres",
      "en-US": "Spirit of Trees",
      "es-SP": "Espíritu de los árboles"
    },
    "unlock": 10
  },
  {
    "id": "80",
    "name": {
      "fr-FR": "Escargot Poussépa",
      "en-US": "Snail Speed",
      "es-SP": "Caracol Noempujes"
    },
    "unlock": 10
  },
  {
    "id": "81",
    "name": {
      "fr-FR": "Perle nacrée des murlocs",
      "en-US": "Murloc's Pearl",
      "es-SP": "Perla de nácar de los murlocs"
    },
    "unlock": 10
  },
  {
    "id": "86",
    "name": {
      "fr-FR": "Surprise de paille",
      "en-US": "Ghost Candy",
      "es-SP": "Sorpresa fantasmagórica"
    },
    "unlock": 10
  },
  {
    "id": "87",
    "name": {
      "fr-FR": "Larve d'oenopterius",
      "en-US": "King Larvae",
      "es-SP": "Larva de oenopterius"
    },
    "unlock": 10
  },
  {
    "id": "88",
    "name": {
      "fr-FR": "Pokuté",
      "en-US": "Hare Nochi Guuuuu!",
      "es-SP": "Pokuté"
    },
    "unlock": 10
  },
  {
    "id": "89",
    "name": {
      "fr-FR": "Oeuf de Tzongre",
      "en-US": "Dragon Egg",
      "es-SP": "Ojo de Tzongre"
    },
    "unlock": 10
  },
  {
    "id": "90",
    "name": {
      "fr-FR": "Fulguro pieds-en-mousse",
      "en-US": "Slippery Bananas",
      "es-SP": "Fulguro pies-pesados"
    },
    "unlock": 10
  },
  {
    "id": "93",
    "name": {
      "fr-FR": "Boit'a'messages",
      "en-US": "Mail Box",
      "es-SP": "Buzón zon"
    },
    "unlock": 10
  },
  {
    "id": "95",
    "name": {
      "fr-FR": "Cagnotte de Tuberculoz",
      "en-US": "Tuber's Bag",
      "es-SP": "Saco de Tubérculo"
    },
    "unlock": 10
  },
  {
    "id": "96",
    "name": {
      "fr-FR": "Perle flamboyante",
      "en-US": "Flameberg Pearl",
      "es-SP": "Perla flameante"
    },
    "unlock": 10
  },
  {
    "id": "99",
    "name": {
      "fr-FR": "Poils de Chourou",
      "en-US": "Chourou's Hair",
      "es-SP": "Pelos de Chourou"
    },
    "unlock": 10
  },
  {
    "id": "101",
    "name": {
      "fr-FR": "Colis surprise",
      "en-US": "Surprise Package",
      "es-SP": "Paquete sorpresa"
    },
    "unlock": 10
  },
  {
    "id": "112",
    "name": {
      "fr-FR": "Pioupiou carnivore",
      "en-US": "Carnivorous PiouPiou",
      "es-SP": "Píopío carnívoro"
    },
    "unlock": 1
  },
  {
    "id": "14",
    "name": {
      "fr-FR": "Délice hallucinogène bleu",
      "en-US": "Blue Hallucinogenic Mushroom",
      "es-SP": "Delicia alucinógena azul"
    },
    "unlock": 10
  },
  {
    "id": "15",
    "name": {
      "fr-FR": "Champignon rigolo rouge",
      "en-US": "Red Delight Mushroom",
      "es-SP": "Seta chuli y roja"
    },
    "unlock": 10
  },
  {
    "id": "16",
    "name": {
      "fr-FR": "Figonassée grimpante",
      "en-US": "Green Beer Mushroom",
      "es-SP": "Pequeña Walalu de los bosques"
    },
    "unlock": 10
  },
  {
    "id": "17",
    "name": {
      "fr-FR": "Petit Waoulalu des bois",
      "en-US": "Golded Mushroom",
      "es-SP": "Seta dorada trepadora"
    },
    "unlock": 10
  },
  {
    "id": "32",
    "name": {
      "fr-FR": "As de pique",
      "en-US": "Ace of Spades",
      "es-SP": "As de picas"
    },
    "unlock": 10
  },
  {
    "id": "33",
    "name": {
      "fr-FR": "As de trêfle",
      "en-US": "Ace of Clubs",
      "es-SP": "As de tréboles"
    },
    "unlock": 10
  },
  {
    "id": "34",
    "name": {
      "fr-FR": "As de carreau",
      "en-US": "Ace of Diamonds",
      "es-SP": "As de diamantes"
    },
    "unlock": 10
  },
  {
    "id": "35",
    "name": {
      "fr-FR": "As de coeur",
      "en-US": "Ace of Hearts",
      "es-SP": "As de corazones"
    },
    "unlock": 10
  },
  {
    "id": "40",
    "name": {
      "fr-FR": "Sagittaire",
      "en-US": "Sagittarius",
      "es-SP": "Sagitario"
    },
    "unlock": 1
  },
  {
    "id": "41",
    "name": {
      "fr-FR": "Capricorne",
      "en-US": "Capricorn",
      "es-SP": "Capricornio"
    },
    "unlock": 1
  },
  {
    "id": "42",
    "name": {
      "fr-FR": "Lion",
      "en-US": "Leo",
      "es-SP": "Leo"
    },
    "unlock": 1
  },
  {
    "id": "43",
    "name": {
      "fr-FR": "Taureau",
      "en-US": "Taurus",
      "es-SP": "Tauro"
    },
    "unlock": 1
  },
  {
    "id": "44",
    "name": {
      "fr-FR": "Balance",
      "en-US": "Libra",
      "es-SP": "Libra"
    },
    "unlock": 1
  },
  {
    "id": "45",
    "name": {
      "fr-FR": "Bélier",
      "en-US": "Aries",
      "es-SP": "Aries"
    },
    "unlock": 1
  },
  {
    "id": "46",
    "name": {
      "fr-FR": "Scorpion",
      "en-US": "Scorpio",
      "es-SP": "Escorpión"
    },
    "unlock": 1
  },
  {
    "id": "47",
    "name": {
      "fr-FR": "Cancer",
      "en-US": "Cancer",
      "es-SP": "Cáncer"
    },
    "unlock": 1
  },
  {
    "id": "48",
    "name": {
      "fr-FR": "Verseau",
      "en-US": "Aquarius",
      "es-SP": "Acuario"
    },
    "unlock": 1
  },
  {
    "id": "49",
    "name": {
      "fr-FR": "Gémeaux",
      "en-US": "Gemini",
      "es-SP": "Géminis"
    },
    "unlock": 1
  },
  {
    "id": "50",
    "name": {
      "fr-FR": "Poisson",
      "en-US": "Pisces",
      "es-SP": "Piscis"
    },
    "unlock": 1
  },
  {
    "id": "51",
    "name": {
      "fr-FR": "Vierge",
      "en-US": "Virgo",
      "es-SP": "Virgo"
    },
    "unlock": 1
  },
  {
    "id": "52",
    "name": {
      "fr-FR": "Elixir du Sagittaire",
      "en-US": "Potion of Sagitarius",
      "es-SP": "Elixir de Sagitario"
    },
    "unlock": 1
  },
  {
    "id": "53",
    "name": {
      "fr-FR": "Elixir du Capricorne",
      "en-US": "Potion of Capricorn",
      "es-SP": "Elixir de Capricornio"
    },
    "unlock": 1
  },
  {
    "id": "54",
    "name": {
      "fr-FR": "Elixir du Lion",
      "en-US": "Potion of Leo",
      "es-SP": "Elixir de Leo"
    },
    "unlock": 1
  },
  {
    "id": "55",
    "name": {
      "fr-FR": "Elixir du Taureau",
      "en-US": "Potion of Taurus",
      "es-SP": "Elixir de Tauro"
    },
    "unlock": 1
  },
  {
    "id": "56",
    "name": {
      "fr-FR": "Elixir de la Balance",
      "en-US": "Potion of Libra",
      "es-SP": "Elixir de Libra"
    },
    "unlock": 1
  },
  {
    "id": "57",
    "name": {
      "fr-FR": "Elixir du Bélier",
      "en-US": "Potion of Aries",
      "es-SP": "Elixir de Aries"
    },
    "unlock": 1
  },
  {
    "id": "58",
    "name": {
      "fr-FR": "Elixir du Scorpion",
      "en-US": "Potion of Scorpio",
      "es-SP": "Elixir de Escorpión"
    },
    "unlock": 1
  },
  {
    "id": "59",
    "name": {
      "fr-FR": "Elixir du Cancer",
      "en-US": "Potion of Cancer",
      "es-SP": "Elixir de Cáncer"
    },
    "unlock": 1
  },
  {
    "id": "60",
    "name": {
      "fr-FR": "Elixir du Verseau",
      "en-US": "Potion of Aquarius",
      "es-SP": "Elixir de Acuario"
    },
    "unlock": 1
  },
  {
    "id": "61",
    "name": {
      "fr-FR": "Elixir des Gémeaux",
      "en-US": "Potion of Gemini",
      "es-SP": "Elixir de Géminis"
    },
    "unlock": 1
  },
  {
    "id": "62",
    "name": {
      "fr-FR": "Elixir du Poisson",
      "en-US": "Potion of Pisces",
      "es-SP": "Elixir de Acuario"
    },
    "unlock": 1
  },
  {
    "id": "63",
    "name": {
      "fr-FR": "Elixir de la Vierge",
      "en-US": "Potion of Virgo",
      "es-SP": "Elixir de Virgo"
    },
    "unlock": 1
  },
  {
    "id": "103",
    "name": {
      "fr-FR": "Vie palpitante",
      "en-US": "Wonderful Life!",
      "es-SP": "Vida palpitante"
    },
    "unlock": 1
  },
  {
    "id": "104",
    "name": {
      "fr-FR": "Vie aventureuse",
      "en-US": "Heart of an Adventurer",
      "es-SP": "Vida aventurosa"
    },
    "unlock": 1
  },
  {
    "id": "105",
    "name": {
      "fr-FR": "Vie épique",
      "en-US": "Final Destiny",
      "es-SP": "Vida épica"
    },
    "unlock": 1
  },
  {
    "id": "20",
    "name": {
      "fr-FR": "Coffre d'Anarchipel",
      "en-US": "Treasure Chest",
      "es-SP": "Cofre de Anarchipiélago"
    },
    "unlock": 10
  },
  {
    "id": "78",
    "name": {
      "fr-FR": "Filandreux rougeoyant",
      "en-US": "Flame Fish",
      "es-SP": "Fibroso rojito"
    },
    "unlock": 10
  },
  {
    "id": "79",
    "name": {
      "fr-FR": "Poisson empereur",
      "en-US": "Fish Emperor",
      "es-SP": "Pez emperador"
    },
    "unlock": 10
  },
  {
    "id": "91",
    "name": {
      "fr-FR": "Couvre-chef de Luffy",
      "en-US": "Luffy's Hat",
      "es-SP": "Sombrero de paja de Luffy"
    },
    "unlock": 10
  },
  {
    "id": "94",
    "name": {
      "fr-FR": "Anneau Antok",
      "en-US": "Antok Ring",
      "es-SP": "Anillo Antok"
    },
    "unlock": 10
  },
  {
    "id": "83",
    "name": {
      "fr-FR": "Jugement dernier",
      "en-US": "Final Judgment",
      "es-SP": "Juicio final"
    },
    "unlock": 10
  },
  {
    "id": "97",
    "name": {
      "fr-FR": "Perle vertillante",
      "en-US": "Green-Moss Pearl ",
      "es-SP": "Perla verdeante"
    },
    "unlock": 10
  },
  {
    "id": "98",
    "name": {
      "fr-FR": "Grosse pé-perle ",
      "en-US": "Super-Purple Pearl ",
      "es-SP": "Gran pe-perla"
    },
    "unlock": 10
  },
  {
    "id": "100",
    "name": {
      "fr-FR": "Pocket-Guu",
      "en-US": "Pocket-Guu",
      "es-SP": "Guu de bolsillo"
    },
    "unlock": 10
  },
  {
    "id": "108",
    "name": {
      "fr-FR": "Parapluie Frutiparc",
      "en-US": "Green Umbrella",
      "es-SP": "Paraguas Frutiparc"
    },
    "unlock": 10
  },
  {
    "id": "29",
    "name": {
      "fr-FR": "Bague 'Thermostat 8'",
      "en-US": "Ring of Fire",
      "es-SP": "Anillo 'Termostato 8'"
    },
    "unlock": 10
  },
  {
    "id": "37",
    "name": {
      "fr-FR": "Collier rafraîchissant",
      "en-US": "Necklace of Frost",
      "es-SP": "Collar refrescante"
    },
    "unlock": 10
  },
  {
    "id": "67",
    "name": {
      "fr-FR": "Anneau de Guillaume Tell",
      "en-US": "Ring of William Tell",
      "es-SP": "Anillo de Omaíta"
    },
    "unlock": 10
  },
  {
    "id": "85",
    "name": {
      "fr-FR": "Baton tonnerre",
      "en-US": "Hammer Time 3000",
      "es-SP": "Martillo"
    },
    "unlock": 10
  },
  {
    "id": "92",
    "name": {
      "fr-FR": "Chapeau violin",
      "en-US": "Chopper's Hat",
      "es-SP": "Sombrero de Tony Tony Chopper"
    },
    "unlock": 10
  },
  {
    "id": "68",
    "name": {
      "fr-FR": "Bougie",
      "en-US": "Candle of Light",
      "es-SP": "Vela"
    },
    "unlock": 10
  },
  {
    "id": "26",
    "name": {
      "fr-FR": "Ampoule 30 watts",
      "en-US": "Light Bulb",
      "es-SP": "Bombilla 30 watios"
    },
    "unlock": 10
  },
  {
    "id": "109",
    "name": {
      "fr-FR": "Flocon simple",
      "en-US": "Snowflake",
      "es-SP": "Copo de nieve simple"
    },
    "unlock": 1
  },
  {
    "id": "110",
    "name": {
      "fr-FR": "Flocon bizarre",
      "en-US": "Glacier",
      "es-SP": "Copo de nieve raro"
    },
    "unlock": 1
  },
  {
    "id": "111",
    "name": {
      "fr-FR": "Flocon ENORME !",
      "en-US": "Gon Ga Jora",
      "es-SP": "Copo de nieve gigantesco"
    },
    "unlock": 1
  },
  {
    "id": "10",
    "name": {
      "fr-FR": "Téléphone-phone-phone",
      "en-US": "Telephone",
      "es-SP": "Teléfono-no-no"
    },
    "unlock": 5
  },
  {
    "id": "114",
    "name": {
      "fr-FR": "Mode Mario",
      "en-US": "Mario Mode!",
      "es-SP": "Modo Mario"
    },
    "unlock": 10
  },
  {
    "id": "1000",
    "name": {
      "fr-FR": "Cristaux d'Hammerfest",
      "en-US": "Hammerfest Crystal",
      "es-SP": "Cristales de Hammerfest"
    },
    "unlock": 10
  },
  {
    "id": "1003",
    "name": {
      "fr-FR": "Bonbon Berlinmauve",
      "en-US": "Seed Candy",
      "es-SP": "Caramelo Berlinmalva"
    },
    "unlock": 10
  },
  {
    "id": "1008",
    "name": {
      "fr-FR": "Diamant Oune-difaïned",
      "en-US": "Difaïned Diamond",
      "es-SP": "Diamante difaïned"
    },
    "unlock": 10
  },
  {
    "id": "1013",
    "name": {
      "fr-FR": "Muffin aux cailloux",
      "en-US": "Muffin",
      "es-SP": "Magdalena de piedras"
    },
    "unlock": 10
  },
  {
    "id": "1017",
    "name": {
      "fr-FR": "Pierres du Changement",
      "en-US": "Magical Stones",
      "es-SP": "Piedras del Cambio"
    },
    "unlock": 10
  },
  {
    "id": "1027",
    "name": {
      "fr-FR": "Réglisse rouge",
      "en-US": "Red Licorice",
      "es-SP": "Chuchería"
    },
    "unlock": 10
  },
  {
    "id": "1041",
    "name": {
      "fr-FR": "Sucette acidulée",
      "en-US": "Lollipop",
      "es-SP": "Piruleta"
    },
    "unlock": 10
  },
  {
    "id": "1043",
    "name": {
      "fr-FR": "Sorbet au plastique",
      "en-US": "Plastic Sorbet",
      "es-SP": "Polo de plástico"
    },
    "unlock": 10
  },
  {
    "id": "1047",
    "name": {
      "fr-FR": "Bleuet",
      "en-US": "Sapphire Mushroom",
      "es-SP": "Seta azulada"
    },
    "unlock": 10
  },
  {
    "id": "1048",
    "name": {
      "fr-FR": "Rougeoyant",
      "en-US": "Ruby Mushroom",
      "es-SP": "Rojeante"
    },
    "unlock": 10
  },
  {
    "id": "1049",
    "name": {
      "fr-FR": "Verdifiant",
      "en-US": "Emerald Mushroom",
      "es-SP": "Verdifiante"
    },
    "unlock": 10
  },
  {
    "id": "1050",
    "name": {
      "fr-FR": "KassDent",
      "en-US": "Sliver Mushroom",
      "es-SP": "Rompedientes"
    },
    "unlock": 10
  },
  {
    "id": "1051",
    "name": {
      "fr-FR": "Pièce d'or secrète",
      "en-US": "Secret Coin",
      "es-SP": "Moneda de oro secreta"
    },
    "unlock": 10
  },
  {
    "id": "1169",
    "name": {
      "fr-FR": "Etoile Toulaho",
      "en-US": "Starfish",
      "es-SP": "Estrella Toulaho"
    },
    "unlock": 10
  },
  {
    "id": "1185",
    "name": {
      "fr-FR": "Anneau hérissé",
      "en-US": "Bristling Ring",
      "es-SP": "Anillo de erizo azul"
    },
    "unlock": 10
  },
  {
    "id": "1022",
    "name": {
      "fr-FR": "Liquide bizarre",
      "en-US": "Bizarre Liquid",
      "es-SP": "Líquido raro"
    },
    "unlock": 10
  },
  {
    "id": "1024",
    "name": {
      "fr-FR": "Liquide étrange",
      "en-US": "Strange Liquid",
      "es-SP": "Líquido extraño"
    },
    "unlock": 10
  },
  {
    "id": "1025",
    "name": {
      "fr-FR": "Oeuf cru",
      "en-US": "Raw Egg",
      "es-SP": "Huevo crudo"
    },
    "unlock": 10
  },
  {
    "id": "1026",
    "name": {
      "fr-FR": "Gland gnan-gnan",
      "en-US": "Potato",
      "es-SP": "Gran ñan-ñan"
    },
    "unlock": 10
  },
  {
    "id": "1028",
    "name": {
      "fr-FR": "Oeuf Au Plat",
      "en-US": "Plain Egg",
      "es-SP": "Huevo frito"
    },
    "unlock": 10
  },
  {
    "id": "1040",
    "name": {
      "fr-FR": "Sushi thon",
      "en-US": "Fish Sushi",
      "es-SP": "Sushi de atún"
    },
    "unlock": 10
  },
  {
    "id": "1069",
    "name": {
      "fr-FR": "Cacahuete secrète",
      "en-US": "Peanut",
      "es-SP": "Cacahuete secreto"
    },
    "unlock": 10
  },
  {
    "id": "1070",
    "name": {
      "fr-FR": "P'tit fantome",
      "en-US": "Small Ghost",
      "es-SP": "Fantasmita"
    },
    "unlock": 10
  },
  {
    "id": "1071",
    "name": {
      "fr-FR": "Cookie deshydraté",
      "en-US": "Hard Cookie",
      "es-SP": "Cookie deshidratada"
    },
    "unlock": 10
  },
  {
    "id": "1073",
    "name": {
      "fr-FR": "Piment farceur",
      "en-US": "Hot Pepper",
      "es-SP": "Pimiento del piquillo"
    },
    "unlock": 10
  },
  {
    "id": "1074",
    "name": {
      "fr-FR": "Soja Max IceCream",
      "en-US": "Bread",
      "es-SP": "Helado de Soja Max"
    },
    "unlock": 10
  },
  {
    "id": "1075",
    "name": {
      "fr-FR": "Bouquet de steack",
      "en-US": "Bouquet",
      "es-SP": "Ramo de carne"
    },
    "unlock": 10
  },
  {
    "id": "1077",
    "name": {
      "fr-FR": "Graine de tournesol",
      "en-US": "Grain",
      "es-SP": "Semilla de girasol"
    },
    "unlock": 10
  },
  {
    "id": "1078",
    "name": {
      "fr-FR": "Croissant confit",
      "en-US": "Croissant",
      "es-SP": "Croissant confitado"
    },
    "unlock": 10
  },
  {
    "id": "1079",
    "name": {
      "fr-FR": "Haricot paresseux",
      "en-US": "Beans",
      "es-SP": "Judía perezosa"
    },
    "unlock": 10
  },
  {
    "id": "1080",
    "name": {
      "fr-FR": "Lapin-choco",
      "en-US": "Chocolate Bunny",
      "es-SP": "Conejo de chocolate"
    },
    "unlock": 10
  },
  {
    "id": "1081",
    "name": {
      "fr-FR": "Biloo",
      "en-US": "Beer",
      "es-SP": "Hidromiel de los Bosques"
    },
    "unlock": 10
  },
  {
    "id": "1082",
    "name": {
      "fr-FR": "Graine de pechume en gelée",
      "en-US": "Jelly with Corn",
      "es-SP": "Gota viscosa"
    },
    "unlock": 10
  },
  {
    "id": "1002",
    "name": {
      "fr-FR": "Canne de Bobble",
      "en-US": "Candy Cane",
      "es-SP": "Bastón de Bobble"
    },
    "unlock": 10
  },
  {
    "id": "1004",
    "name": {
      "fr-FR": "Bonbon Chamagros",
      "en-US": "Raindrop Candy",
      "es-SP": "Caramelo Chamagros"
    },
    "unlock": 10
  },
  {
    "id": "1005",
    "name": {
      "fr-FR": "Bonbon rosamelle-praline",
      "en-US": "Cherry Candy",
      "es-SP": "Caramelo rosa-praliné"
    },
    "unlock": 10
  },
  {
    "id": "1006",
    "name": {
      "fr-FR": "Sucette aux fruits bleus",
      "en-US": "Blue Raspberry Lollipop",
      "es-SP": "Chupakups de frutas azules"
    },
    "unlock": 10
  },
  {
    "id": "1007",
    "name": {
      "fr-FR": "Sucette chlorophylle",
      "en-US": "Green Apple Lollipop",
      "es-SP": "Chupakups de clorofila"
    },
    "unlock": 10
  },
  {
    "id": "1012",
    "name": {
      "fr-FR": "Surprise de Cerises",
      "en-US": "Surprise Cake",
      "es-SP": "Sorpresa de Cerezas"
    },
    "unlock": 10
  },
  {
    "id": "1014",
    "name": {
      "fr-FR": "Gelée des bois",
      "en-US": "Cake",
      "es-SP": "Pastel de madera"
    },
    "unlock": 10
  },
  {
    "id": "1015",
    "name": {
      "fr-FR": "Suprême aux framboises",
      "en-US": "Wedding Cake",
      "es-SP": "Tarta suprema de frambuesas"
    },
    "unlock": 10
  },
  {
    "id": "1016",
    "name": {
      "fr-FR": "Petit pétillant",
      "en-US": "Piece of Cake",
      "es-SP": "Tarta muy rica"
    },
    "unlock": 10
  },
  {
    "id": "1018",
    "name": {
      "fr-FR": "Glace Fraise",
      "en-US": "Strawberry Ice Cream",
      "es-SP": "Helado de fresa"
    },
    "unlock": 10
  },
  {
    "id": "1019",
    "name": {
      "fr-FR": "Coupe Glacée",
      "en-US": "Ice Cream Cup",
      "es-SP": "Copa helada"
    },
    "unlock": 10
  },
  {
    "id": "1023",
    "name": {
      "fr-FR": "Gros acidulé",
      "en-US": "Taffy",
      "es-SP": "Gran azucarado"
    },
    "unlock": 10
  },
  {
    "id": "1045",
    "name": {
      "fr-FR": "Pétale mystérieuse",
      "en-US": "Cashew",
      "es-SP": "Pétalo misterioso"
    },
    "unlock": 10
  },
  {
    "id": "1046",
    "name": {
      "fr-FR": "Gump",
      "en-US": "Gump",
      "es-SP": "Gump"
    },
    "unlock": 10
  },
  {
    "id": "1009",
    "name": {
      "fr-FR": "Oeil de tigre",
      "en-US": "Eye of the Tiger",
      "es-SP": "Ojo del Tigre"
    },
    "unlock": 10
  },
  {
    "id": "1010",
    "name": {
      "fr-FR": "Jade de 12kg",
      "en-US": "12kg Jade",
      "es-SP": "Jade de 12kg"
    },
    "unlock": 10
  },
  {
    "id": "1011",
    "name": {
      "fr-FR": "Reflet-de-lune",
      "en-US": "Moon Glade",
      "es-SP": "Reflejo-de-luna"
    },
    "unlock": 10
  },
  {
    "id": "1001",
    "name": {
      "fr-FR": "Pain à la viande",
      "en-US": "Hamburger",
      "es-SP": "Pan de carne"
    },
    "unlock": 10
  },
  {
    "id": "1021",
    "name": {
      "fr-FR": "Poulet Surgelé",
      "en-US": "Frozen Chicken",
      "es-SP": "Pollo congelado"
    },
    "unlock": 10
  },
  {
    "id": "1042",
    "name": {
      "fr-FR": "Délice d'Aralé",
      "en-US": "Chocolate Mousse",
      "es-SP": "Delicia de Arale"
    },
    "unlock": 10
  },
  {
    "id": "1055",
    "name": {
      "fr-FR": "Boisson kipik",
      "en-US": "Soda",
      "es-SP": "Bebida kipik"
    },
    "unlock": 10
  },
  {
    "id": "1056",
    "name": {
      "fr-FR": "Doigts-de-tuberculoz",
      "en-US": "French Fries",
      "es-SP": "Dedos-de-Tubérculo"
    },
    "unlock": 10
  },
  {
    "id": "1057",
    "name": {
      "fr-FR": "Pizza de Donatello",
      "en-US": "Slice of Pizza",
      "es-SP": "Pizza de Donatello"
    },
    "unlock": 10
  },
  {
    "id": "1142",
    "name": {
      "fr-FR": "Café de fin de projet",
      "en-US": "Project Finisher",
      "es-SP": "Café de fin de proyecto"
    },
    "unlock": 10
  },
  {
    "id": "1149",
    "name": {
      "fr-FR": "Noodles crus",
      "en-US": "Ramen Noodles",
      "es-SP": "Fideos chinos deshidratados"
    },
    "unlock": 10
  },
  {
    "id": "1166",
    "name": {
      "fr-FR": "Oeuf de Poire",
      "en-US": "Pear Egg",
      "es-SP": "Huevo de pera"
    },
    "unlock": 10
  },
  {
    "id": "1020",
    "name": {
      "fr-FR": "Noodles",
      "en-US": "Noodles",
      "es-SP": "Fideos chinos"
    },
    "unlock": 10
  },
  {
    "id": "1039",
    "name": {
      "fr-FR": "Monsieur radis",
      "en-US": "Mr. Radish",
      "es-SP": "Señor rábano"
    },
    "unlock": 10
  },
  {
    "id": "1044",
    "name": {
      "fr-FR": "Manda",
      "en-US": "Manda",
      "es-SP": "Manda"
    },
    "unlock": 10
  },
  {
    "id": "1060",
    "name": {
      "fr-FR": "Bleuette rouge",
      "en-US": "Red Plant",
      "es-SP": "Nadie sabe lo que es"
    },
    "unlock": 10
  },
  {
    "id": "1061",
    "name": {
      "fr-FR": "Perroquet décapité en sauce",
      "en-US": "Parrot Head",
      "es-SP": "Loro decapitado en salsa"
    },
    "unlock": 10
  },
  {
    "id": "1062",
    "name": {
      "fr-FR": "Morvo-morphe",
      "en-US": "Alien Blob",
      "es-SP": "Morvo-morfo"
    },
    "unlock": 10
  },
  {
    "id": "1076",
    "name": {
      "fr-FR": "Daruma-Pastèque",
      "en-US": "Daruma Watermelon",
      "es-SP": "Daruma-Sandía"
    },
    "unlock": 10
  },
  {
    "id": "1161",
    "name": {
      "fr-FR": "Nem aux anchois",
      "en-US": "Breaded Anchovies",
      "es-SP": "Nem de anchoas"
    },
    "unlock": 10
  },
  {
    "id": "1162",
    "name": {
      "fr-FR": "Surimi pamplemousse",
      "en-US": "Grapefruit Sushi",
      "es-SP": "Surimi de pomelo"
    },
    "unlock": 10
  },
  {
    "id": "1163",
    "name": {
      "fr-FR": "Poulpi à l'encre",
      "en-US": "Octopus in Ink",
      "es-SP": "Calamar en su tinta"
    },
    "unlock": 10
  },
  {
    "id": "1167",
    "name": {
      "fr-FR": "Truffe collante sans goût",
      "en-US": "Tasteless Sticky Truffle",
      "es-SP": "Trufa pegajosa sin sabor"
    },
    "unlock": 10
  },
  {
    "id": "1029",
    "name": {
      "fr-FR": "Saucisse piquée",
      "en-US": "Sausage on a Stick",
      "es-SP": "Salchica con palillo de dientes"
    },
    "unlock": 10
  },
  {
    "id": "1030",
    "name": {
      "fr-FR": "Cerise-apéro confite",
      "en-US": "Cherry on a Stick",
      "es-SP": "Cereza con palillo de dientes"
    },
    "unlock": 10
  },
  {
    "id": "1031",
    "name": {
      "fr-FR": "Fromage piqué",
      "en-US": "Cheese on a Stick",
      "es-SP": "Queso con palillo de dientes"
    },
    "unlock": 10
  },
  {
    "id": "1032",
    "name": {
      "fr-FR": "Olive pas mûre",
      "en-US": "Green Olive on a Stick",
      "es-SP": "Aceituna de Estepa"
    },
    "unlock": 10
  },
  {
    "id": "1033",
    "name": {
      "fr-FR": "Olive noire",
      "en-US": "Black Olive on a Stick",
      "es-SP": "Aceituna negra de Estepa"
    },
    "unlock": 10
  },
  {
    "id": "1034",
    "name": {
      "fr-FR": "Oeil de pomme",
      "en-US": "Eyeball on a Stick",
      "es-SP": "Ojo del cochino"
    },
    "unlock": 10
  },
  {
    "id": "1035",
    "name": {
      "fr-FR": "Blob intrusif",
      "en-US": "Jello on a Stick",
      "es-SP": "Aperitivo de Blandiblú"
    },
    "unlock": 10
  },
  {
    "id": "1036",
    "name": {
      "fr-FR": "Gouda mou",
      "en-US": "Gouda on a Stick",
      "es-SP": "Queso holandés blando"
    },
    "unlock": 10
  },
  {
    "id": "1037",
    "name": {
      "fr-FR": "Poulpi empalé",
      "en-US": "Fish on a Stick",
      "es-SP": "Tapa de calamar"
    },
    "unlock": 10
  },
  {
    "id": "1038",
    "name": {
      "fr-FR": "Olive oubliée",
      "en-US": "Stuffed Olive on a Stick",
      "es-SP": "Aceituna olvidada"
    },
    "unlock": 10
  },
  {
    "id": "1164",
    "name": {
      "fr-FR": "Curly",
      "en-US": "Curly",
      "es-SP": "Curly"
    },
    "unlock": 10
  },
  {
    "id": "1052",
    "name": {
      "fr-FR": "Sou d'argent",
      "en-US": "Silver Coin",
      "es-SP": "Moneda de plata"
    },
    "unlock": 10
  },
  {
    "id": "1053",
    "name": {
      "fr-FR": "Sou d'or",
      "en-US": "Gold Coin",
      "es-SP": "Moneda de oro"
    },
    "unlock": 10
  },
  {
    "id": "1054",
    "name": {
      "fr-FR": "Gros tas de sous",
      "en-US": "Big Unemployment",
      "es-SP": "Un montón de dinero"
    },
    "unlock": 10
  },
  {
    "id": "1091",
    "name": {
      "fr-FR": "Jambon de Bayonne",
      "en-US": "Raw Ham",
      "es-SP": "Jamón"
    },
    "unlock": 10
  },
  {
    "id": "1092",
    "name": {
      "fr-FR": "Saucisson entamé",
      "en-US": "Sliced Sausage",
      "es-SP": "Lomo embuchado de jabugo"
    },
    "unlock": 10
  },
  {
    "id": "1093",
    "name": {
      "fr-FR": "Raide red reste",
      "en-US": "Red Remains",
      "es-SP": "Mortadela torcida"
    },
    "unlock": 10
  },
  {
    "id": "1094",
    "name": {
      "fr-FR": "Torchon madrangeais au sirop d'érable",
      "en-US": "Maple Syrup Roll",
      "es-SP": "Bacon al sirope de fresa"
    },
    "unlock": 10
  },
  {
    "id": "1095",
    "name": {
      "fr-FR": "Saucisson de marcassin sauvage",
      "en-US": "Blood Sausage",
      "es-SP": "Salchichón de jabato salvaje"
    },
    "unlock": 10
  },
  {
    "id": "1096",
    "name": {
      "fr-FR": "Tranches de Jaret de Kangourou",
      "en-US": "Kangaroo Liver",
      "es-SP": "Rebanada de Canguro"
    },
    "unlock": 10
  },
  {
    "id": "1097",
    "name": {
      "fr-FR": "Saucissaille de St-Morgelet",
      "en-US": "Sausage of St.Morgelet",
      "es-SP": "Salchichón de la Sierra de Aracena"
    },
    "unlock": 10
  },
  {
    "id": "1098",
    "name": {
      "fr-FR": "Paté d'ongles au truffes",
      "en-US": "Head Cheese",
      "es-SP": "Pavo trufado sin cabello de ángel"
    },
    "unlock": 10
  },
  {
    "id": "1099",
    "name": {
      "fr-FR": "Saucisson maudit scellé",
      "en-US": "Squished Sausage",
      "es-SP": "Salchichón maldito"
    },
    "unlock": 10
  },
  {
    "id": "1063",
    "name": {
      "fr-FR": "Fraise Tagada",
      "en-US": "Tagada-Strawberry",
      "es-SP": "Chuchería de fresa"
    },
    "unlock": 10
  },
  {
    "id": "1064",
    "name": {
      "fr-FR": "Car-En-Sac",
      "en-US": "Car-En-Sac",
      "es-SP": "Píldoras de mentira"
    },
    "unlock": 10
  },
  {
    "id": "1065",
    "name": {
      "fr-FR": "Dragibus",
      "en-US": "Dragibus",
      "es-SP": "Bolitas de chuches"
    },
    "unlock": 10
  },
  {
    "id": "1066",
    "name": {
      "fr-FR": "Krokodile",
      "en-US": "Crocodile",
      "es-SP": "Chuchería de cocodrilo"
    },
    "unlock": 10
  },
  {
    "id": "1067",
    "name": {
      "fr-FR": "Demi Cocobats",
      "en-US": "Half Cocobats",
      "es-SP": "Chuche de infancia irlandesa"
    },
    "unlock": 10
  },
  {
    "id": "1068",
    "name": {
      "fr-FR": "Happy Cola",
      "en-US": "Happy Cola",
      "es-SP": "Una botellita de cola"
    },
    "unlock": 10
  },
  {
    "id": "1083",
    "name": {
      "fr-FR": "Sombrino aux amandes",
      "en-US": "Chocolate with Almond",
      "es-SP": "Bombón de nueces"
    },
    "unlock": 10
  },
  {
    "id": "1084",
    "name": {
      "fr-FR": "Emi-Praline",
      "en-US": "Dark Chocolate",
      "es-SP": "Emi-Praliné"
    },
    "unlock": 10
  },
  {
    "id": "1085",
    "name": {
      "fr-FR": "Frogmaliet aux pepites de chocolat",
      "en-US": "Belgium Chocolate",
      "es-SP": "Bombón con pepitas de chocolate"
    },
    "unlock": 10
  },
  {
    "id": "1086",
    "name": {
      "fr-FR": "Yumi au café",
      "en-US": "White Chocolate with Coffee Bean",
      "es-SP": "Yumi de café"
    },
    "unlock": 10
  },
  {
    "id": "1087",
    "name": {
      "fr-FR": "Bouchée mielleuse nappée au gel de Vodka",
      "en-US": "Vodka Chocolate",
      "es-SP": "Esponjita de gel de Vodka"
    },
    "unlock": 10
  },
  {
    "id": "1088",
    "name": {
      "fr-FR": "Escargot au chocolat persillé",
      "en-US": "Snail Chocolate",
      "es-SP": "Caracol de chocolate de pasta de queso"
    },
    "unlock": 10
  },
  {
    "id": "1089",
    "name": {
      "fr-FR": "Fossile de cacao marbré au fois gras",
      "en-US": "Marble Chocolate",
      "es-SP": "Fósil de chocolate de márbol de foie gras"
    },
    "unlock": 10
  },
  {
    "id": "1090",
    "name": {
      "fr-FR": "Cerisot mariné a la bière",
      "en-US": "Cerisot Marinated in Beer",
      "es-SP": "Caramelo marinado a la cerveza"
    },
    "unlock": 10
  },
  {
    "id": "1106",
    "name": {
      "fr-FR": "Camembert",
      "en-US": "Mozzarella Cheese",
      "es-SP": "Camembert"
    },
    "unlock": 10
  },
  {
    "id": "1107",
    "name": {
      "fr-FR": "Emmental",
      "en-US": "Emmental Cheese",
      "es-SP": "Emmental"
    },
    "unlock": 10
  },
  {
    "id": "1108",
    "name": {
      "fr-FR": "Fromage verni",
      "en-US": "Edam Cheese",
      "es-SP": "Queso barnizado"
    },
    "unlock": 10
  },
  {
    "id": "1109",
    "name": {
      "fr-FR": "Roquefort",
      "en-US": "Blue Cheese",
      "es-SP": "Roquefort"
    },
    "unlock": 10
  },
  {
    "id": "1110",
    "name": {
      "fr-FR": "Fromage frais vigné",
      "en-US": "Swiss Cheese",
      "es-SP": "Queso fresco"
    },
    "unlock": 10
  },
  {
    "id": "1111",
    "name": {
      "fr-FR": "Pâte dessert fromagée",
      "en-US": "Cheesecake",
      "es-SP": "Quesito"
    },
    "unlock": 10
  },
  {
    "id": "1100",
    "name": {
      "fr-FR": "Manquereau-sauce-au-citron",
      "en-US": "Lemon Canned-Fish",
      "es-SP": "Lata de atún en aceite de oliva"
    },
    "unlock": 10
  },
  {
    "id": "1101",
    "name": {
      "fr-FR": "Mini-saucisses en boite",
      "en-US": "Canned Sliced Sausages",
      "es-SP": "Minisalchichas en conserva"
    },
    "unlock": 10
  },
  {
    "id": "1102",
    "name": {
      "fr-FR": "Haricots blanc",
      "en-US": "White Beans",
      "es-SP": "Judías blancas"
    },
    "unlock": 10
  },
  {
    "id": "1103",
    "name": {
      "fr-FR": "Lychees premier prix",
      "en-US": "Canned Lychees",
      "es-SP": "Litchis incaducables"
    },
    "unlock": 10
  },
  {
    "id": "1104",
    "name": {
      "fr-FR": "Zion's Calamar",
      "en-US": "Zion's Cheese",
      "es-SP": "Fabada asturiana en conserva"
    },
    "unlock": 10
  },
  {
    "id": "1105",
    "name": {
      "fr-FR": "Aubergines au sirop",
      "en-US": "Pâté",
      "es-SP": "Conserva de lentejas madrileñas con chorizo"
    },
    "unlock": 10
  },
  {
    "id": "1168",
    "name": {
      "fr-FR": "Sardines",
      "en-US": "Sardines",
      "es-SP": "Sardinas en lata"
    },
    "unlock": 10
  },
  {
    "id": "1112",
    "name": {
      "fr-FR": "Saladou",
      "en-US": "Lettuce",
      "es-SP": "Ensalado"
    },
    "unlock": 10
  },
  {
    "id": "1113",
    "name": {
      "fr-FR": "Poire d'eau",
      "en-US": "Leek",
      "es-SP": "Puerro de agua"
    },
    "unlock": 10
  },
  {
    "id": "1114",
    "name": {
      "fr-FR": "Cacahuète mauve et juteuse",
      "en-US": "Eggplant",
      "es-SP": "Cacahuete aceitoso de malva"
    },
    "unlock": 10
  },
  {
    "id": "1115",
    "name": {
      "fr-FR": "Pommes de pierre",
      "en-US": "Walnut",
      "es-SP": "Manzana de piedra"
    },
    "unlock": 10
  },
  {
    "id": "1116",
    "name": {
      "fr-FR": "Patates douces",
      "en-US": "Hazelnut",
      "es-SP": "Patatas dulces"
    },
    "unlock": 10
  },
  {
    "id": "1117",
    "name": {
      "fr-FR": "Matraque bio",
      "en-US": "Cucumber",
      "es-SP": "Porra bio"
    },
    "unlock": 10
  },
  {
    "id": "1118",
    "name": {
      "fr-FR": "Tomate pacifique",
      "en-US": "Tomato",
      "es-SP": "Tomate pacífico"
    },
    "unlock": 10
  },
  {
    "id": "1119",
    "name": {
      "fr-FR": "Radix",
      "en-US": "Radish",
      "es-SP": "Rabanox"
    },
    "unlock": 10
  },
  {
    "id": "1120",
    "name": {
      "fr-FR": "Haricots verts",
      "en-US": "Green Beans",
      "es-SP": "Judías verdes"
    },
    "unlock": 10
  },
  {
    "id": "1121",
    "name": {
      "fr-FR": "Pomme Sapik",
      "en-US": "Onion",
      "es-SP": "Cebolla Sapik"
    },
    "unlock": 10
  },
  {
    "id": "1122",
    "name": {
      "fr-FR": "Pomme Sapu",
      "en-US": "Garlic",
      "es-SP": "Ajos Sapu"
    },
    "unlock": 10
  },
  {
    "id": "1123",
    "name": {
      "fr-FR": "Echalottes",
      "en-US": "Shallots",
      "es-SP": "Chalote"
    },
    "unlock": 10
  },
  {
    "id": "1124",
    "name": {
      "fr-FR": "Sel rieur",
      "en-US": "River Salt",
      "es-SP": "Sal que ríe"
    },
    "unlock": 10
  },
  {
    "id": "1125",
    "name": {
      "fr-FR": "Poivron vert",
      "en-US": "Green Pepper",
      "es-SP": "Pimiento verde"
    },
    "unlock": 10
  },
  {
    "id": "1126",
    "name": {
      "fr-FR": "Poivron jaune",
      "en-US": "Yellow Pepper",
      "es-SP": "Pimiento amarillo"
    },
    "unlock": 10
  },
  {
    "id": "1127",
    "name": {
      "fr-FR": "Poivron rouge",
      "en-US": "Red Pepper",
      "es-SP": "Pimiento rojo"
    },
    "unlock": 10
  },
  {
    "id": "1128",
    "name": {
      "fr-FR": "Brocolis digérés",
      "en-US": "Digested Broccoli",
      "es-SP": "Brocolis digerido"
    },
    "unlock": 10
  },
  {
    "id": "1129",
    "name": {
      "fr-FR": "Grappe de Radix",
      "en-US": "Bitten Radish",
      "es-SP": "Rácimo de Rabanox"
    },
    "unlock": 10
  },
  {
    "id": "1130",
    "name": {
      "fr-FR": "Lance-poix",
      "en-US": "Snow Peas",
      "es-SP": "Lanza-guisantes"
    },
    "unlock": 10
  },
  {
    "id": "1131",
    "name": {
      "fr-FR": "Haricots blancs",
      "en-US": "White Beans",
      "es-SP": "Judías blancas"
    },
    "unlock": 10
  },
  {
    "id": "1132",
    "name": {
      "fr-FR": "Poires d'eau en grappe",
      "en-US": "Blended Pears",
      "es-SP": "Puñado de puerros de agua"
    },
    "unlock": 10
  },
  {
    "id": "1133",
    "name": {
      "fr-FR": "Artifroid",
      "en-US": "Artichoke",
      "es-SP": "Alcachofa"
    },
    "unlock": 10
  },
  {
    "id": "1134",
    "name": {
      "fr-FR": "Choux-flou",
      "en-US": "Cauliflower",
      "es-SP": "Coliflor"
    },
    "unlock": 10
  },
  {
    "id": "1135",
    "name": {
      "fr-FR": "Chourou",
      "en-US": "Purple Cabbage",
      "es-SP": "Lombarda"
    },
    "unlock": 10
  },
  {
    "id": "1136",
    "name": {
      "fr-FR": "Pom pom pom..",
      "en-US": "Corn",
      "es-SP": "Disparador de Maíz"
    },
    "unlock": 10
  },
  {
    "id": "1141",
    "name": {
      "fr-FR": "Brioche dorée",
      "en-US": "Golden Pastry",
      "es-SP": "Magdalena brioche"
    },
    "unlock": 10
  },
  {
    "id": "1143",
    "name": {
      "fr-FR": "Laxatif aux amandes",
      "en-US": "Almond Laxative",
      "es-SP": "Laxante de almendras"
    },
    "unlock": 10
  },
  {
    "id": "1137",
    "name": {
      "fr-FR": "Hollandais",
      "en-US": "Dutch Cake",
      "es-SP": "Sandwich tostado Holandés"
    },
    "unlock": 10
  },
  {
    "id": "1138",
    "name": {
      "fr-FR": "Fondant XXL au choco-beurre",
      "en-US": "Chocolate Fudge Cake",
      "es-SP": "Pastel XXL de choco-mantequilla"
    },
    "unlock": 10
  },
  {
    "id": "1139",
    "name": {
      "fr-FR": "Pétillante",
      "en-US": "Sparkling Soda",
      "es-SP": "Gaseosa"
    },
    "unlock": 10
  },
  {
    "id": "1140",
    "name": {
      "fr-FR": "Warpoquiche",
      "en-US": "Baked Cracker",
      "es-SP": "Tarta salada de queso"
    },
    "unlock": 10
  },
  {
    "id": "1144",
    "name": {
      "fr-FR": "Smiley croquant",
      "en-US": "Ghost Toast",
      "es-SP": "Smiley crujiente"
    },
    "unlock": 10
  },
  {
    "id": "1145",
    "name": {
      "fr-FR": "Barquette de lave",
      "en-US": "Flat Bread",
      "es-SP": "Barquillo de lava"
    },
    "unlock": 10
  },
  {
    "id": "1146",
    "name": {
      "fr-FR": "Nonoix",
      "en-US": "Cracked Nut",
      "es-SP": "Nonuez"
    },
    "unlock": 10
  },
  {
    "id": "1147",
    "name": {
      "fr-FR": "Amande croquante",
      "en-US": "Crunchy Almond",
      "es-SP": "Almendra crujiente"
    },
    "unlock": 10
  },
  {
    "id": "1148",
    "name": {
      "fr-FR": "Noisette",
      "en-US": "Chestnut",
      "es-SP": "Nuez"
    },
    "unlock": 10
  },
  {
    "id": "1150",
    "name": {
      "fr-FR": "Brioche vapeur",
      "en-US": "Steamed Brioche",
      "es-SP": "Brioche de vapor"
    },
    "unlock": 10
  },
  {
    "id": "1151",
    "name": {
      "fr-FR": "Tartine chocolat-noisette",
      "en-US": "Slice of Chocolate Hazelnut Bread",
      "es-SP": "Tostada con crema de chocolate"
    },
    "unlock": 10
  },
  {
    "id": "1152",
    "name": {
      "fr-FR": "Tartine hémoglobine",
      "en-US": "Slice of Raspberry Hazelnut Bread",
      "es-SP": "Tostada con hemoglobina"
    },
    "unlock": 10
  },
  {
    "id": "1153",
    "name": {
      "fr-FR": "Tartine à l'orange collante",
      "en-US": "Slice of Sticky Orange Bread",
      "es-SP": "Tostada de aceite"
    },
    "unlock": 10
  },
  {
    "id": "1154",
    "name": {
      "fr-FR": "Tartine au miel",
      "en-US": "Slice of Hard Honey Bread",
      "es-SP": "Tostada de mermelada pegajosa"
    },
    "unlock": 10
  },
  {
    "id": "1155",
    "name": {
      "fr-FR": "Lombric nature",
      "en-US": "Earthworm",
      "es-SP": "Lombriz al natural"
    },
    "unlock": 10
  },
  {
    "id": "1159",
    "name": {
      "fr-FR": "Blob périmé",
      "en-US": "Shiny Mushroom",
      "es-SP": "Champiñón podrido"
    },
    "unlock": 10
  },
  {
    "id": "1160",
    "name": {
      "fr-FR": "Bonbon Hélène-fraiche",
      "en-US": "Peppermint Candy",
      "es-SP": "Caramelo de reyes"
    },
    "unlock": 10
  },
  {
    "id": "1058",
    "name": {
      "fr-FR": "Canelé du sud-ouest",
      "en-US": "Hazelnut Chocolate",
      "es-SP": "Canelé de Burdeos"
    },
    "unlock": 10
  },
  {
    "id": "1059",
    "name": {
      "fr-FR": "Eclair noisette choco caramel et sucre",
      "en-US": "Chocolate Eclair",
      "es-SP": "Rebanada de pan con crema de cacao y avellana"
    },
    "unlock": 10
  },
  {
    "id": "1072",
    "name": {
      "fr-FR": "Arbuche de noël",
      "en-US": "Christmas Log",
      "es-SP": "Tronco de Navidad"
    },
    "unlock": 10
  },
  {
    "id": "1156",
    "name": {
      "fr-FR": "Grenade de chantilly",
      "en-US": "Cream Grenade",
      "es-SP": "Granada de crema"
    },
    "unlock": 10
  },
  {
    "id": "1157",
    "name": {
      "fr-FR": "Profies très drôles",
      "en-US": "Chocolate-Dipped Cream Puffs",
      "es-SP": "Profiteroles apetitosos"
    },
    "unlock": 10
  },
  {
    "id": "1158",
    "name": {
      "fr-FR": "Chouchocos",
      "en-US": "Chocolate Delight",
      "es-SP": "Megaprofiterol"
    },
    "unlock": 10
  },
  {
    "id": "1165",
    "name": {
      "fr-FR": "Tartelette framboise",
      "en-US": "Strawberry Tart",
      "es-SP": "Tarta de frambuesa"
    },
    "unlock": 10
  },
  {
    "id": "1170",
    "name": {
      "fr-FR": "Transformer en sucre",
      "en-US": "Sugar Transformer",
      "es-SP": "Transformer de azúcar"
    },
    "unlock": 10
  },
  {
    "id": "1171",
    "name": {
      "fr-FR": "Kitchissime",
      "en-US": "Evil Robot",
      "es-SP": "Kitchisime"
    },
    "unlock": 10
  },
  {
    "id": "1172",
    "name": {
      "fr-FR": "Igorocop",
      "en-US": "RoboCop",
      "es-SP": "Igorocop"
    },
    "unlock": 10
  },
  {
    "id": "1173",
    "name": {
      "fr-FR": "Tidouli didi",
      "en-US": "R2D2",
      "es-SP": "Arto deedo"
    },
    "unlock": 10
  },
  {
    "id": "1174",
    "name": {
      "fr-FR": "Dalek ! Exterminate !",
      "en-US": "Dalek ! Exterminate !",
      "es-SP": "¡Dalek, el Exterminador!"
    },
    "unlock": 10
  },
  {
    "id": "1175",
    "name": {
      "fr-FR": "Robo-malin",
      "en-US": "Robo-Malin",
      "es-SP": "Robo-loco"
    },
    "unlock": 10
  },
  {
    "id": "1176",
    "name": {
      "fr-FR": "Johnny 6",
      "en-US": "Johnny 6",
      "es-SP": "Johnny 6"
    },
    "unlock": 10
  },
  {
    "id": "1177",
    "name": {
      "fr-FR": "Biscuit Transformer",
      "en-US": "Biscuit Transformer",
      "es-SP": "Galleta de Transformer"
    },
    "unlock": 10
  },
  {
    "id": "1178",
    "name": {
      "fr-FR": "Statue: Citron Sorbex",
      "en-US": "Statue: Sorbex Lemon",
      "es-SP": "Estatua: Sorbetex de Limón"
    },
    "unlock": 10
  },
  {
    "id": "1179",
    "name": {
      "fr-FR": "Statue: Bombino",
      "en-US": "Statue: Bombino",
      "es-SP": "Estatua: Bombino"
    },
    "unlock": 10
  },
  {
    "id": "1180",
    "name": {
      "fr-FR": "Statue: Poire Melbombe",
      "en-US": "Statue: Baddybomb Pear",
      "es-SP": "Estatua: Pera Malabomba"
    },
    "unlock": 10
  },
  {
    "id": "1181",
    "name": {
      "fr-FR": "Statue: Tagada",
      "en-US": "Statue: Tagada-Strawberry",
      "es-SP": "Estatua: Tagada"
    },
    "unlock": 10
  },
  {
    "id": "1182",
    "name": {
      "fr-FR": "Statue: Sapeur-kiwi",
      "en-US": "Statue: Kiwi Sapper",
      "es-SP": "Estatua: Kiwi-Zapador"
    },
    "unlock": 10
  },
  {
    "id": "1183",
    "name": {
      "fr-FR": "Statue: Bondissante",
      "en-US": "Statue: Leaper",
      "es-SP": "Estatua: Sandina"
    },
    "unlock": 10
  },
  {
    "id": "1184",
    "name": {
      "fr-FR": "Statue: Ananargeddon",
      "en-US": "Statue: Armaggedon-Pineapple",
      "es-SP": "Estatua: Piñaguedón"
    },
    "unlock": 10
  },
  {
    "id": "1190",
    "name": {
      "fr-FR": "Passe-partout en bois",
      "en-US": "Wooden Master Key",
      "es-SP": "Llave Maestra de Madera"
    },
    "unlock": 10
  },
  {
    "id": "1191",
    "name": {
      "fr-FR": "Clé de Rigor Dangerous",
      "en-US": "Rigor Dangerous Key",
      "es-SP": "Llave de Rigor Dangerous"
    },
    "unlock": 10
  },
  {
    "id": "1194",
    "name": {
      "fr-FR": "Furtok Glaciale",
      "en-US": "Frozen Key",
      "es-SP": "Furtok Glacial"
    },
    "unlock": 10
  },
  {
    "id": "1197",
    "name": {
      "fr-FR": "Clé des Mondes Ardus",
      "en-US": "Key of Difficult Worlds ",
      "es-SP": "Llave de los Mundos Arduos"
    },
    "unlock": 10
  },
  {
    "id": "1198",
    "name": {
      "fr-FR": "Clé piquante",
      "en-US": "Pungent Key",
      "es-SP": "Llave Que Rasca"
    },
    "unlock": 10
  },
  {
    "id": "1199",
    "name": {
      "fr-FR": "Passe-partout de Tuberculoz",
      "en-US": "Tuber's Key",
      "es-SP": "Llave Maestra de Tubérculo"
    },
    "unlock": 10
  },
  {
    "id": "1200",
    "name": {
      "fr-FR": "Clé des cauchemars",
      "en-US": "Nightmare Key",
      "es-SP": "Llave de las Pesadillas"
    },
    "unlock": 10
  },
  {
    "id": "1193",
    "name": {
      "fr-FR": "Clé du Bourru",
      "en-US": "Wine Key",
      "es-SP": "Llave del Bruto"
    },
    "unlock": 1
  },
  {
    "id": "1196",
    "name": {
      "fr-FR": "Autorisation du Bois-Joli",
      "en-US": "Authorization of Bois-Joli",
      "es-SP": "Autorización Madera-Bonita"
    },
    "unlock": 1
  },
  {
    "id": "1192",
    "name": {
      "fr-FR": "Méluzzine",
      "en-US": "Méluzzine Key",
      "es-SP": "Meluzina"
    },
    "unlock": 10
  },
  {
    "id": "1195",
    "name": {
      "fr-FR": "Vieille clé rouillée",
      "en-US": "Old Rusty Key",
      "es-SP": "Vieja Llave Oxidada"
    },
    "unlock": 10
  },
  {
    "id": "1201",
    "name": {
      "fr-FR": "Pad Sounie",
      "en-US": "Sony Controller",
      "es-SP": "Mando Suni"
    },
    "unlock": 10
  },
  {
    "id": "1202",
    "name": {
      "fr-FR": "Pad Frusion 64",
      "en-US": "Nintendo 64 Controller",
      "es-SP": "Mando Nientiendo 64"
    },
    "unlock": 10
  },
  {
    "id": "1203",
    "name": {
      "fr-FR": "Pad Game-Pyramid",
      "en-US": "Game-Cube Controller",
      "es-SP": "Mando Game-Pirámide"
    },
    "unlock": 10
  },
  {
    "id": "1204",
    "name": {
      "fr-FR": "Pad Sey-Ga",
      "en-US": "Genesis Controller",
      "es-SP": "Mando Sey-Ga"
    },
    "unlock": 10
  },
  {
    "id": "1205",
    "name": {
      "fr-FR": "Pad Super Frusion",
      "en-US": "SNES Controller",
      "es-SP": "Mando Super Nientiendo"
    },
    "unlock": 10
  },
  {
    "id": "1206",
    "name": {
      "fr-FR": "Pad du Système Maitre",
      "en-US": "NES Controller",
      "es-SP": "Mando del Sistema Maestro"
    },
    "unlock": 10
  },
  {
    "id": "1207",
    "name": {
      "fr-FR": "Pad Frusion Entertainment System",
      "en-US": "Master System Controller",
      "es-SP": "Mando Nientiendo Entertainment System"
    },
    "unlock": 10
  },
  {
    "id": "1208",
    "name": {
      "fr-FR": "Manette S-Téhéf",
      "en-US": "Joystick",
      "es-SP": "Mando S-Tehef"
    },
    "unlock": 10
  },
  {
    "id": "1209",
    "name": {
      "fr-FR": "Canette Express",
      "en-US": "Can Beer",
      "es-SP": "Lata Express"
    },
    "unlock": 10
  },
  {
    "id": "1210",
    "name": {
      "fr-FR": "Bouteille aux 2064 bulles",
      "en-US": "Sealed Beer",
      "es-SP": "Cerveza de las fiestas en Bordeaux"
    },
    "unlock": 10
  },
  {
    "id": "1211",
    "name": {
      "fr-FR": "Mousse volante",
      "en-US": "Draft Beer",
      "es-SP": "Espuma voladora"
    },
    "unlock": 10
  },
  {
    "id": "1212",
    "name": {
      "fr-FR": "Vin Merveilleux",
      "en-US": "Wonderful Wine",
      "es-SP": "Vino burgués del Gran Teatro"
    },
    "unlock": 10
  },
  {
    "id": "1213",
    "name": {
      "fr-FR": "Liqueur maléfique",
      "en-US": "Evil Liquor",
      "es-SP": "Licor maléfico"
    },
    "unlock": 10
  },
  {
    "id": "1214",
    "name": {
      "fr-FR": "Tampon MT",
      "en-US": "MT Stamp",
      "es-SP": "Sello MotionTwin"
    },
    "unlock": 10
  },
  {
    "id": "1215",
    "name": {
      "fr-FR": "Facture gratuite",
      "en-US": "Free Bill",
      "es-SP": "Factura gratis"
    },
    "unlock": 10
  },
  {
    "id": "1216",
    "name": {
      "fr-FR": "Post-It de François",
      "en-US": "Sticky Notes",
      "es-SP": "Post-It de François, no entiendo tu letra"
    },
    "unlock": 10
  },
  {
    "id": "1217",
    "name": {
      "fr-FR": "Pot à crayon solitaire",
      "en-US": "Pencil Container",
      "es-SP": "Lapicero solitario sin pegatina de perro"
    },
    "unlock": 10
  },
  {
    "id": "1218",
    "name": {
      "fr-FR": "Agrafeuse du Chaos",
      "en-US": "Chaos Stapler",
      "es-SP": "Grapadora del Caos"
    },
    "unlock": 10
  },
  {
    "id": "1219",
    "name": {
      "fr-FR": "Miroir bancal",
      "en-US": "Mirror of Bancal",
      "es-SP": "Espejo de la cábala"
    },
    "unlock": 10
  },
  {
    "id": "1220",
    "name": {
      "fr-FR": "Etoile du Diable",
      "en-US": "Star of the Devil",
      "es-SP": "Estrella demoníaca"
    },
    "unlock": 10
  },
  {
    "id": "1225",
    "name": {
      "fr-FR": "Miroir des Sables",
      "en-US": "Mirror of Sables",
      "es-SP": "Espejo de las Arenas"
    },
    "unlock": 10
  },
  {
    "id": "1226",
    "name": {
      "fr-FR": "Etoile des Diables Jumeaux",
      "en-US": "Star of the Devil's Twins",
      "es-SP": "Estrella de los Diablos Gemelos"
    },
    "unlock": 10
  },
  {
    "id": "1227",
    "name": {
      "fr-FR": "Sceau d'amitié",
      "en-US": "Mixed Heart",
      "es-SP": "Sello de amistad"
    },
    "unlock": 10
  },
  {
    "id": "1237",
    "name": {
      "fr-FR": "Neige-o-glycérine",
      "en-US": "Snow Bomb",
      "es-SP": "Nieve a la glicerina"
    },
    "unlock": 10
  },
  {
    "id": "1238",
    "name": {
      "fr-FR": "Pass-Pyramide",
      "en-US": "Pyramid Pass",
      "es-SP": "Pase VIP Pirámide"
    },
    "unlock": 10
  },
  {
    "id": "1221",
    "name": {
      "fr-FR": "Poudre de plage magique",
      "en-US": "Shovel",
      "es-SP": "Polvo de playa mágica"
    },
    "unlock": 5
  },
  {
    "id": "1222",
    "name": {
      "fr-FR": "Matériel d'architecte",
      "en-US": "Bucket of Sand",
      "es-SP": "Material de arquitecto"
    },
    "unlock": 10
  },
  {
    "id": "1223",
    "name": {
      "fr-FR": "Maquette en sable",
      "en-US": "Sand Castle",
      "es-SP": "Maqueta de arena"
    },
    "unlock": 5
  },
  {
    "id": "1224",
    "name": {
      "fr-FR": "Winkel",
      "en-US": "Winkel O'Riely",
      "es-SP": "Wink"
    },
    "unlock": 3
  },
  {
    "id": "1228",
    "name": {
      "fr-FR": "Insigne de l'ordre des Ninjas",
      "en-US": "Symbol of the Ninjas",
      "es-SP": "Insignia de la orden de los Ninjas"
    },
    "unlock": 10
  },
  {
    "id": "1236",
    "name": {
      "fr-FR": "Insigne du Mérite",
      "en-US": "Award of Merit",
      "es-SP": "Insignia del Mérito"
    },
    "unlock": 10
  },
  {
    "id": "1229",
    "name": {
      "fr-FR": "Couteau suisse japonais",
      "en-US": "Magic Katana",
      "es-SP": "Cuchillo suizo japonés"
    },
    "unlock": 10
  },
  {
    "id": "1230",
    "name": {
      "fr-FR": "Shuriken de second rang",
      "en-US": "Fuma Shuriken",
      "es-SP": "Shuriken de segundo rango"
    },
    "unlock": 10
  },
  {
    "id": "1231",
    "name": {
      "fr-FR": "Shuriken d'entraînement",
      "en-US": "Shuriken",
      "es-SP": "Shuriken de entrenamiento"
    },
    "unlock": 10
  },
  {
    "id": "1232",
    "name": {
      "fr-FR": "Najinata",
      "en-US": "Kunai",
      "es-SP": "Najinata"
    },
    "unlock": 10
  },
  {
    "id": "1233",
    "name": {
      "fr-FR": "Lance-boulettes de Précision",
      "en-US": "Blowgun of Precision",
      "es-SP": "Lanza-bolitas de Precisión"
    },
    "unlock": 10
  },
  {
    "id": "1234",
    "name": {
      "fr-FR": "Ocarina chantant",
      "en-US": "Singing Ocarina",
      "es-SP": "Ocarina cantante"
    },
    "unlock": 10
  },
  {
    "id": "1235",
    "name": {
      "fr-FR": "Armure de la nuit",
      "en-US": "Armor of the Night",
      "es-SP": "Casco samurai"
    },
    "unlock": 10
  }
];
