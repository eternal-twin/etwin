import { Injectable } from "@angular/core";
import { $TimeQuery } from "@eternaltwin/core/core/time-query";
import { $EtwinDinoparcDinoz, NullableEtwinDinoparcDinoz } from "@eternaltwin/core/dinoparc/etwin-dinoparc-dinoz";
import { $EtwinDinoparcUser, NullableEtwinDinoparcUser } from "@eternaltwin/core/dinoparc/etwin-dinoparc-user";
import { GetDinoparcDinozOptions } from "@eternaltwin/core/dinoparc/get-dinoparc-dinoz-options";
import { GetDinoparcUserOptions } from "@eternaltwin/core/dinoparc/get-dinoparc-user-options";
import { Observable, of as rxOf } from "rxjs";
import { catchError as rxCatchError } from "rxjs/operators";

import { RestService } from "../rest/rest.service.mjs";

@Injectable()
export class DinoparcService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUser(options: Readonly<GetDinoparcUserOptions>): Observable<NullableEtwinDinoparcUser> {
    return this.#rest
      .get(
        ["archive", "dinoparc", options.server, "users", options.id],
        {
          queryType: $TimeQuery,
          query: {time: options.time},
          resType: $EtwinDinoparcUser,
        },
      )
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  getDinoz(options: Readonly<GetDinoparcDinozOptions>): Observable<NullableEtwinDinoparcDinoz> {
    return this.#rest
      .get(
        ["archive", "dinoparc", options.server, "dinoz", options.id],
        {
          queryType: $TimeQuery,
          query: {time: options.time},
          resType: $EtwinDinoparcDinoz,
        },
      )
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
