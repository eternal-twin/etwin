import { NgModule } from "@angular/core";

import { ForumService } from "./forum.service.mjs";

@NgModule({
  imports: [
    // RestModule,
  ],
  providers: [
    ForumService,
  ],
})
export class ForumModule {
}
