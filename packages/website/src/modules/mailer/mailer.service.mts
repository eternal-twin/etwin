import {Injectable} from "@angular/core";
import {$GetOutboundEmailRequests, GetOutboundEmailRequests} from "@eternaltwin/core/mailer/get-outbound-email-requests";
import {$GetOutboundEmails, GetOutboundEmails} from "@eternaltwin/core/mailer/get-outbound-emails";
import {$OutboundEmail, OutboundEmail} from "@eternaltwin/core/mailer/outbound-email";
import {$OutboundEmailListing, OutboundEmailListing} from "@eternaltwin/core/mailer/outbound-email-listing";
import {
  $OutboundEmailRequestListing,
  OutboundEmailRequestListing
} from "@eternaltwin/core/mailer/outbound-email-request-listing";
import {$SendMarktwinEmail, SendMarktwinEmail} from "@eternaltwin/core/mailer/send-marktwin-email";
import {Observable} from "rxjs";

import {RestService} from "../rest/rest.service.mjs";

@Injectable()
export class MailerService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getOutboundEmails(
    query: GetOutboundEmails,
  ): Observable<OutboundEmailListing> {
    return this.#rest.get(["outbound_email"], {
      queryType: $GetOutboundEmails,
      query,
      resType: $OutboundEmailListing,
    });
  }

  getOutboundEmailRequests(
    query: GetOutboundEmailRequests,
  ): Observable<OutboundEmailRequestListing> {
    return this.#rest.get(["outbound_email_request"], {
      queryType: $GetOutboundEmailRequests,
      query,
      resType: $OutboundEmailRequestListing,
    });
  }

  sendMarktwinEmail(cmd: SendMarktwinEmail): Observable<OutboundEmail> {
    return this.#rest.post(["outbound_email"], {
      reqType: $SendMarktwinEmail,
      req: cmd,
      resType: $OutboundEmail,
    });
  }
}
