import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";

import { BrowserRestService } from "./rest.service.browser.mjs";
import { RestService } from "./rest.service.mjs";

@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    {provide: RestService, useClass: BrowserRestService},
  ],
})
export class BrowserRestModule {
}
