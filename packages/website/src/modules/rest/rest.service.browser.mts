import {HttpClient} from "@angular/common/http";
import {ApplicationRef, Injectable} from "@angular/core";
import {TransferState} from "@angular/platform-browser";
import {readOrThrow} from "kryo";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {firstValueFrom, Observable, of as rxOf} from "rxjs";
import {filter as rxFilter, map as rxMap, take} from "rxjs/operators";

import {environment} from "../../environments/environment.mjs";
import {
  GetInput,
  QueryRequestOptions,
  RequestOptions,
  RestService,
  SimpleRequestOptions,
  toTransferStateKey
} from "./rest.service.mjs";

const MISSING_TRANSFER_STATE: unique symbol = Symbol("MISSING_TRANSFER_STATE");

@Injectable()
export class BrowserRestService extends RestService {
  readonly #httpClient: HttpClient;
  readonly #transferState: TransferState;
  #isCacheActive: boolean;

  constructor(appRef: ApplicationRef, httpClient: HttpClient, transferState: TransferState) {
    super();
    this.#httpClient = httpClient;
    this.#transferState = transferState;
    this.#isCacheActive = true;
    // Stop using the cache if the application has stabilized, indicating
    // initial rendering is complete.
    firstValueFrom(appRef.isStable
      .pipe(rxFilter((isStable) => isStable)))
      .then(() => this.#isCacheActive = false);
  }

  public delete<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("delete", route, options);
  }

  public get<Query, Res>(route: readonly string[], options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Observable<Res> {
    return this.request("get", route, options);
  }

  public patch<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("patch", route, options);
  }

  public post<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("post", route, options);
  }

  public put<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("put", route, options);
  }

  private request<Query, Req, Res>(method: string, route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    if (this.#isCacheActive && method === "get") {
      const key = toTransferStateKey({route, queryType: options.queryType, query: options.query} as GetInput<Query>);
      const cached = this.#transferState.get(key, MISSING_TRANSFER_STATE);
      if (cached !== MISSING_TRANSFER_STATE) {
        return rxOf(readOrThrow(options.resType, JSON_VALUE_READER, cached));
      }
    }

    const uri = this.resolveUri(route);
    const rawReq: object | undefined = options.reqType !== undefined ? options.reqType.write(JSON_VALUE_WRITER, options.req) : undefined;
    const rawQuery: Record<string, string> | undefined = options.queryType !== undefined ? options.queryType.write(JSON_VALUE_WRITER, options.query) : undefined;
    return this.#httpClient.request(method, uri, {body: rawReq, withCredentials: true, params: rawQuery})
      .pipe(rxMap((raw): Res => {
        try {
          return readOrThrow(options.resType, JSON_VALUE_READER, raw);
        } catch (err) {
          console.error(`API Error: ${uri}`);
          console.error(err);
          throw err;
        }
      }));
  }

  private resolveUri(route: readonly string[]): string {
    return `${environment.apiBase}/${route.map(encodeURIComponent).join("/")}`;
  }
}
