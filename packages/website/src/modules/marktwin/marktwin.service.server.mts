import { Injectable } from "@angular/core";
import { Grammar } from "@eternaltwin/marktwin/grammar";

import { MarktwinService } from "./marktwin.service.mjs";

@Injectable()
export class ServerMarktwinService extends MarktwinService {
  public constructor() {
    super();
  }

  public renderMarktwin(grammar: Readonly<Grammar>, input: string): string {
    return "marktwin rendering is not supported by server-side rendering";
  }
}
