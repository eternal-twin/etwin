import { NgModule } from "@angular/core";

import { BrowserAuthService } from "./auth.service.browser.mjs";
import { AuthService } from "./auth.service.mjs";

@NgModule({
  providers: [
    {provide: AuthService, useClass: BrowserAuthService},
  ],
  imports: [
    // RestModule,
  ],
})
export class BrowserAuthModule {
}
