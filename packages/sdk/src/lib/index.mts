import {ChildProcess, spawn} from "node:child_process";
import {fileURLToPath} from "node:url";

import {firstValueFrom, NEVER, Observable} from "rxjs";
import {filter} from "rxjs/operators";

import {execAsync} from "./process-utils.mjs";

export class Sdk {
  #exe: URL;
  #version: string;

  private constructor(exe: URL, version: string) {
    this.#exe = exe;
    this.#version = version;
  }

  public async startServer(): Promise<EternaltwinServer> {
    return EternaltwinServer.start(this.#exe);
  }

  public async getVersion(): Promise<string> {
    return this.#version;
  }

  public static async fromExe(exe: URL): Promise<Sdk> {
    const out = await execAsync({
      command: exe,
      encoding: "utf-8",
      args: ["version", "--format", "json"],
      timeout: 10,
    });
    const version = JSON.parse(out.stdout);
    return new Sdk(exe, version.eternaltwin);
  }
}

enum ServerState {
  Init,
  Run,
  Stop,
}

interface ServerEvent {
  event: string;
}

export class EternaltwinServer {
  #process: ChildProcess;
  #state: ServerState;
  #stdout$: Observable<ServerEvent>;
  // #stdout: ServerState;
  // #stderr: ServerState;

  constructor(process: ChildProcess) {
    const stdout = process.stdout;
    const stderr = process.stderr;
    if (stdout === null || stderr === null) {
      throw new Error("failed to retrieve child process pipes");
    }

    this.#process = process;
    this.#state = ServerState.Init;
    this.#stdout$ = NEVER;
  }

  async #waitReady(): Promise<void> {
    const found$ = this.#stdout$.pipe(filter((ev) => ev.event === "Listen" || ev.event === "Error"));
    const found = await firstValueFrom(found$);
    if (found.event === "Error") {
      throw new Error("ServerError");
    }
  }

  public getPort(): string {
    return "port";
  }

  static async start(exe: URL): Promise<EternaltwinServer> {
    const exePath = fileURLToPath(exe);
    const child: ChildProcess = spawn(exePath, ["backend", "--profile", "sdk", "--format", "json"], {stdio: "pipe"});
    await using server = new EternaltwinServer(child);
    await server.#waitReady();
    return server;
  }

  public async stop(): Promise<void> {
    if (this.#state === ServerState.Stop) {
      return;
    }
    this.#state = ServerState.Stop;
    this.#process.kill("SIGTERM");
    this.#stdout$ = NEVER;
  }

  public async [Symbol.asyncDispose]() {
    await this.stop();
  }
}
