import {ChildProcess,spawn} from "node:child_process";
import {StringDecoder} from "node:string_decoder";
import {fileURLToPath, pathToFileURL} from "node:url";

import {Url} from "@eternaltwin/core/core/url";
import {findUp} from "find-up";
// import fs from "fs";
import process from "process";
// import {Transform} from "node:stream";

export interface ExecAsyncOptions {
  /**
   * Command to execute
   */
  readonly command: URL,

  /**
   * Process arguments
   */
  readonly args: readonly string[],

  /**
   * Timeout in milliseconds
   */
  readonly timeout: number,

  readonly encoding?: "utf-8",
}

export type ProcessEnd = {exitCode: number; signal: null} | {exitCode: null; signal: NodeJS.Signals};

export interface ProcessOutput<Stdout, Stderr = Stdout> {
  end: ProcessEnd;
  stdout: Stdout;
  stderr: Stderr;
}

export async function execAsync(options: ExecAsyncOptions & {encoding: "utf-8"}): Promise<ProcessOutput<string>>;
export async function execAsync(options: ExecAsyncOptions & {encoding: undefined}): Promise<ProcessOutput<Uint8Array>>;
export async function execAsync(options: ExecAsyncOptions): Promise<ProcessOutput<Uint8Array | string>> {
  const encoding = options.encoding;
  const exePath = fileURLToPath(options.command);
  const child: ChildProcess = spawn(exePath, options.args, {stdio: "pipe"});
  const stdout = child.stdout;
  const stderr = child.stderr;
  if (stdout === null || stderr === null) {
    throw new Error("failed to retrieve child process pipes");
  }
  return new Promise((resolve, reject) => {
    const stdoutChunks: Uint8Array[] = [];
    const stderrChunks: Uint8Array[] = [];
    const onSuccess = () => {
      const exitCode: number | null = child.exitCode;
      const signal: NodeJS.Signals | null = child.signalCode;
      let end: ProcessEnd;
      if (exitCode !== null) {
        end = {exitCode, signal: null};
      } else if (signal !== null) {
        end = {exitCode: null, signal};
      } else {
        reject(new Error("child process exit but none of `exitCode` and `signalCode` are defined"));
        return;
      }
      if (encoding === "utf-8") {
        const out: ProcessOutput<string> = {
          end,
          stderr: decodeUtf8Chunks(stderrChunks),
          stdout: decodeUtf8Chunks(stdoutChunks),
        };
        resolve(out);
      } else {
        const out: ProcessOutput<string> = {
          end,
          stderr: decodeUtf8Chunks(stderrChunks),
          stdout: decodeUtf8Chunks(stdoutChunks),
        };
        resolve(out);
      }
    };
    const onError = (e: Error) => {
      const stdoutStr = decodeUtf8Chunks(stdoutChunks);
      const stderrStr = decodeUtf8Chunks(stderrChunks);
      reject(new Error(`process error: stdout=${stdoutStr} stderr=${stderrStr}: ${e}`));
    };
    child.once("error", (e) => {
      onError(e);
    });
    child.once("exit", () => {
      onSuccess();
    });
    stdout.on("data", (chunk: Uint8Array) => {
      stdoutChunks.push(chunk);
    });
    stderr.on("data", (chunk: Uint8Array) => {
      stderrChunks.push(chunk);
    });
  });
}

export async function* readUtf8(chunks: AsyncIterable<Uint8Array>): AsyncGenerator<string, void, void> {
  const decoder = new StringDecoder("utf-8");
  for await (const chunk of chunks) {
    yield decoder.write(chunk as any);
  }
  yield decoder.end();
}

export async function* readLines(textStream: AsyncIterable<string>): AsyncGenerator<string, void, void> {
  const NL = "\n";
  const NL_LEN = NL.length;

  let partial: string = "";
  for await (const text of textStream) {
    let nlIndex = text.indexOf(NL);
    if (nlIndex < 0) {
      partial += text;
      continue;
    }
    let current = nlIndex + NL_LEN;
    yield partial + text.substring(0, current);
    for (;;) {
      nlIndex = text.indexOf(NL, current);
      if (nlIndex < 0) {
        break;
      }
      const next = nlIndex + NL_LEN;
      yield text.substring(current, next);
      current = next;
    }
    partial = text.substring(current);
  }
  yield partial;
}

export interface ResolvedEternaltwinExec {
  uri: Url,
  version: string,
}

export interface FindExeOptions {
  readonly start?: Url,
  readonly patterns?: readonly string[]
}

export async function findExe(options: FindExeOptions): Promise<ResolvedEternaltwinExec> {
  const start = options.start ?? pathToFileURL(process.cwd());
  const startPath = fileURLToPath(start);
  const exePath: string | undefined = await findUp(["./target/debug/etwin_cli", "./target/release/etwin_cli", "./target/debug/etwin_cli.exe", "./target/release/etwin_cli.exe"], {cwd: startPath});
  if (exePath === undefined) {
    throw new Error(`Eternaltwin executable not found from ${start}`);
  }
  const exe = pathToFileURL(exePath);
  return {
    uri: exe,
    version: await tryGetVersion(exe),
  };
  // const configText: string = await fs.promises.readFile(configPath, {encoding: "utf-8"});
  // return parseConfig(configText);
}

export async function tryGetVersion(exe: Url): Promise<string> {
  const exePath = fileURLToPath(exe);
  const child: ChildProcess = spawn(exePath, ["version", "--format", "json"], {stdio: "pipe"});
  const stdout = child.stdout;
  const stderr = child.stderr;
  if (stdout === null || stderr === null) {
    throw new Error("failed to retrieve child process pipes");
  }
  return new Promise((resolve, reject) => {
    const stdoutChunks: Uint8Array[] = [];
    const stderrChunks: Uint8Array[] = [];
    const onSuccess = () => {
      const stdoutStr = decodeUtf8Chunks(stdoutChunks);
      const stderrStr = decodeUtf8Chunks(stderrChunks);
      if (stderrStr !== "") {
        reject(new Error(`unexpected stderr data: ${stderrStr}`));
      } else {
        const data = JSON.parse(stdoutStr);
        resolve(Reflect.get(data, "eternaltwin"));
      }
    };
    const onError = () => {
      const stdoutStr = decodeUtf8Chunks(stdoutChunks);
      const stderrStr = decodeUtf8Chunks(stderrChunks);
      reject(new Error(`process error: stdout=${stdoutStr} stderr=${stderrStr}`));
    };
    child.once("error", () => {
      onError();
    });
    child.once("exit", () => {
      onSuccess();
    });
    stdout.on("data", (chunk: Uint8Array) => {
      stdoutChunks.push(chunk);
    });
    stderr.on("data", (chunk: Uint8Array) => {
      stderrChunks.push(chunk);
    });
  });
}

function decodeUtf8Chunks(chunks: readonly Uint8Array[]): string {
  const decoder = new StringDecoder("utf-8");
  let str = "";
  for (const chunk of chunks) {
    str += decoder.write(chunk as any);
  }
  str += decoder.end();
  return str;
}


// export interface Config {
//   etwin: EtwinConfig;
//   db: DbConfig;
//   clients: Map<string, ClientConfig>
//   auth: AuthConfig;
//   forum: ForumConfig;
// }
//
// export async function getLocalConfig(): Promise<Config> {
//   const cwd: string = process.cwd();
//   const configPath: string | undefined = await findUp("etwin.toml", {cwd});
//   if (configPath === undefined) {
//     throw new Error(`Config file \`etwin.toml\` not found from ${cwd}`);
//   }
//   const configText: string = await fs.promises.readFile(configPath, {encoding: "utf-8"});
//   return parseConfig(configText);
// }
