import type {Context as OtelContext, TextMapPropagator, TextMapSetter} from "@opentelemetry/api";
import {W3CTraceContextPropagator} from "@opentelemetry/core";
import type {IncomingMessage, ServerResponse} from "http";
import ProxyServer from "http-proxy";
import type {DefaultContext, DefaultState, Middleware, ParameterizedContext} from "koa";

export interface KoaProxyContext extends DefaultContext {
  opentelemetry?: OtelContext;
}

export type ProxyTarget = URL | { socketPath: string };

export interface ProxyOptions {
  target: ProxyTarget;
}

const HEADER_TEXT_MAP_SETTER: TextMapSetter<Partial<Record<string, string>>> = {
  set(headers: Record<string, string>, key: string, value: string): void {
    Reflect.set(headers, key, value);
  }
};

export function koaProxy<State = DefaultState, Context extends KoaProxyContext = KoaProxyContext, ResponseBody = unknown>(options: ProxyOptions): Middleware<State, Context, ResponseBody> {
  const propagator: TextMapPropagator<Record<string, string>> = new W3CTraceContextPropagator();

  const proxy: ProxyServer<IncomingMessage, ServerResponse> = ProxyServer.createProxyServer({
    // This requires a specific type annotation because we use `socketPath` without a host or port.
    // See <https://github.com/http-party/node-http-proxy/issues/1178>
    target: options.target as ProxyServer.ProxyTarget,
  });

  return (cx: ParameterizedContext<State, Context, ResponseBody>): Promise<void> => {
    return new Promise((resolve, reject) => {
      const res: ServerResponse = cx.res;
      let settled: boolean = false;
      const onSuccess = () => {
        if (!settled) {
          settled = true;
          res.removeListener("finish", onSuccess);
          res.removeListener("close", onSuccess);
          resolve();
        }
      };
      const onError = (err: Error): void => {
        if (!settled) {
          settled = true;
          res.removeListener("finish", onSuccess);
          res.removeListener("close", onSuccess);
          reject(err);
        }
      };
      if (res.closed) {
        onSuccess();
        return;
      }
      res.once("finish", onSuccess);
      res.once("close", onSuccess);

      const opentelemetry: OtelContext | undefined = cx.opentelemetry;
      const headers: Record<string, string> = {};
      if (opentelemetry !== undefined) {
        propagator.inject(opentelemetry, headers, HEADER_TEXT_MAP_SETTER);
      }

      proxy.web(cx.req, cx.res, {headers}, onError);
    });
  };
}
