#!/usr/bin/env node
import process from "node:process";
import { fileURLToPath } from "node:url";

import { getExecutableUri } from "@eternaltwin/exe";
import { foregroundChild } from "foreground-child";

async function main(): Promise<void> {
  const executableUri: URL = await getExecutableUri();
  const args: string[] = process.argv.slice(2);
  foregroundChild(fileURLToPath(executableUri), args);
}

main()
  .catch((err: Error): never => {
    console.error(err.stack);
    process.exit(1);
  });
