import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {RecordIoType, RecordType} from "kryo/record";

import {$TaskKind, TaskKind} from "./task-kind.mjs";

export interface CreateJob {
  kind: TaskKind;
  state: any;
}

export const $CreateJob: RecordIoType<CreateJob> = new RecordType<CreateJob>({
  properties: {
    kind: {type: $TaskKind},
    state: {type: $Any},
  },
  changeCase: CaseStyle.SnakeCase,
});
