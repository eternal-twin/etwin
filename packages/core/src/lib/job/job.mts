import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import {$Ucs2String} from "kryo/ucs2-string";

export interface Job {
  id: string;
}

export const $Job: RecordIoType<Job> = new RecordType<Job>({
  properties: {
    id: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
