import { Ucs2StringType } from "kryo/ucs2-string";

export type TaskKind = string;

export const $TaskKind: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 32,
  pattern: /^[A-Z][A-Za-z0-9]{0,31}$/,
});
