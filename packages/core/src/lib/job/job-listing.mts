import { RecordIoType } from "kryo/record";

import {$Listing, Listing} from "../core/listing.mjs";
import {$Job, Job} from "./job.mjs";

export type JobListing = Listing<Job>;

export const $JobListing = $Listing.apply($Job) as RecordIoType<JobListing>;
