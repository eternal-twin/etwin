import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {$Uint8} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

import {$PeriodLower, PeriodLower} from "../core/period-lower.mjs";

export interface GetOutboundEmails {
  /**
   * API time, for time-travel querying
   */
  time?: Date;
  /**
   * Filter emails created in the provided period
   */
  createdAt?: PeriodLower;
  /**
   * Maximum number of results to return
   */
  limit: number;
}

export const $GetOutboundEmails: RecordIoType<GetOutboundEmails> = new RecordType<GetOutboundEmails>({
  properties: {
    time: {type: $Date, optional: true},
    createdAt: {type: $PeriodLower, optional: true},
    limit: {type: $Uint8},
  },
  changeCase: CaseStyle.SnakeCase,
});
