import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type OutboundEmailId = UuidHex;

export const $OutboundEmailId: Ucs2StringType = $UuidHex;
