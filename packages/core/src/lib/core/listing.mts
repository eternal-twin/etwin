import {CaseStyle, IoType} from "kryo";
import {ArrayType} from "kryo/array";
import {GenericIoType, GenericType} from "kryo/generic";
import {$Uint16, $Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

export interface Listing<T> {
  offset: number;
  limit: number;
  count: number;
  items: readonly T[];
}

export const $Listing: GenericIoType<<T>(t: T) => Listing<T>> = new GenericType({
  apply: <T, >(t: IoType<T>): RecordIoType<Listing<T>> => new RecordType({
    properties: {
      offset: {type: $Uint32},
      limit: {type: $Uint16},
      count: {type: $Uint32},
      items: {type: new ArrayType({itemType: t, maxLength: 1000})},
    },
    changeCase: CaseStyle.SnakeCase,
  }),
});
