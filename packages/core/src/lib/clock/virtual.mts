import {ClockService} from "./service.mjs";

export class VirtualClock implements ClockService {
  readonly #startTime: Date;

  public constructor(startTime?: Date) {
    if (startTime === undefined) {
      startTime = new Date("2020-01-01T00:00:00.000Z");
    }
    this.#startTime = new Date(startTime.getTime());
  }

  public now(): Date {
    return new Date(this.#startTime);
  }

  public nowUnixS(): number {
    return Math.floor(this.now().getTime() / 1000);
  }

  public nowUnixMs(): number {
    return Math.floor(this.now().getTime());
  }
}
