import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$UserId, UserId} from "./user-id.mjs";

export interface EnableUser {
  idempotencyKey: UuidHex;
  userId: UserId;
}

export const $EnableUser: RecordIoType<EnableUser> = new RecordType<EnableUser>({
  properties: {
    idempotencyKey: {type: $UuidHex},
    userId: {type: $UserId},
  },
  changeCase: CaseStyle.SnakeCase,
});
