import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableDate, NullableDate} from "../core/nullable-date.mjs";
import {$CompleteUser, CompleteUser} from "./complete-user.mjs";

export interface EnableUserOut {
  deletedAt: NullableDate,
  current: CompleteUser,
  username: unknown,
  dinoparcCom: unknown,
  enDinoparcCom: unknown,
  hammerfestEs: unknown,
  hammerfestFr: unknown,
  hfestNet: unknown,
  spDinoparcCom: unknown,
  twinoid: unknown,
}

export const $EnableUserOut: RecordIoType<EnableUserOut> = new RecordType<EnableUserOut>({
  properties: {
    deletedAt: {type: $NullableDate},
    current: {type: $CompleteUser},
    username: {type: $Any},
    dinoparcCom: {type: $Any},
    enDinoparcCom: {type: $Any},
    hammerfestEs: {type: $Any},
    hammerfestFr: {type: $Any},
    hfestNet: {type: $Any},
    spDinoparcCom: {type: $Any},
    twinoid: {type: $Any},
  },
  changeCase: CaseStyle.SnakeCase,
});
