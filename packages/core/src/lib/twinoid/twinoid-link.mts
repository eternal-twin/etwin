import {CaseStyle, IoType} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$LatestTemporal, LatestTemporal} from "../temporal/latest-temporal.mjs";
import {$ShortTwinoidSite, ShortTwinoidSite} from "./short-twinoid-site.mjs";
import {
  $NullableTwinoidLinkUser,
  NullableTwinoidLinkUser,
} from "./twinoid-link-user.mjs";

export interface TwinoidLink {
  site: ShortTwinoidSite;
  user: LatestTemporal<NullableTwinoidLinkUser>;
}

export const $TwinoidLink: RecordIoType<TwinoidLink> = new RecordType<TwinoidLink>({
  properties: {
    site: {type: $ShortTwinoidSite},
    user: {type: $LatestTemporal.apply($NullableTwinoidLinkUser) as IoType<LatestTemporal<NullableTwinoidLinkUser>>},
  },
  changeCase: CaseStyle.SnakeCase,
});
