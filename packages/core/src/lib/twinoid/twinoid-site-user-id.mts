import {$Uint32,IntegerType} from "kryo/integer";

export type TwinoidSiteUserId = number;

export const $TwinoidSiteUserId: IntegerType = $Uint32;
