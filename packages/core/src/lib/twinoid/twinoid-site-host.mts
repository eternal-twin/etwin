import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";
import { Ucs2StringType } from "kryo/ucs2-string";

export type TwinoidSiteHost = string;

export const $TwinoidSiteHost: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 100,
  pattern: /[0-9a-z-]+(\.[0-9a-z-]+)+/,
});

export type NullableTwinoidSiteHost = null | TwinoidSiteHost;

export const $NullableTwinoidSiteHost: TryUnionType<NullableTwinoidSiteHost> = new TryUnionType({variants: [$Null, $TwinoidSiteHost]});
