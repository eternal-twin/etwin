import {$Uint32,IntegerType} from "kryo/integer";

export type TwinoidSiteId = number;

export const $TwinoidSiteId: IntegerType = $Uint32;
