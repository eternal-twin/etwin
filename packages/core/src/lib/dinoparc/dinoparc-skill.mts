import { LiteralUnionType } from "kryo/literal-union";
import { $Ucs2String } from "kryo/ucs2-string";

/**
 * A Dinoparc skill.
 */
export type DinoparcSkill =
  | "Bargain"
  | "Camouflage"
  | "Climb"
  | "Cook"
  | "Counterattack"
  | "Dexterity"
  | "Dig"
  | "EarthApprentice"
  | "FireApprentice"
  | "FireProtection"
  | "Intelligence"
  | "Juggle"
  | "Jump"
  | "Luck"
  | "MartialArts"
  | "Medicine"
  | "Mercenary"
  | "Music"
  | "Navigation"
  | "Perception"
  | "Provoke"
  | "Run"
  | "Saboteur"
  | "ShadowPower"
  | "Spy"
  | "Stamina"
  | "Steal"
  | "Strategy"
  | "Strength"
  | "Survival"
  | "Swim"
  | "ThunderApprentice"
  | "TotemThief"
  | "WaterApprentice";

export const $DinoparcSkill: LiteralUnionType<DinoparcSkill> = new LiteralUnionType({
  type: $Ucs2String,
  values: [
    "Bargain",
    "Camouflage",
    "Climb",
    "Cook",
    "Counterattack",
    "Dexterity",
    "Dig",
    "EarthApprentice",
    "FireApprentice",
    "FireProtection",
    "Intelligence",
    "Juggle",
    "Jump",
    "Luck",
    "MartialArts",
    "Medicine",
    "Mercenary",
    "Music",
    "Navigation",
    "Perception",
    "Provoke",
    "Run",
    "Saboteur",
    "ShadowPower",
    "Spy",
    "Stamina",
    "Steal",
    "Strategy",
    "Strength",
    "Survival",
    "Swim",
    "ThunderApprentice",
    "TotemThief",
    "WaterApprentice",
  ]
});
