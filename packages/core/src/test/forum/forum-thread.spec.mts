import { ObjectType } from "../../lib/core/object-type.mjs";
import { $ForumThread, ForumThread } from "../../lib/forum/forum-thread.mjs";
import { registerJsonIoTests } from "../helpers.mjs";

describe("ForumThread", function () {
  registerJsonIoTests<ForumThread>(
    $ForumThread,
    "core/forum/forum-thread",
    new Map([
      ["create-thread-result", {
        type: ObjectType.ForumThread,
        id: "c7606465-3e37-43d2-802b-eefbd1439995",
        key: null,
        title: "Hello!",
        ctime: new Date("2022-04-09T10:11:11.824Z"),
        section: {
          type: ObjectType.ForumSection,
          id: "4c30f41d-773a-45d5-9267-af92b4467ae7",
          key: "fr_main",
          displayName: "Forum Général (fr-FR)",
          ctime: new Date("2022-04-07T16:29:07.672Z"),
          locale: "fr-FR",
          threads: {
            count: 1,
          },
          self: {
            roles: [],
          }
        },
        posts: {
          offset: 0,
          limit: 10,
          count: 1,
          items: [
            {
              type: ObjectType.ForumPost,
              id: "8bc7192f-b357-4858-8c26-fd84cc1f1349",
              ctime: new Date("2022-04-09T10:11:11.824Z"),
              author: {
                type: ObjectType.UserForumActor,
                user: {
                  type: ObjectType.User,
                  id: "f19092d9-96d8-41a6-9f53-a6f648ec7e46",
                  displayName: {
                    current: {
                      value: "Elseabora",
                    },
                  },
                },
              },
              revisions: {
                count: 1,
                last: {
                  type: ObjectType.ForumPostRevision,
                  id: "c9bddf32-645d-4cdc-85c8-87553cec9b12",
                  time: new Date("2022-04-09T10:11:11.824Z"),
                  author: {
                    type: ObjectType.UserForumActor,
                    user: {
                      type: ObjectType.User,
                      id: "f19092d9-96d8-41a6-9f53-a6f648ec7e46",
                      displayName: {
                        current: {
                          value: "Elseabora",
                        },
                      },
                    },
                  },
                  content: {
                    marktwin: "This is the content of **the message**.\n",
                    html: "This is the content of <strong>the message</strong>.<br />\n",
                  },
                  moderation: null,
                  comment: null,
                },
              },
            },
          ],
        },
        isPinned: false,
        isLocked: false,
      }],
      ["get-thread-result", {
        type: ObjectType.ForumThread,
        id: "c7606465-3e37-43d2-802b-eefbd1439995",
        key: null,
        title: "Hello!",
        ctime: new Date("2022-04-09T10:11:11.824Z"),
        section: {
          type: ObjectType.ForumSection,
          id: "4c30f41d-773a-45d5-9267-af92b4467ae7",
          key: "fr_main",
          displayName: "Forum Général (fr-FR)",
          ctime: new Date("2022-04-07T16:29:07.672Z"),
          locale: "fr-FR",
          threads: {
            count: 1,
          },
          self: {
            roles: [],
          }
        },
        posts: {
          offset: 0,
          limit: 10,
          count: 1,
          items: [
            {
              type: ObjectType.ForumPost,
              id: "8bc7192f-b357-4858-8c26-fd84cc1f1349",
              ctime: new Date("2022-04-09T10:11:11.824Z"),
              author: {
                type: ObjectType.UserForumActor,
                user: {
                  type: ObjectType.User,
                  id: "f19092d9-96d8-41a6-9f53-a6f648ec7e46",
                  displayName: {
                    current: {
                      value: "Elseabora",
                    },
                  },
                },
              },
              revisions: {
                count: 1,
                last: {
                  type: ObjectType.ForumPostRevision,
                  id: "c9bddf32-645d-4cdc-85c8-87553cec9b12",
                  time: new Date("2022-04-09T10:11:11.824Z"),
                  author: {
                    type: ObjectType.UserForumActor,
                    user: {
                      type: ObjectType.User,
                      id: "f19092d9-96d8-41a6-9f53-a6f648ec7e46",
                      displayName: {
                        current: {
                          value: "Elseabora",
                        },
                      },
                    },
                  },
                  content: {
                    marktwin: "This is the content of **the message**.\n",
                    html: "This is the content of <strong>the message</strong>.<br />\n",
                  },
                  moderation: null,
                  comment: null,
                },
              },
            },
          ],
        },
        isPinned: false,
        isLocked: false,
      }],
    ])
  );
});
