import { ObjectType } from "../../lib/core/object-type.mjs";
import {$TwinoidUser} from "../../lib/twinoid/twinoid-user.mjs";
import { registerJsonIoTests } from "../helpers.mjs";

describe("EtwinTwinoidUser", function () {
  registerJsonIoTests(
    $TwinoidUser,
    "core/twinoid/etwin-twinoid-user",
    new Map([
      [
        "demurgos",
        {
          type: ObjectType.TwinoidUser as const,
          id: "38",
          archivedAt: new Date("2023-11-01T21:14:15.691Z"),
          displayName: "Demurgos",
          links: [
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 1,
                host: "twinoid.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 10,
                host: "muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 11,
                host: "fever.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 15,
                host: "kingdom.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 18,
                host: "kube.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 2,
                host: "www.dinorpg.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 1171056,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 20,
                host: "kube.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 21,
                host: "kingdom.es.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 22,
                host: "intrusion.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 23,
                host: "kube.es.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 24,
                host: "snake.es.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 25,
                host: "elbruto.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 26,
                host: "snake.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 27,
                host: "hotel.es.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 28,
                host: "majority.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 29,
                host: "fever.es.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 3,
                host: "en.dinorpg.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 30,
                host: "mybrute.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 31,
                host: "labrute.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 32,
                host: "fever.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 33,
                host: "snake.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 34,
                host: "hotel.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 35,
                host: "hotel.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 36,
                host: "mb2.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 38,
                host: "odyssey.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 40,
                host: "mush.en.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 41,
                host: "mush.vg",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 42,
                host: "quiz.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 43,
                host: "www.naturalchimie.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 43986,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 45,
                host: "es.dinorpg.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 49,
                host: "www.zombinoia.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 5,
                host: "www.dinorpg.de",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 50,
                host: "snake.de.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 51,
                host: "fever.de.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 52,
                host: "kube.de.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 53,
                host: "meinbrutalo.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 54,
                host: "kingdom.de.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 57,
                host: "www.alphabounce.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 1146675,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 6,
                host: "www.hordes.fr",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 533844,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 62,
                host: "hotel.de.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 64,
                host: "www.kadokado.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 3059855,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 7,
                host: "www.die2nite.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 71,
                host: "teacher-story.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 75,
                host: "arkadeo.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 76,
                host: "mush.twinoid.es",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 77,
                host: "mush.twinoid.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 8,
                host: "www.dieverdammten.de",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 82,
                host: "street-writer.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 83,
                host: "cash.motion-twin.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 87,
                host: "uppercup-football.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: null,
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 9,
                host: "kingdom.muxxu.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 77836,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 92,
                host: "rockfaller.com",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
            {
              site: {
                type: ObjectType.TwinoidSite as const,
                id: 94,
                host: "monster-hotel.net",
              },
              user: {
                latest: {
                  period: {
                    start: new Date("2023-11-01T21:14:27.956Z"),
                    end: null,
                  },
                  retrieved: {
                    latest: new Date("2023-11-01T21:14:27.956Z"),
                  },
                  value: {
                    type: ObjectType.TwinoidSiteUser as const,
                    id: 38,
                  },
                },
              },
            },
          ],
          etwin: {
            current: {
              link: {
                time: new Date("2023-11-01T21:14:15.710Z"),
                user: {
                  type: ObjectType.User as const,
                  id: "ab23b432-35a6-44a8-bb00-fc8196581132",
                  displayName: {
                    current: {
                      value: "Demurgos",
                    },
                  },
                },
              },
              unlink: null,
              user: {
                type: ObjectType.User as const,
                id: "ab23b432-35a6-44a8-bb00-fc8196581132",
                displayName: {
                  current: {
                    value: "Demurgos",
                  },
                },
              },
            },
            old: [],
          }
        }
      ],
    ])
  );
});
