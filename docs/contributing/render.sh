#!/usr/bin/env ysh
var SCRIPT_DIR = $(cd "$_this_dir" { pwd })

cd $SCRIPT_DIR {
  # `dot` binary from `Graphviz`
  dot -Tsvg -o dependencies.svg dependencies.dot
}
