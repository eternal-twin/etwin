# Publish

This article documents how to publish a release.

## Create a release

Use `cargo xtask <version>` to create a new release. This will update all the manifest files: Rust `Cargo.toml`, Node
`package.json`, PHP `composer.json`, etc. It will also create a Git commit and tag.

## Publish the main packages

The main packages are the packages needed to build the main executable in `./bin`.

```
cargo xtask publish main
```
