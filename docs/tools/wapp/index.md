[Home](../index.md) | [Tools](./index.md)

# Wapp

[Official website](https://bitnami.com/stack/wapp)

**Wapp** is an all-in-one installer for Windows development using Apache,
PostgreSQL and PHP. It is similar to the well-known Wamp tool, except using
PostgreSQL as the database.

**Wapp is the recommended solution for PHP projects on Windows.**

## Installation

ℹ Wapp only supports Windows systems.

During the installation there will be two important steps: installation
directory selection and password selection for the Postgres admin user.

1. Download [the latest Windows installer](https://bitnami.com/stack/wapp/installer).
   You don't have to create any account, pick "No thanks, just take me to the download"
   if a pop-up appears. As of 2022-05-24, the latest version is 8.1.6-0.
2. Execute the installer
   ![Installer - Welcome](./installer-00.png)
3. **Disable all the components**: they should not be installed by Wapp.
   Frameworks and libraries will be installed later if needed, by the _Composer package manager_.
   ![Installer - Select Components](./installer-01.png)
4. Select the **Wapp installation directory**. This guide will use the default, for this version it is `C:\Bitnami\wappstack-8.1.6-0`.
   ![Installer - Installaion folder](./installer-02.png)
5. Configure the **Postgres admin password**. `postgres` is the database management account.
   If you pick a complex password **make sure to remember it** (e.g. using a password manager).
   For dev environments, I like to use a weak password like `dev` or `root`: the DB won't contain anything important anyway.
   ![Installer - PostgreSQL postgres user password](./installer-03.png)
6. Disable the cloud stuff with Bitnami, you won't need it.
   ![Installer - Deploy wappstack to the Cloud in One Click](./installer-04.png)
7. If you're happy with the picked installation directory and Postgres password, start the installation.
   ![Installer - Ready to Install](./installer-05.png)
8. The installation may take a few minutes.
   ![Installer - Installing](./installer-06.png)
9. The server needs to be allowed by the firewall. You may allow access to private networks, avoid
   granting access to public networks.
   ![Installer - Firewall](./installer-07.png)
10. At the end of the installation, start the Wapp to check if it works.
    ![Installer - Firewall](./installer-08.png)
11. Pick **Yes** when Windows asks for the permission to run Wapp. Running servers
    depends on elevated permissions.
    ![Installer - Firewall](./installer-09.png)

# Usage

You should see the following screen when starting Wapp.

![Wapp - Home](./home.png)

Wapp automatically starts the Apache server, accessible at the address
<http://localhost/>, and PostgreSQL.



# Next steps

- Configure a database for your project.
- Enable your PHP project in Wapp, using a virtual host.
