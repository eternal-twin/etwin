# 2021-11: Este mes en Eternaltwin #2

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Renombramos el grupo de Gitlab y los paquetes PHP

Como anunciamos en la edición previa de “Este mes en Eternaltwin”, estamos en
proceso de corregir la forma de escribir el nombre del proyecto para que sea
“Eternaltwin”, una sola palabra sin guion. Como parte de este proceso, hicimos
2 cambios que pueden impactar a los desarrolladores.

El grupo de Gitlab con todo nuestro proyecto ha sido renombrado. La nueva URL es
<https://gitlab.com/eternaltwin/>.

Nuestros paquetes PHP compartidos han sido renombrados y actualizados. Los
nuevos nombres son [eternaltwin/oauth-client](https://packagist.org/packages/eternaltwin/oauth-client)
y [eternaltwin/etwin](https://packagist.org/packages/eternaltwin/etwin).

# Eternaltwin

El trabajo de mover el código del foro a Rust continúa. La única funcionalidad
faltante es el soporte para editar publicaciones.

Un nuevo contribuidor se nos unió: Nayr ayuda a arreglar algunos problemas
viejos de las traducciones. En particular, ayudó a remover las traducciones a
“esperanto” porque estaban desactualizadas.

# Eternal Kingdom

![Eternal Kingdom - Alpha 2](./kingdom.gif)

¡La segunda alfa de Eternal Kingdom ya está aquí!
Puedes leer todos los detalles [aquí](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-2).
El proyecto sigue en sus primeras etapas: no hay cuentas de usuarios ni límite de turnos.
Puedes probar la versión actual. [Puedes probar la versión actual.](https://kingdom.eternaltwin.org/).
Puedes editar la URL en las páginas de administración y ciudad para cambiar el
id (entre 43 y 63).

Nota para quienes no hablan francés: las traducciones a inglés y español serán parte de la próxima versión.

# Neoparc

[Juega Neoparc, la nueva versión de Dinoparc](https://neoparc.eternaltwin.org/)

Encontrarás las funcionalidades del antiguo Dinoparc (Dinoz, Torneos, Isla de
Jazz, Centro de Fusión, Poción Oscura, Arena Ermitaña, Mercado, etc.), pero
también nuevas funcionalidades y eventos que serán añadidos gradualmente. El
juego ha sido mejorado en general por lo que la jugabilidad es más disfrutable.

Solo falta implementar las “Guerras de Clanes” y estará todo el contenido
original (¡mejorado!). ¡Luego, funcionalidades realmente nuevas de la comunidad
Neoparciana irán aterrizando de a una, en algún momento en 2022!

Acabamos de terminar el evento de Halloween 2021. Fue una cacería de ladrones
por todo Dinoland. ¡Más de 50 jugadores participaron en esta competencia!

Empezando el 1 de diciembre, un **evento especial de Navidad** ocurrirá en
Dinoland. ¡Habrá muchos premios e incluso algunos cambios permanentes para
descubrir durante el evento!

La comunidad es muy activa en [el servidor de Discord](https://discord.gg/ERc3svy)
y de veras amamos cuando jugadores frescos (que ni siquiera han jugado el juego
original) se nos unen. ¡Únetenos! ¡Tu primer Dinoz te espera, sediento de
aventura!

Un punto importante: ¡tus Dinoz viejos no han sido perdidos! Una vez hayas
ingresado a Neoparc (luego de registrarte en Eternaltwin), puedes importar todos
tus Dinoz de “en.dinoparc.com” desde la pestaña “Mi Cuenta”, no cambiarán en
absoluto (raza, apariencia, nivel, habilidades).

Amistosamente, ¡la comunidad de Neoparc!

# eMush

![eMush - v0.4.1](./emush.png)

Con el estreno de la versión 0.4.1, el equipo de eMush se enorgullece en
anunciar el lanzamiento de su quinta nave espacial como una prueba alfa
cerrada. Pasó mucho tiempo desde la última nave y muchos cambios y
actualizaciones aterrizaron.

Entre los cambios principales, descubrirás el nuevo prototipo de interfaz. La
enfermería y laboratorio ahora tienen una interfaz para dejarte interactuar con
los ítems, el estante, las puertas, y otros jugadores, así como moverte dentro
de la habitación. Esto es un esfuerzo continuo y aún necesitamos un nuevo
desarrollador para trabajar en la parte de Phaser o alguien para crear el
entorno en Tiled.

Siguiendo con la interfaz, se han agregado tooltips a los botones e íconos para
dejarte saber lo que estás a punto de hacer. También implementamos enfermedades.
De momento solo se agregó la intoxicación alimentaria, cuidado con lo que comes.

Acciones secretas y furtivas, así como cámaras de seguridad, fortalecen el
núcleo del juego del Mush. Ahora es posible jugar como la contaminación usando
un estilo sigiloso.

La forma en la que las variables y los porcentajes funcionan fue revisada.
Permite que se completen efectos de herramientas, algunos efectos de estado o
enfermedades, así como permitir la suma de proyectos, investigación y
habilidades.

Algunas acciones fueron añadidas.

PS: para los testers anteriores, no se preocupen. Las chances de un incendio han
sido reducidas.


# MyHordes

_estática de radio_

¡Hola amigos y gracias por usar nuestro sistema radiofónico inalámbrico! Soy
Connor y hoy resumiré la situación de MyHordes. (última actualización del
juego: 1.0-beta10, 2021-11-08)

![MyHordes - Wireless technology ad](./myhordes.png)

**- ¿Qué recursos tenemos disponibles?**

Tenemos información sobre la lógica del juego y la mayoría de los recursos
originales obtenidos de varias wikis cubriendo las múltiples versiones de Hordes
y mucha información de juego recolectada por nosotros y la comunidad. No tenemos
el código fuente original del juego.

**- ¿Podemos jugar el juego?**

Sí. Pueden encontrar una versión de desarrollo actual en <https://myhordes.eu/>.
El juego actualmente se encuentra en la fase de beta abierta.

Es jugable en su totalidad y la mayoría de funcionalidades de Hordes y sus
variantes ya funcionan, pero algo de contenido y funciones de los juegos
originales aún faltan o pueden ser ligeramente diferentes.

**- ¿En qué se está trabajando actualmente?**

Estamos trabajando en múltiples funcionalidades: refactorizando la organización
de los foros, retocando el desempeño y terminando la integración de MyHordes a
Etwin.

**- ¿Qué queda por hacer?**

Debemos arreglar todos los bugs restantes, implementar cosas que faltan y pulir
el diseño (en particular la parte responsiva). También queda un montón de
trabajo de traducciones por hacer (el juego está siendo desarrollado en alemán
y traducido al francés, inglés y español. La traducción francesa es la que está
más completa), todos pueden contribuir a la recolección de elementos faltantes y
las traducciones en Gitlab o los hilos de los Foros Globales dentro del juego.

Nuestro plan para el futuro cercano es terminar de implementar todas las
funcionalidades faltantes hasta tener todas las de los juegos originales,
arreglar todos los bugs (o al menos el 99%) y retocar el juego lo mejor posible
para hacerlo fiel al original (por ejemplo: drop rates de items).

# CroqueMotel

Incluso si no hay alfa todavía, la recreación de CroqueMotel va bien.

Establecimos una lista inicial de las diferentes funcionalidades, y desde ahí
el desarrollo pudo empezar. La wiki está bastante completa y la versión
desofuscada del juego nos deja recolectar un montón de detalles de cómo el mismo
funciona.

Ahora mismo la arquitectura principal del proyecto está ahí (base de datos,
servidor web) y estamos trabajando en reproducir el hotel.

El siguiente paso es el soporte para las primeras interacciones para construir
el hotel, y luego todo el manejo de clientes.

# Palabras finales

¡Gracias a todos los contribuidores! Pueden enviar [sus mensajes para
diciembre.](https://gitlab.com/eternaltwin/etwin/-/issues/35).
