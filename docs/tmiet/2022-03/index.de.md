# 2022-03: Diesen Monat in Eternaltwin #6

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele
und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Bibni hat [die Spieleliste](https://eternaltwin.org/games) aktualisiert und
[eine Liste aller Nachrichtenartikel](https://eternaltwin.org/docs/tmiet/) hinzugefügt.

# Neoparc

Hallo Dinoz-Meister von [Neoparc](https://neoparc.eternaltwin.org/)!

Wir haben alle unsere Daten in eine neue Datenbank migriert. Dies wird die
meisten unserer jüngsten Probleme lösen, und ihr solltet bereits eine
verbesserte Leistung spüren.

Wöchentliche Schlachtzüge werden nun eine einzige 30-minütige Party sein!
Die Fertigkeit "Flammenbad" wurde neu kalibriert und die Punktezählung in den
Spielerprofilen wurde korrigiert.

Wir arbeiten jetzt an Clans und Kriegen.

![Neoparc](./neoparc.png)

# Eternal Kingdom

Während der letzten Monate haben wir die 0.3.2 Alpha für [Eternal Kingdom](https://kingdom.eternaltwin.org/).
veröffentlicht. Diese Version behebt hauptsächlich verschiedene Fehler und
bringt uns die Möglichkeit, unseren Lord zu töten, indem man "/death" benutzt.
Die vollständigen Patch Notes sind auf Discord.

Die 4. Alpha ist jetzt in Arbeit. Weitere Einzelheiten zu den geplanten
Änderungen findest du auf dem [GitLab Wiki - Alpha 4 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4).
Wir haben die Überarbeitung des Ereignisprotokollsystems (für die
Seiten 'Stadt', 'Welt' und 'Verwaltung') abgeschlossen.

Der nächste große Schritt ist die Implementierung der Weltkarte. Der am meisten
erwartete Teil des Spiels 💃.

Wir sehen uns bald 😸!

# Eternal DinoRPG

¡[EternalDinoRPG](https://dinorpg.eternaltwin.org/) schreitet weiter voran! Wir
haben eine neue Vorproduktionsversion veröffentlicht. Dort könnt ihr Dinoz
kaufen, reisen, den fliegenden Laden benutzen, die Bestenliste und
Spielerprofile ansehen!

Nach dem Upload der Version 0.2.0 wurden viele kleine Bugs gemeldet und sie
sind bereits behoben. Wir sind dabei, unsere Admin-Seite zu vervollständigen,
und wir sollten die neue Version 0.2.1 bald hochladen. Sie wird es uns
ermöglichen, euch Gold, epische Belohnungen oder sogar Status auf eurem Dinoz
zu geben, damit ihr die heutige Welt entdecken könnt.

Die größte Neuigkeit in diesem Monat ist die Möglichkeit, ein Dinoz ohne Flash
anzuzeigen. Dies ist vorerst nur ein Proof-of-Concept, und wir werden es
verbessern. Es ermöglicht bereits ein vollständiges Moueffe ohne Flash anzeigen.
Sobald es fertig ist und funktioniert, werden alle Dinoz-Rassen folgen!

Was das Importsystem betrifft, so befindet es sich in der Endphase, und wir
warten nur noch auf einige Eternaltwin-Updates.

# MyHordes

_Radioknistern_ Hallo zusammen, hier ist _rauschen_ Ben, wie immer live über das
wunderbare drahtlose Funksystem in euren Hütten. Es gibt viele Dinge zu
berichten, also fangen wir gleich damit an! _rauschen_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Die untoten Horden scheinen entgültig jede Spur von Anstand verloren zu haben.
Berichten zufolge warten sie nicht einmal mehr, bis kleine Städte eine
Einwohnerzahl von 40 tapferen Bürgern erreicht haben, bevor sie mit ihren
bösartigen nächtlichen Angriffen beginnen! Glücklicherweise scheint ein
mysteriöser Fremder unklarer Herkunft diesen armen Leuten zu helfen _rauschen_
und ich muss mir immer noch selbst die Hosen anziehen!

Wo wir gerade von allgemeiner Seltsamkeit sprechen, Connor scheint mich zu
meiden, seit ich mein Koboldkostüm benutzt habe, um seinen
Lieblings-Järpen-Tisch zu stehlen. *rauschen* gesehen, wie er hinter der Scheune
ein seltsames kreuzförmiges Gebilde aus Schokolade gebaut hat. Ich bin mir nicht
sicher, was es damit auf sich hat, aber ich habe das Gefühl, dass ich den
Aufnahmeraum zwischen dem 13. und 21. April nicht verlassen sollte...

In Ordnung, ich denke, das sollte das meiste der neuesten Geschehnisse in
unserer schönen zombieverseuchten Wüste abdecken. _rauschen_ Hoffentlich werden
wir alle die Nacht überleben und _grrrrrrrrz_

# Abschließende Worte

Du kannst deine >Nachrichten für April< senden.

Dieser Artikel wurde bearbeitet von: Bibni, Biosha, Brainbox, Demurgos, Jonathan,
MisterSimple, Patate404 und Schroedinger.
