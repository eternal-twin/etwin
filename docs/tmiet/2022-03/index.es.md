# 2022-03: Este mes en Eternaltwin #6

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Eternaltwin

Bibni actualizó la [lista de juegos](https://eternaltwin.org/games) y agregó una
[lista con todos los artículos de noticias](https://eternaltwin.org/docs/tmiet/).

# Neoparc

¡Hola maestros de Dinoz de [Neoparc](https://neoparc.eternaltwin.org/)!

Migramos todos nuestros datos a una nueva base de datos. Esto resolverá la
mayoría de nuestros problemas recientes, y ya deberían sentir que el desempeño
ha mejorado.

¡Las Incursiones Semanales ahora serán una sola fiesta de 30 minutos! La
habilidad Ducha de Flamas ha sido ligeramente recalibrada y los recuentos de
puntos en perfiles de jugadores han sido arreglados.

Ahora estamos trabajando en clanes y guerras.

![Neoparc](./neoparc.png)

# Eternal Kingdom

Durante los últimos pocos meses, liberamos la Alfa 0.3.2 para
[Eternal Kingdom](https://kingdom.eternaltwin.org/).
Esta versión principalmente arreglaba varios errores y traía la opción de matar
a nuestro Señor al usar “/death”. Las notas del parche completas están en Discord.

La 4ta Alfa está ahora en progreso. Pueden encontrar más detalles acerca de lo
que está planeado en la wiki de  [GitLab - Roadmap de la Alfa 4.](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4).
Terminamos de retrabajar el Sistema de Registro de Eventos (para las páginas de
Capital, Mapa & Gestión).

El siguiente gran paso es implementar el Mapa del Mundo. La parte más anticipada
del juego💃

¡Nos vemos pronto 😸!

# Eternal DinoRPG

¡[EternalDinoRPG](https://dinorpg.eternaltwin.org/) sigue avanzando! Lanzamos
una nueva versión de preproducción. ¡Allí, pueden comprar algunos dinoz, viajar,
usar la tienda voladora, revisar la tabla de clasificaciones, y ver perfiles de
usuarios!

Siguiendo la subida de la versión 0.2.0, muchos errores pequeños fueron
reportados y ya están arreglados. Estamos completando nuestra página de admin,
y deberíamos subir la nueva versión 0.2.1 pronto. Nos dejará darles oro,
recompensas épicas o incluso estados en sus dinoz, para que puedan descubrir el
mundo de hoy.

La mayor noticia para este mes es la habilidad de mostrar un dinoz sin Flash.
Esto es solamente una prueba de concepto por ahora, y lo mejoraremos. Ya nos
deja mostrar un Moueffe completo sin Flash. Una vez esté listo y funcione,
¡todas las razas le seguirán!

Con respecto al sistema de importación, está en sus etapas finales, y solo
estamos esperando a algunas actualizaciones de Eternaltwin.

# MyHordes

Reproducir a [MyHordes](https://myhordes.eu/).

`*estática de radio*` Hola a todos, `*cortado*` Ben aquí, una vez más llegando
en vivo a sus chozas vía el maravilloso sistema radiofónico inalámbrico.
`*cortado*` Hay muchas cosas para reportar, ¡así que vayamos directo a
ello! `*cortado*`

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Primero que nada, parece que las hordas muerto vivientes han perdido todo rastro
de modales. Según se informa, ¡ya no `*cortado*` esperan a que los pueblos
amateur lleguen a una población de 40 valientes habitantes antes de empezar su
vicioso ataque nocturno! Afortunadamente, un Extraño Misterioso de origen ambiguo
parece estar ayudando a estos pobres muchachos `*cortado*` … ¡y aquí estoy yo,
todavía teniendo que ponerme los pantalones yo mismo!

Hablando de extrañeza general, Connor parece estar evitándome desde que usé mi
traje de duendecillo para robar su mesa Järpen favorita. `*cortado*` visto
construyendo un extraño artilugio con forma de cruz hecho de chocolate detrás
del granero. No estoy seguro de qué se trata todo esto, pero siento que no
debería salir de la sala de grabación entre el 13 y el 21 de abril…

De acuerdo, creo que esto debería cubrir la mayoría de los sucesos más
recientes en nuestro hermoso desierto infestado de zombies. `*cortado*` Con
algo de suerte, todos sobreviviremos la noche y  `*grrrrrrrrz*`

# Palabras finales

Pueden mandar  [sus mensajes para abril.](https://gitlab.com/eternaltwin/etwin/-/issues/42).

Este artículo fue editado por: Bibni, Biosha, Brainbox, Demurgos, Jonathan, MisterSimple,
Patate404 y Schroedinger.
