# 2023-04: Dieser Monat in Eternaltwin #19

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Eternaltwin braucht Entwickler! Alle Spiele schreiten in ihrem eigenen Rhythmus voran, aber die Haupt-Website, die alles vereinheitlichen soll, hat eine schwere Zeit. Wenn du interessiert bist, kontaktiere Patate404#8620, Demurgos#8218 oder Biosha#2584 auf Discord.

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

Hallo zusammen!

Wir danken euch für die Einsendungen zum MyHordes-Kunstwettbewerb. Allerdings haben wir festgestellt, dass wir nicht viele Einsendungen haben, was vielleicht an der recht kurzen Frist liegt, weshalb wir beschlossen haben, euch ein wenig mehr Zeit zu geben. Der Wettbewerb wurde bis zum 14.05.2023 erweitert. Alle Informationen findest du [hier](https://myhordes.eu/jx/forum/9/68041).

# eMush

Willkommen in diesem Bereich, der [🍄 eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

Keine Neuigkeiten diesen Monat, das Team macht eine Pause 🏖️

Bis zum nächsten Update. Dort werden die Jägern eingeführt 👀

🎮 Spiele [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk)

Danke fürs Lesen und bis bald auf der Daedalus!

# Eternal DinoRPG

Willkommen in diesem Bereich, der [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) gewidmet ist.

Seid gegrüßt, Dinoz-Meister!

Auch diesen Monat gibt es keine News, das Team arbeitet hart an der Überarbeitung des Codes.

Das Team ist immer noch auf der Suche nach einem neuen Namen für das Spiel. Haltet euch nicht zurück!

Wir sehen uns bald wieder. Das EternalDino-Team.

# Alphabounce, Frutiparc und KadoKado

Blue112 hat mehrere dieser Minispiele auf seiner Website neu implementiert. Im Moment wird noch kein Spielstand gespeichert und es gibt keine Schnittstelle, die alles miteinander verbindet, aber die Spiele sind spielbar!

Sie befinden sich auf seiner [persönlichen Website](https://tmp.blue112.eu/games/) und sollten bald in Eternaltwin integriert werden.

# Abschließende Worte

Du kannst deine [Nachrichten für Mai](https://gitlab.com/eternaltwin/etwin/-/issues/61) senden.

Dieser Artikel wurde bearbeitet von: Biosha, Demurgos, Dylan57, Evian, Fer, Patate404 und Schroedinger.
