# 2023-04 : Ce mois-ci sur Eternaltwin n°19

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

Eternaltwin needs developers! All games progress at their own rythm but the main websiteThe different games are progressing at their own pace, but the main site used to unify everything is having a hard time. If you are interested, contact on Discord Patate404#8620, Demurgos#8218 or Biosha#2584.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

Hello everybody !

We thank you for the submissions for the MyHordes art contest, however we noticed that we don't have many submissions, maybe due to a rather short deadline, for this reason we decided to give you a little more time. The new end date is May 14, 2023, [all the information is available on this subject](https://myhordes.eu/jx/forum/9/68041). To your pencils!

# eMush

Welcome to this section dedicated to [🍄 eMush](https://emush.eternaltwin.org/),
the project to save Mush.

No gazette this month, the team is taking a break 🏖

See you soon for the update introducing the hunters 👀

🎮 Play [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk)

Thanks for reading, and see you soon on the Daedalus!

# Eternal DinoRPG

Hello everyone and thank you for your attention for this part dedicated to [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Greetings Masters Dinoz!

No gazette either this month, the team is working hard on the code refactoring.

The team is still looking for a new name for the game. Don't hold back!

See you soon. The EternalDino team.

# Alphabounce, Frutiparc et KadoKado

Blue112 has reimplemented several of these mini games on its site. For the moment no score is saved and there is no interface linking everything, but the games are playable!

It's on his [personal site](https://tmp.blue112.eu/games/) and should be integrated to Eternaltwin soon.

# Closing words

You can send [your messages for Mai](https://gitlab.com/eternaltwin/etwin/-/issues/61).

This article was edited by: Biosha, Demurgos, Dylan57, Evian, Fer, Patate404 et Schroedinger.
