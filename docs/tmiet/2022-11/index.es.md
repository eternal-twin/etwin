# 2022-11: Este Mes En Eternaltwin #14

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

La [aplicación de Eternaltwin](https://eternaltwin.org/docs/desktop) te permite
jugar fácilmente a los juegos Flash de Motion Twin. La actualización 0.6.4 fue
lanzada para arreglar el acceso a Minitroopers.

# eMush

Bienvenidos a esta sección dedicada a [eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

## ¿Qué hay de nuevo?

La última vez describimos las tres tareas principales para lanzar eMush versión 0.5.0
como una alfa abierta, la actualización **Spores for All!**:

- 🗃️ Archivo de la nave espacial, para tener un registro de tus logros
- 🇬🇧 Traducciones al inglés
- 💾 Migraciones de base de datos, para desplegar nuevas versiones sin tener que reiniciar las naves espaciales existentes

Estamos avanzando lento pero seguro.

eMush ahora tiene un sistema de archivo simple para guardar parte de la información sobre tu
nave espacial una vez que se completa una partida.

Además... ¡ahora puedes jugar en inglés!

![eMush](./emush.png)

## Lo que se viene

La actualización **Spores for All!** nos mantendrá ocupados por unos meses. Tenemos que
trabajar la estructura de nuestra base de datos para almacenar toda la información que
necesitamos para proveerles tablas de clasificación épicas 👀

La migración de base de datos para evitar borrar el progreso de los jugadores
también es un punto importante. Deberías poder ostentar tus logros con los demás
hasta el fin de los tiempos 💪

Una vez que estos problemas estén solucionados, lanzaremos la alfa abierta.

## ¡Genial! ¿Cuándo?

Seguiremos actualizándolos a través de EMEET, pero la mejor manera de seguir el progreso
es unirse al [servidor de Discord de Eternaltwin](https://discord.gg/SMJavjs3gk).

eMush es un proyecto de Código Abierto. También puedes seguir nuestro progreso en GitLab.
En particular, el progreso de [la actualización **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

¡Muchas gracias por su atención, los veré luego a bordo del Daedalus! ❤️

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_ Hola a todos. este es Ben, brindándoles los últimos acontecimientos de todo el desierto.

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Lo siento mucho por _interferencia_ problemas de recepción que puedan tener.
_interferencia_ enjambre de cuervos sentándose en la antena _interferencia_.
Parece ser que han crecido en números... He enviado a Connor para espantarlos,
pero todavía no ha regresado. _interferencia_ tal vez envió al Interno a mirar
por él.

Buenas noticias para esos de ustedes que utilizan estas novedosas unidades
receptoras portables. Hemos hecho numerosas mejoras para _interferencia_ más
disfrutable y fácil de usar.

Por último pero no menos importante, gracias al nuevo Statisto-Tron 40K que
nuestros recolectores trajeron, podemos transmitirles toda la información en
vivo acerca de cuántos oyentes tenemos. No se preocupen, _interferencia_ sin
problemas de privacidad, ¡definitivamente no recolectamos información privada
de nadie! Hasta la próxima... y estamos **fuera**. _silencio_ _interferencia_
vender todo a los patrocinadores _interferencia_ finalmente escapar de este
basurero _interferencia_ **¿TODAVÍA ESTAMOS AL AIRE?** _pitido_ _interferencia_

# Palabras finales

Pueden enviarnos [sus mensajes de diciembre](https://gitlab.com/eternaltwin/etwin/-/issues/56).

Este artículo ha sido editado por: Brainbox, Demurgos, Evian, Fer, Patate404 y Schroedinger.
