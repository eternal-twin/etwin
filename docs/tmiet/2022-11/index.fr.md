# 2022-11 : Ce mois-ci sur Eternaltwin n°14

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

[L'application Eternaltwin](https://eternaltwin.org/docs/desktop) permet de
jouer facilement aux jeux Flash de Motion Twin. La mise à jour 0.6.4 a été
publiée pour réparer l'accès à Minitroopers.

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

La dernière fois, nous vous avions parlé des trois grands travaux nécessaires
pour lancer la version 0.5 d'eMush en alpha ouverte, la mise à jour **Spores for All!** :

- 🗃️ l'archivage des vaisseaux terminés, afin de garder les traces de vos exploits
- 🇬🇧 la traduction du jeu en anglais
- 💾 les migrations de notre base de données, afin de déployer de nouvelles
  fonctionnalités sans détruire les vaisseaux en cours

Nous avançons dessus doucement mais sûrement.

eMush a désormais un système d'archivage rudimentaire qui enregistre une partie des informations des parties terminées.

De plus, le jeu est désormais totalement jouable en anglais !

![eMush](./emush.png)

## Prochainement

La mise à jour **Spores for All!** va nous occuper pendant quelques mois. Nous
devons notamment retravailler la structure de notre base de données pour archiver
toutes les informations nécessaires en fin de partie afin de vous afficher des
classements épiques 👀

Les migrations de la base de données pour éviter d'effacer vos avancées sont
également un point de blocage important. Nous voulons vous permettre de flex
jusqu'à la fin des temps 💪

Mais une fois ces problèmes résolus, ce sera le lancement d'eMush en alpha ouverte.

## Super ! Quand ça ?

Nous continuerons à vous communiquer nos avancées à travers cette gazette,
mais le meilleur moyen d'être informé reste de rejoindre [le discord d'Eternaltwin](https://discord.gg/SMJavjs3gk).

eMush étant un projet open-source, nos avancées sont également disponibles sur GitLab,
en particulier [l'avancement de la mise à jour **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Merci de nous avoir lu, et à bientôt sur le Daedalus ! ❤️

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour à vous tous, ici Ben pour vos infos du désert !

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Je suis désolé pour les _brouillé_ problèmes de réception. _brouillé_ nuée de
corbeaux sur l'antenne _brouillé_. Ils sont de plus en plus nombreux... J'ai
envoyé Connor les chasser, mais il n'est toujours pas revenu. _brouillé_ vais
envoyer le stagiaire.

Bonne nouvelle pour ceux d'entre vous qui utilisent ces nouveaux appareils
de réception portables. Nous avons apporté diverses améliorations afin de
_brouillé_ l'expérience plus agréable.

Finalement, grâce au Statisto-Tron 40K rammené du désert, nous pouvons désormais
vous transmettre en direct notre nombre d'auditeurs. Pas d'inquiétude, _brouillé_
aucune donnée privée n'est collectée. À la prochaine ! **Coupé** _silence_
_brouillé_ pouvoir tout vendre aux publicitaires _brouillé_ enfin se tirer de
ce trou _brouillé_. **ON EST TOUJOURS EN DIRECT ?!** _bip_ _brouillé_

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de décembre](https://gitlab.com/eternaltwin/etwin/-/issues/56).

Cet article a été édité par : Brainbox, Demurgos, Evian, Fer, Patate404 et Schroedinger.
