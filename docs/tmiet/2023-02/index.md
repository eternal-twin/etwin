# 2023-02: This Month In Eternaltwin #17

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

Eternaltwin needs developers! All games progress at their own rythm but the main websiteThe different games are progressing at their own pace, but the main site used to unify everything is having a hard time. If you are interested, contact on Discord Patate404#8620, Demurgos#8218 or Biosha#2584.

# eMush

Welcome to this section dedicated to [🍄 eMush](https://emush.eternaltwin.org/),
the project to save Mush.

![eMush - Spores for All](./emush.png)

With its **Spores for All Update!**, eMush has entered **open alpha**! 🎉
👉 [🍄 eMush](https://emush.eternaltwin.org/) 👈

The main new features of the "Spores for All Update!" are:

🇬🇧 **English language support**: NERON can now insult you in the language of Shaskespeare! 🧐

🗃 **Ranking and endgame pages**! Now you can tell the world how you died (pathetically) trying to save the human race. 🏅

💾 **Administration panel** and database migrations: we can now add features much more easily, and without removing your exploits! 😭 🎉

This is a major milestone for eMush. We're very happy with how far we've come, and we hope this will allow us to bring you more frequent changes in the future.
Keep in mind that this is still an incomplete alpha, and that your Daedalus is not immune to black holes that appear without warning between two lines of code...
That said: have fun and see you soon for research and projects!

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

Hello everyone, I hope everything's fine for you during this cold and windy season _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

Season 15 of MyHordes has just been deployed!

As agreed, this season is only a transition season, which will contain a lot of identical stuff to the Hordes gameplay data, shared by Skool.

But not only! We have also made various changes and improvements.
I invite you to connect to the game to discover the complete list of changes

**And don't forget, the death is nothing in MyHordes.**

# Eternal DinoRPG

Welcome to this section dedicated to [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Greetings Dinoz Masters!
After version 0.5.0, part of the team shifted its focus towards importing original dinorpg accounts. We also don't want anyone's dinoz to vanish! The team hopes to share good progress on that part next month.

The other part of the team worked on game related content and released the 0.5.1:

- more balanced monsters, that will be slightly less mean to your dinoz
- The Old Joe gathered the rumors of Dinoland and can tell you how to help
- Finally, the dinoz skills that affects maximum life and elements should now be taken into account.

Also, someone is telling me last minute in the ear that the game is looking for a new name. Don't hold back!
Cheers! The EternalDino team.

# DinoCard

Dinocard is back! Here is a sneak peak.
![Dinocard - Sneak peak](./dinocard.png)

We started to work on bringing the game to life!

The website is not yet available but we need you to [choose the name of the future version of the Game](https://forms.gle/eZrtjfrcfUVgfAxu9).

More news in March 😄

# Closing words

You can send [your messages for March](https://gitlab.com/eternaltwin/etwin/-/issues/59).

This article was edited by: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Jahaa, Patate404, Schroedinger et Zenoo.
