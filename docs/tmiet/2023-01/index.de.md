# 2023-01: Dieser Monat in Eternaltwin #16

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

_Evian_ korrigierte Skywar in [der Eternaltwin-Anwendung](https://eternaltwin.org/docs/desktop),
Version 0.6.6.

# eMush

Willkommen in diesem Bereich, der [🍄 eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

## Was ist neu?

Letztes Jahr haben wir erwähnt, dass Administratoren individuelle Schiffe erstellen können.

![eMush](./emush0.png)

Wir freuen uns, ankündigen zu können, dass diese Funktion fast fertig ist! 🎉
Das ist ein großer Schritt für die offene Alpha-Version von eMush.

Dank dieses Admin-Panels werden wir in der Lage sein, neue Gegenstände, Charaktere, Waffen usw. hinzuzufügen. Ohne jedes Mal die Datenbank zurückzusetzen!

So können wir uns auf die nächsten Schritte konzentrieren und deine Leistungen präsentieren. Da ein Bild
mehr sagt als tausend Worte, lassen wir dich unsere Fortschritte sehen:

![eMush](./emush1.png)

![eMush](./emush2.png)

![eMush](./emush3.png)

Ziemlich gut oder? 😄

## Demnächst

Hier sind die Ziele, die wir uns für die nächste eMush-Version gesetzt haben:

- 🇬🇧 Englische Übersetzungen ✅
- 🗃️ Raumschiff-Archivierung, um den Überblick über deine Erfolge zu behalten 🚧
- 💾 Datenbankmigrationen, um neue Versionen zu installieren, ohne die bestehenden
  Raumschiffe zurückzusetzen 🚧

Bei all diesen Aufgaben sind wir gut vorangekommen. Es bleiben nur noch ein paar
kleinere technische Probleme. Wir waren noch nie so nah an der offenen Alpha.

## Großartig! Wann?

Wie immer werden wir dich über TMIET auf dem Laufenden halten. Der beste Weg, den
Fortschritt zu verfolgen, ist der [Eternaltwin discord](https://discord.gg/SMJavjs3gk).

eMush ist ein Open-Source-Projekt, du kannst unsere Fortschritte auch auf GitLab verfolgen. Insbesondere den [Fortschritt des Spores for All!-Updates](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Vielen Dank für deine Aufmerksamkeit, und bis bald an Bord der Daedalus!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radioknistern_

Hallo zusammen, ich hoffe, dass es euch in dieser kalten und windigen Jahreszeit gut geht _verschlüsselt_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Dank der harten Arbeit unserer Entwickler steht die Saison 15 unmittelbar bevor. Zur Erinnerung _rauschen_ alles wird für den 11. Februar fertig sein. Also macht bereit  _rauschen_

Es wird dich auch freuen zu sehen, dass der Altar nicht beschädigt und rostig ist wie immer,
schließlich können wir nicht zulassen, dass diese Untoten alles ruinieren. _knistern_

Ein kleines Wort zu den 43 Kannibalismusakten... _rauschen_ Mmmmh... Dem ist nichts
hinzuzufügen.

Ich denke, es gibt nichts anderes, richtig Ben? _rauschen_ Aber schließ diese verdammte Tür!

# Eternal DinoRPG

Willkommen in diesem Bereich, der [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) gewidmet ist.

Wir haben vor ein paar Tagen die Version 0.5.0 veröffentlicht. Sie fügt die "Pigmou" und die
ersten Missionen ein. Wir sehen bereits etwa 60 Pigmou und 30 abgeschlossene Missionen!

Wir arbeiten bereits an der nächsten Version. Sie wird neue Inhalte bieten: Ihr werdet mit Nicht-Spieler-Charakteren (NSCs) sprechen können und neue Missionen erhalten!

# Eternal Kingdom

Hallo zusammen, hier sind die Neuigkeiten für [Eternal Kingdom](https://kingdom.eternaltwin.org/)!

Nach der Veröffentlichung der Pre-Alpha 5 Version ([siehe Änderungsdetails](https://eternaltwin.org/forum/threads/9074673d-a530-4780-9fc2-4ebc8743152a)),
waren Hunderte von Spielern auf unserer Testkarte. Ich danke euch allen für euere
Interesse!

Ein neues Mitglied ist unserem Team beigetreten: _NSunshine_, er arbeitet an der Aktualisierung der Einheiten-Oberfläche. Talsi arbeitet an der Verbesserung unserer Karte (neue Rendering-Engine,
Weiterentwicklung der Städte, Generäle, ...). Er arbeitet auch an der Übersetzung von Städtenamen
in verschiedene Sprachen. Wir arbeiten gerade an der Version Alpha 5. Sie wird sich auf Generäle (Einheitentransfers, Bewegung), Gesundheit und Handel konzentrieren.

Nochmals vielen Dank für all eure Unterstützung, Nachrichten und Fehlerberichte! Bis bald!

# Eternalfest

Bald ist der 10. Jahrestag von [Eternalfest](https://eternalfest.net),
das Projekt startete am 5. März 2013. Um dies zu feiern, haben wir ein _massives_ Update im Januar veröffentlicht.

**Die Gegenstände, die ihr im Laufe der Jahre gesammelt habt, könnt ihr jetzt im neuen Tab _Mein Profil_ begutachten.** Dies war die am meisten gewünschte Funktion, und jetzt ist sie endlich da! 🎉
Wir werden die Profilseite in den nächsten Updates weiter verbessern.

Die andere wichtige Neuerung ist die vollständige Unterstützung von In-Game-Übersetzungen. Das erste Spiel das diese Funktion nutzt, soll im Februar erscheinen.

Es gibt noch eine ganze Reihe anderer Funktionen. Du kannst jetzt Spiele zu Favoriten hinzufügen,
Spiele können mehrere Versionen parallel haben (z. B. "main" und "beta"), du kannst eine beliebige Anzahl von Aktualisierungen im Voraus planen, usw.

Diese Funktionen waren möglich dank _Demurgos_ und _Moulins_, die das Spielverwaltungssystem umgebaut haben, um flexibler zu sein. Dies war eine langwierige Aufgabe, die
endlich abgeschlossen ist. Damit ist die Migration von Eternalfest
auf die Programmiersprache Rust abgeschlossen. Dieses neue Spielsystem wird auch in Eternaltwin importiert werden und als Basis für eine verbesserte Integration mit den
verschiedenen Spielen (Spielprofile, Belohnungen) dienen.

# Neoparc

Willkommen [Dinoz-Meister](https://neoparc.eternaltwin.org)!

**Der erste Clankrieg ist offiziell eröffnet!**
Details findet ihr im _Hilfe_-Abschnitt der Website. Wir hoffen, das bringt euch Ruhm und Reichtum! Viel Glück und seid unbarmherzig!

![Neoparc - Clan War](./neoparc.png)

Außerdem haben wir verschiedene Korrekturen veröffentlicht, insbesondere in der Umgebung des Missionszentrums Dinoville. Ihr erhaltet jetzt einen _Pruniac_, wenn ein Ei schlüpft. Die Raubzugbelohnungen wurden ebenfalls gesenkt (z.B. keine kostenlosen Irma-Tränke).

Außerdem hat die Fertigkeit "Ingenieur" jetzt einen Nutzen: Sie erhöht die Haltbarkeit von
Dinojak-Bieren, Prismatik-Anhängern und Gegenständen, die in der _Anomalienhöhle_ [es: Caverna de la Anomalía] verkauft werden.

# Abschließende Worte

Du kannst deine [Nachrichten für Februar](https://gitlab.com/eternaltwin/etwin/-/issues/58) senden.

Dieser Artikel wurde bearbeitet von: Bibni, Biosha, Demurgos, Dylan57, Evian,
Fer, Jonathan, Patate404 und Schroedinger.
