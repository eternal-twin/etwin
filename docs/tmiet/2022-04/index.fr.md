# 2022-03 : Ce mois-ci sur Eternaltwin n°6

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Dans les derniers articles, nous avons parlé du passage d'[Eternaltwin](https://eternaltwin.org/)
de TypeScript vers Rust. Pendant la migration, nous avions un système qui ajoute
de la compatibilité entre les deux langages, afin de pouvoir travailler morceau
par morceau. Ce mois-ci nous avons configuré les requêtes internes pour qu'elles
passent directement au code Rust.

Ça a plutôt bien marché... sauf pour le forum.

Nous avons trouvé un grave bug dans notre client de base de donnée, qui cause
des crashs lors des visites sur le forum. Nous travaillons sur un correctifs.
En attendant, le forum est temporairement désactivé.

Mis à part cela, des personnes ont commencé à travailler sur Frutiparc, Kube,
et LaBrute.

# Neoparc

Oyez oyez, braves maitres [dinoz](https://neoparc.eternaltwin.org/) !

Afin de booster l’économie, remplir les caisses de la Banque arboricole et
lutter contre la surpopulation de Dinoville, les Souverains de Dinoland déclarent
**le retour des guerres de Clans** !

Pour bientôt. Pas de suite. Baissez ces armes…

Afin de vous préparer au mieux, il est désormais possible d’enregistrer votre
Clan pour une modique somme à la mairie de Dinoville (prévoyez un livre pour
la salle d’attente, le gars à l’accueil est un vrai moulin à paroles). Une fois
enregistré, votre clan pourra accueillir vos amis, vos assassins et vos
taverniers. À vous de décorer votre intérieur, et faites le bien, les autres
Clans pourront toujours venir vous espi… voir comme c’est bien beau chez vous,
dis donc !

Bref, profitez bien de cette paix sur le royaume, car bientôt les cors de la
guerre vont sonner partout, de jour comme de nuit ! (Et non, on ne pourra
pas porter plainte pour tapage nocturne.) Voilà qui laissera peut-être une
chance au Boss de raid ou au Ouistiti journalier d’échapper à votre envie de
sang qui sera bientôt comblée !

![Neoparc](./neoparc.png)

# Eternalfest

Le 1er avril, nous avons publié un nouveau jeu ainsi qu'une grosse mise à jour
sur [Eternalfest](https://eternalfest.net/).

Le nouveau jeu est notre traditionelle contrée "poisson d'avril" :
[Eternalfestfest](https://eternalfest.net/games/a7cae442-8ced-4c05-836b-605b0f64bc8f).

[La faille éternelle](https://eternalfest.net/games/a5ac93dd-28f1-4740-b94b-8292a26dbb39)
a reçue une énorme mise à jour. La dimension au niveau 20 est désormais disponible,
avec un labyrinthe bourré de mécanismes et puzzles, s'étalant sur plus de 50 niveaux.
Êtes-vous prêt à vous mesurer à l'éternel dieu des pics qui vous attend au bout ?
Et ainsi mettre fin à son reigne tyranique (les terrifiants pics cachés) une
bonne fois pour toutes ?

Nous vous recommandons vivement de l'essayer et (re-)découvrir une des meilleures
contrées sur Eternalfest.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_grésillements_ Bonjour tout le monde et merci d'utiliser notre système
radiophonique sans fil ! Je suis Connor et aujourd'hui je vous adresse un
message après _brouillé_ vacances.

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Vous le savez sûrement, ces derniers temps ont été plutôt calmes. J'espère que
vous avez pu vous reposer et profiter des Croix en chocolat. Et _brouillé_

Nos chers amis développeurs ont mis en place la possibilité de réinitialiser
votre âme MyHordes **une seule fois**, pour ceux qui veulent continuer à jouer sur
Hordes et pouvoir importer "à la toute fin". Vous trouverez les infos détaillées
dans la page Importation. _brouillé_

De plus, nos traducteurs travaillent d'arrache-pied pour traduire le jeu,
actuellement la traduction française est intégralement finie, quant aux
traductions espagnoles et anglaises, elles sont respectivement à 97% et 93%. _brouillé_

Sur un autre sujet, je tenais à vous annoncer qu'un événement d'une grande
ampleur arrive dans nos contrées sableuses : le **Duel des Huit**, vous
trouverez des infos dans vos MPs, ainsi que sur le sujet de l'Event ! _brouillé_

![MyHordes - Duel des Huit](./ddh.png)

Les villes spéciales Chaman ont étées réalisées, permettant de trouver la
source des perturbations sur le plan du plexus solaire. Sachez que _brouillé_

D'ailleurs, vous pouvez désormais activer la Toxine dans vos villes privées,
qui avait été supprimée du jeu d'origine. Hâte de voir qui sera en mesure de _brouillé_

C'était tout pour le moment, à la prochaine ! _grésillements_

_grésillements_ Bordel, qu'est-ce que Ben a encore foutu avec les câbles...?!
_grésillements_

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de mai](https://gitlab.com/eternaltwin/etwin/-/issues/44).

Cet article a été édité par : Demurgos, Dios, Dylan57, Jonathan, MisterSimple,
Patate404, Rikrdo, Schroedinger, Unpuis et Valedres.
