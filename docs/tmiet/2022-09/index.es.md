# 2022-09: Este Mes En Eternaltwin #12

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

Todos los juegos fueron mudados al nuevo sistema de desplegamiento. Los líderes de proyectos ahora pueden desplegar nuevas versiones cuando lo deseen.

# eMush

Bienvenidos a esta sección dedicada a [eMush](https://emush.eternaltwin.org/), el proyecto dedicado a salvar Mush.


## ¿Qué hay de nuevo?

La última vez que compartimos algunas noticias fue en julio-agosto 2022. Recién empezamos a trabajar en enfermedades para la Disease Update.

![eMush1](./emush1.png)

Continuamos el trabajo en esta actualización. ¡Estamos muy contentos por anunciarles que todas las enfermedades - físicas, psicológicas o heridas - ahora están funcionando!

![eMush2](./emush2.png)

Otra gran característica fue la adición del panel de administrador.

![eMush3](./emush3.png)

Nos permitirá manejar las configuraciones del juego más fácilmente: no tendremos que reiniciar la base de datos cada vez.

Esto nos acerca mucho a nuestra meta de plazo medio: abrir un periodo de prueba alfa. 🤞

## Lo que se viene

Ahora que las enfermedades están hechas, estamos trabajando en cómo enfermarnos. La mayoría de las maneras están hechas: chance al azar, intoxicación por comida, ETS, esporas...

![eMush4](./emush4.png)

La pieza principal faltante son las heridas causadas por armas, lo cual será nuestra próxima área de foco para la actualización.

Todavía tenemos que implementar los cuadros de información sobre los síntomas de enfermedades (podría ser útil), ¡y entonces terminaremos con la Disease Update!

(Así que una nueva alfa está bastante cerca. 👀)

## ¡Genial! ¿Cuándo? ¿Cómo juego?

No podemos brindarles información en este momento. Les informaremos en cuanto decidamos una fecha.

Si no puedes esperar, la mejor manera de mantener el contacto y seguir todo el progreso es unirse al [Discord de Eternaltwin](https://discord.gg/SMJavjs3gk). ¡También nos muestras tu apoyo!

eMush es un proyecto de Código Abierto. Nuestro progreso también se encuentra [disponible en Gitlab]
(https://gitlab.com/eternaltwin/mush/mush/-/milestones/8#tab-issues).


¡Muchas gracias por su atención, espero verlos pronto a bordo del Daedalus!

# Neoparc

¡Es el inicio no oficial de la guerra de clanes en [Neoparc](https://neoparc.eternaltwin.org/)!


¡Hola, maestro Dinoz!

Tenemos algunas actualizaciones para tu amado clan. Ahora puedes comenzar a llenar tus tótems y anotar puntos para la guerra de clanes en curso.

Ten cuidado, esta primera guerra de clanes puede que todavía necesite algunos ajustes y balanceo. Puede que haya algunos problemas al inicio. Hasta la próxima actualización, ¡les deseo a todos mucha suerte con sus excavaciones!

Cuidado, la guerra no es el único peligro. ¡Hay varios rumores sobre espíritus espeluznantes merodeando por todo Dinoland! ¡Únete al evento de Halloween!

Ah, una última cosa: puedes descargar la información de tus Dinoz desde el menú de "Mi Cuenta".

¡Diviértanse!

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_

¡Hola a todos! Espero que todo esté bien para ustedes, y especialmente para los que piensen salir con _interferencia_


![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

La caza de bugs continúa. Nuestro equipo de desarrolladores, acompañado por 2 guardianes y un explorador, viajan por todo el World-Beyond para proveerlos con lo más _interferencia_

Preparaciones para la temporada 15 están en camino. ¡Asegúrate de ir _interferencia_

Además, ¡me han dicho que los shamanes finalmente han entendido que mojar dos veces la misma área es inútil!  _ruido de radio_

Cough _ruido de radio_

# Palabras finales

Pueden enviarnos [sus mensajes de octubre(https://gitlab.com/eternaltwin/etwin/-/issues/54).

Este artículo ha sido editado por: Demurgos, Dylan57, Evian, Fer, Jonathan, Patate404 y Schroedinger.
