# 2022-12: Este Mes En Eternaltwin #15

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

_Biosha_ implementó una herramienta para monitorear el estado del sitio web y enviar
alertas cuando un problema es detectado. Esta herramienta será desplegada pronto.

_Moulins_ actualizó el archivo de Hammerfest para devolver los estados de inventarios
y misiones.

# Eternalfest

[Eternalfest](https://eternalfest.net/) te permite jugar niveles clásicos y nuevos del
juego de plataformas Hammerfest.

_Rarera_ y _Maxdefolsch_ lanzaron un nuevo juego el 25 de diciembre: [L'ancienne source](https://eternalfest.net/games/eb10aaf1-23dc-4b3f-a543-66a0596459d4).
Contiene cientos de grandes niveles, les recomendamos encarecidamente que lo prueben.

También hubo un montón de trabajo para cambiar cómo se manejan los juegos internamente.
Esto debería permitir el soporte de algunas funciones muy esperadas, como lo son las
páginas de inventarios, soporte completo de traducción, o progresos de cargas precisos.
El lado del servidor está casi completo, y ahora estamos trabajando en la interface. Este
trabajo también sirve como prototipo para un soporte mejorado de los juegos
a nivel Eternaltwin (y así remplazar la configuración manual).

# eMush

Bienvenidos a esta sección dedicada a [eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

## ¿Qué hay de nuevo?

En este buen mes de diciembre, los administradores de eMush y locos de la
personalización se regocijarán. Verán, hemos trabajado duro para que muchas naves
Daedalus muy diferentes, con sus propias configuraciones, puedan despegar lado a lado.
Y lo mejor, todo puede ser configurado directamente desde el panel de administrador
del sitio web.

![eMush](./emush.png)

Entre otras mejoras, hemos mejorado los archivos de las naves viejas. Una vez que
un juego termina, limpiamos la mayoría de las entidades y sólo guardamos la información
relacionada a tu legado digital.

El último mes anunciamos la finalización de las traducciones de inglés. Bueno,
los administradores ahora pueden elegir el lenguaje cuando crean un Daedalus.

## Lo que se viene

Continuaremos trabajando en la migración de base de datos, así podremos desplegar
actualizaciones sin necesidad de reiniciar naves existentes.

Además trabajaremos en mejoras para los perfiles de los jugadores.

## ¿Cuándo es la alfa abierta?

Continuaremos compartiendo nuestro progreso a través de EMEET, pero la mejor
manera de seguir el progreso es uniéndose al [canal de Discord de EternalTwin](https://discord.gg/SMJavjs3gk).

eMush es un proyecto de Código Abierto. También puedes seguir nuestro progreso en GitLab.
En particular, el progreso de [la actualización **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

¡Muchas gracias por su atención, los veré luego a bordo del Daedalus!

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_

Hola a todos. Espero que todo esté bien para ustedes en esta temporada fría _interferencia_

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Espero que no hayan comido muchos chocolates navideños porque aparentemente
están _interferencia_

También creo que deberías comparar tu _interferencia_. Así que desarrollamos
un sistema de clasificación único para todas las Distinciones.

Las preparaciones para la temporada 15 están en camino. Abrimos una encuesta
buscando participantes para la beta cerrada, ¡disponible en la frecuencia 102.4 MHz
(Foros del mundo), hasta el 8 de enero de 2023! _ruido de radio_

Además, hemos optimizado varios aspectos de nuestro sitio web para que sea
más conveniente en tus sistemas móviles GPS Anthrax _ruido de radio_

Mmmmmh, creo que hay _interferencia_ pero qué es este _interferencia_ ¡BEN! ¡Dile
a ese cuervo que salga del _interferencia_

# Palabras finales

Pueden enviarnos [sus mensajes de enero](https://gitlab.com/eternaltwin/etwin/-/issues/57).

Este artículo ha sido editado por: Breut, Demurgos, Dylan57, Fer, Patate404, and Schroedinger.
