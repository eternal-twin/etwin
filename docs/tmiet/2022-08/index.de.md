# 2022-08: Dieser Monat in Eternaltwin #11

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

## Motion-Twin Quellen

Anfang August hat Skool von Motion-Twin die Quelltexte für die meisten
Twinoid-Spiele [veröffentlicht](https://github.com/motion-twin/WebGamesArchives).
Dies ermöglichte es uns, einige Projekte voranzutreiben, zu vervollständigen
oder sogar einige Projekte zu beginnen.

## Allgemein

Wir setzen die Einführung des neuen Einsatzsystems fort: Es ermöglicht den Projektleitern
ihre eigene Implementierung zu verwalten. Wir haben im Juli mit Neoparc begonnen; im August haben wir es für La Brute und Kingdom aktiviert. Im September werden wir die Migration abschließen und alle übrigen Spiele übertragen.

Das Ziel für das Jahresende wird sein, einige seit langem bestehende Probleme im Zusammenhang mit
Konten zu beheben: E-Mail-Überprüfung, Passwortabfrage, Kontolöschung...

# App Update

Mit der [Eternaltwin-App] (https://eternaltwin.org/docs/desktop) könnt ihr
alle Eternaltwin- und Motion-Twin-Spiele mit Flash spielen. Wir haben kürzlich die
brandneue Version `0.6` dank **Aksamyt** veröffentlicht.

Die wichtigste Änderung ist die Aktualisierung verschiedener interner Komponenten.
Insbesondere wird die Leistung verbessert und behebt Kompatibilitätsprobleme mit MyHordes.

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radio knistern_

Hallo zusammen, wir sehen uns nach einer Pause! Für immer mehr _undeutlich_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Wart ihr enttäuscht, dass ihr während des Angriffs keinen Zugang zur
Frequenz 102,4 MHz (Weltforum) hatten? _Radio knistern_ Das ist jetzt
Geschichte! Wir haben unseren neuartigen Verstärker aktualisiert, so dass ihr
diese Frequenz während des Angriffs nutzen könnt.

Habt ihr schon einmal davon geträumt, zu wissen, wann ihr die Kürbisse pflücken
oder euch vor dem Weihnachtsmann in die Nase zuhalten müsst?
Jetzt könnt ihr den Zeitraum der saisonalen Ereignisse wissen! _Radio knistern_

Bastelt ihr gerne mit Werkzeugen? Keine Sorge, wir bieten euch diese erstaunliche
Swagger-Dokumentation für nur den Preis von _scrambled_

Es ist Zeit für den Alle-Seelen-Zugang auf MyHordes. Nur für heute, morgen und alle
absehbaren Tage danach, könnt ihr jede abgelegene oder kleine Stadt betreten,
solange ihr 100 Seelenpunkte habt.

Einige unserer Bürger haben festgestellt, dass sie eine alte Bibliothek gefunden haben. Aber sie
waren nicht sehr vorsichtig mit den Büchern, und sie wurden in der ganzen
Welt verteilt. Haltet die Augen offen, vielleicht findet ihr einige dieser neuen Bücher.
Vielleicht findest du auch weitere Spuren der Vergangenheit, wenn du in die Städte gehst.
_Radio knistern_ kannst du dem Architekten Wa... _undeutlich_ danken.

Das wäre alles für dieses Mal, und vor allem: Bleibt hydriert! _Radio knistern_

# PiouPiouZ

Nun, da Motion-Twin den Quellcode für den größten Teil ihres Spiels veröffentlicht hat (siehe oben),
haben wir alles, was wir brauchen, um Pioupiouz wiederzubeleben. Uns fehlten der Level
Editor und die letzten Versionen des Spiels. Aber das ist jetzt behoben.

Wir sind fast fertig mit der Erstellung einer Website, auf der man Levels mit
Flash spielen und tauschen kann. Wir haben auch mit der Arbeit an einer HTML5-Version (ohne Flash) begonnen.

# Abschließende Worte

Du kannst deine [Nachrichten für September](https://gitlab.com/eternaltwin/etwin/-/issues/53) senden.

Dieser Artikel wurde bearbeitet von: Biosha, Demurgos, Evian, Fer, Jonathan, Patate404, SylvanSH und Schroedinger.
