# 2022-07: This Month In Eternaltwin #10

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

We enabled the new server and its deployment system. Project leaders can now
send updates online by themselves. Neoparc is the first project to use the new
server for its "beta" version. We will reach out to other projects to help them
move to the new system.

# Eternalfest

On July 26th 2022, the largest [Eternalfest](https://eternalfest.net/) game got
its final update. **Hackfest** is now complete after 10 years of work.

This game has over 1600 levels, quests and achievements. Return to Hackfest to
overcome the challenges in Valhalla and the Final Maze, and face the most
powerful Eternal to save the world once again!

# Eternal DinoRPG

The [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) team is relatively quiet
on Discord regarding development recently. But don't you worry, we are still
actively working on the project. Recent and ongoing focus is to migrate to a
new database manager (from sequelize to TypeORM for the curious). This change is
needed to give the team the ability to update the database architecture and
schemes without wiping it out. This is a huge amount of work, which also gives
us the opportunity to clean our code base and reduce code debt. We are nearing
the end of this work and are satisfied with the progress and the new advantages
we are benefiting from it.

On top of this, Swagger is being integrated for the curious who want to toy and
test our API directly. Last but not least, all libraries on the project are in
the process of being updated.

Jolu and Biosha will be on summer vacation (not together ;D) et will be back for
the start of the school year and finish 0.4.0!

_A few words from Biosha, the project leader:_

> From my personal perspective, it has been now a bit over a year since I took
> the lead of the project. I am very happy with all the work that has been done.
> One year ago, you could only log in with Eternaltwin and buy a dinoz. Today,
> you can buy a dinoz, see it, move, gain experience and levels, lean skill, get
> status, buy items in the shops, publish news, see the ranking, etc.
> We learned a lot throughout the project and are still learning. One year ago,
> we had for objective to publish our first version online. With a few months,
> and thanks to Demurgos, version 0.1.0 was released 11th December 2021! Since
> then, there has been 114 commits and 25k lines of codes changed.
> The road ahead is still long until a version close to the original game, but
> we are still hopeful and getting closer every day.

Thanks everyone for the help and support. Happy summer vacation!

# Neoparc

Hello [Dinoz masters](http://neoparc.eternaltwin.org/), let's start this
TMIET edition with the current work. As many of you know already, we're
preparing a new "clan war" system to fit the number of active Neoparc players.
This system will be slightly tweaked compared to what was on Dinoparc, but it
will remain as fun and competitve as what you may have known in the past. There
will be no change to dark dinoz and totems, but also some new changes! Besides,
we work of the Dinoville mission center, the perfect place to get some extra
coins and items for your daily tasks! The pace is a bit slower during summer,
but we're still very excited about the project.

From a more technical side, Neoparc will soon move to a more powerful and
feature-rich server. It will not impact you directly, but it will make it
easier to deploy patches and bugfixes. We've already migrated the beta version
of Neoparc, the main version will move in August.

Furthermore, we were able to get a one-hour meeting with **warp**, the original
Dinoparc creator from the Motion-Twin era. We started by introducing Eternaltwin,
the different projects, and Neoparc. Then, we presented a Neoparc (live demo)
and focused on the new features such as the weekly raids, the "Goupignon", the
"cherry" dinoz, the farming technique, the revamped Bazar, and the new items.
Warp seemed very moved that our community was able to work on preserving his
legacy. He trusts our project wholesale. We asked him to contact his former
coworkers at Motion-Twin to update the old Dinoparc website and point to
Neoparc. There's no promise here, but it's nice that he'll ask directly.

Overall there was a small amount of new features this month, but there were a
lot of important improvements and contacts in the background.

I wish you a nice day in Dinoland.

Until next time, Jonathan.

# eMush

Welcome the **[eMush](https://emush.eternaltwin.org/)** section of _This
Month in Eternaltwin_ for July 2022, the Mush preservation project.

## What's new?

The last news we posted were in May 2022, the version 0.4.2 of the game looked
like this:

![0.4.2](./emush-0.4.2.png)

Nowadays the version 0.4.4 (still in development) looks more like this:

![0.4.4](./emush-0.4.4.png)

As you can see, these last two months saw **many improvements**, the main one
being the **2D isometric interface** from the version 0.4.3. It was extensively
tested by our alpha testers in June.

We spent July fixing bugs found during this alpha. Most of the gameplay issues
are already fixed. Graphical glitches will soon follow.

The next **major update** of the game is seeing a lot of progress: **diseases**.
You'll soon (re)discover some **"fun" effects**...

![Space rabies](./emush-disease.png)

**Physical diseases** are almost all done. Our goal is to implement **mental
illnesses** and **wounds**, so the update is ready for **October**.

## Next steps

We will then work on **skills**.

It also means we'll implement allmost all the qctions from the original game.
Right now about **40% of actions are available** for all characters... including
the amazing Boring Speech.

![Boring Speech](./emush-speech.png)

(yes, it's real: we fixed the bug so you now properly receive 3MP 😳)

## Roadmap

As of new, we're still missing some important features on eMush:

- 🚧 Diseases and wounds
- ❌ Skills
- ❌ Research
- ❌ Projects
- ❌ The communication terminal
- ❌ Hunters and piloting
- ❌ Planets and travel
- ❌ Expeditions

However, the following mechanisms are already available (in closed alpha):

- ✅ Joining a Daedalus with 15 other players, in isometric view
- ✅ Being Mush, getting spores, infecting humans
- ✅ Hitting and kills the Mushs (or humans)
- ✅ Enduring shaking, fires, engine failures... et you can heal yourself, put out
  the fires, fix the issues... (or get murdered by NERON once there are no humans)

It's kinda like in the original game, no ? 🙂

## Great! How to play?

For now, the game is only available during closed alpha sessions. We run them
at each major update.

We'll let you know when the next session starts. Please join the [Eternaltwin
Discord](https://discord.gg/ERc3svy) to get the latest news about the project.

Thank you all for your support and help, see you later in the Daedalus!

# Closing words

You can send [your messages for August](https://gitlab.com/eternaltwin/etwin/-/issues/52).

This article was edited by: Biosha, Demurgos, Evian, Fer, Jonathan, MisterSimple,
Patate404, SylvanSH, and Schroedinger.
