# 2022-07 : Ce mois-ci sur Eternaltwin n°10

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

Nous avons inauguré le nouveau serveur et son système de déploiement. Les chefs
de projets peuvent dorénavant envoyer des mises à jour par eux-mêmes. Neoparc
est le premier projet à utiliser le nouveau serveur pour sa version "beta".
Nous allons contacter les autres projets pour les aider à migrer vers le nouveau
système.

# Eternalfest

Le 26 juillet 2022, la plus grande contrée d'[Eternalfest](https://eternalfest.net/)
a reçue sa mise à jour finale. **Hackfest** est maintenant complète après 10 ans
de travail.

La contrée contient plus de 1600 niveaux, des quêtes et des trophées. Retournez
dans Hackfest traverser les défis de Valhalla et l'Ultime Dédale, et affrontez
le puissant Eternel pour sauver le monde une fois de plus !

# Eternal DinoRPG

The team is relatively quiet on Discord regarding development recently. But don't you worry, we are still actively working on the project. Recent and ongoing focus is to migrate to a new database manager (from sequelize to TypeORM for the curious). This change is necessary to give the team the ability to update the database architecture and schemes without wiping it out. This is a huge amount of work, which also gives us the opportunity to clean our code base and reduce code debt. We are nearing the end of this work and are satisfied with the progress and the new advantages we are benefiting from it.
On top of this, Swagger is being integrated for the curious who want to toy and test our API directly. Last but not least, all libraries on the project are in the process of being updated.
Visually speaking, from a player perspective, nothing changed. The website have not been updated.
Jolu and Biosha will be on summer vacation (not together ;D) et will be back for the start of the school year and finish 0.4.0!
A few words from Biosha, the project leader:
From my personal perspective, it has been now a little bit more than a year since I took the lead of the project. I am very happy with all the work that has been done. One year ago, you could only log in with Eternaltwin and buy a dinoz. Today, you can buy a dinoz, see it, move, gain experience and levels, lean skill, get status, buy items in the shops, publish news, see the ranking, etc.
We learned a lot throughout the project and are still learning. One year ago, we had for objective to publish our first version online. With a few months, and thanks to Demurgos, version 0.1.0 was released 11th December 2021! Since then, there has been 114 commits and 25k lines of codes changed.
The road ahead is still long until a version close to the original game, but we are still hopeful and getting closer every day.
Thanks everyone for the help and support. Happy summer vacation!

# Neoparc

Bonjour [maîtres Dinoz](http://neoparc.eternaltwin.org/), parlons un peu des
travaux en cours pour commencer l'édition Juillet 2022 du TMIET. Plusieurs
d'entre vous sont déjà au courant, nous travaillons sur la mise en place d'un
nouveau système de guerre des clans qui sera adapté au nombre de joueurs
actuellement actifs sur Neoparc. Ce système sera un peu différent de ce qui était
en place auparavant sur Dinoparc, mais il sera tout aussi plaisant et compétitif
que ce que nous avons connu par le passé. Vous pouvez vous attendre à des
mécaniques inchangées pour les Dinoz Sombres et les Totems, mais aussi vous
préparer pour de la nouveauté ! Sinon, nous travaillons aussi en parallèle
sur le poste des missions de Dinoville, l'endroit parfait pour aller gratter
quelques pièces et objets supplémentaires une fois toutes les actions journalières
effectuées ! Nous avançons un peu plus lentement en cette période estivale,
mais nous restons tout aussi motivés.

Par ailleurs, sur une note plus technique, Neoparc aura sous peu une migration
vers un serveur bien plus puissant et fonctionnel que l'ancien. Cela ne changera
pas votre expérience de jeu, mais me facilitera grandement la vie sur les
déploiements de patchs et de bugfixs. Nous avons déjà migré la version beta de
Neoparc, la version principale migrera en août.

De plus, nous avons été en mesure d'avoir un entretien d'une heure avec **warp**,
le créateur original de Dinoparc de l'époque de Motion-Twin. Nous avons commencé
par lui faire une mise en contexte d'Eternaltwin et de ses différents projets,
tels que Neoparc. Par la suite, nous avons fait une démo de Neoparc et mettant
surtout l'accent sur les nouveautés incorporées tel que le Raid hebdomadaire, le
Goupignon, les Dinoz "Cerises", la technique de farm, la refonte du Bazar et les
nouveaux objets. Warp semblait très touché de voir que la communauté s'était
organisée pour faire revivre des parties de son héritage, et il était très
optimiste par rapport à notre projet dans son intégralité. Nous lui avons
demandé de lancer une bouteille à la mer vers ses anciens collègues de Motion-Twin, pour qu'ils
puissent peut-être mettre une annonce officielle de Neoparc sur l'ancien Dinoparc
à moyen terme. Rien n'est promis, mais c'est déjà bien qu'il puisse demander
directement.

Somme toute, un mois tranquille au niveau des features et nouveautés, mais assez
important en arrière-scène pour les développements et les contacts faits.

Je vous souhaite une belle journée sur Dinoland.

À bientôt, Jonathan.

# eMush

Bienvenue à tous dans cette section de Ce Mois-ci sur Eternaltwin juillet 2022
consacré à **[eMush](https://emush.eternaltwin.org/)**, le projet de préservation de Mush.

## Quoi de neuf ?

La dernière fois que nous vous avions donné des nouvelles en mai 2022, la
version 0.4.2 du jeu ressemblait à ça :

![0.4.2](./emush-0.4.2.png)

Aujourd'hui la version 0.4.4 (toujours en cours de développement) ressemble plus à ceci :

![0.4.4](./emush-0.4.4.png)

Comme vous pouvez le voir ces deux derniers mois ont été **riches en améliorations**,
dont la plus notable a été l'ajout de l'**interface 2D isométrique** lors de la
version 0.4.3, qui a été ardemment testée par nos alpha-testeurs durant le mois
de juin.

Nous avons ainsi passé le mois de juillet à corriger les bugs de cette alpha.
La majorité des problèmes de gameplay sont déjà résolus. Les bugs graphiques
devraient suivre sous peu.

La prochaine **mise à jour majeure** du jeu avance également à grand pas :
**les maladies**. Vous pourrez bientôt (re)découvrir des **symptômes "amusants"**...

![Rage spatiale](./emush-disease.png)

Les **maladies physiques** sont presque toutes fonctionnelles, notre objectif
est d'implémenter les **maladies psychologiques** et les **plaies** pour
finaliser la mise à jour d'ici **octobre**.

## Prochainement

Nous commencerons ensuite à travailler sur les **compétences**.

Ce qui veut aussi dire implémenter la quasi-totalité des actions du jeu original,
sachant pour le moment environ **40% d'entre elles sont déjà disponibles** pour
tous les personnages... dont l'indispensable Discours Barbant.

![Discours Barbant](./emush-talk.png)

(oui, vous ne rêvez pas, nous avons corrigé le bug le concernant et il donne
bien 3PM sur eMush 😳)

## Roadmap

Pour l'heure, il manque plusieurs fonctionnalités importantes sur eMush :

- 🚧 Les maladies et plaies
- ❌ Les compétences
- ❌ Les recherches
- ❌ Les projets
- ❌ Le terminal de communication
- ❌ Les hunters et le pilotage
- ❌ Les planètes et le voyage
- ❌ Les expéditions

Cependant, les mécanismes ci-dessous sont d'ores et déjà disponibles (en alpha fermée) :

- ✅ Rejoindre un Daedalus en vue isométrique avec 15 autres joueurs
- ✅ Être Mush, extirper des spores, poinçonner et convertir des humains
- ✅ Frapper et tuer les Mushs (ou les humains)
- ✅ Subir des secousses, plaques, incendies, pannes... et vous en soigner, les
  éteindre, les réparer... (ou être assassinés par NERON par ce qu'il n'y a plus d'humains)

Ça ressemble un peu à ce qu'on fait sur le jeu original non ? 🙂

## Super ! Comment on joue ?

Pour le moment, le jeu n'est disponible que lors de sessions d'alpha fermées,
lancées régulièrement à chaque mise à jour majeure.

Nous vous tiendrons informés du prochain lancement. N'hésitez pas à rejoindre
le [Discord d'Eternaltwin](https://discord.gg/ERc3svy) pour avoir les informations
les plus récentes concernant l'avancement du projet.

Merci à tous pour votre soutien et votre aide, et à bientôt sur le Daedalus !

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition d'août](https://gitlab.com/eternaltwin/etwin/-/issues/52).

Cet article a été édité par : Biosha, Demurgos, Evian, Fer, Jonathan, MisterSimple,
Patate404, SylvanSH et Schroedinger.
