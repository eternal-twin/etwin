# 2022-01: Este mes en Eternaltwin #4

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Eternaltwin

Completamos la migración de todos los servicios de
[Eternaltwin](https://eternaltwin.org/) a Rust.

Esta migración fue el tema principal de 2021, y ahora deberíamos cosechar los
beneficios: confiabilidad y desempeño.

## Lanzamiento v0.10.0

Lanzamos la versión **0.10.0** [del paquete Eternaltwin](https://www.npmjs.com/package/@eternal-twin/cli).
También actualizamos [el cliente PHP](https://packagist.org/packages/eternaltwin/etwin)
acordemente. Los clientes para otras plataformas serán actualizados progresivamente.

Esta actualización es principalmente sobre arreglar algunos problemas internos
menores. Pueden leer [los cambios completos en este Merge
Request](https://gitlab.com/eternaltwin/etwin/-/merge_requests/410).

# Siguientes Funcionalidades

Trabajaremos en cerrar problemas existentes, arreglar traducciones y aplicar
algunos cambios de estilo.

La siguiente funcionalidad será arreglar los problemas de la administración de
cuenta: reportar errores significativos, arreglar problemas de redirección,
habilitar reinicios de contraseña, etc.

También estamos empezando a diseñar cómo implementar el sistema de recompensas
compartidas (títulos e íconos).

# Neoparc

¡Hola queridos maestros de Dinoz de [Neoparc](https://neoparc.eternaltwin.org/)!

Primero que nada, las incursiones semanales fueron rebalanceadas con respecto a
ritmos y estadísticas de jefe.

La mayoría de las habilidades faltantes fueron añadidas al juego.

7 habilidades completamente nuevas están disponibles, por lo que cada total de
elementos teorético para dinoz clásicos está balanceado. ¡Ahora es tu turno de
descubrirlos!

Finalmente, hubo algunos cambios al bazar. Ahora pueden intercambiar objetos de la guarida secreta a través del bazar.

¡Tengan un buen juego!

# Eternal Kingdom

¡[Eternal Kingdom](https://kingdom.eternaltwin.org/) ahora te deja jugar con tu
cuenta Eternaltwin! ¡Diviértete e intenta elevarte a través de la tabla de
clasificación!

## Funcionalidades Principales

Ahora tenemos un logo impresionante (hecho por **Seidanoob#5281**).

Ahora hay un registro para tu ciudad: ya no tienes que recordar lo que hiciste.

Las páginas actuales fueron casi completamente traducidas a inglés y español. Gracias a todos.

Ahora puedes reclutar tus generales. 💯

El pie de página de la página web fue actualizado para hacer juego con Eternaltwin. 💃

Algunas páginas nuevas empiezan a aparecer incluso aunque no estén completamente
listas (tabla de clasificación, selección de mundo, …).

## Mejoras Principales

Ya no tendrás que refrescar la página luego de acciones de usuario. Puedes quedarte en la página de selección de edificios y los botones serán actualizados automáticamente. La opción de construir será habilitada cuando un constructor esté disponible (o deshabilitada cuando pierdas a tu último constructor).

Como jugador/tester, por favor comparte tu feedback en [el foro de Eternal
Kingdom](https://eternaltwin.org/forum/sections/407e8a9e-8c5a-4177-b13c-9a7d746c7d24)
o directamente [en Discord](https://discord.gg/ERc3svy) en el canal #kingdom.

Si quieres ayudar con el desarrollo, estamos buscando a un desarrollador de
BACKEND (PHP/servidor Symfony) o un desarrollador de frontend (Web: HTML/JS/CSS).

Gracias a todos por su apoyo 😄

# MyHordes

_estática de radio_

¡Hola amigos y gracias por usar nuestro sistema radiofónico inalámbrico! Soy
Connor y hoy les envío un pequeño mensaje sobre [MyHordes](https://myhordes.eu/).

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Como seguramente sabrán, últimamente ha estado bastante tranquilo. Espero que
hayan podido descansar. Y _cortado_

Sobre otro tema, quería anunciar que deberían encontrar el sitio del juego en
motores de búsqueda dentro de poco normalmente. Espero que ningún zombie venga
al mismo tiempo... Además _cortado_

Los Consejos están en plena marcha en las ciudades. El juego ha sido globalmente
optimizado y es más rápido en todas las plataformas. No todo es perfecto pero
los desarrolladores están progresando a su ritmo, es más al hablar de ellos
les envío este pequeño consejo, pero no puedo probarles su autenticidad a
ustedes ya que ha sido copiado incontables veces.

---

_Brainbox_: Hola a todos.

_Brainbox_: La primera orden del día es proponer algo de contenido nuevo para la Temporada 15. ¿Alguna idea?

_Shaitan_: ¡Nuevos pictos dorados!

_Adri_: ¿Menos jugadores por ciudad, pero ciudades más cortas?

_Brainbox_: Así que… ¿nadie tiene buenas ideas, entonces?

_Dylan57_: Yo propongo que rehagamos todo el sistema de héroes.

_Adri_: ¿Él está ahí?

_Shaitan_: Bueno, eso no es famoso tampoco.

_Nayr_: *eh eh*

_Dylan57_: ¿Escucharon eso?

_La fatiga por otro lado está al frente y al centro…_

_Brainbox_: Bueno como no me están ayudando, voy a buscar en la caja de ideas.

_Shaitan_: ¡Oh no!

_Dylan57_: ¡En serio!

_Adri_: Inepto

_Brainbox_: Inmediatamente vamos a sacar al azar.

_Brainbox_: Cada uno de ustedes viene a conseguir una hoja con una idea escrita en mi mano, por favor.

_Shaitan_: ¿Por qué no lo hacemos con una encuesta para variar?

_Dylan57_: Sí genio, con 34 ideas de mierda por segundo…

_...Nadie va a saber qué les pasará a los papeles. Pero el sorteo fue hecho..._

---

Eso fue todo por ahora, ¡nos vemos la próxima vez! _estática de radio_

# Eternal DinoRPG

El trabajo en [eDinoRPG](https://dinorpg.eternaltwin.org/) continúa.

Completamos nuestra meta de fin de año al implementar el “raspador” DinoRPG,
una herramienta para archivar información del sitio web oficial. Nos dejará
archivar habilidades de Dinoz, dinoz sacrificados, y cualquier otra información
que la falte a la API. Ahora estamos trabajando en habilitarla en el servidor
de Eternaltwin.

¡Mientras tanto ya estamos preparando la versión 0.2.0!

Ni bien sea habilitada la importación de cuentas, también habilitaremos: una
pantalla de carga, pop-ups de errores, una página del perfil, un selector de
lenguaje, un sistema para mover los dinoz.

También estamos trabajando en tablas de clasificación y recompensas épicas.

Hasta que publiquemos todo, aquí hay una pequeña vista previa:

![Vista previa del movimiento en Eternal DinoRPG](./edinorpg.gif)

# ePopotamo

¡La versión alfa de [ePopotamo](https://epopotamo.eternaltwin.org/), el juego
de palabras a lo Scrabble, es la última adición a la lista de juegos Eternaltwin!
Ahora estamos oficialmente abiertos, pueden probarlo en [https://epopotamo.eternaltwin.org/](https://epopotamo.eternaltwin.org/).

Ahora mismo es una versión alfa. Significa que pueden revisarlo y reportar
problemas. Lo básico está ahí: un solo juego, colocar letras, revisar palabras,
opciones, puntajes, etc. A medida que pase el tiempo iremos mejorando el juego
hasta que esté completamente listo.

De nuevo, ¡por favor reporten cualquier problema que encuentren!

# Palabras finales

Lanzamos un par de juegos en 2021, y esperamos lanzar incluso más en 2022. ¡Gracias a todos por su ayuda!

Pueden mandar sus [mensajes para febrero](https://gitlab.com/eternaltwin/etwin/-/issues/39).

Este artículo fue editado por: Bibni, Biosha, Demurgos, Dylan57, Fabianh, Jonathan, MisterSimple, Nadawoo, Patate404 y Schroedinger.
