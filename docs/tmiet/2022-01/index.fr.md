# 2022-01 : Ce mois-ci sur Eternaltwin n°4

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Nous avons complété la migration de tous les services d'[Eternaltwin](https://eternaltwin.org/)
vers Rust.

Cette migration était le thème principal de 2021, et maintenant nous en tirons
les bénéfices en termes de fiabilité et performance.

## Publication v0.10.0

Nous avons publié la version **0.10.0** du [package Eternaltwin](https://www.npmjs.com/package/@eternal-twin/cli).
Nous avons également mis à jour [le client PHP](https://packagist.org/packages/eternaltwin/etwin).
Les clients pour les autres plateformes seront mis à jour progressivement.

Cette mise à jour a principalement corrigé des bugs mineurs. Vous pouvez lire
[tous les détails dans cette Merge Request](https://gitlab.com/eternaltwin/etwin/-/merge_requests/410).

## Prochaines fonctionnalités

Nous travaillerons à corriger les problèmes signalés, mettre à jour les
traductions et appliquer quelques modifications au niveau du style du site.

Les prochaines fonctionnalités seront liées à la gestion des comptes :
messages d'erreur plus utiles, correction des problèmes de redirection,
activation des réinitialisations de mot de passe, etc.

Nous avons aussi commencé à concevoir le système de récompenses (titres, pictos).

# Neoparc

Bonjour maîtres Dinoz de [Neoparc](https://neoparc.eternaltwin.org/) !

Tout d'abord, les Raids hebdomadaires ont été calibrés au niveau des horaires et
des stats du boss.

La plupart des compétences encore non implémentées ont été ajoutées sur le jeu.

Un total de 7 nouvelles compétences font leur entrée, dans le but d'uniformiser
le total théorique de chaque élément d'un Dinoz classique. À vous de les
découvrir !

Finalement, des ajouts ont été apportés au bazar. Les objets de la tanière secrète
peuvent maintenant être échangés via le bazar.

Bon jeu !

# Eternal Kingdom

[Eternal Kingdom](https://kingdom.eternaltwin.org/) vous permet désormais de
jouer avec votre compte Eternaltwin ! Amusez-vous bien et essayez de grimper
le classement !

## Ajouts principaux

Un super logo a débarqué (Réalisé par **Seidanoob#5281**)

L'historique de la capitale est là : plus besoin de mémoriser ce que vous
avez fait auparavant.

Les pages actuelles ont reçu une traduction anglaise et espagnole quasi complète
(merci à tout le monde)

Vous pouvez désormais recruter vos généraux 💯

Le pied de page a été mis à jour pour correspondre à Eternaltwin. 💃

Quelques pages ont commencé à faire leur apparition mais ne sont pour l'instant
pas complètement fonctionnelles (classement, choix du monde, ...).

## Améliorations principales

La plupart des actions du joueur ne nécessiteront plus de rafraichir la page.
Vous pouvez rester sur la page du choix de bâtiments et les boutons se mettront
à jour automatiquement. L'option de construction se rendra disponible à la
création d'un ouvrier (ou disparaîtra lors de la famine du dernier ouvrier).

En tant que joueur/testeur, n'hésitez pas à faire vos retours sur [le forum
Eternal Kingdom](https://eternaltwin.org/forum/sections/407e8a9e-8c5a-4177-b13c-9a7d746c7d24)
ou directement sur [le Discord](https://discord.gg/ERc3svy) dans
le salon **#kingdom**.

Si vous voulez nous aider à programmer, nous recherchons un développeur BACKEND
(Serveur en PHP / Symfony) ou un développeur FRONT (Web : HTML/JS/CSS).

Merci à tout le monde pour votre soutien 😄

# MyHordes

_grésillements de radio_

Bonjour tout le monde et merci d'utiliser notre système radiophonique sans fil !
Je suis Connor et aujourd'hui je vous adresse un petit message concernant
[MyHordes](https://myhordes.eu/).

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Vous le savez sûrement, ces derniers temps ont été plutôt calmes. J'espère que
vous avez pu vous reposer. Et _brouillé_

Sur un autre sujet, je tenais à vous annoncer que normalement vous devriez
trouver le site du jeu sur les moteurs de recherche sous peu. En espérant
qu'aucun zombie ne vienne en même temps... De plus _brouillé_

Les Conseils battent leur plein dans les villes. Le jeu a été globalement
optimisé et est plus rapide sur toutes les plateformes. Tout n'est pas parfait
mais les devs avancent à leur rythme, d'ailleurs en parlant d'eux je vous envoie
ce petit conseil, mais je ne peux pas vous prouver de son authenticité vu qu'il
a été copié d'innombrables fois.

---

_Brainbox_ : Bonjour à tous.

_Brainbox_ : La première chose à faire est de proposer du nouveau contenu
pour la saison 15. Des idées ?

_Shaitan_ : Des nouveaux pictos dorés !

_Adri_ : Moins de joueurs par ville, mais des villes plus courtes ?

_Brainboax_ : Donc... personne n'a de meilleures idées, alors ?

_Dylan57_ : Je propose qu'on retravaille tout le système des héros.

_Adri_ : Il est là lui ?

_Shaitan_ : Eh bien, ce n'est pas fameux non plus.

_Nayr_ : *héhé*

_Dylan57_ : Vous avez entendu ça ?

_La fatigue se fait sentir..._

_Brainbox_ : Eh bien puisque vous ne m'aidez pas, je vais regarder dans la boîte à idées.

_Shaitan_ : Oh non !

_Dylan57_ : Sérieusement !

_Adri_ : Le blaireau

_Brainbox_ : On procède au tirage au sort

_Brainbox_ : Chacun d'entre vous vient chercher une feuille où une idée est
écrite dans ma main SVP.

_Shaitan_ : Pourquoi on ferait pas un sondage pour changer ?

_Dylan57_ : Oui le génie, avec les 34 SALC par seconde...

_...Personne ne saura ce qu'il adviendra des feuilles. Mais le tirage au sort a été fait..._

---

C'était tout pour le moment, à la prochaine ! _grésillements_

# Eternal DinoRPG

Bonjour à tous et bonne année avec un peu de retard !

Le développement de [eDinoRPG](https://dinorpg.eternaltwin.org/) suit son cours.

Nous avons accompli nos objectifs pour la fin de l'année en terminant le
"scraper", un outil pour archiver les données du site officiel. Il permettra
d'archiver les compétences de vos dinoz, les dinoz sacrifiés ainsi
que tout ce qui n'est pas fourni par l'API. Nous travaillons à l'activer
au niveau du serveur Eternaltwin.

Pendant ce temps, nous préparons la version 0.2.0 !

Dès que l'import sera disponible, nous activerons aussi : un écran de
chargement, des pop-ups pour les erreurs, une page de profil, un sélecteur
de langue, le moteur de déplacement des dinoz.

Nous travaillons aussi sur les classements et récompenses épiques.

En attendant qu'on publie tout cela, voilà déjà un petit aperçu du tour de
Dinoland :

![Eternal DinoRPG - aperçu des déplacements](./edinorpg.gif)

# ePopotamo

La version alpha d'[ePopotamo](https://epopotamo.eternaltwin.org/),
le jeu de mots à-la-Scrabble vient enrichir la les jeux d'Eternaltwin !
Le jeu est officiellement ouvert, vous pouvez le tester
sur [https://epopotamo.eternaltwin.org/](https://epopotamo.eternaltwin.org/).

Il s'agit pour l'instant d'une version alpha, c'est-à-dire que son but est que
vous puissiez tester les fonctionnalités et nous faire remonter les bugs.
Les fonctions de base sont en place : une partie unique, pose des lettres,
validation des mots, options, scores, etc. Dans un second temps, le jeu sera
complété jusqu'à devenir 100 % opérationnel.

Une nouvelle fois, n'hésitez pas à nous faire remonter les bugs et anomalies que
vous rencontrez !

# Le mot de la fin

Nous avons publié quelques jeux en 2021, et nous espérons en publier encore plus
en 2022. Merci pour votre aide !

Vous pouvez envoyer [vos messages pour
l'édition de février](https://gitlab.com/eternaltwin/etwin/-/issues/39).

Cet article a été édité par : Bibni, Biosha, Demurgos, Dylan57, Fabianh,
Jonathan, MisterSimple, Nadawoo, Patate404 et Schroedinger.
