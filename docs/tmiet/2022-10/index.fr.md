# 2022-10 : Ce mois-ci sur Eternaltwin n°13

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

## Support de Crowdin

[Eternaltwin est maintenant sur Crowdin](https://eternaltwin.crowdin.com/). Crowdin
est un site pour collaborer sur les traductions. Crowdin nous soutient en nous
offrant un compte "Entreprise" gratuitement.

Si vous êtes un joueur souhaitant aide, vous pouvez désormais aider vos projets
favoris plus facilement. Si vous êtes un chef de jeu, veuillez contacter _Demurgos_
afin de configurer votre projet.

## Migration vers Rust

Le nouveau serveur Eternaltwin implémenté en Rust a été activé en octobre, et
le vieux serveur Node.js a été entièrement supprimé.

Le code d'Eternaltwin a été importé depuis Eternalfest, et datait d'environ 2015.
Il avait différents problèmes qui causaient des crashs réguliers. Afin de corriger
ça, un important travail interne a été démarré pour migrer vers Rust, qui est
plus performant et stable. Ça a été un succès : il n'y plus aucun crash et
les performances sont très bonnes.

Pendant cette migration, nous avions un grand nombre de vérifications et couches
de compatibilité entre l'ancien et nouveau code. Elles ont permis d'éviter les
défaillances, mais rendaient l'ajout de nouvelles fonctionnalités bien compliqué.
Maintenant que le travail de migration est bien fini, nous allons nous concentrer
sur des fonctionnalités annoncées depuis longtemps comme une meilleure gestion
des comptes (inscription par email, vérification d'email, réinitialisation de
mot de passe).

Voilà un diagramme de l'évolution des langages utilisés par Eternaltwin :

![Eternaltwin](./loc.svg)

# Eternal DinoRPG

Bonjour [maîtres dinoz](https://dinorpg.eternaltwin.org/) !

La version 0.4.0 de notre alpha ouverte est en ligne depuis quelques jours.
Vous pouvez toujours vous balader, combattre, monter en niveau et voir vos _moueffes_.

Visuellement, peu de choses ont changé mais un gros travail a été réalisé pour
faciliter le développement du jeu. Nous allons ainsi pouvoir sortir plus
fréquemment de nouvelles versions.

Cette version apporte quand même une grosse nouveauté : Les PNJ ! Pour le
moment seuls deux sont accesibles mais petit à petit de nouveaux feront leur
apparition. L'un de ces deux d'ailleurs peut vous donner des choses intéressantes.

A bientôt !

# eMush

Bienvenue à tous dans cette section de consacrés à
[eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

La dernière fois, nous vous avions annoncé le lancement de l'alpha de la
**Disease Update**, une mise à jour majeure d'eMush. Cette mise à jour complète
le système de maladies du jeu, avec **plus de 60 maladies et blessures**,
dont certaines qui ne fonctionnent pas sur Mush. De valeureux testeurs ont
depuis rejoint un vaisseau pour affronter ces nouveaux fléaux.

![eMush](./emush.png)

Grâce à leur aide, nous nous affairons à résoudre les anomalies génétiques
provoquées par la mise à jour (langue cisaillée qui repousse, Mushs allergiques
au Mush... 😱).

## Prochainement

En parallèle, nous travaillons également sur la prochaine mise à jour qui nous
permettra de lancer eMush en alpha ouverte. Nous nous concentrons pour ça sur
trois aspects principaux :

- les **migrations de notre base de données**, afin de déployer de nouvelles
  fonctionnalités sans détruire les vaisseaux en cours
- l'**archivage des vaisseaux terminés**, afin de garder les traces de vos exploits
- la **traduction du jeu en anglais**

Sur ce dernier point, grâce à l'aide de nombreux contributeurs pour récolter les
textes de Mush et traduire ceux absents, nous sommes passés en un mois à 65% du
jeu traduit en anglais.

## J'ai raté l'alpha ! Comment on joue ?

Selon la date où vous lisez cette news, l'alpha est peut-être toujours ouverte. 👀

Vous trouverez les instructions pour participer sur [le discord
Eternaltwin](https://discord.gg/SMJavjs3gk).

eMush étant un projet open-source, nos avancées sont également disponibles sur GitLab,
en particulier [les notes de mise à jour de la Disease Update](https://gitlab.com/eternaltwin/mush/mush/-/releases/0.4.4).

Merci de nous avoir lu, et à bientôt sur le Daedalus !

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour à vous tous, ici Ben pour vos infos du désert !

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

J'ai eu vent que de nombreux citoyens _brouillé_ citrouilles dans le désert.
Ce n'est pas la première année et risque de devenir un phénomène récurrent...
Mais cette fois on en a vues moins. Qu'est-ce que tout cela peut bien vouloir
dire ? _brouillé_

On a aussi entendu dire que certaines villes auraient arrêtées de se plier aux
règles, ou auraient inventé les leurs. _brouillé_ signale des faits d'empoisonnements,
citoyens courant dans le désert encombrés de nombreux sacs en même temps et
même... _bruissement de feuilles_ creusant au même endroit plusieurs fois ?
Quelle époque étrange _brouillé_

Oh, ça me rappelle que _brouillé_ avez sûrement remarqué le son clair de ma voix.
C'est grâce à la rénovation de notre station radio, avec du nouveau matos !
Nous continuons à tout en œuvre pour vous apporter les dernières infos du
_brouillé_ _BIIIIIIIIIP_ _bruit statique_ de quoi, l'antenne est en feu ???
OH MON D _brouillé_

# Eternal Kingdom

Voilà quelque temps déjà depuis le dernier message au sujet [d'Eternal Kingdom](https://kingdom.eternaltwin.org/).

Nous continuons de mettre à jour l'**alpha 0.4**. Nous avons corrigé de nombreux
soucis, [les détails sont sur le forum](https://eternaltwin.org/forum/threads/733e22e2-cbf8-474e-bd28-3183d67a2bc6).

Nous travaillons à deux choses en ce moment : la généraion de la carte et
quelques tâches de maintenance.

Nous avons revu l'outil de génération de la carte du monde. En particulier
manière dont nous affichons les bâtiments.

![Carte du monde Kingdom avec différents bâtiments](./kingdom.png)

Nous prévoyons aussi un peu de maintenance en prévision de l'alpha 0.5.
- Migrer le projet vers Symfony 5 (pour de meilleures performances)
- Configurer un projet Crowdin pour les traducteurs
- Ajouter des tests auto, pour corriger les bugs plus facilement.

Nous aimerions déployer la version alpha 0.5 en novembre.


# PiouPiouZ

Dans le monde des Piou, tout va pour le mieux.

Le développement d'un client de jeu sans Flash progresse bien. Les Piou peuvent
s'y mouvoir mais y sont pour le moment dépourvu de la plupart de leurs capacités
qui les aideront à terminer les niveaux.

[Voici un petit sneak peek de cette nouvelle version, jouable sans Flash](https://public.thetoto.fr/piou/pixi/).

_Aucun Piou n'a été maltraité durant le développement._

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de novembre](https://gitlab.com/eternaltwin/etwin/-/issues/55).

Cet article a été édité par : Bibni, Biosha, Brainbox, Demurgos, Evian, Fer,
Patate404 et Schroedinger.
