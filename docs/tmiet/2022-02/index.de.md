# 2022-02: Dieser Monat in Eternaltwin #5

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die
Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Im Rahmen der Archivierung und des Imports von DinoRPG-Daten kann Eternaltwin nun
automatisch Benutzersitzungen auf Twinoid und Twinoid-Spiele erstellen.
Dies ist ein wichtiger Schritt, um vollständige Twinoid-Archive zu ermöglichen.

# eMush

Im Februar haben wir uns auf die Verbesserung der [eMush](https://emush.eternaltwin.org/)
Benutzeroberfläche fokusiert.

Zu den neuen Funktionen gehören Feuereffekte, zerbrochene Gegenstände oder die Hervorhebung
für ausgewählte/übersehene Elemente. Das Fixieren der Anzeigereihenfolge der verschiedenen Elemente
in einem Raum war eine Herausforderung, ist aber jetzt gelöst. Wir müssen noch einige
Pfadfindungsprobleme beheben.

Dank dieser Fortschritte können wir die Deadalus-Räume nach und nach hinzufügen. Vielleicht können wir
bald einen vollständigen Daedalus erwarten? Ihr werdet es in der nächsten Ausgabe sehen.
Wir suchen noch Entwickler (Front und Back). Auch die letzten grafischen

Änderungen funktionieren aus technischer Sicht, aber es wäre toll, wenn jemand mit mehr Erfahrung im Bereich Grafik uns helfen könnte.
[Bonus: Feuereffekt-Video.](https://eternaltwin.org/assets/docs/tmie./emush.mp4)

# MyHordes

_Radioknacksen_ Hallo? Ist das Teil hier an? _rauschen_ Ich hoffe, ihr alle
könnt mich gut über unser wundervolles _rauschen_ tragungssystem verstehen.
Ich bin Ben und vertrete heute Connor, der sich trotz des leckeren Steaks,
das er vorhin gegessen hat, irgendwie total entkräftet fühlt... Was wohl mit
ihm los ist? _rauschen_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Für diejenigen unter euch, die Angst hatten, am Valentinstag alleine zu sein,
haben wir eine brandneue Freundesliste ins Spiel integriert. Sie kann aktuell
noch nicht allzu viel, aber wir werden in Zukunft weitere Funktionen hinzufügen.

Uns erreichen Berichte aus allen Landesteilen über Gruppen von Bürgern mit …
_Papierrascheln_… Schamanenmasken? Sie scheinen sich alle am 27. Februar in
einer Stadt versammeln zu wollen. Mehrere professionelle Schamanen in einer
Stadt… hat man so etwas schon gehört? _rauschen_

Abgesehen davon waren wir damit beschäftigt, unsere Servertechnologie auf die
neuste Version zu aktualisieren. Ich weis, das hört sich für all die
Nicht-Techniker da draußen sicherlich öde an, aber mit einem gut geschmierten
Unterbau können wir in sauberes Spielerlebnis für alle garantieren _rauschen_
zumindest sobald wir alle kleinen Problemchen mit dem Upgrade ausgebügelt haben.

Und damit sage ich Ciau und wünsche euch allen eine ereignisarme Nacht!
_rauschen_ So, wie schalte ich das Teil jetzt ab? An diesem Kabel zie _grrrrrrrrz_

# Eternalfest

[Eternalfest](https://eternalfest.net/) arbeitet an dem neuen Spielsystem.
Das Ziel ist es, Gegenstände und Quests vollständig zu unterstützen. Der erste
Schritt wird sein, das Berechtigungssystem zu aktualisieren und regulären
Benutzern das Testen von Leveln zu ermöglichen.

Der Lader (engl. loader) wurde aktualisiert. Diese neue Version hat eine
verbesserte Fehlerberichterstattung, die es Erstellern ermöglicht Abstürze
besser zu verstehen. Sie sollte bald veröffentlicht werden, zusammen mit einer neuen
Version des Spielecompilers, der ebenfalls die Fehlerberichterstattung verbessert.

# Neoparc

[Neoparc](https://neoparc.eternaltwin.org/) konzentriert sich auf interne
Korrekturen und Verbesserungen. Die wichtigste Änderung ist die Umstellung des
Datenbanksystems um die Leistung und Zuverlässigkeit zu verbessern. Wir wechseln
von ElasticSearch zu PostgreSQL. Diesen Monat haben wir einen Teil unserer Daten
erfolgreich übertragen: Nachrichten und Bazar-Listings. Dinoz und Konten
verwenden noch das alte System.

# Abschließende Worte

Du kannst [deine Nachrichten für März] (https://gitlab.com/eternaltwin/etwin/-/issues/40) senden.

Dieser Artikel wurde editiert von Brainbox, Breut, Demurgos, MisterSimple, Patate404 und Schroedinger.
