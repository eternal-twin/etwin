# 2022-02: Este mes en Eternaltwin #5

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Eternaltwin

Como parte del esfuerzo para archivar e importar información de DinoRPG,
Eternaltwin ahora puede automáticamente crear sesiones de usuario en Twinoid y
juegos Twinoid. Esto es un paso importante para habilitar archivos Twinoid
completos.

# eMush

En febrero, nos concentramos en mejorar la interfaz de usuario de
[eMush](https://emush.eternaltwin.org/).

Entre las nuevas funcionalidades, ahora tenemos efectos para fuego, objetos
rotos, o resaltado para objetos seleccionados/al pasar el ratón. Arreglar el
orden de muestra de varios elementos en una habitación fue desafiante, pero
ahora está resuelto. Aún necesitamos arreglar algunos problemas con la búsqueda
de caminos.

Todo este progreso nos permite agregar las habitaciones del Daedalus una por una.
¿Tal vez podamos esperar un Daedalus completo pronto? Ya verán en la siguiente
edición.

Seguimos buscando desarrolladores (front y back). Además, los últimos cambios
gráficos funcionan desde un punto de vista técnico, pero sería genial si
alguien con más experiencia en gráficos nos pudiera ayudar.

[Bonus: video de efectos de fuego](https://eternaltwin.org/assets/docs/tmie./emush.mp4).

# MyHordes

Reproducir a [MyHordes](https://myhordes.eu/).

_estática de radio_

¿Hola? ¿Está encendida esta cosa? _cortado_ Espero que todos puedan escucharme
por nuestro maravilloso _cortado_ sistema inalámbrico. Soy Ben, y estoy cubriendo
a Connor hoy, quien se siente extrañamente sin energía a pesar de haber comido
un sabroso bistec más temprano… ¿Me pregunto qué hay con eso? _cortado_

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Para aquellos de ustedes que temían estar solos en San Valentín, hemos añadido
un sistema de lista de amigos completamente nuevo. Sigue siendo un poco básico
ahora mismo, pero le iremos añadiendo funcionalidades adicionales a medida que
avancemos.

Ha habido avistamientos extraños de grandes grupos de gente con …_revuelve
papeles_… ¿máscaras de Chamán? Parece que planean reunirse en un pueblo el
**27 de febrero**. Múltiples chamanes profesionales en un pueblo… ¿quién ha
escuchado de tal cosa? _cortado_

Aparte de eso, hemos estado ocupados mejorando nuestra tecnología de servidores
a la última versión. Sé que no suena revolucionario para todos ustedes que no
son técnicos, pero una base técnica actualizada asegura una experiencia de juego
fluida y estable para todos _cortado_ al menos luego de que hayamos resuelto todos
los errores que vinieron con la actualización.

Y con eso, ¡les digo adiós a todos y les deseo una noche sin acontecimientos!
_cortado_ Entonces, ¿cómo apago esto? ¿Puedo simplemente cortar este ca _grrrrrrrrz_

# Eternalfest

[Eternalfest](https://eternalfest.net/) está trabajando en el nuevo sistema de
juego.  El objetivo final es completamente mantener objetos y misiones. El primer
paso será actualizar el sistema de permisos y permitir que usuarios regulares
prueben jugar niveles.

El cargador fue actualizado. Esta nueva versión ha mejorado el reportaje de
errores para ayudar a los creadores a entender fallos. Debería ser lanzado pronto,
junto con una nueva versión de compilado del juego que también mejora el
reportaje de errores.

# Neoparc

[Neoparc](https://neoparc.eternaltwin.org/) se está concentrando en arreglos
internos y mejoras. El cambio más importante es el cambio del sistema de base
de datos para mejorar desempeño y confiabilidad. Nos estamos mudando desde
ElasticSearch a PostgreSQL. Hemos exitosamente transferido parte de nuestra
información este mes: mensajes y listados del bazar. Dinoz y cuentas siguen
usando el sistema viejo.

# Palabras finales

Pueden mandar [sus mensajes para marzo](https://gitlab.com/eternaltwin/etwin/-/issues/40).
Este artículo fue editado por: Brainbox, Breut, Demurgos, MisterSimple, Patate404 y Schroedinger.
