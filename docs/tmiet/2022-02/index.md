# 2022-02: This Month In Eternaltwin #5

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

As part of the effort to archive and import DinoRPG data, Eternaltwin can now
automatically create user sessions on Twinoid and Twinoid games.
This is an important step to enable full Twinoid archives.

# eMush

In February, we focused on improving the [eMush](https://emush.eternaltwin.org/)
user interface.

Among the new features, we now have fire effects, broken items, or highlighting
for selected/hovered items. Fixing the display order of the various elements
in a room was challenging. but is now solved. We still need to fix some
pathfinding issues.

All this progress lets us add the Deadalus rooms one by one. Maybe we could
expect a full Daedalus soon? You'll see in the next edition.

We are still looking for developers (front and back). Also, the latest graphical
changes work from a technical point of view, but it would be great if someone
with more experience in graphics could help us.

[Bonus: fire effects video.](https://eternaltwin.org/assets/docs/tmie./emush.mp4)

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello? Is this thing on? _scrambled_ I hope you all can hear me over our
wonderful _scrambled_ wireless system. I’m Ben, and I’m filling in for Connor
today, who feels strangely unenergetic despite eating a tasty steak earlier...
I wonder what’s up with that? _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

For those of you who were scared to be alone on Valentine’s Day, we’ve added a
brand-new friend list system. It’s still a bit bare-bones right now, but we
will be adding additional features to it as we move on.

There have been strange sightings of large groups of people with …
_paper shuffle_… Shaman masks? It seems they plan to gather in a town on
the **27th of February**. Multiple professional shamans in one town… who has
ever heard of such a thing? _scrambled_

Other than that, we’ve been busy upgrading our server technology to the latest
version. I know it doesn’t sound revolutionary to y’all non-technicians out
there, but an up-to-date technical foundation ensures a smooth and stable
gameplay experience for everyone _scrambled_ at least after we’ve ironed out
all the hiccups that came with the upgrade.

And with that, I bid you all farewell and wish you an uneventful night!
_scrambled_ So, how do I turn this off? Can I just rip this co _grrrrrrrrz_

# Eternalfest

[Eternalfest](https://eternalfest.net/) is working on the new game
system. The end goal is to fully support items and quests. The first step
will be to update the permission system and allow regular users to playtest
levels.

The loader was updated. This new version has improved error reporting to help
creators understand crashes. It should be released soon, along with a new
game compiler version which also improves error reporting.

# Neoparc

[Neoparc](https://neoparc.eternaltwin.org/) is focusing on internal fixes and
improvements. The most import change is the change of the database system
to improve performance and reliability. We are moving from ElasticSearch to
PostgreSQL. We successfully transferred part of our data this month: messages
and bazar listings. Dinoz and accounts still use the old system.

# Closing words

You can send [your messages for March](https://gitlab.com/eternaltwin/etwin/-/issues/40).

This article was edited by: Brainbox, Breut, Demurgos, MisterSimple, Patate404, and Schroedinger.
