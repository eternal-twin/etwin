# Zenoo

**Hallo Zenoo! Vielen Dank, dass du dir ein paar Minuten Zeit nehmest um diese Fragen zu beantworten. Du hattest im Sommer 2022 mit der Übernahme von LaBrute einen großen Auftritt gehabt. Kannst du dich in ein paar Worten vorstellen?**

_Hallo, ich bin Zenoo, ein ehemaliger Hordes-Spieler aus den alten Tagen. Ich bin ein Vollzeit-Entwickler, mit anderen persönlichen Projekten auf der Seite, einschließlich LaBrute vor kurzem._

**Dein erstes Posting mit einem Mockup deiner Version von LaBrute war am 30. Juli und die erste Version 1.0.0 wurde am 10. August veröffentlicht. Wie habt ihr es geschafft, so schnell ein funktionierendes Spiel herauszubringen? Habt ihr ein Geheimnis zu verraten?**

_Wenn ich mir die Git-Historie ansehe, habe ich Anfang/Mitte August mit der Arbeit an diesem Projekt begonnen, aber die ersten 2-3 Wochen waren dem Entschlüsseln/Abrufen von Assets aus den MT-Quelldateien gewidmet (die damals noch nicht so verfügbar waren wie heute)._

_Ich habe also Ende August begonnen, das Projekt wirklich zu programmieren. Die v1 wurde bald darauf veröffentlicht, aber man sollte bedenken, dass diese v1 die erste funktionierende Version war, der aber noch eine Menge Dinge fehlten. Die Visualisierung der Kämpfe gab es zum Beispiel nicht, die Spieler hatten nur eine textuelle Ansicht dessen, was in den Kämpfen passierte._

_Was die Produktionsgeschwindigkeit angeht, so versuche ich, in meiner Methodik effizient zu sein, und ich organisiere meine Entwicklung streng. Alle Aufgaben sind in einem Arbeitsablauf gruppiert (verfügbar auf [Github](https://github.com/Zenoo/labrute/issues), falls ihr mir einmal helfen wollt/könnt), der es mir ermöglicht, alles zu vergessen, was ich zu tun habe, wenn ich eine Aufgabe beginne, und mich voll und ganz darauf zu konzentrieren._

**Wir können feststellen, dass du sehr intensiv an dem Projekt arbeitest, indem du Sitzungen abhälst**
![Interview - Git Graph of Zenoo](./interview_zenoo.png)

**LaBrute beginnt sich zu stabilisieren und zieht immer mehr Spieler an. Kannst du uns ein paar Statistiken über die Anzahl der erstellten Schläger geben?**

_Eigentlich neige ich dazu, auf Sparflamme zu laufen, wenn ich etwas anfange, das liegt wahrscheinlich an der Befriedigung, wenn ich ein Hindernis überwunden habe. Mit der Zeit werden die Hindernisse immer größer, was mich dazu bringt, aufzuhören und später wieder anzufangen, wenn der Hype vorbei ist und ich neue Ideen gefunden habe._

_Was die Statistiken betrifft, so kann ich leider keine für die gesamte Entwicklung liefern, da es mehrere Rücksetzungen gab. Seit dem letzten Reset (30. Januar) wurden insgesamt 11925 Brutes erstellt._

**Oh ja, in der Tat, fast 12k Brutes in weniger als einem Monat, das ist wirklich beeindruckend! Du hast vorhin gesagt, dass du ein ehemaliger Hordes-Spieler bist. Ich habe den Eindruck, dass du der Schöpfer von Zavatar bist, einer Website zur Erstellung von Avataren?**

_Wow, das ist eine lange Zeit her! Ich habe die V2 von Zavatars zusammen mit Tsha (einer Person mit einem verrückten Zeichentalent) entwickelt, die bereits einige Jahre vor der Einstellung von Zavatars V1 für das Projekt verantwortlich war._

_Die Website ist seit ihrer Eröffnung im April 2016 immer noch in Betrieb, wenn auch im Moment sehr ruhig. Im Gegensatz zur laufenden Entwicklung auf LaBrute wurde dieses Projekt in einem Rutsch entwickelt, ziemlich genau. Ich kann mich nicht daran erinnern, dass ich ein Update machen musste, um einen Fehler zu beheben, es war eine andere Zeit, mit einer viel einfacheren Entwicklung als heute 😄._

_Übrigens, es ist möglich, dass Zavatars eines Tages auf MyHordes erscheint, wer weiß?_

_...und nicht nur Zavatars!_

**Sehr gute Nachrichten für Zavatars! Und warum habt ihr als Hordians euch entschieden, LaBrute zu entwickeln, anstatt an MyHordes mitzuarbeiten? Haben euch die verwendeten Technologien nicht zugesagt?**

_Das stimmt, MyHordes ist in PHP entwickelt, einer Sprache, die ich lieber nicht mehr anrühre, ich habe sie vor zehn Jahren zu oft benutzt, ich finde sie zu rückständig im Vergleich zu dem, was wir heute haben._

**Was denkst du, wie weit bist du mit LaBrute schon gekommen? Hast du ein endgültiges Ziel, um zu sagen "Ich bin fertig", oder planst du, weitere Funktionen hinzuzufügen, auch wenn das bedeutet, v2 zu überarbeiten?**

_Es ist schwer, den Fortschritt von LaBrute einzuschätzen, einfach weil es sich um ein kontinuierliches Entwicklungsprojekt handelt, das die Rückmeldungen und Vorschläge der Benutzer berücksichtigt. Wenn man die Entwicklungsideen, die täglich eingehen, nicht mitzählt, würde ich sagen, dass ich derzeit bei 30% des Fortschritts bin._

_Ich habe nicht wirklich ein endgültiges Ziel, außer dass ich mindestens alle Funktionen haben möchte, die es vorher gab, und keine Bugs. Im Vergleich zu V2 ist diese neue Version eigentlich eine Mischung aus V1/V2, wobei ich versuche, das Beste von beiden zu erhalten. Ich plane auch, einige Funktionen hinzuzufügen, die weder in V1 noch in V2 enthalten waren._

**Stellen wir uns für einen Moment vor, dass wir deine Version von LaBrute als fertig betrachten. Du konntest alle interessanten Funktionen aus v1 und v2 hinzufügen. Welche Funktion oder welchen Spielmodus wirst du hinzufügen? Oder vielleicht gar nichts und wechselst zu einem anderen Spiel, um die Entwicklung zu beschleunigen?**

_Sobald V1 und V2 implementiert sind, denke ich darüber nach, ein Erfolgssystem, Quests, Verbesserungen des Turnierbelohnungssystems und Schläger-Opfer hinzuzufügen. Sobald das alles fertig ist, möchte ich mich mit DinoRPG beschäftigen, falls es noch nicht fertig ist. Diese Art von Spiel mit automatischen Kämpfen und einem täglichen Leveling-System ist für mich sehr befriedigend._

_Ich hätte später gerne an Hordes gearbeitet, aber da es bereits fertig ist, wird es nicht mehr dazu kommen. Ein anderes Spiel aus dem MT-Universum, das vor 10-15 Jahren meine Aufmerksamkeit erregte, war Zepirates, das das gleiche Prinzip des täglichen Levelns hat. Wenn ich in der Zwischenzeit nichts anderes finde, kann ich vielleicht dort einsteigen._

**So viele gute Nachrichten! Was war für dich bisher die größte Herausforderung bei der Entwicklung von LaBrute? Und was ist die Funktion, die dir am meisten Angst macht, die am schwersten zu integrieren ist?**

_Das größte Problem, das ich mit LaBrute habe, ist, dass es nicht einfach zu integrieren ist. Das größte Problem, auf das ich bisher gestoßen bin, war das Einrichten der Brute-Assets und ihre korrekte Darstellung in der Kampf-Engine. Das Erstellen der Sprites der Brutes mit benutzerdefinierten Erscheinungsbildern ist eine monumentale Aufgabe._

_Dies ist auch meine Antwort auf die zweite Frage. Um die Implementierung der Schläger-Sprites mit benutzerdefinierten Farben/Formen und die Implementierung der Waffen- und Schildanzeige abzuschließen, muss ich Hunderte, wenn nicht Tausende von Quelldateien erstellen, von denen jede die Logik der Anzeige der Farben/Formen auf jedem Frame jeder Animation enthält._

_**Das ist eine Riesenarbeit.**_

**In der Tat haben wir das gleiche Problem bei DinoRPG, und wir werden es nach und nach angehen, weil jeder Dinoz in der statischen Anzeige (außerhalb des Kampfes) bereits hundert Dateien darstellt. Letzte Frage: Kannst du uns sagen, was der Inhalt des nächsten großen Updates von LaBrute sein wird?**

_Es wird wahrscheinlich die Umsetzung der verschiedenen Ränge der Brutes sein, nachdem sie ein Turnier gewonnen haben._

**Gut! Danke für alle Antworten! Hast du noch etwas hinzuzufügen? 🙂**

_Schaut gerne im Discord vorbei, um mir euer Feedback und mir Fehler mitzuteilen. Es hilft dem Projekt voranzukommen._
