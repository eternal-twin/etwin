# Zenoo

**Hello Zenoo! Thank you for taking a few moments to answer these questions. You made a big entrance in the summer of 2022 by taking over LaBrute. Can you introduce yourself in a few words?**

_Hello, I'm Zenoo, a former Hordes player from the old days. I'm a full time developer, with other personal projects on the side, including LaBrute more recently._

**Your first post showing a mockup of your version of LaBrute was on July 30th and the first version 1.0.0 was released on August 10th. How did you manage to get a working game out so quickly? Do you have a secret to share?**

_Looking at the Git history, I really started working on this project in early/mid August, but the first 2-3 weeks were dedicated to decrypting/retrieving assets from the MT source files (at the time not yet available as they are today)._

_So actually I started to really code the project at the end of August. The v1 was released soon after, but it should be remembered that this v1 was the first functional version, but it was missing a lot of things. The visualization of the fights didn't exist, for example, the players only had a textual view of what was happening in the fights._

_As for the speed of production, I try to be efficient in my methodology, and I strictly organize my development. All tasks are grouped in a workflow (available on my [Github](https://github.com/Zenoo/labrute/issues), if you ever want to come and help), which allows me to forget about everything I have to do when I start a task, and to concentrate fully on it._

**Indeed we can notice that you are working on the project by sessions quite intensively. ![Interview - Git Graph of Zenoo](./interview_zenoo.png)**

**LaBrute starts to be stable and attracts more and more players. Can you give us some stats about the number of created bullies?**

_Actually, I tend to run on fumes when I start something, it's probably due to the satisfaction when I manage to overcome an obstacle. As time goes by, the obstacles get bigger and bigger, which makes me stop and start again later, once the hype is over and new ideas are found._

_As far as stats are concerned, I unfortunately can't provide any for the whole development, as there have been several resets. Since the last reset (January 30th), there has been a total of 11925 brutes created._

**Oh yes, indeed, almost 12k brutes in less than a month, that's really impressive! You said earlier that you were a former Hordes player. It seems to me that you are the creator of Zavatar, an avatar creation website?**

_Wow, that's a long time ago! I actually created the V2 of Zavatars with Tsha (a person with a crazy talent for drawing), who was already managing Zavatars V1 several years before it was abandoned._

_The site is still running since it opened in April 2016, although very quiet at the moment. Unlike the ongoing development on LaBrute, this project was developed in one go, pretty much. I don't remember having to do an update to fix a bug, it was a different time, with much simpler development than today 😄._

_By the way, it's possible that Zavatars will appear on MyHordes one day, who knows?_

_... And not only Zavatars!_

**Very good news for Zavatars! And as Hordians, why did you choose to develop LaBrute instead of helping on MyHordes? Didn't the technologies used suit you?**

_That's right, MyHordes is developed in PHP, a language that I prefer not to touch anymore, I used it too much ten years ago, I find it too backward compared to what we have nowadays._

_**What percentage of progress do you think you are at with LaBrute? Do you have a final goal to say "I'm done" or do you plan to add features even if it means redoing v2?**

_It's hard to estimate the progress of LaBrute, simply because it's a continuous development project, which takes into account feedbacks and suggestions from users. Not counting the development ideas that come in on a daily basis, I would say that I am currently at 30% of progress._

_I don't really have a final goal, except to have at least all the features that existed before, and no bugs. Compared to V2, this new version is actually a mix of V1/V2, trying to keep the best of both. I also plan to add some features that were neither in V1 nor in V2._

**Let's imagine for a minute that you consider your version of LaBrute finished. You have been able to add all the interesting features from v1 and v2. What feature or game mode will you add? Or maybe nothing and move to another game to speedrun the development?**

_Once V1 and V2 are implemented, I'm thinking of adding an achievement system, quests, improvements to the tournament reward system, and bully sacrifice. Once all this is done, if DinoRPG is not finished yet, I want to look into it. This kind of game with auto-battles and a daily leveling system is very satisfying for me._

_I would have done Hordes again later on, but since it's already managed, it won't happen. Another game out of the MT universe that caught my attention 10-15 years ago was Zepirates, which has the same daily leveling principle. If I don't find anything else in the meantime, maybe I can get into it._

**So much good news! What has been your biggest challenge on the development of LaBrute so far? And what is the feature that scares you the most, that seems to be the hardest to integrate?**

_The biggest problem I have with LaBrute is that it's not easy to integrate. The biggest problem I've encountered so far has been setting up the brute assets and displaying them correctly in the combat engine. Creating the brutes sprites with custom appearances is a monumental task._

_This is also my answer to the second question. Finishing the implementation of the bully sprites with custom colors/shapes, and the implementation of the weapon and shield display will require me to create hundreds if not thousands of source files, each one including the logic of displaying the colors/shapes on each frame of each animation._

_**Bref, a huge job.**_

**Indeed, we have the same problem on DinoRPG and we are going there little by little because each dinoz in static display (out of combat) represents already a hundred files. Last question : can you tell us what will be the content of the next big update of LaBrute?**

_It will probably be the implementation of the different ranks of brute, after winning a tournament._

**Great! Thanks for all your answers! Do you have anything else to add 🙂 .**

_Feel free to drop by the Discord to share your feedback and notify me of any bugs you encounter, it helps the project move forward._
