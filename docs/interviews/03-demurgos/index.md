# Demurgos

**Hello Demurgos! Thank you for taking a few moments to answer these questions. Most of Eternaltwin has already heard of you but can you introduce yourself?**

_Hello 👋_

_In real life my name is Charles and I'm a computer engineer, but on the Internet I'm better known as Demurgos for my Open Source contributions and my work on Eternalfest and Eternaltwin. I live in France (around Lyon) and I will be 28 in September. I like puzzle games, cycling and otters._

**You are one of the founding members of EternalTwin but also of EternalFest! Can you tell us more about this project which is already 10 years old?**

_Since I was a kid I've always played on a computer, but I only started to use the internet in the middle of the year 2000 when I was about 10 years old. It was then the golden age of the Flash games and I had a lot of fun on Miniclip or Armor Games. I quickly discovered Prizee (which was collaborating with MT), then Hammerfest as my first MT game. I immediately loved this platform game where you play a snowman for its arcade aspect and its mechanics (surprisingly complex). Hammerfest, thanks to its forum, is also the first internet community I joined. So it's a game I'm attached to._

**So you started playing Hammerfest almost from the beginning? Were you a person who used a lot of code to get flakes?**

_Yes, I joined Hammerfest at the very beginning (my id is "127" on Hammerfest; also I have id "38" on Twinoid: those are my lowest ids so I like to show off a bit with them even if it's not that important 😛 )._

_In fact, the "free to play" model was not very mature at the time. We had 5 games offered at the creation of the account then nothing. You had to pay. At the beginning I worked a lot with the reset of the account. 5 games, reset, and start again. I ended up buying an SMS code that gave me access to a few games a week. I then put Hammerfest aside for a while, and when I came back I had a bunch of games accumulated._

_Behind that I played many Motion-Twin games, and was an active member of Muxxu and then Twinoid. In 2012, streaming was becoming popular. Hammerfest being the most interesting MT game to watch live, there was a period where many people streamed their games. After being inactive on Hammerfest for a few years, this craze allowed me to reconnect with Hammerfest players. During a stream at the end of October 2012, I joined a Skype group that would change my life: "The Carrotian Hideout"._

**The Carrotian Hideout, isn't that the group of hammerfest geniuses you see in the game at the very beginning?**

_Yes, the name is based on that, in the official game it's "the Carrotian Council", but on Skype we were "the hideout"._

**After how long did you become the possessor of the carrot, the Holy Grail of the Hammerfestians?**

_I was pretty bad. I got my carrot pretty late. I'd have to find the date again but it might have been late 2012 after joining the Skype group (and thanks to their advice). Then it took me another 2-3 years to get the snow-o-glycerin (final quest of the game)._

**I'll let you continue the story.**

_I was also Hammerfest Augur (that was the interlocutor system with MT admins) and I started coding in 2010, in second grade (HTML/PHP), then I made browser extensions for Muxxu/Twinoid ("MotionMax") and made the interractive map OdysseyMap (like Fevermap but for Odyssey)._

_Hammerfest had an active community of level builders. There was no level editor, but people used Paint to draw their levels and then shared them on their blogs. In 2010 or so, a project caused quite a stir on Hammerfest: the player gary380 created the "Forgotten Temple", a hacked version of Hammerfest with new levels. The MT has banned the hack from being distributed._

_Towards the end of the year, the player GlobZOsiris (a very famous player, he was the first one to complete the final quest of the game) joined the Skype group. He had access to the Forgotten Temple and explained us how to play it and make our own levels. I was not enthusiastic because the MT had forbidden to play it, and the way it worked made it easy to cheat on the real game (rankings, quests). But the other members of the group loved it and started to create their own levels, send them to each other, play them, etc._

_In March 2013, I was the last person who still refused to play it. The MT has a habit of not touching its games once they are finished, so having the possibility to have new content was very tempting. So I decided to code my own system that would allow to share levels and play them, but the sharing would be automatic (website instead of sending ZIPs manually) and it would be isolated from the official website (no possibility to use it to send fake results to the official website). So I modified the loader, coded a basic hosting system and sent the whole thing to a free host; a big fiddle but it worked._

_And I finally shared this site with the group and posted this message :_

_**[05/03/2013 21:22:56] Demurgos: Ce sera eternalfest**_

_After March 2013, we worked a lot on the technical side (there were a lot of challenges, but we'll avoid getting lost in them). Where the Forgotten Temple failed to get MT approval, our goal was to have something much cleaner and have the right to share our work._

_During the summer we created a Twinoid account to share the progress: [Eternalfest](https://twinoid.com/user/7019378)._

_On Thursday, September 5th, we opened a topic on the forum to announce the project to the community: [Project Eternalfest](http://www.hammerfest.fr/forum.html/thread/476324/)_

_Since the MT had banned the Forgotten Temple, and what we were doing was technically not allowed by the rules, we were afraid to get banned. So we used an anonymous account and talked about us with aliases. It was a bit like a "secret society" (the pyramid logo of Eternalfest was also part of it...). It was fun 😛._

_We also posted a topic on Twinoid where we presented the game and asked to chat with the admins. I remember that it caused a lot of trouble for the moderators 😄._

_We sent a detailed mail to the MT describing how the game works and how to play it. After a few reminders, we finally got an answer at the end of 2013: "keep this to yourself, forbidden to broadcast"._

_It was a pity, but we accepted. We still continued to create levels, make live shows, share news. During the first three years, we created a few dozens of regions, redid the site 3 times (I was learning a lot about software), set up a system for modding the game, etc. Over time, the group members also became close friends._

_In 2015 or 2016, there was the following trigger: there was a big malfunction on the official server that made it inaccessible for a week. Since Blobzone, Conquisla or Pioupiouz had already closed, and Hammerfest was about ten years old (released on February 27, 2006 if I remember correctly), it was feared that this was the end of the game. Fortunately, the site was finally fixed, but it pushed us to review a bit the functioning:_

1. _Eternalfest had to be 100% independent from Hammerfest (until then it was using a browser extension that added a menu to the official site)_
2. _It was necessary to set up a solution to archive its account and the forum_.

_So we went from a basic level sharing system to a full site; this is the version we still have today. In the summer of 2017, we also decided to open Eternalfest in beta to the public. There was still no right to play (we still respect the MT's decision), but players were offered the possibility to link their account and create an archive of their fridge._

_There were between 100 and 200 people who joined this beta, it was really great to see these first users 🙂._

_In parallel to that there was already the story of the end of Flash announced for a long time. Mozilla had a project called Shumway to continue to play Flash files but without the plugin. Unfortunately this project has been abandoned. So I decided to take up this work with [Open Flash](https://open-flash.github.io/), a suite of tools to play the files without the need of the plugin. I was thinking to develop it enough to offer it to open-source projects, but to make it pay for companies (it was never done in the end, but it was my idea of activity after my studies)._

_So I was less active on Eternalfest and more focused on other projects, but developing my own tools to manipulate Flash allowed the next major feature: the ability to inject custom graphics (and thus complete the mods)._

_In 2017 and 2018, the Motion-Twin sites were less and less active. But I also joined a new communication platform, Discord. I found there a server dedicated to the game with a good community of new players it revived the interest in Hammerfest/Eternalfest and posed many debates. Since 2013, we respected the MT's decision not to broadcast Eternalfest; but it was still a shame. We had a great project that we had been working on for 5 years, we were proud of it, but we were not allowed to share it._

_We also had feedback from MT admins or ex-MTs who liked the project in a personal way; but no official answer. We tried to follow up with emails, but still no response... After much discussion, we finally made the decision to open Eternalfest to the public; and stay in a "grey" area where we don't have explicit permission, but hope to stay out of trouble. In retrospect, it seems obvious, but it wasn't an easy decision._

_So the first country published was "Eternoel" at Christmas 2018. It was a huge success. Because of our huge back-catalog, we started the process of publishing all our games. With 1 new game (county) every 2 weeks or so for 2-3 years. While the publishing process was taking place, I went back to my personal projects (Open Flash) and the archiving of the forum._

_But on March 27, 2020 Motion-Twin posted an announcement: they will close their games due to the end of Flash. And that turned my plans upside down (beginning of Eternaltwin). Since then I was mainly focused on Eternaltwin, which caused a pause in the new features for Eternalfest._

_But I was able to spend a few months on Eternalfest again at the end of 2022 and in January 2023 we released a new major update that added fridge management for Eternalfest games (until then fridges were not visible) + a complete redesign of the internal part of the site that had not really moved since 2018._

_We're coming up to March 5th 2023, where we'll celebrate the 10th anniversary of this adventure 🙂._

**Thanks for sharing your story Demu, it's really interesting to see how much has been done and how we got here. So Eternalfest is really a part of your life. Did it help you professionally or is it the other way around?**

_Yes, I think Eternalfest has been a huge plus professionally. It helped me in many ways :_

1. _**Technical skills**. You learn by doing, and it allowed me to practice a lot: security, software architecture, DB management, deployments, compiling, etc._

2. _**Open Source**. Working on Eternalfest has been my gateway to Open Source in a broad sense. When I have a problem on a personal project, I will fix the problem at the source. I was able to become very active in Node tools by becoming a maintainer of Gulp, a member of Babel, or Typed (the ancestor of DefinitelyTyped, I wrote the first TypeScript definitions for the big Node libs). I wrote the test coverage report merge algo for V8 (used by Deno and c8). More recently I contributed to Postgresql and SQLx. These are all big and well known projects in the web, I'm proud to have worked on them ; and it helps to find a job 😛._

3. _**Collaboration**. I started Eternalfest/Eternaltwin but I was never alone. These projects have been possible thanks to the help of many, many people. For that we had to find how to share the work, make compromises, manage expectations and disagreements. There are a dozen active members for Eternalfest, and more than sixty contributors for Eternaltwin. Working as a team is a big plus._

4. _**Publish**. Starting a project is relatively easy, but publishing it and maintaining it behind is much harder. Succeeding in this has also helped me professionally. I would like to congratulate all the contributors who were able to release their game, even in beta. You are very strong._

**But how do you manage to divide your time between your work, the development of Eternalfest, Eteraltwin and your other occupations?**

_It's complicated unfortunately, the days are too short. First of all, real life has priority: if family or friends want to spend time together, I never refuse. Secondly, I have the chance to work completely from home, so I avoid transportation. I work until 5-6pm; then I have some free time. I work on Eternalfest/Eternaltwin in the evening, sometimes quite late; and on weekends. When I'm alone, I don't take much free time (that's also why I'm always willing to participate with people). It also depends on the periods, sometimes there is more work; sometimes there is less. But I don't complain too much, I like what I do. An important point is that I avoid deadlines for my projects: it stresses me out. In general, it's important that participating in Eternaltwin remains a pleasure, you shouldn't force yourself._

_My priority on Eternaltwin is to give a maximum of autonomy to the different teams. Since I don't have much time, I don't want to block the progress of others. The best example for this is the implementation of the new deployment system in the summer of 2022: before I had to deploy manually, after that the teams could send the updates themselves._

**Do you have enough time to see otters?**

_The "otter" part of the Lyon zoo was closed because of the Covid, so I couldn't see them ; and since then I haven't been back there 😭._

**A last word?**

_If there are any who are looking for work right now and have contributed to Eternaltwin, I am willing to support your work / make cover letters if you are interested. I am amazed at the work that has been done and would love to help you if possible._
