# SQLite

The SQLite database engine is offered as an alternative to Postgres. Postgres is used in production, SQLite is intended
to help with local development.

SQLite is missing a few features compared to Postgres. This article provides guidance when adapting SQL queries
written for Postgres so they are compatible with SQLite.

## UUID

```postgresql
-- Postgresql
create table t (
  "foo" uuid not null
)
```

```sqlite
-- Postgresql
create table t (
  "foo" blob not null check (length(foo) = 16)
)
```

## Range, Period

- [PostgreSQL 16 - Range Types](https://www.postgresql.org/docs/16/rangetypes.html)
- [PostgreSQL 16 - Range/Multirange Functions and Operators](https://www.postgresql.org/docs/current/functions-range.html)

Eternaltwin uses ranges extensively. The `period` types in particular are defined as ranges of timestamps and used
to track when values are valid. For SQLite, replace columns of type `range` with a pair of columns with the `_start`
and `_end` suffixes and a check for the order.

### PeriodLower

```postgresql
-- PostgreSQL
create table t (
  "foo" period_lower not null
)
```

```sqlite
-- SQLite
create table t (
  "foo_start" text not null check (foo_start = strftime('%FT%T+00:00', foo_start)),
  "foo_end" text null check (foo_end = strftime('%FT%T+00:00', foo_end)),
  constraint foo_ck check (foo_start < foo_end)
)
```

### Membership

```postgresql
-- PostgreSQL
select ... where range @> item;
```

```sqlite
-- SQLite
select ... where range_start <= item and item < range_end;
```

### Overlap

```postgresql
-- PostgreSQL
select ... where range0 && range1;
```

```sqlite
-- SQLite
select ... where range0_start < range1_end && range1_start < range0_end;
-- variant with parameters
select ... where ? < range1_end && range1_start < ?;
```

## Enum

Use a text field with checked values.
