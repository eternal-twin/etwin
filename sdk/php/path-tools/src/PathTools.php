<?php declare(strict_types=1);

namespace Eternaltwin\PathTools;

abstract class PathTools {
  public abstract static function join(string ...$paths): string;

  public abstract static function isAbsolute(string $path): bool;

  public abstract static function relative(string $from, string $to): string;

  /**
   * Normalize inner segments
   */
  protected static function normalizeInner(string $path, string $separator, bool $allowExtraParents, callable $isSeparator) {
    $pathLen = strlen($path);
    $result = [];
    $dots = 0; // Consecutive dots in the current segment, `null` if non-dots or more than 2 dots
    $segmentStart = 0; // Start index of the current segment
    $parent = 0; // Extra parent paths
    for ($i = 0; $i < $pathLen + 1; $i++){
      $chr = $i < $pathLen ? $path[$i] : $separator;
      if ($isSeparator($chr)) {
        if ($dots !== null && $dots < 2) {
          // 0 or 1 dot: no-op
        } else if ($dots === 2) {
          if (empty($result)) {
            $parent += 1;
          } else {
            array_pop($result);
          }
        } else {
          $segment = substr($path, $segmentStart, $i - $segmentStart);
          $result[] = $segment;
        }
        $dots = 0;
        $segmentStart = $i + 1;
      } else if ($chr === "." && $dots !== null) {
        $dots = $dots < 2 ? $dots + 1 : null;
      } else {
        $dots = null;
      }
    }
    if (!$allowExtraParents) {
      $parent = 0;
    }
    return str_repeat(".." . $separator, $parent) . implode($separator, $result);
  }
}
