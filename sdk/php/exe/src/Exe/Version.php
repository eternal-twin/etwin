<?php declare(strict_types=1);

namespace Eternaltwin\Exe;

final class Version {
  // `0.0.0` is used during development
  // the `publish.sh` scripts writes the true value on publication
  const VERSION = "0.0.0";
}
