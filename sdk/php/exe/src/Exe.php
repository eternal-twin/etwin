<?php declare(strict_types=1);

namespace Eternaltwin;

final class Exe {
  public static function path(): string {
    throw new \Error("package `eternaltwin/exe` was not built");
  }
}
