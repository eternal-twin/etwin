use async_trait::async_trait;
use axum::extract::FromRequest;
use axum::http::{header, HeaderMap, HeaderName, HeaderValue, StatusCode};
use axum::response::{IntoResponse, Response};
use eternaltwin_core::types::{DisplayErrorChain, WeakError};

pub struct ProstError(WeakError);

impl IntoResponse for ProstError {
  fn into_response(self) -> Response {
    let http_status = StatusCode::UNPROCESSABLE_ENTITY;
    let pb_status = tonic::Code::InvalidArgument;
    let grpc_status = tonic::Status::new(pb_status, DisplayErrorChain(&self.0).to_string());
    let mut headers = HeaderMap::new();
    headers.insert(
      header::CONTENT_TYPE,
      header::HeaderValue::from_static("application/grpc"),
    );
    // tonic uses an old `http` version, so we need to handle compat here to emit the proper headers for the status
    let mut legacy_headers = HeaderMap::new();
    grpc_status
      .add_header(&mut legacy_headers)
      .expect("adding grpc headers always succeeds");
    for (key, value) in legacy_headers.iter() {
      headers.insert(
        HeaderName::from_bytes(key.as_str().as_bytes()).expect("converting to a header name succeeds"),
        HeaderValue::from_bytes(value.as_bytes()).expect("converting to a header value succeeds"),
      );
    }
    (http_status, headers).into_response()
  }
}

#[derive(Debug, Clone, Copy, Default)]
#[must_use]
pub struct Prost013<T>(pub T);

#[async_trait]
impl<T, S> FromRequest<S> for Prost013<T>
where
  T: prost::Message + Default,
  S: Send + Sync,
{
  type Rejection = ProstError;

  async fn from_request(req: axum::extract::Request, state: &S) -> Result<Self, Self::Rejection> {
    let mut bytes = bytes::Bytes::from_request(req, state)
      .await
      .map_err(|e| ProstError(WeakError::wrap(e)))?;

    match T::decode(&mut bytes) {
      Ok(value) => Ok(Prost013(value)),
      Err(err) => Err(ProstError(WeakError::wrap(err))),
    }
  }
}

impl<T> IntoResponse for Prost013<T>
where
  T: prost::Message + Default,
{
  fn into_response(self) -> Response {
    let mut buf = bytes::BytesMut::with_capacity(128);
    match &self.0.encode(&mut buf) {
      Ok(()) => buf.into_response(),
      Err(err) => (StatusCode::INTERNAL_SERVER_ERROR, err.to_string()).into_response(),
    }
  }
}
