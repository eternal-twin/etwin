use crate::http::WithAuthError;
use async_trait::async_trait;
use auto_impl::auto_impl;
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::password::Password;
use eternaltwin_core::user::{ShortUser, UserId, Username};

#[cfg(feature = "http")]
pub mod http;
#[cfg(feature = "mem")]
pub mod mem;

pub enum EtwinAuth {
  Guest,
  Session(String),
  Token(String),
  Credentials { username: Username, password: Password },
}

#[derive(Debug, thiserror::Error)]
pub enum GetSelfError {
  #[error("failed to apply authentication")]
  WithAuth(#[from] WithAuthError),
  #[error("failed to send request")]
  Send(#[source] reqwest::Error),
  #[error("failed to receive response")]
  Receive(#[source] reqwest::Error),
}

#[derive(Debug, thiserror::Error)]
pub enum GetUserError {
  #[error("failed to apply authentication")]
  WithAuth(#[from] WithAuthError),
  #[error("failed to send request")]
  Send(#[source] reqwest::Error),
  #[error("failed to receive response")]
  Receive(#[source] reqwest::Error),
}

#[async_trait]
#[auto_impl(&, Arc)]
pub trait EtwinClient: Send + Sync {
  async fn get_self(&self, auth: &EtwinAuth) -> Result<AuthContext, GetSelfError>;

  async fn get_user(&self, auth: &EtwinAuth, user_id: UserId) -> Result<ShortUser, GetUserError>;
}
