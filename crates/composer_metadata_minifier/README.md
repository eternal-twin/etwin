# Rust port of composer/metadata-minifier

This is a Rust port of [composer/metadata-minifer](https://github.com/composer/metadata-minifier).
Any divergence is a bug, please report it.
