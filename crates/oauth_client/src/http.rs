use crate::{GetAccessTokenError, RfcOauthClient};
use ::url::Url;
use async_trait::async_trait;
use eternaltwin_core::oauth::RfcOauthAccessToken;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::time::Duration;

const USER_AGENT: &str = "EtwinOauth";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpRfcOauthClientOptions {
  pub authorization_endpoint: Url,
  pub token_endpoint: Url,
  pub callback_endpoint: Url,
  pub client_id: String,
  pub client_secret: String,
}

impl HttpRfcOauthClientOptions {
  pub fn authorization_uri(&self, scope: &str, state: &str) -> Url {
    let mut url = self.authorization_endpoint.clone();
    url
      .query_pairs_mut()
      .append_pair("access_type", "offline")
      .append_pair("response_type", "code")
      .append_pair("redirect_uri", self.callback_endpoint.as_str())
      .append_pair("client_id", self.client_id.as_str())
      .append_pair("scope", scope)
      .append_pair("state", state);
    url
  }
}

pub struct HttpRfcOauthClient {
  options: HttpRfcOauthClientOptions,
  client: Client,
}

impl HttpRfcOauthClient {
  pub fn new(options: HttpRfcOauthClientOptions) -> Self {
    Self {
      options,
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()
        .expect("building the client always succeeds"),
    }
  }
}

#[async_trait]
impl RfcOauthClient for HttpRfcOauthClient {
  fn authorization_uri(&self, scope: &str, state: &str) -> Url {
    self.options.authorization_uri(scope, state)
  }

  fn authorization_server(&self) -> &str {
    self
      .options
      .authorization_endpoint
      .host_str()
      .expect("invalid authorization endpoint")
  }

  async fn get_access_token(&self, code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError> {
    let res = self
      .client
      .post(self.options.token_endpoint.clone())
      .basic_auth(
        self.options.client_id.as_str(),
        Some(self.options.client_secret.as_str()),
      )
      .form(&OauthAccessTokenRequest {
        client_id: self.options.client_id.clone(),
        client_secret: self.options.client_secret.clone(),
        redirect_uri: self.options.callback_endpoint.clone(),
        code: code.to_string(),
        grant_type: RfcOauthGrantType::AuthorizationCode,
      })
      .send()
      .await
      .map_err(GetAccessTokenError::SendRequest)?;

    // TODO: Check content type (application/json or text/html)
    let body = res.bytes().await.map_err(GetAccessTokenError::ReceiveResponse)?;
    // TODO: Don't hardcode specific error format
    if body.as_ref() == br#"{"error":"invalid_grant"}"# {
      return Err(GetAccessTokenError::InvalidGrant);
    }
    let parsed: RfcOauthAccessToken = serde_json::from_slice(&body)
      .map_err(|e| GetAccessTokenError::ParseResponse(e, String::from_utf8_lossy(&body).to_string()))?;
    Ok(parsed)
  }
}

#[cfg(feature = "neon")]
impl neon::prelude::Finalize for HttpRfcOauthClient {}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct OauthAccessTokenRequest {
  client_id: String,
  client_secret: String,
  redirect_uri: Url,
  code: String,
  grant_type: RfcOauthGrantType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum RfcOauthGrantType {
  #[serde(rename = "authorization_code")]
  AuthorizationCode,
}
