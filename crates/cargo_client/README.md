# Cargo registry client

This cargo registry client is currently used for the needs of [https://eternaltwin.org/](https://eternaltwin.org/).
If you want to claim the name, send an email at `contact@eternaltwin.org`.
