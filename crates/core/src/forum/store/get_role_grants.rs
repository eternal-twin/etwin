use crate::core::Request;
use crate::forum::{ForumSectionIdRef, RawForumRoleGrant};
use crate::types::WeakError;
use crate::user::UserIdRef;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetRoleGrants {
  pub section: ForumSectionIdRef,
  pub user: Option<UserIdRef>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetRoleGrantsError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetRoleGrantsError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetRoleGrants {
  type Response = Result<Vec<RawForumRoleGrant>, GetRoleGrantsError>;
}
