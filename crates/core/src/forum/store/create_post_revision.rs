use crate::core::Request;
use crate::forum::{
  ForumPostIdRef, ForumPostRevisionComment, ForumPostRevisionContent, RawCreateForumPostRevisionResult, RawForumActor,
};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreatePostRevision {
  pub actor: RawForumActor,
  pub post: ForumPostIdRef,
  pub body: Option<ForumPostRevisionContent>,
  pub mod_body: Option<ForumPostRevisionContent>,
  pub comment: Option<ForumPostRevisionComment>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreatePostRevisionError {
  #[error(transparent)]
  Other(WeakError),
}

impl CreatePostRevisionError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for CreatePostRevision {
  type Response = Result<RawCreateForumPostRevisionResult, CreatePostRevisionError>;
}
