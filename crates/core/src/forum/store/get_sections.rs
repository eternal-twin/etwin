use crate::core::{Listing, Request};
use crate::forum::RawForumSectionMeta;
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetSections {
  pub offset: u32,
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetSectionsError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetSectionsError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetSections {
  type Response = Result<Listing<RawForumSectionMeta>, GetSectionsError>;
}
