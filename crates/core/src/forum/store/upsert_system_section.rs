use crate::core::{LocaleId, Request};
use crate::forum::{ForumSection, ForumSectionDisplayName, ForumSectionKey};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpsertSystemSection {
  pub key: ForumSectionKey,
  pub display_name: ForumSectionDisplayName,
  pub locale: Option<LocaleId>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum UpsertSystemSectionError {
  #[error(transparent)]
  Other(WeakError),
}

impl UpsertSystemSectionError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for UpsertSystemSection {
  type Response = Result<ForumSection, UpsertSystemSectionError>;
}
