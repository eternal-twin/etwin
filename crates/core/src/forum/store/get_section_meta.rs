use crate::core::Request;
use crate::forum::{ForumSectionRef, RawForumSectionMeta};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetSectionMeta {
  pub section: ForumSectionRef,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetSectionMetaError {
  #[error("section not found")]
  NotFound,
  #[error(transparent)]
  Other(WeakError),
}

impl GetSectionMetaError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetSectionMeta {
  type Response = Result<RawForumSectionMeta, GetSectionMetaError>;
}
