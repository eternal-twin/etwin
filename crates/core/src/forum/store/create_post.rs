use crate::core::Request;
use crate::forum::{ForumPostRevisionContent, ForumThreadRef, RawCreateForumPostResult, RawForumActor};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreatePost {
  pub actor: RawForumActor,
  pub thread: ForumThreadRef,
  pub body: ForumPostRevisionContent,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreatePostError {
  #[error(transparent)]
  Other(WeakError),
}

impl CreatePostError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for CreatePost {
  type Response = Result<RawCreateForumPostResult, CreatePostError>;
}
