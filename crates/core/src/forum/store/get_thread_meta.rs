use crate::core::Request;
use crate::forum::{ForumThreadRef, RawForumThreadMeta};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetThreadMeta {
  pub thread: ForumThreadRef,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetThreadMetaError {
  #[error("thread not found")]
  NotFound,
  #[error(transparent)]
  Other(WeakError),
}

impl GetThreadMetaError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetThreadMeta {
  type Response = Result<RawForumThreadMeta, GetThreadMetaError>;
}
