extern crate core;

use ::opentelemetry::logs::{self, LoggerProvider};
use ::opentelemetry::trace::{self, Span, SpanContext, Status, Tracer, TracerProvider};
use ::opentelemetry::{Context, InstrumentationLibrary, KeyValue};
use core::fmt;
use opentelemetry::trace::{SpanId, TraceFlags, TraceId, TraceState};
use std::borrow::Cow;
use std::cmp::Ordering;

use opentelemetry::logs::{AnyValue, LogRecord, Logger, Severity};
use opentelemetry::Key;
use std::ops::Deref;
use std::str::FromStr;
use std::sync::Arc;
use std::time::SystemTime;

#[derive(Debug, Clone)]
pub struct ArcDynTracerProvider {
  pub provider: Arc<DynTracerProvider>,
}

impl ArcDynTracerProvider {
  pub fn new(provider: DynTracerProvider) -> Self {
    Self {
      provider: Arc::new(provider),
    }
  }

  /// Create a new dyn tracer using a noop impl
  pub fn noop() -> Self {
    Self::new(DynTracerProvider::noop())
  }

  /// Create a new dyn tracer using a regular impl
  pub fn regular(provider: opentelemetry_sdk::trace::TracerProvider) -> Self {
    Self::new(DynTracerProvider::regular(provider))
  }
}

impl Deref for ArcDynTracerProvider {
  type Target = DynTracerProvider;

  fn deref(&self) -> &Self::Target {
    self.provider.deref()
  }
}

impl TracerProvider for ArcDynTracerProvider {
  type Tracer = DynTracer;

  fn library_tracer(&self, library: Arc<InstrumentationLibrary>) -> Self::Tracer {
    let provider: &DynTracerProvider = self.deref();
    provider.library_tracer(library)
  }
}

#[derive(Debug)]
pub enum DynTracerProvider {
  Regular(opentelemetry_sdk::trace::TracerProvider),
  Noop(trace::noop::NoopTracerProvider),
}

impl DynTracerProvider {
  /// Create a new noop dyn tracer
  pub fn noop() -> Self {
    Self::Noop(trace::noop::NoopTracerProvider::new())
  }

  /// Create a new regular dyn tracer
  pub fn regular(provider: opentelemetry_sdk::trace::TracerProvider) -> Self {
    Self::Regular(provider)
  }
}

impl TracerProvider for DynTracerProvider {
  type Tracer = DynTracer;

  fn library_tracer(&self, library: Arc<InstrumentationLibrary>) -> Self::Tracer {
    match self {
      Self::Regular(provider) => DynTracer::Regular(provider.library_tracer(library)),
      Self::Noop(provider) => DynTracer::Noop(provider.library_tracer(library)),
    }
  }
}

#[derive(Debug)]
pub enum DynTracer {
  Regular(opentelemetry_sdk::trace::Tracer),
  Noop(trace::noop::NoopTracer),
}

impl Tracer for DynTracer {
  type Span = DynSpan;

  fn build_with_context(&self, builder: trace::SpanBuilder, parent_cx: &Context) -> Self::Span {
    match self {
      Self::Regular(tracer) => DynSpan::Regular(tracer.build_with_context(builder, parent_cx)),
      Self::Noop(tracer) => DynSpan::Noop(tracer.build_with_context(builder, parent_cx)),
    }
  }
}

#[derive(Debug)]
#[allow(clippy::large_enum_variant)]
pub enum DynSpan {
  Regular(opentelemetry_sdk::trace::Span),
  Noop(trace::noop::NoopSpan),
}

impl Span for DynSpan {
  fn add_event_with_timestamp<T>(&mut self, name: T, timestamp: SystemTime, attributes: Vec<KeyValue>)
  where
    T: Into<Cow<'static, str>>,
  {
    match self {
      Self::Regular(span) => Span::add_event_with_timestamp(span, name, timestamp, attributes),
      Self::Noop(span) => Span::add_event_with_timestamp(span, name, timestamp, attributes),
    }
  }

  fn span_context(&self) -> &SpanContext {
    match self {
      Self::Regular(span) => Span::span_context(span),
      Self::Noop(span) => Span::span_context(span),
    }
  }

  fn is_recording(&self) -> bool {
    match self {
      Self::Regular(span) => Span::is_recording(span),
      Self::Noop(span) => Span::is_recording(span),
    }
  }

  fn set_attribute(&mut self, attribute: KeyValue) {
    match self {
      Self::Regular(span) => Span::set_attribute(span, attribute),
      Self::Noop(span) => Span::set_attribute(span, attribute),
    }
  }

  fn set_status(&mut self, status: Status) {
    match self {
      Self::Regular(span) => Span::set_status(span, status),
      Self::Noop(span) => Span::set_status(span, status),
    }
  }

  fn update_name<T>(&mut self, new_name: T)
  where
    T: Into<Cow<'static, str>>,
  {
    match self {
      Self::Regular(span) => Span::update_name(span, new_name),
      Self::Noop(span) => Span::update_name(span, new_name),
    }
  }

  fn add_link(&mut self, span_context: SpanContext, attributes: Vec<KeyValue>) {
    match self {
      Self::Regular(span) => Span::add_link(span, span_context, attributes),
      Self::Noop(span) => Span::add_link(span, span_context, attributes),
    }
  }

  fn end_with_timestamp(&mut self, timestamp: SystemTime) {
    match self {
      Self::Regular(span) => Span::end_with_timestamp(span, timestamp),
      Self::Noop(span) => Span::end_with_timestamp(span, timestamp),
    }
  }
}

#[derive(Debug, Clone)]
pub struct ArcDynLoggerProvider {
  pub provider: Arc<DynLoggerProvider>,
}

impl ArcDynLoggerProvider {
  pub fn new(provider: DynLoggerProvider) -> Self {
    Self {
      provider: Arc::new(provider),
    }
  }

  /// Create a new dyn logger using a noop impl
  pub fn noop() -> Self {
    Self::new(DynLoggerProvider::noop())
  }

  /// Create a new dyn logger using a regular impl
  pub fn regular(provider: opentelemetry_sdk::logs::LoggerProvider) -> Self {
    Self::new(DynLoggerProvider::regular(provider))
  }
}

impl Deref for ArcDynLoggerProvider {
  type Target = DynLoggerProvider;

  fn deref(&self) -> &Self::Target {
    self.provider.deref()
  }
}

impl LoggerProvider for ArcDynLoggerProvider {
  type Logger = DynLogger;

  fn library_logger(&self, library: Arc<InstrumentationLibrary>) -> Self::Logger {
    let provider: &DynLoggerProvider = self.deref();
    provider.library_logger(library)
  }
}

#[derive(Debug)]
pub enum DynLoggerProvider {
  Regular(opentelemetry_sdk::logs::LoggerProvider),
  Noop(logs::NoopLoggerProvider),
}

impl DynLoggerProvider {
  /// Create a new noop dyn logger
  pub fn noop() -> Self {
    Self::Noop(logs::NoopLoggerProvider::new())
  }
  /// Create a new regular dyn logger
  pub fn regular(provider: opentelemetry_sdk::logs::LoggerProvider) -> Self {
    Self::Regular(provider)
  }
}

impl LoggerProvider for DynLoggerProvider {
  type Logger = DynLogger;

  fn library_logger(&self, library: Arc<InstrumentationLibrary>) -> Self::Logger {
    match self {
      Self::Regular(provider) => DynLogger::Regular(provider.library_logger(library)),
      Self::Noop(provider) => DynLogger::Noop(provider.library_logger(library)),
    }
  }
}

#[derive(Debug)]
pub enum DynLogger {
  Regular(opentelemetry_sdk::logs::Logger),
  Noop(<logs::NoopLoggerProvider as LoggerProvider>::Logger),
}

impl Logger for DynLogger {
  type LogRecord = DynLogRecord;

  fn create_log_record(&self) -> Self::LogRecord {
    match self {
      Self::Regular(logger) => DynLogRecord::Regular(logger.create_log_record()),
      Self::Noop(logger) => DynLogRecord::Noop(logger.create_log_record()),
    }
  }

  fn emit(&self, record: Self::LogRecord) {
    match (self, record) {
      (Self::Regular(logger), DynLogRecord::Regular(record)) => logger.emit(record),
      (Self::Noop(logger), DynLogRecord::Noop(record)) => logger.emit(record),
      _ => panic!("`<DynLogger as Logger>::emit called with incompatible record value"),
    }
  }

  fn event_enabled(&self, level: Severity, target: &str) -> bool {
    match self {
      Self::Regular(logger) => logger.event_enabled(level, target),
      Self::Noop(logger) => logger.event_enabled(level, target),
    }
  }
}

#[derive(Debug)]
#[allow(clippy::large_enum_variant)]
pub enum DynLogRecord {
  Regular(opentelemetry_sdk::logs::LogRecord),
  Noop(<<logs::NoopLoggerProvider as LoggerProvider>::Logger as Logger>::LogRecord),
}

impl LogRecord for DynLogRecord {
  fn set_event_name(&mut self, name: &'static str) {
    match self {
      Self::Regular(record) => record.set_event_name(name),
      Self::Noop(record) => record.set_event_name(name),
    }
  }

  fn set_target<T>(&mut self, target: T)
  where
    T: Into<Cow<'static, str>>,
  {
    match self {
      Self::Regular(record) => record.set_target(target),
      Self::Noop(record) => record.set_target(target),
    }
  }

  fn set_timestamp(&mut self, timestamp: SystemTime) {
    match self {
      Self::Regular(record) => record.set_timestamp(timestamp),
      Self::Noop(record) => record.set_timestamp(timestamp),
    }
  }

  fn set_observed_timestamp(&mut self, timestamp: SystemTime) {
    match self {
      Self::Regular(record) => record.set_observed_timestamp(timestamp),
      Self::Noop(record) => record.set_observed_timestamp(timestamp),
    }
  }

  fn set_severity_text(&mut self, text: &'static str) {
    match self {
      Self::Regular(record) => record.set_severity_text(text),
      Self::Noop(record) => record.set_severity_text(text),
    }
  }

  fn set_severity_number(&mut self, number: Severity) {
    match self {
      Self::Regular(record) => record.set_severity_number(number),
      Self::Noop(record) => record.set_severity_number(number),
    }
  }

  fn set_body(&mut self, body: AnyValue) {
    match self {
      Self::Regular(record) => record.set_body(body),
      Self::Noop(record) => record.set_body(body),
    }
  }

  fn add_attributes<I, K, V>(&mut self, attributes: I)
  where
    I: IntoIterator<Item = (K, V)>,
    K: Into<Key>,
    V: Into<AnyValue>,
  {
    match self {
      Self::Regular(record) => record.add_attributes(attributes),
      Self::Noop(record) => record.add_attributes(attributes),
    }
  }

  fn add_attribute<K, V>(&mut self, key: K, value: V)
  where
    K: Into<Key>,
    V: Into<AnyValue>,
  {
    match self {
      Self::Regular(record) => record.add_attribute(key, value),
      Self::Noop(record) => record.add_attribute(key, value),
    }
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TraceParentFromSliceError {
  #[error("invalid input size, expected=26, actual={0}")]
  Size(usize), // replace by Option<u64> if serialization is needed
  #[error("invalid `is_remote` bool value, actual={0}")]
  IsRemote(u8),
}

/// Struct representing a parsed `traceparent` header (span context without `trace_state`)
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TraceParent {
  pub trace_id: TraceId,
  pub span_id: SpanId,
  pub trace_flags: TraceFlags,
  pub is_remote: bool,
}

impl TraceParent {
  pub fn from_span_context(span_cx: &SpanContext) -> Self {
    Self {
      trace_id: span_cx.trace_id(),
      span_id: span_cx.span_id(),
      trace_flags: span_cx.trace_flags(),
      is_remote: span_cx.is_remote(),
    }
  }
  pub fn into_span_context(self) -> SpanContext {
    SpanContext::new(
      self.trace_id,
      self.span_id,
      self.trace_flags,
      self.is_remote,
      TraceState::NONE,
    )
  }

  fn into_primitive_tuple(self) -> (u128, u64, u8, bool) {
    (
      u128::from_be_bytes(self.trace_id.to_bytes()),
      u64::from_be_bytes(self.span_id.to_bytes()),
      self.trace_flags.to_u8(),
      self.is_remote,
    )
  }

  pub fn from_slice(slice: &[u8]) -> Result<Self, TraceParentFromSliceError> {
    let arr: &[u8; 26] = slice
      .try_into()
      .map_err(|_| TraceParentFromSliceError::Size(slice.len()))?;
    // todo: use array methods instead of slice methods once <https://github.com/rust-lang/rust/issues/90091>
    //       is implemented to split the input array
    let (trace_id, span_id, trace_flags, is_remote) = {
      let tail = arr.as_slice();
      let (trace_id, tail) = tail.split_first_chunk::<16>().expect("16 <= 26");
      debug_assert_eq!(tail.len(), 10);
      let (span_id, tail) = tail.split_first_chunk::<8>().expect("8 <= 10");
      debug_assert_eq!(tail.len(), 2);
      let trace_flags = tail[0];
      let is_remote = tail[1];
      (trace_id, span_id, trace_flags, is_remote)
    };
    Ok(Self {
      trace_id: TraceId::from_bytes(*trace_id),
      span_id: SpanId::from_bytes(*span_id),
      trace_flags: TraceFlags::new(trace_flags),
      is_remote: match is_remote {
        0 => false,
        1 => true,
        v => return Err(TraceParentFromSliceError::IsRemote(v)),
      },
    })
  }

  pub fn into_array(self) -> [u8; 26] {
    let mut res = [0u8; 26];
    // todo: use array methods instead of slice methods once <https://github.com/rust-lang/rust/issues/90091>
    //       is implemented to split the result array
    let (trace_id, span_id, trace_flags, is_remote) = {
      let tail = res.as_mut_slice();
      let (trace_id, tail) = tail.split_first_chunk_mut::<16>().expect("16 <= 26");
      debug_assert_eq!(tail.len(), 10);
      let (span_id, tail) = tail.split_first_chunk_mut::<8>().expect("8 <= 10");
      debug_assert_eq!(tail.len(), 2);
      let (trace_flags, tail) = tail.split_first_chunk_mut::<1>().expect("1 <= 2");
      let trace_flags = &mut trace_flags[0];
      debug_assert_eq!(tail.len(), 1);
      let is_remote = &mut tail[0];
      (trace_id, span_id, trace_flags, is_remote)
    };
    *trace_id = self.trace_id.to_bytes();
    *span_id = self.span_id.to_bytes();
    *trace_flags = self.trace_flags.to_u8();
    *is_remote = if self.is_remote { 1 } else { 0 };
    res
  }
}

impl PartialOrd for TraceParent {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(Ord::cmp(self, other))
  }
}

impl Ord for TraceParent {
  fn cmp(&self, other: &Self) -> Ordering {
    Ord::cmp(&self.into_primitive_tuple(), &other.into_primitive_tuple())
  }
}

const SUPPORTED_TRACEPARENT_FORMAT_VERSION: u8 = 0;

impl fmt::Display for TraceParent {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      "{:02x}-{}-{}-{:02x}",
      SUPPORTED_TRACEPARENT_FORMAT_VERSION,
      self.trace_id,
      self.span_id,
      self.trace_flags & TraceFlags::SAMPLED,
    )
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TraceParentParseError {
  #[error("expected exactly 4 parts separated by hyphens")]
  PartCount,
  #[error("invalid version part")]
  Version,
  #[error("invalid trace id part")]
  TraceId,
  #[error("invalid span id part")]
  SpanId,
  #[error("invalid flags part")]
  Flags,
  #[error("unsupported format version")]
  UnsupportedVersion,
}

impl FromStr for TraceParent {
  type Err = TraceParentParseError;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let mut parts = input.split_terminator('-');
    let version = parts.next().ok_or(TraceParentParseError::PartCount)?;
    let version = u8::from_str_radix(version, 16).map_err(|_| TraceParentParseError::Version)?;
    const MAX_VERSION: u8 = 254;
    if version > MAX_VERSION {
      return Err(TraceParentParseError::Version);
    }
    if version != SUPPORTED_TRACEPARENT_FORMAT_VERSION {
      return Err(TraceParentParseError::UnsupportedVersion);
    }

    let trace_id = parts.next().ok_or(TraceParentParseError::PartCount)?;
    // Ensure trace id is lowercase
    if trace_id.chars().any(|c| c.is_ascii_uppercase()) {
      return Err(TraceParentParseError::TraceId);
    }
    // Parse trace id section
    let trace_id = TraceId::from_hex(trace_id).map_err(|_| TraceParentParseError::TraceId)?;

    let span_id = parts.next().ok_or(TraceParentParseError::PartCount)?;
    // Ensure span id is lowercase
    if span_id.chars().any(|c| c.is_ascii_uppercase()) {
      return Err(TraceParentParseError::SpanId);
    }
    // Parse span id section
    let span_id = SpanId::from_hex(span_id).map_err(|_| TraceParentParseError::SpanId)?;

    let trace_flags = parts.next().ok_or(TraceParentParseError::PartCount)?;
    // Parse trace flags section
    let trace_flags = u8::from_str_radix(trace_flags, 16).map_err(|_| TraceParentParseError::Flags)?;

    // Ensure opts are valid for version 0
    if version == 0 && trace_flags > 2 {
      return Err(TraceParentParseError::Flags);
    }

    // Build trace flags clearing all flags other than the trace-context
    // supported sampling bit.
    let trace_flags = TraceFlags::new(trace_flags) & TraceFlags::SAMPLED;

    Ok(Self {
      trace_id,
      span_id,
      trace_flags,
      is_remote: true,
    })
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for TraceParent {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    self.to_string().serialize(serializer)
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for TraceParent {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    use serde::de::Error;
    let inner = Cow::<str>::deserialize(deserializer)?;
    let value = Self::from_str(inner.as_ref()).map_err(|e| D::Error::custom(crate::types::DisplayErrorChain(&e)))?;
    Ok(value)
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<::sqlx::Postgres> for TraceParent {
  fn type_info() -> ::sqlx::postgres::PgTypeInfo {
    ::sqlx::postgres::PgTypeInfo::with_name("opentelemetry_trace_parent_bytes")
  }

  fn compatible(ty: &::sqlx::postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == <Vec<u8> as ::sqlx::Type<::sqlx::Postgres>>::type_info()
  }
}

#[cfg(feature = "sqlx-postgres")]
impl<'r, Db: ::sqlx::Database> ::sqlx::Decode<'r, Db> for TraceParent
where
  &'r [u8]: ::sqlx::Decode<'r, Db>,
{
  fn decode(
    value: <Db as ::sqlx::database::HasValueRef<'r>>::ValueRef,
  ) -> Result<Self, Box<dyn ::std::error::Error + 'static + Send + Sync>> {
    let value: &[u8] = <&[u8] as ::sqlx::Decode<Db>>::decode(value)?;
    Ok(Self::from_slice(value).map_err(Box::new)?)
  }
}

#[cfg(feature = "sqlx")]
impl<'q, Db: ::sqlx::Database> ::sqlx::Encode<'q, Db> for TraceParent
where
  [u8; 26]: ::sqlx::Encode<'q, Db>,
{
  fn encode_by_ref(&self, buf: &mut <Db as sqlx::database::HasArguments<'q>>::ArgumentBuffer) -> sqlx::encode::IsNull {
    let bytes = self.into_array();
    bytes.encode_by_ref(buf)
  }
}
