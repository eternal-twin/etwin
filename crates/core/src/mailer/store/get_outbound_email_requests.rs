use crate::core::{Instant, Listing, Request};
use crate::mailer::store::OutboundEmailRequest;
use crate::types::WeakError;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetOutboundEmailRequestsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

/// Retrieve the list of outbound email requests
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetOutboundEmailRequests {
  /// System time
  pub now: Instant,
  /// Query time
  pub time: Instant,
  /// Maximum number of results to return
  pub limit: u32,
}

impl Request for GetOutboundEmailRequests {
  type Response = Result<Listing<OutboundEmailRequest>, GetOutboundEmailRequestsError>;
}
