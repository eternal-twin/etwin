use crate::core::{IdempotencyKey, Instant, Request};
use crate::email::EmailAddress;
use crate::mailer::store::{EmailContentPayload, OutboundEmail, OutboundEmailId};
use crate::types::WeakError;
use crate::user::UserIdRef;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreateOutboundEmailError {
  #[error("the provided deadline {0} must be in the future (now={1})")]
  InvalidDeadline(Instant, Instant),
  #[error("idempotency key conflict, the outbound email {0} already uses the key {1}")]
  Conflict(OutboundEmailId, IdempotencyKey),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreateOutboundEmail<Opaque> {
  pub idempotency_key: IdempotencyKey,
  /// Current system time
  pub now: Instant,
  /// Who triggered sending this email
  pub actor: UserIdRef,
  /// Time after which the email should no longer be sent
  pub deadline: Instant,
  pub sender: EmailAddress,
  pub recipient: EmailAddress,
  pub payload: EmailContentPayload<Opaque>,
}

impl<Opaque> Request for CreateOutboundEmail<Opaque> {
  type Response = Result<OutboundEmail<Opaque>, CreateOutboundEmailError>;
}
