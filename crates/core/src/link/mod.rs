pub mod store;

use crate::core::{BiValue, Instant, PeriodLower, RawUserDot, Request, UserDot};
use crate::dinoparc::{DinoparcUserIdRef, ShortDinoparcUser};
use crate::email::EmailAddress;
use crate::hammerfest::{HammerfestUserIdRef, ShortHammerfestUser};
use crate::twinoid::{ShortTwinoidUser, TwinoidUserIdRef};
use crate::types::WeakError;
use crate::user::{ShortUser, UserIdRef};
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::error::Error;
use std::fmt;
use thiserror::Error;

declare_new_uuid! {
  pub struct UserLinkId(Uuid);
  pub type ParseError = UserLinkIdParseError;
  const SQL_NAME = "user_link_id";
}

declare_new_uuid! {
  pub struct UserLinkProofId(Uuid);
  pub type ParseError = UserLinkProofIdParseError;
  const SQL_NAME = "user_link_proof_id";
}

declare_new_enum!(
  pub enum UserLinkType {
    #[str("Discord")]
    Discord,
    #[str("Email")]
    Email,
    #[str("Gitlab")]
    Gitlab,
    #[str("Github")]
    Github,
  }
  pub type ParseError = UserLinkTypeParseError;
  const SQL_NAME = "user_link_type";
);

declare_new_enum!(
  pub enum UserLinkVisibility {
    #[str("Private")]
    Private,
    #[str("Friend")]
    Friend,
    #[str("Public")]
    Public,
  }
  pub type ParseError = UserLinkVisibilityParseError;
  const SQL_NAME = "user_link_visibility";
);

#[cfg(feature = "serde")]
pub trait RemoteUserIdRef: Clone + PartialEq + Eq + fmt::Debug + Serialize + for<'a> Deserialize<'a> {}
#[cfg(not(feature = "serde"))]
pub trait RemoteUserIdRef: Clone + PartialEq + Eq + fmt::Debug {}

impl RemoteUserIdRef for DinoparcUserIdRef {}
impl RemoteUserIdRef for HammerfestUserIdRef {}
impl RemoteUserIdRef for TwinoidUserIdRef {}

#[cfg(feature = "serde")]
pub trait RemoteUser: Clone + PartialEq + Eq + fmt::Debug + Serialize + for<'a> Deserialize<'a> {}
#[cfg(not(feature = "serde"))]
pub trait RemoteUser: Clone + PartialEq + Eq + fmt::Debug {}

impl RemoteUser for ShortDinoparcUser {}
impl RemoteUser for ShortHammerfestUser {}
impl RemoteUser for ShortTwinoidUser {}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawLink<T: RemoteUserIdRef> {
  pub link: RawUserDot,
  pub unlink: (),
  pub etwin: UserIdRef,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub remote: T,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Link<T: RemoteUser> {
  pub link: UserDot,
  pub unlink: (),
  #[cfg_attr(feature = "serde", serde(rename = "user", bound(deserialize = "T: RemoteUser")))]
  pub remote: T,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OldRawLink<T: RemoteUserIdRef> {
  pub link: RawUserDot,
  pub unlink: RawUserDot,
  pub etwin: UserIdRef,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub remote: T,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OldLink<T: RemoteUser> {
  pub link: UserDot,
  pub unlink: UserDot,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUser")))]
  pub remote: T,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VersionedRawLink<T: RemoteUserIdRef> {
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub current: Option<RawLink<T>>,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub old: Vec<OldRawLink<T>>,
}

impl<T: RemoteUserIdRef> Default for VersionedRawLink<T> {
  fn default() -> Self {
    Self {
      current: None,
      old: vec![],
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VersionedLink<T: RemoteUser> {
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUser")))]
  pub current: Option<Link<T>>,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUser")))]
  pub old: Vec<OldLink<T>>,
}

impl<T: RemoteUser> Default for VersionedLink<T> {
  fn default() -> Self {
    Self {
      current: None,
      old: vec![],
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VersionedRawLinks {
  pub dinoparc_com: VersionedRawLink<DinoparcUserIdRef>,
  pub en_dinoparc_com: VersionedRawLink<DinoparcUserIdRef>,
  pub hammerfest_es: VersionedRawLink<HammerfestUserIdRef>,
  pub hammerfest_fr: VersionedRawLink<HammerfestUserIdRef>,
  pub hfest_net: VersionedRawLink<HammerfestUserIdRef>,
  pub sp_dinoparc_com: VersionedRawLink<DinoparcUserIdRef>,
  pub twinoid: VersionedRawLink<TwinoidUserIdRef>,
}

impl Default for VersionedRawLinks {
  fn default() -> Self {
    Self {
      dinoparc_com: VersionedRawLink {
        current: None,
        old: vec![],
      },
      en_dinoparc_com: VersionedRawLink {
        current: None,
        old: vec![],
      },
      hammerfest_es: VersionedRawLink {
        current: None,
        old: vec![],
      },
      hammerfest_fr: VersionedRawLink {
        current: None,
        old: vec![],
      },
      hfest_net: VersionedRawLink {
        current: None,
        old: vec![],
      },
      sp_dinoparc_com: VersionedRawLink {
        current: None,
        old: vec![],
      },
      twinoid: VersionedRawLink {
        current: None,
        old: vec![],
      },
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
pub struct VersionedLinks {
  pub dinoparc_com: VersionedLink<ShortDinoparcUser>,
  pub en_dinoparc_com: VersionedLink<ShortDinoparcUser>,
  pub hammerfest_es: VersionedLink<ShortHammerfestUser>,
  pub hammerfest_fr: VersionedLink<ShortHammerfestUser>,
  pub hfest_net: VersionedLink<ShortHammerfestUser>,
  pub sp_dinoparc_com: VersionedLink<ShortDinoparcUser>,
  pub twinoid: VersionedLink<ShortTwinoidUser>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TouchLinkOptions<T: RemoteUserIdRef> {
  pub etwin: UserIdRef,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub remote: T,
  pub linked_by: UserIdRef,
}

impl Request for TouchLinkOptions<DinoparcUserIdRef> {
  type Response = Result<VersionedRawLink<DinoparcUserIdRef>, TouchLinkError<DinoparcUserIdRef>>;
}

impl Request for TouchLinkOptions<HammerfestUserIdRef> {
  type Response = Result<VersionedRawLink<HammerfestUserIdRef>, TouchLinkError<HammerfestUserIdRef>>;
}

impl Request for TouchLinkOptions<TwinoidUserIdRef> {
  type Response = Result<VersionedRawLink<TwinoidUserIdRef>, TouchLinkError<TwinoidUserIdRef>>;
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DeleteLinkOptions<T: RemoteUserIdRef> {
  pub etwin: UserIdRef,
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub remote: T,
  pub unlinked_by: UserIdRef,
}

impl Request for DeleteLinkOptions<DinoparcUserIdRef> {
  type Response = Result<VersionedRawLink<DinoparcUserIdRef>, DeleteLinkError<DinoparcUserIdRef>>;
}

impl Request for DeleteLinkOptions<HammerfestUserIdRef> {
  type Response = Result<VersionedRawLink<HammerfestUserIdRef>, DeleteLinkError<HammerfestUserIdRef>>;
}

impl Request for DeleteLinkOptions<TwinoidUserIdRef> {
  type Response = Result<VersionedRawLink<TwinoidUserIdRef>, DeleteLinkError<TwinoidUserIdRef>>;
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EtwinLink {
  pub link: UserDot,
  pub unlink: (),
  #[cfg_attr(feature = "serde", serde(rename = "user"))]
  pub etwin: ShortUser,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OldEtwinLink {
  pub link: UserDot,
  pub unlink: UserDot,
  #[cfg_attr(feature = "serde", serde(rename = "user"))]
  pub etwin: ShortUser,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Default, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VersionedEtwinLink {
  pub current: Option<EtwinLink>,
  pub old: Vec<OldEtwinLink>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetLinkOptions<T: RemoteUserIdRef> {
  #[cfg_attr(feature = "serde", serde(bound(deserialize = "T: RemoteUserIdRef")))]
  pub remote: T,
  pub time: Option<Instant>,
}

impl Request for GetLinkOptions<DinoparcUserIdRef> {
  type Response = Result<VersionedRawLink<DinoparcUserIdRef>, WeakError>;
}

impl Request for GetLinkOptions<HammerfestUserIdRef> {
  type Response = Result<VersionedRawLink<HammerfestUserIdRef>, WeakError>;
}

impl Request for GetLinkOptions<TwinoidUserIdRef> {
  type Response = Result<VersionedRawLink<TwinoidUserIdRef>, WeakError>;
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetLinksFromEtwinOptions {
  pub etwin: UserIdRef,
  pub time: Option<Instant>,
}

impl Request for GetLinksFromEtwinOptions {
  type Response = Result<VersionedRawLinks, WeakError>;
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum TouchLinkError<T: RemoteUserIdRef> {
  #[error("cannot link as the remote user is already linked to the etwin user {0:?}")]
  ConflictEtwin(UserIdRef),
  #[error("cannot link as the etwin user is already linked to the remote user {0:?}")]
  ConflictRemote(T),
  #[error("cannot link as the remote user is already linked to the etwin user {0:?} and the etwin user is already linked to the remote user {1:?}")]
  ConflictBoth(UserIdRef, T),
  #[error(transparent)]
  Other(WeakError),
}

impl<T: RemoteUserIdRef> TouchLinkError<T> {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(Error, Debug)]
pub enum DeleteLinkError<T: RemoteUserIdRef> {
  #[error("link not found for the etwin user {0:?} and remote {1:?}")]
  NotFound(UserIdRef, T),
  #[error(transparent)]
  Other(WeakError),
}

impl<T: RemoteUserIdRef> DeleteLinkError<T> {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawDeleteAllLinks {
  pub now: Instant,
  pub etwin: UserIdRef,
  pub unlinked_by: UserIdRef,
}

impl Request for RawDeleteAllLinks {
  type Response = Result<(), WeakError>;
}

pub struct GetUserLinks {
  pub now: Instant,
  pub time: Instant,
  // pub actor: UserIdRef, // TODO: pass actor to check visibility (public/private/friend) based on the actor
  /// If non-empty, restrict to provided users
  pub users: BTreeSet<UserIdRef>,
  /// If non-empty, restrict to provided link types
  pub link_type: BTreeSet<UserLinkType>,
}

pub enum UserLink {
  Email(AnyUserLink<EmailUserLink>),
}

pub struct AnyUserLink<Target> {
  pub id: UserLinkId,
  pub period: PeriodLower,
  pub visibility: BiValue<UserLinkVisibility>,
  pub rank: BiValue<u16>,
  pub target: Target,
}

pub struct InputUserLinkProof {}

pub struct EmailUserLink {
  pub email: EmailAddress,
}
