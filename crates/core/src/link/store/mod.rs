use crate::core::Handler;
use crate::dinoparc::DinoparcUserIdRef;
use crate::hammerfest::HammerfestUserIdRef;
use crate::link::{
  DeleteLinkError, DeleteLinkOptions, GetLinkOptions, GetLinksFromEtwinOptions, RawDeleteAllLinks, TouchLinkError,
  TouchLinkOptions, VersionedRawLink, VersionedRawLinks,
};
use crate::twinoid::TwinoidUserIdRef;
use crate::types::WeakError;
use async_trait::async_trait;
use std::ops::Deref;

#[async_trait]
pub trait LinkStore:
  Send
  + Sync
  + Handler<TouchLinkOptions<DinoparcUserIdRef>>
  + Handler<TouchLinkOptions<HammerfestUserIdRef>>
  + Handler<TouchLinkOptions<TwinoidUserIdRef>>
  + Handler<DeleteLinkOptions<DinoparcUserIdRef>>
  + Handler<DeleteLinkOptions<HammerfestUserIdRef>>
  + Handler<DeleteLinkOptions<TwinoidUserIdRef>>
  + Handler<GetLinkOptions<DinoparcUserIdRef>>
  + Handler<GetLinkOptions<HammerfestUserIdRef>>
  + Handler<GetLinkOptions<TwinoidUserIdRef>>
  + Handler<GetLinksFromEtwinOptions>
  + Handler<RawDeleteAllLinks> // + Handler<RawCreateEmailUserLink>
{
  async fn touch_dinoparc_link(
    &self,
    cmd: TouchLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, TouchLinkError<DinoparcUserIdRef>> {
    Handler::<TouchLinkOptions<DinoparcUserIdRef>>::handle(self, cmd).await
  }
  async fn touch_hammerfest_link(
    &self,
    cmd: TouchLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, TouchLinkError<HammerfestUserIdRef>> {
    Handler::<TouchLinkOptions<HammerfestUserIdRef>>::handle(self, cmd).await
  }
  async fn touch_twinoid_link(
    &self,
    cmd: TouchLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, TouchLinkError<TwinoidUserIdRef>> {
    Handler::<TouchLinkOptions<TwinoidUserIdRef>>::handle(self, cmd).await
  }
  async fn delete_dinoparc_link(
    &self,
    cmd: DeleteLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, DeleteLinkError<DinoparcUserIdRef>> {
    Handler::<DeleteLinkOptions<DinoparcUserIdRef>>::handle(self, cmd).await
  }
  async fn delete_hammerfest_link(
    &self,
    cmd: DeleteLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, DeleteLinkError<HammerfestUserIdRef>> {
    Handler::<DeleteLinkOptions<HammerfestUserIdRef>>::handle(self, cmd).await
  }
  async fn delete_twinoid_link(
    &self,
    cmd: DeleteLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, DeleteLinkError<TwinoidUserIdRef>> {
    Handler::<DeleteLinkOptions<TwinoidUserIdRef>>::handle(self, cmd).await
  }
  async fn get_link_from_dinoparc(
    &self,
    query: GetLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, WeakError> {
    Handler::<GetLinkOptions<DinoparcUserIdRef>>::handle(self, query).await
  }
  async fn get_link_from_hammerfest(
    &self,
    query: GetLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, WeakError> {
    Handler::<GetLinkOptions<HammerfestUserIdRef>>::handle(self, query).await
  }
  async fn get_link_from_twinoid(
    &self,
    query: GetLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, WeakError> {
    Handler::<GetLinkOptions<TwinoidUserIdRef>>::handle(self, query).await
  }
  async fn get_links_from_etwin(&self, query: GetLinksFromEtwinOptions) -> Result<VersionedRawLinks, WeakError> {
    Handler::<GetLinksFromEtwinOptions>::handle(self, query).await
  }
  async fn delete_all_links(&self, cmd: RawDeleteAllLinks) -> Result<(), WeakError> {
    Handler::<RawDeleteAllLinks>::handle(self, cmd).await
  }
}

impl<T> LinkStore for T where
  T: Send
    + Sync
    + Handler<TouchLinkOptions<DinoparcUserIdRef>>
    + Handler<TouchLinkOptions<HammerfestUserIdRef>>
    + Handler<TouchLinkOptions<TwinoidUserIdRef>>
    + Handler<DeleteLinkOptions<DinoparcUserIdRef>>
    + Handler<DeleteLinkOptions<HammerfestUserIdRef>>
    + Handler<DeleteLinkOptions<TwinoidUserIdRef>>
    + Handler<GetLinkOptions<DinoparcUserIdRef>>
    + Handler<GetLinkOptions<HammerfestUserIdRef>>
    + Handler<GetLinkOptions<TwinoidUserIdRef>>
    + Handler<GetLinksFromEtwinOptions>
    + Handler<RawDeleteAllLinks>
{
}

/// Like [`Deref`], but the target has the bound [`LinkStore`]
pub trait LinkStoreRef: Send + Sync {
  type LinkStore: LinkStore + ?Sized;

  fn link_store(&self) -> &Self::LinkStore;
}

impl<TyRef> LinkStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: LinkStore,
{
  type LinkStore = TyRef::Target;

  fn link_store(&self) -> &Self::LinkStore {
    self.deref()
  }
}
