use self::user_disabled::UserDisabled;
use self::user_enabled::UserEnabled;

pub mod user_disabled;
pub mod user_enabled;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
#[non_exhaustive]
pub enum UserEvent {
  UserDisabled(UserDisabled),
  UserEnabled(UserEnabled),
}
