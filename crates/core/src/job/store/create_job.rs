use crate::core::{Instant, Request};
use crate::job::{StoreJob, TaskKind};
use crate::opentelemetry::TraceParent;
use crate::types::WeakError;
use crate::user::UserIdRef;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct CreateJob<OpaqueTask> {
  pub now: Instant,
  pub user: Option<UserIdRef>,
  pub producer_span: Option<TraceParent>,
  pub kind: TaskKind,
  pub kind_version: u32,
  pub task: OpaqueTask,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreateJobError {
  #[error(transparent)]
  Other(WeakError),
}

impl CreateJobError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl<OpaqueTask> Request for CreateJob<OpaqueTask> {
  type Response = Result<StoreJob, CreateJobError>;
}
