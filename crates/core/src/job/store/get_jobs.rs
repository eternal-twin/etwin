use crate::core::{Listing, Request};
use crate::job::{StoreJob, TaskStatus};
use crate::types::WeakError;
use crate::user::UserIdRef;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct GetJobs {
  pub status: Option<TaskStatus>,
  pub creator: Option<Option<UserIdRef>>,
  pub offset: u32,
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetJobsError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetJobsError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetJobs {
  type Response = Result<Listing<StoreJob>, GetJobsError>;
}
