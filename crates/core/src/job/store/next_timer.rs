use crate::core::{Instant, Request};
use crate::types::WeakError;
use std::error::Error;

/// Retrieve the earliest timer ready to trigger
#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct NextTimer {}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum NextTimerError {
  #[error(transparent)]
  Other(WeakError),
}

impl NextTimerError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for NextTimer {
  type Response = Result<Option<Instant>, NextTimerError>;
}
