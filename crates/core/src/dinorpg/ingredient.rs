#[cfg(feature = "sqlx-postgres")]
use crate::pg_num::PgU16;

declare_new_int! {
  pub struct DinorpgIngredientCount(u16);
  pub type RangeError = DinorpgIngredientQuantityRangeError;
  const BOUNDS = 0..1_000;
  type SqlType = PgU16;
  const SQL_NAME = "dinorpg_ingredient_quantity";
}

declare_new_enum!(
  /// Provides types and implementations related to ingredients in DinoRPG
  pub enum DinorpgIngredient {
    #[str("MerouLujidane")]
    MerouLujidane,
    #[str("PoissonVengeur")]
    PoissonVengeur,
    #[str("AnGuiliGuilille")]
    AnGuiliGuilille,
    #[str("Globulos")]
    Globulos,
    #[str("SuperPoisson")]
    SuperPoisson,
    #[str("TouffeDeFourrure")]
    TouffeDeFourrure,
    #[str("RocheRadioActive")]
    RocheRadioActive,
    #[str("GriffesAcerees")]
    GriffesAcerees,
    #[str("CorneEnChocolat")]
    CorneEnChocolat,
    #[str("OeilVisqueux")]
    OeilVisqueux,
    #[str("LangueMonstrueuse")]
    LangueMonstrueuse,
    #[str("EnergieFoudre")]
    EnergieFoudre,
    #[str("EnergieAir")]
    EnergieAir,
    #[str("EnergieEau")]
    EnergieEau,
    #[str("EnergieFeu")]
    EnergieFeu,
    #[str("EnergieBois")]
    EnergieBois,
    #[str("SilexTaille")]
    SilexTaille,
    #[str("FragmentDeTexteAncien")]
    FragmentDeTexteAncien,
    #[str("VieilAnneauPrecieux")]
    VieilAnneauPrecieux,
    #[str("CaliceCisele")]
    CaliceCisele,
    #[str("CollierKarat")]
    CollierKarat,
    #[str("BrocheEnParfaitEtat")]
    BrocheEnParfaitEtat,
    #[str("SuperbeCouronneRoyale")]
    SuperbeCouronneRoyale,
    #[str("FeuillesDePelinae")]
    FeuillesDePelinae,
    #[str("BoletPhaliskBlanc")]
    BoletPhaliskBlanc,
    #[str("OrchideeFantasque")]
    OrchideeFantasque,
    #[str("RacinesDeFigonicia")]
    RacinesDeFigonicia,
    #[str("SadiquaeMordicus")]
    SadiquaeMordicus,
    #[str("Flaureole")]
    Flaureole,
    #[str("SporeEtheral")]
    SporeEtheral,
    #[str("PousseSombre")]
    PousseSombre,
    #[str("GrainedeDevoreuse")]
    GrainedeDevoreuse,
    #[str("DentDeDorogon")]
    DentDeDorogon,
    #[str("BrasMecanique")]
    BrasMecanique,
  }
  pub type ParseError = DinorpgIngredientNameParseError;
  const SQL_NAME = "dinorpg_ingredient_name";
);
