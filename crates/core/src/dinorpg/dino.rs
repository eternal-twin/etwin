use crate::dinorpg::mission::DinorpgMission;
use crate::dinorpg::npc::DinorpgNpc;
/// Defines the types relative to a dino and their associated implementations
use crate::dinorpg::DinorpgServer;
#[cfg(feature = "sqlx-postgres")]
use crate::pg_num::{PgU16, PgU32};
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::serialize_ordered_map;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
#[cfg(feature = "sqlx-postgres")]
use sqlx::{postgres, Postgres};
use std::collections::HashMap;

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgDinozElements {
  pub air: u8,
  pub fire: u8,
  pub water: u8,
  pub wood: u8,
  pub thunder: u8,
}

declare_new_int! {
  pub struct DinorpgDinozExperience(u32);
  pub type RangeError = DinorpgExperienceRangeError;
  const BOUNDS = 0..1_000_000;
  type SqlType = PgU32;
  const SQL_NAME = "dinorpg_experience";
}

declare_decimal_id! {
  pub struct DinorpgDinozId(u32);
  pub type ParseError = DinorpgDinozIdParseError;
  const BOUNDS = 0..1_000_000_000;
  const SQL_NAME = "dinorpg_dinoz_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgDinoz"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgDinozIdRef {
  pub server: DinorpgServer,
  pub id: DinorpgDinozId,
}

declare_new_int! {
  pub struct DinorpgDinozLevel(u16);
  pub type RangeError = DinorpgLevelRangeError;
  const BOUNDS = 0..=80;
  type SqlType = PgU16;
  const SQL_NAME = "dinorpg_level";
}

declare_new_int! {
  pub struct DinorpgDinozLife(u16);
  pub type RangeError = DinorpgLifeRangeError;
  const BOUNDS = 0..1_000;
  type SqlType = PgU16;
  const SQL_NAME = "dinorpg_life";
}

declare_new_int! {
  pub struct DinorpgDinozMaxLife(u16);
  pub type RangeError = DinorpgMaxLifeRangeError;
  const BOUNDS = 0..1_000;
  type SqlType = PgU16;
  const SQL_NAME = "dinorpg_max_life";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozMission {
  pub id: DinorpgDinozId,
  #[cfg_attr(feature = "serde", serde(serialize_with = "serialize_ordered_map"))]
  pub missions: HashMap<DinorpgNpc, Vec<DinorpgMission>>,
}

declare_new_string! {
  pub struct DinorpgDinozName(String);
  pub type ParseError = DinorpgDinozNameParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_name";
}

declare_new_string! {
  /// Skin code for the appearance of a Dinoz
  pub struct DinorpgDinozSkin(String);
  pub type ParseError = DinorpgDinozSkinParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_skin";
}

declare_new_string! {
  /// Checksum for a DinorpgDinozSkin
  pub struct DinorpgDinozSkinCheck(String);
  pub type ParseError = DinorpgDinozSkinCheckParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_skin_check";
}
// End of dinoz specific declarations

impl DinorpgDinozId {
  pub const fn and_server(&self, server: DinorpgServer) -> DinorpgDinozIdRef {
    DinorpgDinozIdRef { server, id: *self }
  }
}

// TODO: Check why the names are different (`raw_` prefix or not)
#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<Postgres> for DinorpgDinozElements {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("dinorpg_dinoz_elements")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == postgres::PgTypeInfo::with_name("raw_dinorpg_dinoz_elements")
  }
}

#[cfg(feature = "sqlx-postgres")]
impl<'r> sqlx::Decode<'r, Postgres> for DinorpgDinozElements {
  fn decode(value: postgres::PgValueRef<'r>) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let mut decoder = postgres::types::PgRecordDecoder::new(value)?;

    let fire = decoder.try_decode::<crate::pg_num::PgU8>()?;
    let wood = decoder.try_decode::<crate::pg_num::PgU8>()?;
    let water = decoder.try_decode::<crate::pg_num::PgU8>()?;
    let thunder = decoder.try_decode::<crate::pg_num::PgU8>()?;
    let air = decoder.try_decode::<crate::pg_num::PgU8>()?;

    Ok(Self {
      fire: fire.into(),
      wood: wood.into(),
      water: water.into(),
      thunder: thunder.into(),
      air: air.into(),
    })
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Encode<'_, Postgres> for DinorpgDinozElements {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(crate::pg_num::PgU8::from(self.fire));
    encoder.encode(crate::pg_num::PgU8::from(self.wood));
    encoder.encode(crate::pg_num::PgU8::from(self.water));
    encoder.encode(crate::pg_num::PgU8::from(self.thunder));
    encoder.encode(crate::pg_num::PgU8::from(self.air));
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}
