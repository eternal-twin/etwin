/// Defines the types relative to a DinoRPG user and their associated implementations
use crate::dinorpg::DinorpgServer;
use crate::twinoid::TwinoidUserDisplayName;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};

declare_decimal_id! {
  pub struct DinorpgUserId(u32);
  pub type ParseError = DinorpgUserIdParseError;
  const BOUNDS = 0..1_000_000_000;
  const SQL_NAME = "dinorpg_user_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgUserIdRef {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortDinorpgUser {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
  pub display_name: TwinoidUserDisplayName,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgProfile {
  pub user: ShortDinorpgUser,
}

impl DinorpgUserId {
  pub const fn and_server(&self, server: DinorpgServer) -> DinorpgUserIdRef {
    DinorpgUserIdRef { server, id: *self }
  }
}

impl ShortDinorpgUser {
  pub const fn as_ref(&self) -> DinorpgUserIdRef {
    DinorpgUserIdRef {
      server: self.server,
      id: self.id,
    }
  }
}
