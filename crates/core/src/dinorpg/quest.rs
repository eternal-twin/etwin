/// Defines global quests a user can do in DinoRPG.
/// Note they are different from the missions given by NPCs.
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};

declare_new_enum! {
  pub enum DinorpgQuest {
    #[str("AuraKabuki")]
    AuraKabuki,
  }
  pub type ParseError = DinorpgUserQuestParseError;
  const SQL_NAME = "dinorpg_quest_name";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgQuestStatus {
  pub id: DinorpgQuest,
  pub progress: u32,
}
