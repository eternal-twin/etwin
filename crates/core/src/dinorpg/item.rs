#[cfg(feature = "sqlx-postgres")]
use crate::pg_num::PgU16;

declare_new_int! {
  pub struct DinorpgItemCount(u16);
  pub type RangeError = DinorpgItemQuantityRangeError;
  const BOUNDS = 0..1_000;
  type SqlType = PgU16;
  const SQL_NAME = "dinorpg_item_quantity";
}

declare_new_enum!(
  /// Defines the types and implementations related to items in DinoRPG
  pub enum DinorpgItem {
    #[str("PotiondIrma")]
    PotiondIrma,
    #[str("PotiondAnge")]
    PotiondAnge,
    #[str("NuageBurger")]
    NuageBurger,
    #[str("PainChaudAuthentique")]
    PainChaudAuthentique,
    #[str("TarteALaViande")]
    TarteALaViande,
    #[str("RationDeCombat")]
    RationDeCombat,
    #[str("RationDeSurvie")]
    RationDeSurvie,
    #[str("MerguezDeGobelin")]
    MerguezDeGobelin,
    #[str("Pampleboum")]
    Pampleboum,
    #[str("CasqueDeSecours")]
    CasqueDeSecours,
    #[str("PetitPoivron")]
    PetitPoivron,
    #[str("Briquet")]
    Briquet,
    #[str("FlammecheDeSecours")]
    FlammecheDeSecours,
    #[str("BouclierRefrigere")]
    BouclierRefrigere,
    #[str("DrageeFuca")]
    DrageeFuca,
    #[str("Monochromatique")]
    Monochromatique,
    #[str("PiqureDePoisonite")]
    PiqureDePoisonite,
    #[str("CostumeDeLorie")]
    CostumeDeLorie,
    #[str("CostumeDeGardeVegetox")]
    CostumeDeGardeVegetox,
    #[str("CostumeDeGobelin")]
    CostumeDeGobelin,
    #[str("NoyauDePampleboum")]
    NoyauDePampleboum,
    #[str("AmourPortable")]
    AmourPortable,
    #[str("DetecteurDeDanger")]
    DetecteurDeDanger,
    #[str("OnguentDuDemon")]
    OnguentDuDemon,
    #[str("PirhanozEnSachet")]
    PirhanozEnSachet,
    #[str("PaysDeCendre")]
    PaysDeCendre,
    #[str("Abysse")]
    Abysse,
    #[str("Amazonie")]
    Amazonie,
    #[str("FeuDeStElme")]
    FeuDeStElme,
    #[str("Ouranos")]
    Ouranos,
    #[str("TheConcentre")]
    TheConcentre,
    #[str("StabilisateurTemporel")]
    StabilisateurTemporel,
    #[str("Elixir")]
    Elixir,
    #[str("Bannissement")]
    Bannissement,
    #[str("Belier")]
    Belier,
    #[str("Braise")]
    Braise,
    #[str("Balance")]
    Balance,
    #[str("Biere")]
    Biere,
    #[str("Encyclopedie")]
    Encyclopedie,
    #[str("Antichromatique")]
    Antichromatique,
    #[str("Antipoison")]
    Antipoison,
    #[str("ManipulateurTemporel")]
    ManipulateurTemporel,
    #[str("PoudreDimensionelle")]
    PoudreDimensionelle,
    #[str("BaguetteDuSorcier")]
    BaguetteDuSorcier,
    #[str("SiffletAmical")]
    SiffletAmical,
    #[str("DinozCube")]
    DinozCube,
    #[str("AmortisseurTemporel")]
    AmortisseurTemporel,
    #[str("LarmeDeVie")]
    LarmeDeVie,
    #[str("MasqueCuzcous")]
    MasqueCuzcous,
    #[str("CombinaisonAntiGravite")]
    CombinaisonAntiGravite,
    #[str("SteroideFeerique")]
    SteroideFeerique,
    #[str("CadenasMaudit")]
    CadenasMaudit,
    #[str("Trouillometre")]
    Trouillometre,
    #[str("VoleurDeVie")]
    VoleurDeVie,
    #[str("SphereDeFeu")]
    SphereDeFeu,
    #[str("SphereDeBois")]
    SphereDeBois,
    #[str("SphereDeEau")]
    SphereDeEau,
    #[str("SphereDeFoudre")]
    SphereDeFoudre,
    #[str("SphereDeAir")]
    SphereDeAir,
    #[str("TicketDemon")]
    TicketDemon,
    #[str("BonDuTresor")]
    BonDuTresor,
    #[str("BebeToufufu")]
    BebeToufufu,
    #[str("BebeMahamuti")]
    BebeMahamuti,
    #[str("OeufDeSoufflet")]
    OeufDeSoufflet,
    #[str("OeufDeSmog")]
    OeufDeSmog,
    #[str("RizAmnesique")]
    RizAmnesique,
    #[str("BraceletEnTik")]
    BraceletEnTik,
    #[str("NapodinodOr")]
    NapodinodOr,
    #[str("EtoileMagique")]
    EtoileMagique,
    #[str("TicketDeNoel")]
    TicketDeNoel,
    #[str("TicketParrain")]
    TicketParrain,
    #[str("TicketTaque")]
    TicketTaque,
    #[str("AmiBambooz")]
    AmiBambooz,
    #[str("PileSousMarine")]
    PileSousMarine,
    #[str("BougiedAnniversaire")]
    BougiedAnniversaire,
    #[str("PetardMagique")]
    PetardMagique,
    #[str("OeufDeFerossDeNoel")]
    OeufDeFerossDeNoel,
    #[str("OeufDeSmogDeNoel")]
    OeufDeSmogDeNoel,
    #[str("OeufDeFerossRare")]
    OeufDeFerossRare,
    #[str("BebeWanwanRare")]
    BebeWanwanRare,
    #[str("OeufDeSantazRare")]
    OeufDeSantazRare,
    #[str("OeufDeRockyRare")]
    OeufDeRockyRare,
    #[str("OeufDeSmogAnniversaire")]
    OeufDeSmogAnniversaire,
    #[str("OeufsDeWinksRare")]
    OeufsDeWinksRare,
    #[str("OeufDeNuagozRare")]
    OeufDeNuagozRare,
    #[str("OeufDeNoel")]
    OeufDeNoel,
    #[str("OeufDeSirainRare")]
    OeufDeSirainRare,
    #[str("ElixirDeVie")]
    ElixirDeVie,
    #[str("OeufDeKabuki")]
    OeufDeKabuki,
    #[str("OeufDeKabukiRare")]
    OeufDeKabukiRare,
  }
  pub type ParseError = DinorpgItemNameParseError;
  const SQL_NAME = "dinorpg_item_name";
);
