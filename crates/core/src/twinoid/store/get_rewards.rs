use crate::{
  core::Request,
  twinoid::{ArchivedTwinoidStatsAndRewards, TwinoidSiteIdRef, TwinoidUserIdRef},
  types::WeakError,
};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetRewardsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl RawGetRewardsError {
  pub fn other<E: std::error::Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetRewardsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct GetRewards {
  pub user: TwinoidUserIdRef,
  pub site: TwinoidSiteIdRef,
}

impl Request for GetRewards {
  type Response = Result<ArchivedTwinoidStatsAndRewards, RawGetRewardsError>;
}
