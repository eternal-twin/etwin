use crate::core::Request;
use crate::twinoid::client::SafeUser;
use crate::twinoid::store::MissingUserRelations;
use crate::twinoid::TwinoidApiAuth;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum TouchSafeUserListError {
  #[error("missing relations: {0:?}")]
  MissingRelations(MissingUserRelations),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct TouchSafeUserList {
  pub auth: TwinoidApiAuth,
  pub users: Vec<SafeUser>,
}

impl Request for TouchSafeUserList {
  type Response = Result<(), TouchSafeUserListError>;
}
