use crate::core::Request;
use crate::twinoid::client::FullUser;
use crate::twinoid::store::MissingUserRelations;
use crate::twinoid::TwinoidApiAuth;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum TouchFullUserListError {
  #[error("missing relations: {0:?}")]
  MissingRelations(MissingUserRelations),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct TouchFullUserList {
  pub auth: TwinoidApiAuth,
  pub users: Vec<FullUser>,
}

impl Request for TouchFullUserList {
  type Response = Result<(), TouchFullUserListError>;
}
