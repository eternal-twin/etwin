use crate::core::{Instant, Request};
use crate::oauth::TwinoidAccessToken;
use crate::twinoid::TwinoidUserIdRef;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetTwinoidAccessToken {
  pub time: Instant,
  pub twinoid_user: Option<TwinoidUserIdRef>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetTwinoidAccessTokenError {
  #[error("no matching access token found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl Request for GetTwinoidAccessToken {
  type Response = Result<TwinoidAccessToken, GetTwinoidAccessTokenError>;
}
