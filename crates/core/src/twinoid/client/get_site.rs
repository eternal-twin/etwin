use crate::core::Request;
use crate::twinoid::api::{Projector, Site};
use crate::twinoid::{TwinoidApiAuth, TwinoidSiteId};
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum GetSiteError {
  #[error("invalid token")]
  InvalidToken,
  #[error("server failed to encode response")]
  Encoding,
  #[error(transparent)]
  Other(#[from] WeakError),
}

pub struct GetSite<P: Projector<Site>> {
  pub auth: TwinoidApiAuth,
  pub id: TwinoidSiteId,
  pub projector: P,
}

impl<P: Projector<Site>> Request for GetSite<P> {
  type Response = Result<P::Projection, GetSiteError>;
}
