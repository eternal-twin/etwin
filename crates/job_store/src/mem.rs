use async_trait::async_trait;
use eternaltwin_core::core::{Handler, Instant, Listing};
use eternaltwin_core::job::{
  store, JobId, ShortStoreJob, ShortStoreTask, StoreJob, StoreTask, TaskId, TaskKind, TaskRevId, TaskStatus, Tick,
};
use eternaltwin_core::opentelemetry::TraceParent;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserIdRef;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::{BTreeMap, BTreeSet, HashSet};
use std::convert::TryFrom;
use std::sync::RwLock;

pub struct MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue> {
  uuid_generator: TyUuidGenerator,
  state: RwLock<MemJobStoreState<OpaqueTask, OpaqueValue>>,
}

impl<TyUuidGenerator, OpaqueTask, OpaqueValue> MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  pub fn new(uuid_generator: TyUuidGenerator) -> Self {
    Self {
      uuid_generator,
      state: RwLock::new(MemJobStoreState::new()),
    }
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::CreateJob<OpaqueTask>>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, command: store::CreateJob<OpaqueTask>) -> Result<StoreJob, store::CreateJobError> {
    let job_id = JobId::from(self.uuid_generator.uuid_generator().next());
    let task_id = TaskId::from(self.uuid_generator.uuid_generator().next());
    let mut state = self.state.write().expect("failed to lock mem job store state");
    Ok(state.create_job(command, job_id, task_id))
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::GetJobs>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, query: store::GetJobs) -> Result<Listing<StoreJob>, store::GetJobsError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_jobs(query)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::GetJob>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, query: store::GetJob) -> Result<StoreJob, store::GetJobError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_job(query)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::GetTask, fn() -> (OpaqueTask, OpaqueValue)>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, query: store::GetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, store::GetTaskError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_task(query)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::GetTasks, fn() -> (OpaqueTask, OpaqueValue)>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(
    &self,
    query: store::GetTasks,
  ) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, store::GetTasksError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_tasks(query)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::UpdateTask<OpaqueTask, OpaqueValue>>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(
    &self,
    command: store::UpdateTask<OpaqueTask, OpaqueValue>,
  ) -> Result<TaskRevId, store::UpdateTaskError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.update_task(command)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::CreateTimer>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, command: store::CreateTimer) -> Result<(), store::CreateTimerError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.create_timer(command)
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::NextTimer>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, _command: store::NextTimer) -> Result<Option<Instant>, store::NextTimerError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    Ok(state.next_timer())
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> Handler<store::OnTimer>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn handle(&self, command: store::OnTimer) -> Result<(), store::OnTimerError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.on_timer(command.now)
  }
}

struct MemStoreTask<OpaqueTask, OpaqueValue> {
  /// Id for this task
  pub id: TaskId,
  /// Reference to overall job containing this task.
  pub job: JobId,
  /// Revision for task, increment on every update
  pub revision: u32,
  /// Handler name / Task type
  pub kind: TaskKind,
  /// Handler version / version of the state
  pub kind_version: u32,
  /// Time when the task was created.
  ///
  /// It may not have been polled ever.
  pub created_at: Instant,
  /// Last time when task polling completed.
  pub polled_at: Option<Tick>,
  /// Task status
  pub status: TaskStatus,
  /// How strongly a task asked to be prioritized
  pub starvation: i32,
  /// Serialized state
  pub state: OpaqueTask,
  /// Serialized value (if complete)
  pub output: Option<OpaqueValue>,
}

impl<OpaqueTask, OpaqueValue> MemStoreTask<OpaqueTask, OpaqueValue> {
  pub const fn rev_id(&self) -> TaskRevId {
    TaskRevId {
      id: self.id,
      rev: self.revision,
    }
  }
}

struct MemJob {
  id: JobId,
  created_at: Instant,
  created_by: Option<UserIdRef>,
  producer_span: Option<TraceParent>,
  task: TaskId,
}

struct MemJobStoreState<OpaqueTask, OpaqueValue> {
  jobs: BTreeMap<JobId, MemJob>,
  jobs_by_created_at: BTreeSet<(Instant, JobId)>,
  tasks: BTreeMap<TaskId, MemStoreTask<OpaqueTask, OpaqueValue>>,
  timers: BTreeSet<(Instant, TaskId)>,
}

impl<OpaqueTask, OpaqueValue> Default for MemJobStoreState<OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  fn default() -> Self {
    Self::new()
  }
}

impl<OpaqueTask, OpaqueValue> MemJobStoreState<OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  pub fn new() -> Self {
    Self {
      jobs: BTreeMap::new(),
      jobs_by_created_at: BTreeSet::new(),
      tasks: BTreeMap::new(),
      timers: BTreeSet::new(),
    }
  }

  pub fn create_job(&mut self, cmd: store::CreateJob<OpaqueTask>, job_id: JobId, task_id: TaskId) -> StoreJob {
    let mem_job = MemJob {
      id: job_id,
      created_at: cmd.now,
      created_by: cmd.user,
      producer_span: cmd.producer_span,
      task: task_id,
    };
    let mem_task = MemStoreTask {
      id: task_id,
      job: job_id,
      revision: 0,
      kind: cmd.kind,
      kind_version: cmd.kind_version,
      created_at: cmd.now,
      polled_at: None,
      status: TaskStatus::Available,
      starvation: 0,
      state: cmd.task,
      output: None,
    };
    let job = StoreJob {
      id: mem_job.id,
      created_at: mem_job.created_at,
      created_by: mem_job.created_by,
      producer_span: mem_job.producer_span,
      task: ShortStoreTask {
        id: mem_task.id,
        revision: mem_task.revision,
        kind: mem_task.kind.clone(),
        kind_version: mem_task.kind_version,
        created_at: mem_task.created_at,
        polled_at: mem_task.polled_at,
        status: mem_task.status,
        starvation: mem_task.starvation,
      },
    };
    self.tasks.insert(task_id, mem_task);
    self.jobs.insert(job_id, mem_job);
    job
  }

  fn get_jobs(&self, query: store::GetJobs) -> Result<Listing<StoreJob>, store::GetJobsError> {
    let mut matched = self
      .jobs_by_created_at
      .iter()
      .rev()
      .filter_map(|(_, job_id)| {
        let job = self.jobs.get(job_id).expect("job exists");
        let task = self.tasks.get(&job.task).expect("task exists");
        if query.status.map(|s| task.status == s).unwrap_or(true) {
          Some((job, task))
        } else {
          None
        }
      })
      .peekable();
    let is_empty = matched.peek().is_none();
    let mut matched = matched
      .skip(usize::try_from(query.offset).expect("`u32` should fit into `usize`"))
      .peekable();
    let mut items: Vec<StoreJob> = Vec::new();
    for _ in 0..query.limit {
      if matched.peek().is_some() {
        let (job, task) = matched.next().expect("task exists");
        items.push(StoreJob {
          id: job.id,
          created_at: job.created_at,
          created_by: job.created_by,
          producer_span: job.producer_span,
          task: ShortStoreTask {
            id: task.id,
            revision: task.revision,
            kind: task.kind.clone(),
            kind_version: task.kind_version,
            created_at: task.created_at,
            polled_at: task.polled_at,
            status: task.status,
            starvation: task.starvation,
          },
        });
      }
    }
    let remaining = matched.count();
    let count = if is_empty {
      0
    } else {
      query.offset + u32::try_from(items.len() + remaining).expect("`u32` should fit into `usize`")
    };

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  fn get_job(&self, query: store::GetJob) -> Result<StoreJob, store::GetJobError> {
    let job = self.jobs.get(&query.id).ok_or(store::GetJobError::NotFound(query.id))?;
    let task = self.tasks.get(&job.task).expect("task exists");

    Ok(StoreJob {
      id: job.id,
      created_at: job.created_at,
      created_by: job.created_by,
      producer_span: job.producer_span,
      task: ShortStoreTask {
        id: task.id,
        revision: task.revision,
        kind: task.kind.clone(),
        kind_version: task.kind_version,
        created_at: task.created_at,
        polled_at: task.polled_at,
        status: task.status,
        starvation: task.starvation,
      },
    })
  }

  pub fn get_task(&self, query: store::GetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, store::GetTaskError> {
    let mem_task = self
      .tasks
      .get(&query.id)
      .ok_or(store::GetTaskError::NotFound(query.id))?;
    self.mem_to_store_task(mem_task).map_err(store::GetTaskError::Other)
  }

  fn get_tasks(
    &self,
    query: store::GetTasks,
  ) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, store::GetTasksError> {
    let mut matched = self
      .tasks
      .values()
      .filter(|t| {
        let status_match = query.status.map(|s| t.status == s).unwrap_or(true);
        let skip_polled_match = query.skip_polled.map(|tick| t.polled_at == Some(tick)).unwrap_or(true);
        status_match && skip_polled_match
      })
      .peekable();
    let is_empty = matched.peek().is_none();
    let mut matched = matched
      .skip(usize::try_from(query.offset).expect("`u32` should fit into `usize`"))
      .peekable();
    let mut items: Vec<StoreTask<OpaqueTask, OpaqueValue>> = Vec::new();
    for _ in 0..query.limit {
      if matched.peek().is_some() {
        let mem_task = matched.next().expect("task exists");
        items.push(self.mem_to_store_task(mem_task).map_err(store::GetTasksError::Other)?);
      }
    }
    let remaining = matched.count();
    let count = if is_empty {
      0
    } else {
      query.offset + u32::try_from(items.len() + remaining).expect("`u32` should fit into `usize`")
    };

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  fn mem_to_store_task(
    &self,
    mem_task: &MemStoreTask<OpaqueTask, OpaqueValue>,
  ) -> Result<StoreTask<OpaqueTask, OpaqueValue>, WeakError> {
    let mem_job = self.jobs.get(&mem_task.job).ok_or_else(|| {
      WeakError::new(format!(
        "mem job not found; job_id = {}, task_id = {}",
        mem_task.job, mem_task.id
      ))
    })?;
    Ok(StoreTask {
      id: mem_task.id,
      job: ShortStoreJob {
        id: mem_job.id,
        created_at: mem_job.created_at,
        created_by: mem_job.created_by,
        producer_span: mem_job.producer_span,
      },
      revision: mem_task.revision,
      kind: mem_task.kind.clone(),
      kind_version: mem_task.kind_version,
      created_at: mem_task.created_at,
      polled_at: mem_task.polled_at,
      status: mem_task.status,
      starvation: mem_task.starvation,
      state: mem_task.state.clone(),
      output: mem_task.output.clone(),
    })
  }

  pub fn update_task(
    &mut self,
    cmd: store::UpdateTask<OpaqueTask, OpaqueValue>,
  ) -> Result<TaskRevId, store::UpdateTaskError> {
    let task = self
      .tasks
      .get_mut(&cmd.rev_id.id)
      .ok_or(store::UpdateTaskError::NotFound(cmd.rev_id.id))?;
    if task.revision != cmd.rev_id.rev {
      return Err(store::UpdateTaskError::Conflict {
        task: cmd.rev_id.id,
        expected: cmd.rev_id.rev,
        actual: task.revision,
      });
    }
    task
      .revision
      .checked_add(1)
      .ok_or(store::UpdateTaskError::RevisionOverflow(cmd.rev_id.id))?;
    task.polled_at = Some(cmd.tick);
    task.status = cmd.status;
    task.starvation = cmd.starvation;
    task.state = cmd.state;
    task.output = cmd.output;
    let new_rev_id = task.rev_id();
    Ok(new_rev_id)
  }

  pub fn create_timer(&mut self, cmd: store::CreateTimer) -> Result<(), store::CreateTimerError> {
    if !self.tasks.contains_key(&cmd.task_id) {
      return Err(store::CreateTimerError::NotFound(cmd.task_id));
    }
    self.timers.insert((cmd.deadline, cmd.task_id));
    Ok(())
  }

  pub fn next_timer(&self) -> Option<Instant> {
    self.timers.first().map(|(deadline, _task_id)| *deadline)
  }

  pub fn on_timer(&mut self, time: Instant) -> Result<(), store::OnTimerError> {
    let mut updated: HashSet<TaskId> = HashSet::new();
    self.timers.retain(|(deadline, task_id)| {
      if *deadline <= time {
        updated.insert(*task_id);
        false
      } else {
        true
      }
    });
    for task_id in updated {
      let task = self
        .tasks
        .get_mut(&task_id)
        .expect("task must exists since it had a timer");
      // TODO: this need way more care...
      task.status = TaskStatus::Available;
    }
    Ok(())
  }
}
