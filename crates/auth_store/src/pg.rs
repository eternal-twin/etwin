use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::auth::{
  AuthStore, CreateSessionOptions, CreateValidatedEmailVerificationOptions, RawDeleteAllSessions, RawSession, SessionId,
};
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, SecretString};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserId;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use sqlx::PgPool;

pub struct PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
  #[allow(unused)]
  database_secret: SecretString,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(
    clock: TyClock,
    database: TyDatabase,
    uuid_generator: TyUuidGenerator,
    database_secret: SecretString,
  ) -> Self {
    Self {
      clock,
      database,
      uuid_generator,
      database_secret,
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> AuthStore for PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_validated_email_verification(
    &self,
    _options: &CreateValidatedEmailVerificationOptions,
  ) -> Result<(), WeakError> {
    eprintln!("Warning: PgAuthStore#create_validated_email_verification is a no-op stub");
    Ok(())
  }

  async fn create_session(&self, options: &CreateSessionOptions) -> Result<RawSession, WeakError> {
    let session_id = SessionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      ctime: Instant,
    }

    let row: Row = sqlx::query_as::<_, Row>(
      r"
          INSERT INTO sessions(
            session_id, user_id, ctime, atime, data
          )
          VALUES (
            $1::SESSION_ID, $2::USER_ID, $3::INSTANT, $3::INSTANT, '{}'
          )
          RETURNING ctime;
          ",
    )
    .bind(session_id)
    .bind(options.user.id)
    .bind(now)
    .fetch_one(&*self.database)
    .await
    .map_err(WeakError::wrap)?;
    Ok(RawSession {
      id: session_id,
      user: options.user,
      ctime: row.ctime,
      atime: row.ctime,
    })
  }

  async fn get_and_touch_session(&self, session: SessionId) -> Result<Option<RawSession>, WeakError> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      ctime: Instant,
      atime: Instant,
      user_id: UserId,
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r"
      UPDATE sessions
      SET atime = $2::INSTANT
      WHERE session_id = $1::SESSION_ID
      RETURNING sessions.ctime, sessions.atime, sessions.user_id;
      ",
    )
    .bind(session)
    .bind(now)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(row.map(|row| RawSession {
      id: session,
      user: row.user_id.into(),
      ctime: row.ctime,
      atime: row.atime,
    }))
  }

  async fn delete_all_sessions(&self, cmd: &RawDeleteAllSessions) -> Result<(), WeakError> {
    // language=PostgreSQL
    let _result = sqlx::query(
      r"
      DELETE FROM sessions
      WHERE user_id = $1::USER_ID
      ",
    )
    .bind(cmd.user.id)
    .execute(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(())
  }
}

#[cfg(test)]
mod test {
  use super::PgAuthStore;
  use crate::test::TestApi;
  use eternaltwin_core::auth::AuthStore;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_user_store::pg::PgUserStore;
  use opentelemetry::trace::noop::NoopTracerProvider;
  use opentelemetry::trace::TracerProvider;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn AuthStore>, Arc<VirtualClock>, Arc<dyn UserStore>> {
    let config = eternaltwin_config::Config::for_test();
    let tracer_provider = NoopTracerProvider::new();
    let tracer = tracer_provider.tracer("auth_store_test");

    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let database_secret = SecretString::new("dev_secret".to_string());
    let uuid_generator = Arc::new(Uuid4Generator);
    let auth_store: Arc<dyn AuthStore> = Arc::new(PgAuthStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      uuid_generator,
      database_secret.clone(),
    ));

    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      database_secret,
      tracer,
      Uuid4Generator,
    ));

    TestApi {
      auth_store,
      clock,
      user_store,
    }
  }

  test_dinoparc_store!(
    #[serial]
    || make_test_api().await
  );
}
