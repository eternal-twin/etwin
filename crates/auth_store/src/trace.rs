use async_trait::async_trait;
use eternaltwin_core::auth::{
  AuthStore, CreateSessionOptions, CreateValidatedEmailVerificationOptions, RawDeleteAllSessions, RawSession, SessionId,
};
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use opentelemetry::trace::{FutureExt, Status, TraceContextExt, Tracer};
use opentelemetry::{Context, KeyValue};
use std::borrow::Cow;

pub struct TraceAuthStore<TyClock, TyTracer, TyAuthStore> {
  clock: TyClock,
  tracer: TyTracer,
  inner: TyAuthStore,
}

impl<TyClock, TyTracer, TyAuthStore> TraceAuthStore<TyClock, TyTracer, TyAuthStore> {
  pub fn new(clock: TyClock, tracer: TyTracer, inner: TyAuthStore) -> Self {
    Self { clock, tracer, inner }
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyAuthStore> AuthStore for TraceAuthStore<TyClock, TyTracer, TyAuthStore>
where
  TyClock: ClockRef,
  TyAuthStore: AuthStore + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn create_validated_email_verification(
    &self,
    _options: &CreateValidatedEmailVerificationOptions,
  ) -> Result<(), WeakError> {
    eprintln!("Warning: TraceAuthStore#create_validated_email_verification is a no-op stub");
    Ok(())
  }

  async fn create_session(&self, cmd: &CreateSessionOptions) -> Result<RawSession, WeakError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthStore::create_session")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.create_session(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new("user.id", v.user.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_and_touch_session(&self, cmd: SessionId) -> Result<Option<RawSession>, WeakError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthStore::get_and_touch_session")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_and_touch_session(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        match v.as_ref() {
          Some(v) => {
            span.set_attribute(KeyValue::new("id", v.id.to_string()));
            span.set_attribute(KeyValue::new("user.id", v.user.id.to_string()));
          }
          None => {
            span.set_attribute(KeyValue::new("id", false));
            span.set_attribute(KeyValue::new("user.id", false));
          }
        }
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn delete_all_sessions(&self, cmd: &RawDeleteAllSessions) -> Result<(), WeakError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthStore::delete_all_sessions")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.delete_all_sessions(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(()) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
