use core::fmt;
use eternaltwin_core::core::{FinitePeriod, Instant};
use eternaltwin_core::types::WeakError;
use futures_util::future::BoxFuture;
use opentelemetry::logs::{LogError, LogResult};
use opentelemetry::trace::{SpanId, TraceError};
use opentelemetry::{Array, ExportError, InstrumentationLibrary, Value};
use opentelemetry_sdk::export::logs::{LogBatch, LogExporter};
use opentelemetry_sdk::export::trace::{ExportResult, SpanData, SpanExporter};
use opentelemetry_sdk::logs::LogRecord;
use opentelemetry_sdk::Resource;
use std::sync::{Arc, Mutex};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("write failed")]
struct JsonlWriteError(#[source] WeakError);

impl ExportError for JsonlWriteError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("flush failed")]
struct JsonlFlushError(#[source] WeakError);

impl ExportError for JsonlFlushError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("exporter was shut down")]
struct ShutExporterError;

impl ExportError for ShutExporterError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

pub struct JsonlOtelExporter<W: ?Sized> {
  writer: Option<Arc<Mutex<W>>>,
  resource: Resource,
}

impl<W: ?Sized> JsonlOtelExporter<W> {
  pub const NAME: &'static str = "JsonlOtelExporter";

  pub fn new(writer: Arc<Mutex<W>>) -> Self {
    Self {
      writer: Some(writer),
      resource: Resource::empty(),
    }
  }
}

impl<W: ?Sized> fmt::Debug for JsonlOtelExporter<W> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_str("JsonOtelExporter(...)")
  }
}

impl<W: ?Sized> SpanExporter for JsonlOtelExporter<W>
where
  W: std::io::Write + Send + Sync,
{
  fn export(&mut self, batch: Vec<SpanData>) -> BoxFuture<'static, ExportResult> {
    let mut res: ExportResult = Ok(());
    if let Some(writer) = self.writer.as_deref() {
      let writer = writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
      match writer {
        Ok(mut writer) => {
          let writer = &mut *writer;

          for span in batch {
            let span = JsonSpan::new(&self.resource, &span);
            let write_res = serde_json::to_writer(&mut *writer, &span)
              .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
            if let Err(e) = write_res {
              res = Err(e);
              break;
            }
            let write_res = writer
              .write_all(b"\n")
              .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
            if let Err(e) = write_res {
              res = Err(e);
              break;
            }
          }
        }
        Err(e) => res = Err(e),
      }
    } else {
      res = Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn shutdown(&mut self) {
    self.writer = None;
  }

  fn force_flush(&mut self) -> BoxFuture<'static, ExportResult> {
    let res = if let Some(writer) = self.writer.as_mut() {
      writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .and_then(|mut w| w.flush())
        .map_err(|e| TraceError::ExportFailed(Box::new(JsonlFlushError(WeakError::wrap(e)))))
    } else {
      Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.resource = resource.clone();
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct JsonSpan<S: AsRef<str>> {
  trace_id: String,
  span_id: String,
  parent_span_id: Option<String>,
  name: S,
  period: FinitePeriod,
  attributes: Vec<(S, serde_json::Value)>, // todo: trace_state, flags, kind, dropped_attributes_count, events, dropped_events_count, links, dropped_links_count, status
}

impl<'a> JsonSpan<&'a str> {
  pub fn new(resource: &'a Resource, span: &'a SpanData) -> Self {
    Self {
      trace_id: span.span_context.trace_id().to_string(),
      span_id: span.span_context.span_id().to_string(),
      parent_span_id: if span.parent_span_id == SpanId::INVALID {
        None
      } else {
        Some(span.parent_span_id.to_string())
      },
      name: span.name.as_ref(),
      period: FinitePeriod {
        start: Instant::from_system(span.start_time),
        end: Instant::from_system(span.end_time),
      },
      attributes: resource
        .iter()
        .chain(span.attributes.iter().map(|kv| (&kv.key, &kv.value)))
        .map(|(key, value)| (key.as_str(), otel_value_to_json(value)))
        .collect(),
    }
  }
}

fn otel_value_to_json(otel: &Value) -> serde_json::Value {
  match otel {
    Value::Bool(v) => serde_json::Value::Bool(*v),
    Value::I64(v) => serde_json::Value::Number(serde_json::Number::from(*v)),
    Value::F64(v) => serde_json::Number::from_f64(*v)
      .map(serde_json::Value::Number)
      .unwrap_or(serde_json::Value::Null),
    Value::String(v) => serde_json::Value::String(v.to_string()),
    Value::Array(Array::Bool(v)) => serde_json::Value::Array(v.iter().map(|v| serde_json::Value::Bool(*v)).collect()),
    Value::Array(Array::I64(v)) => serde_json::Value::Array(
      v.iter()
        .map(|v| serde_json::Value::Number(serde_json::Number::from(*v)))
        .collect(),
    ),
    Value::Array(Array::F64(v)) => serde_json::Value::Array(
      v.iter()
        .map(|v| {
          serde_json::Number::from_f64(*v)
            .map(serde_json::Value::Number)
            .unwrap_or(serde_json::Value::Null)
        })
        .collect(),
    ),
    Value::Array(Array::String(v)) => {
      serde_json::Value::Array(v.iter().map(|v| serde_json::Value::String(v.to_string())).collect())
    }
  }
}

impl<W: ?Sized> LogExporter for JsonlOtelExporter<W>
where
  W: std::io::Write + Send + Sync,
{
  fn export<'life0, 'life1, 'async_trait>(
    &'life0 mut self,
    batch: LogBatch<'life1>,
  ) -> BoxFuture<'async_trait, LogResult<()>>
  where
    'life0: 'async_trait,
    'life1: 'async_trait,
    Self: 'async_trait,
  {
    let mut res: LogResult<()> = Ok(());
    if let Some(writer) = self.writer.as_deref() {
      let writer = writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .map_err(|e| LogError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
      match writer {
        Ok(mut writer) => {
          let writer = &mut *writer;
          for (record, instrumentation_lib) in batch.iter() {
            let log = JsonlLog(&self.resource, instrumentation_lib, record);
            let write_res = writeln!(writer, "{log}")
              .map_err(|e| LogError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
            if let Err(e) = write_res {
              res = Err(e);
              break;
            }
          }
        }
        Err(e) => res = Err(e),
      }
    } else {
      res = Err(LogError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn shutdown(&mut self) {
    self.writer.take();
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.resource = resource.clone();
  }
}

struct JsonlLog<'l>(&'l Resource, &'l InstrumentationLibrary, &'l LogRecord);

impl fmt::Display for JsonlLog<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let (_resource, _instr_lib, log) = (self.0, self.1, self.2);
    write!(f, "{:?}", &log.body)
  }
}
