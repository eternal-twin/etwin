use crate::opentelemetry::human::HumanOtelExporter;
use crate::opentelemetry::hyper_client::HyperClient;
use crate::opentelemetry::jsonl::JsonlOtelExporter;
use eternaltwin_config::opentelemetry::exporter::ExporterConfig;
use eternaltwin_core::types::DisplayErrorChain;
use opentelemetry::logs::{LogResult, Severity};
use opentelemetry::trace::TraceResult;
use opentelemetry::{Context, InstrumentationLibrary};
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_sdk::export::logs::{LogBatch, LogExporter};
use opentelemetry_sdk::export::trace::{SpanData, SpanExporter};
use opentelemetry_sdk::logs::{BatchLogProcessor, LogData, LogProcessor, LogRecord};
use opentelemetry_sdk::trace::{BatchSpanProcessor, Span, SpanProcessor};
use opentelemetry_sdk::Resource;
use smallvec::SmallVec;
use std::collections::{BTreeMap, HashMap};
use std::str::FromStr;
use std::sync::{Arc, Mutex, RwLock};
use tonic::metadata::{MetadataKey, MetadataMap};

#[derive(Clone)]
pub struct OpentelemetryBackendProvider {
  config: BTreeMap<String, ExporterConfig<()>>,
  stdout: Arc<Mutex<dyn std::io::Write + Send + Sync + 'static>>,
}

impl OpentelemetryBackendProvider {
  pub(crate) fn from_config<TyMeta>(
    config: &BTreeMap<String, ExporterConfig<TyMeta>>,
    stdout: Arc<Mutex<dyn std::io::Write + Send + Sync + 'static>>,
  ) -> Self {
    Self {
      config: config
        .iter()
        .map(|(k, v)| (k.clone(), v.clone_without_meta()))
        .collect(),
      stdout,
    }
  }

  pub fn empty() -> Self {
    Self {
      config: BTreeMap::new(),
      stdout: Arc::new(Mutex::new(Vec::<u8>::new())),
    }
  }

  /// instantiate fresh span exporters
  fn span_exporters(&self) -> Vec<Box<dyn SpanExporter>> {
    let mut res: Vec<Box<dyn SpanExporter>> = Vec::new();
    for exporter_config in self.config.values() {
      match exporter_config.r#type.value.as_str() {
        "Grpc" => {
          let exporter = opentelemetry_otlp::new_exporter()
            .tonic()
            .with_endpoint(exporter_config.endpoint.value.as_str())
            .with_timeout(exporter_config.timeout.value.into_std())
            .with_metadata({
              let mut metadata = MetadataMap::with_capacity(exporter_config.metadata.value.len());
              for (key, value) in exporter_config.metadata.value.iter() {
                metadata.insert(
                  MetadataKey::from_str(key).expect("invalid metadata key"),
                  value.value.parse().unwrap(),
                );
              }
              metadata
            });
          res.push(Box::new(
            exporter.build_span_exporter().expect("grpc exporter is valid"),
          ));
        }
        "HttpJson" => {
          let exporter = opentelemetry_otlp::new_exporter()
            .http()
            .with_endpoint(exporter_config.endpoint.value.as_str())
            .with_timeout(exporter_config.timeout.value.into_std())
            .with_http_client(HyperClient::default())
            .with_headers({
              let mut metadata: HashMap<String, String> = HashMap::new();
              for (key, value) in exporter_config.metadata.value.iter() {
                metadata.insert(key.clone(), value.value.clone());
              }
              metadata
            });
          res.push(Box::new(
            exporter.build_span_exporter().expect("http exporter is valid"),
          ));
        }
        "Human" => {
          let exporter = HumanOtelExporter::new(Arc::clone(&self.stdout));
          res.push(Box::new(exporter));
        }
        "Jsonl" => {
          let exporter = JsonlOtelExporter::new(Arc::clone(&self.stdout));
          res.push(Box::new(exporter));
        }
        ty => unimplemented!("exporter type {ty:?}"),
      };
    }
    res
  }

  /// instantiate fresh log exporters
  fn log_exporters(&self) -> Vec<Box<dyn LogExporter>> {
    let mut res: Vec<Box<dyn LogExporter>> = Vec::new();
    for exporter_config in self.config.values() {
      match exporter_config.r#type.value.as_str() {
        "Grpc" => {
          let exporter = opentelemetry_otlp::new_exporter()
            .tonic()
            .with_endpoint(exporter_config.endpoint.value.as_str())
            .with_timeout(exporter_config.timeout.value.into_std())
            .with_metadata({
              let mut metadata = MetadataMap::with_capacity(exporter_config.metadata.value.len());
              for (key, value) in exporter_config.metadata.value.iter() {
                metadata.insert(
                  MetadataKey::from_str(key).expect("invalid metadata key"),
                  value.value.parse().unwrap(),
                );
              }
              metadata
            });
          res.push(Box::new(exporter.build_log_exporter().expect("grpc exporter is valid")));
        }
        "HttpJson" => {
          let exporter = opentelemetry_otlp::new_exporter()
            .http()
            .with_endpoint(exporter_config.endpoint.value.as_str())
            .with_timeout(exporter_config.timeout.value.into_std())
            .with_http_client(HyperClient::default())
            .with_headers({
              let mut metadata: HashMap<String, String> = HashMap::new();
              for (key, value) in exporter_config.metadata.value.iter() {
                metadata.insert(key.clone(), value.value.clone());
              }
              metadata
            });
          res.push(Box::new(exporter.build_log_exporter().expect("http exporter is valid")));
        }
        "Human" => {
          let exporter = HumanOtelExporter::new(Arc::clone(&self.stdout));
          res.push(Box::new(exporter));
        }
        "Jsonl" => {
          let exporter = JsonlOtelExporter::new(Arc::clone(&self.stdout));
          res.push(Box::new(exporter));
        }
        ty => unimplemented!("exporter type {ty:?}"),
      };
    }
    res
  }

  pub fn write_span_batch(&self, resource: &Resource, spans: &[SpanData]) {
    if spans.is_empty() {
      return;
    }
    let mut exporters = self.span_exporters();
    let resource = resource.clone();
    let batch = Vec::from(spans);
    tokio::spawn(async move {
      for exporter in exporters.iter_mut() {
        exporter.set_resource(&resource);
        if let Err(e) = exporter.export(batch.clone()).await {
          eprintln!("error exporting traces: {}", DisplayErrorChain(&e));
        }
        exporter.shutdown();
      }
    });
  }

  pub fn write_log_batch(&self, resource: &Resource, logs: &[LogData]) {
    if logs.is_empty() {
      return;
    }
    let mut exporters = self.log_exporters();
    let resource = resource.clone();
    let batch = Vec::from(logs);
    tokio::spawn(async move {
      let batch: Vec<(_, _)> = batch.iter().map(|ld| (&ld.record, &ld.instrumentation)).collect();
      for exporter in exporters.iter_mut() {
        exporter.set_resource(&resource);
        if let Err(e) = exporter.export(LogBatch::new(&batch)).await {
          eprintln!("error exporting traces: {}", DisplayErrorChain(&e));
        }
        exporter.shutdown();
      }
    });
  }
}

#[derive(Debug)]
struct SharedProcessorState<TyProc: ?Sized> {
  /// Last value received in the outer `set_resource`.
  resource: Resource,
  processor: TyProc,
}

#[derive(Default, Debug, Clone)]
#[allow(unused)]
pub struct OpentelemetryBackend {
  log_processors: SmallVec<[SharedLogProcessor; 3]>,
  span_processors: SmallVec<[SharedSpanProcessor; 3]>,
}

#[allow(unused)]
impl OpentelemetryBackend {
  pub fn new() -> Self {
    Self {
      log_processors: SmallVec::new(),
      span_processors: SmallVec::new(),
    }
  }

  /// Register a new log exporter using a batch processor
  pub fn batch_log_exporter<TyLogExporter: LogExporter + 'static>(
    &mut self,
    exporter: TyLogExporter,
  ) -> SharedLogProcessor {
    let processor = BatchLogProcessor::builder(exporter, opentelemetry_sdk::runtime::Tokio).build();
    let shared = SharedLogProcessor(Arc::new(Mutex::new(SharedProcessorState {
      resource: Resource::empty(),
      processor,
    })));
    self.log_processors.push(shared.clone());
    shared
  }

  /// Register a new span exporter using a batch processor
  pub fn batch_span_exporter<TySpanExporter: SpanExporter + 'static>(
    &mut self,
    exporter: TySpanExporter,
  ) -> SharedSpanProcessor {
    let processor = BatchSpanProcessor::builder(exporter, opentelemetry_sdk::runtime::Tokio).build();
    let shared = SharedSpanProcessor(Arc::new(RwLock::new(SharedProcessorState {
      resource: Resource::empty(),
      processor,
    })));
    self.span_processors.push(shared.clone());
    shared
  }

  pub fn span_processors(&self) -> impl Iterator<Item = &SharedSpanProcessor> {
    self.span_processors.iter()
  }

  pub fn log_processors(&self) -> impl Iterator<Item = &SharedLogProcessor> {
    self.log_processors.iter()
  }
}

#[derive(Debug, Clone)]
pub struct SharedLogProcessor(Arc<Mutex<SharedProcessorState<dyn LogProcessor>>>);

impl SharedLogProcessor {
  #[allow(unused)]
  pub fn write_batch(&self, resource: &Resource, logs: &[LogData]) {
    if logs.is_empty() {
      return;
    }
    let mut inner = self
      .0
      .lock()
      .expect("SharedLogProcessor::write_batch should succeed locking the inner value");
    let state = &mut *inner;
    let needs_flush = &state.resource != resource;
    if needs_flush {
      state
        .processor
        .force_flush()
        .expect("SharedLogProcessor::write_batch should succeed flush");
      state.processor.set_resource(resource);
    }
    for log in logs {
      state.processor.emit(&mut log.record.clone(), &log.instrumentation);
    }
    if needs_flush {
      state
        .processor
        .force_flush()
        .expect("SharedLogProcessor::write_batch should succeed flush");
    }
    state.processor.set_resource(&state.resource);
  }
}

impl LogProcessor for SharedLogProcessor {
  fn emit(&self, data: &mut LogRecord, instrumentation: &InstrumentationLibrary) {
    self
      .0
      .lock()
      .expect("SharedLogProcessor::emit should succeed locking the inner value")
      .processor
      .emit(data, instrumentation)
  }

  fn force_flush(&self) -> LogResult<()> {
    self
      .0
      .lock()
      .expect("SharedLogProcessor::force_flush should succeed locking the inner value")
      .processor
      .force_flush()
  }

  fn shutdown(&self) -> LogResult<()> {
    self
      .0
      .lock()
      .expect("SharedLogProcessor::shutdown should succeed locking the inner value")
      .processor
      .shutdown()
  }

  fn event_enabled(&self, level: Severity, target: &str, name: &str) -> bool {
    self
      .0
      .lock()
      .expect("SharedLogProcessor::event_enabled should succeed locking the inner value")
      .processor
      .event_enabled(level, target, name)
  }

  fn set_resource(&self, resource: &Resource) {
    let mut inner = self
      .0
      .lock()
      .expect("SharedLogProcessor::set_resource should succeed locking the inner value");
    let state = &mut *inner;
    if &state.resource != resource {
      state
        .processor
        .force_flush()
        .expect("SharedSpanProcessor::set_resource should succeed flush");
      state.resource = resource.clone();
    }
    state.processor.set_resource(resource)
  }
}

#[derive(Debug, Clone)]
pub struct SharedSpanProcessor(Arc<RwLock<SharedProcessorState<dyn SpanProcessor>>>);

impl SharedSpanProcessor {
  pub fn write_batch(&self, resource: &Resource, spans: &[SpanData]) {
    if spans.is_empty() {
      return;
    }
    let mut inner = self
      .0
      .write()
      .expect("SharedSpanProcessor::write_batch should succeed locking the inner value");
    let state = &mut *inner;
    let needs_flush = &state.resource != resource;
    if needs_flush {
      state
        .processor
        .force_flush()
        .expect("SharedSpanProcessor::write_batch should succeed flush");
      state.processor.set_resource(resource);
    }
    for span in spans {
      state.processor.on_end(span.clone());
    }
    if needs_flush {
      state
        .processor
        .force_flush()
        .expect("SharedSpanProcessor::write_batch should succeed flush");
    }
    state.processor.set_resource(&state.resource);
  }
}

impl SpanProcessor for SharedSpanProcessor {
  fn on_start(&self, span: &mut Span, cx: &Context) {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::on_start should succeed locking the inner value")
      .processor
      .on_start(span, cx)
  }

  fn on_end(&self, span: SpanData) {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::on_end should succeed locking the inner value")
      .processor
      .on_end(span)
  }

  fn force_flush(&self) -> TraceResult<()> {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::force_flush should succeed locking the inner value")
      .processor
      .force_flush()
  }

  fn shutdown(&self) -> TraceResult<()> {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::shutdown should succeed locking the inner value")
      .processor
      .shutdown()
  }

  fn set_resource(&mut self, resource: &Resource) {
    let mut inner = self
      .0
      .write()
      .expect("SharedSpanProcessor::set_resource should succeed locking the inner value");
    let state = &mut *inner;
    if &state.resource != resource {
      state
        .processor
        .force_flush()
        .expect("SharedSpanProcessor::set_resource should succeed flush");
      state.resource = resource.clone();
    }
    state.processor.set_resource(resource)
  }
}
