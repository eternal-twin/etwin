use crate::ConfigValue;
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::types::DisplayErrorChain;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct AppConfig<TyMeta> {
  pub display_name: ConfigValue<String, TyMeta>,
  pub uri: ConfigValue<Url, TyMeta>,
  pub oauth_callback: ConfigValue<Url, TyMeta>,
  pub secret: ConfigValue<String, TyMeta>,
}

impl<TyMeta> AppConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch_ref(&mut self, config: PartialAppConfig, meta: TyMeta) {
    if let SimplePatch::Set(display_name) = config.display_name {
      self.display_name = ConfigValue::new_meta(display_name, meta.clone());
    }
    if let SimplePatch::Set(uri) = config.uri {
      self.uri = ConfigValue::new_meta(uri, meta.clone());
    }
    if let SimplePatch::Set(oauth_callback) = config.oauth_callback {
      self.oauth_callback = ConfigValue::new_meta(oauth_callback, meta.clone());
    }
    if let SimplePatch::Set(secret) = config.secret {
      self.secret = ConfigValue::new_meta(secret, meta.clone());
    }
  }

  pub(crate) fn default_for_key(key: &str, meta: TyMeta) -> Self {
    Self {
      display_name: ConfigValue::new_meta(key.to_string(), meta.clone()),
      uri: ConfigValue::new_meta(
        {
          let s = format!("http://{key}.localhost/");
          match Url::parse(s.as_str()) {
            Ok(u) => u,
            Err(e) => panic!("invalid default uri {s:?}: {}", DisplayErrorChain(&e)),
          }
        },
        meta.clone(),
      ),
      oauth_callback: ConfigValue::new_meta(
        {
          let s = format!("http://{key}.localhost/oauth/callback");
          match Url::parse(s.as_str()) {
            Ok(u) => u,
            Err(e) => panic!("invalid default oauth_callback {s:?}: {}", DisplayErrorChain(&e)),
          }
        },
        meta.clone(),
      ),
      secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PartialAppConfig {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub display_name: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub uri: SimplePatch<Url>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub oauth_callback: SimplePatch<Url>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub secret: SimplePatch<String>,
}
