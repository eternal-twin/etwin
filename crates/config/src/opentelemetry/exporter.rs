use crate::ConfigValue;
use eternaltwin_core::core::Duration;
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct ExporterConfig<TyMeta> {
  pub r#type: ConfigValue<String, TyMeta>,
  pub color: ConfigValue<String, TyMeta>,
  pub target: ConfigValue<String, TyMeta>,
  pub endpoint: ConfigValue<String, TyMeta>,
  pub timeout: ConfigValue<Duration, TyMeta>,
  pub metadata: ConfigValue<BTreeMap<String, ConfigValue<String, TyMeta>>, TyMeta>,
}

impl<TyMeta> ExporterConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch_ref(&mut self, config: PartialExporterConfig, meta: TyMeta) {
    if let SimplePatch::Set(typ) = config.r#type {
      self.r#type = ConfigValue::new_meta(typ, meta.clone());
    }
    if let SimplePatch::Set(color) = config.color {
      self.color = ConfigValue::new_meta(color, meta.clone());
    }
    if let SimplePatch::Set(target) = config.target {
      self.target = ConfigValue::new_meta(target, meta.clone());
    }
    if let SimplePatch::Set(endpoint) = config.endpoint {
      self.endpoint = ConfigValue::new_meta(endpoint, meta.clone());
    }
    if let SimplePatch::Set(timeout) = config.timeout {
      self.timeout = ConfigValue::new_meta(timeout, meta.clone());
    }
    if let SimplePatch::Set(metadata) = config.metadata {
      match metadata {
        None => self.metadata.value.clear(),
        Some(metadata) => {
          for (key, value) in metadata {
            match value {
              None => self.metadata.value.remove(&key),
              Some(value) => self
                .metadata
                .value
                .insert(key, ConfigValue::new_meta(value, meta.clone())),
            };
          }
        }
      }
    }
  }

  pub(crate) fn default_for_key(_key: &str, meta: TyMeta) -> Self {
    Self {
      r#type: ConfigValue::new_meta("Human".to_string(), meta.clone()),
      color: ConfigValue::new_meta("Auto".to_string(), meta.clone()),
      target: ConfigValue::new_meta("eternaltwin://stdout".to_string(), meta.clone()),
      endpoint: ConfigValue::new_meta("http://localhost:4317/".to_string(), meta.clone()),
      timeout: ConfigValue::new_meta(Duration::from_seconds(5), meta.clone()),
      metadata: ConfigValue::new_meta(BTreeMap::new(), meta.clone()),
    }
  }
}

impl<TyMeta> ExporterConfig<TyMeta> {
  pub fn clone_without_meta(&self) -> ExporterConfig<()> {
    ExporterConfig {
      r#type: self.r#type.clone_without_meta(),
      color: self.color.clone_without_meta(),
      target: self.target.clone_without_meta(),
      endpoint: self.endpoint.clone_without_meta(),
      timeout: self.timeout.clone_without_meta(),
      metadata: ConfigValue::new(
        self
          .metadata
          .value
          .iter()
          .map(|(k, v)| (k.clone(), v.clone_without_meta()))
          .collect(),
      ),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PartialExporterConfig {
  /// Exporter type:
  /// - `Human`: Human-Readable logs
  /// - `Jsonl`: JSON Lines
  /// - `Grpc`: Opentelemetry Protocol (GRPC variant)
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub r#type: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub color: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub target: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub endpoint: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub timeout: SimplePatch<Duration>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub metadata: SimplePatch<Option<BTreeMap<String, Option<String>>>>,
}
