pub mod exporter;

use crate::opentelemetry::exporter::{ExporterConfig, PartialExporterConfig};
use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::core::Duration;
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct OpentelemetryConfig<TyMeta> {
  pub enabled: ConfigValue<bool, TyMeta>,
  pub attributes: ConfigValue<BTreeMap<String, ConfigValue<String, TyMeta>>, TyMeta>,
  pub exporter: ConfigValue<BTreeMap<String, ExporterConfig<TyMeta>>, TyMeta>,
}

impl<TyMeta> OpentelemetryConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: OpentelemetryConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(enabled) = config.enabled {
      self.enabled = ConfigValue::new_meta(enabled, meta.clone());
    }
    if let SimplePatch::Set(attributes) = config.attributes {
      match attributes {
        None => self.attributes.value.clear(),
        Some(attributes) => {
          for (key, value) in attributes {
            match value {
              None => self.attributes.value.remove(&key),
              Some(value) => self
                .attributes
                .value
                .insert(key, ConfigValue::new_meta(value, meta.clone())),
            };
          }
        }
      }
    }
    if let SimplePatch::Set(exporter) = config.exporter {
      match exporter {
        None => self.exporter.value.clear(),
        Some(exporter) => {
          for (key, exporter) in exporter {
            match exporter {
              None => drop(self.exporter.value.remove(&key)),
              Some(exporter) => {
                match self.exporter.value.entry(key) {
                  Entry::Vacant(e) => {
                    let c = ExporterConfig::default_for_key(e.key(), meta.clone());
                    e.insert(c).patch_ref(exporter, meta.clone())
                  }
                  Entry::Occupied(mut e) => e.get_mut().patch_ref(exporter, meta.clone()),
                };
              }
            }
          }
        }
      }
    }
    self
  }

  pub fn clone_without_meta(&self) -> OpentelemetryConfig<()> {
    OpentelemetryConfig {
      enabled: self.enabled.clone_without_meta(),
      attributes: ConfigValue::new(
        self
          .attributes
          .value
          .iter()
          .map(|(k, v)| (k.clone(), v.clone_without_meta()))
          .collect(),
      ),
      exporter: ConfigValue::new(
        self
          .exporter
          .value
          .iter()
          .map(|(k, v)| (k.clone(), v.clone_without_meta()))
          .collect(),
      ),
    }
  }
}

impl OpentelemetryConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    use BuiltinEternaltwinConfigProfile::*;
    Self {
      enabled: ConfigValue::new_meta(true, meta.clone()),
      attributes: ConfigValue::new_meta(
        [
          (
            "service.name".to_string(),
            ConfigValue::new_meta("eternaltwin".to_string(), meta.clone()),
          ),
          (
            "deployment.environment".to_string(),
            ConfigValue::new_meta("dev".to_string(), meta.clone()),
          ),
          (
            "service.version".to_string(),
            ConfigValue::new_meta(env!("CARGO_PKG_VERSION").to_string(), meta.clone()),
          ),
        ]
        .into_iter()
        .collect(),
        meta.clone(),
      ),
      exporter: ConfigValue::new_meta(
        match profile {
          Production | Test => BTreeMap::new(),
          Dev => [(
            "stdout".to_string(),
            ExporterConfig {
              r#type: ConfigValue::new_meta("Human".to_string(), meta.clone()),
              color: ConfigValue::new_meta("Auto".to_string(), meta.clone()),
              target: ConfigValue::new_meta("eternaltwin://stdout".to_string(), meta.clone()),
              endpoint: ConfigValue::new_meta("http://localhost:4317/".to_string(), meta.clone()),
              timeout: ConfigValue::new_meta(Duration::from_seconds(5), meta.clone()),
              metadata: ConfigValue::new_meta(BTreeMap::new(), meta.clone()),
            },
          )]
          .into_iter()
          .collect(),
          Sdk => [(
            "stdout".to_string(),
            ExporterConfig {
              r#type: ConfigValue::new_meta("Json".to_string(), meta.clone()),
              color: ConfigValue::new_meta("Never".to_string(), meta.clone()),
              target: ConfigValue::new_meta("eternaltwin://stdout".to_string(), meta.clone()),
              endpoint: ConfigValue::new_meta("http://localhost:4317/".to_string(), meta.clone()),
              timeout: ConfigValue::new_meta(Duration::from_seconds(5), meta.clone()),
              metadata: ConfigValue::new_meta(BTreeMap::new(), meta.clone()),
            },
          )]
          .into_iter()
          .collect(),
        },
        meta.clone(),
      ),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct OpentelemetryConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub enabled: SimplePatch<bool>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub attributes: SimplePatch<Option<BTreeMap<String, Option<String>>>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub exporter: SimplePatch<Option<BTreeMap<String, Option<PartialExporterConfig>>>>,
}
