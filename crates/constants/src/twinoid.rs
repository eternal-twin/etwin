pub use crate::generated::twinoid_achievements::ACHIEVEMENTS;
pub use crate::generated::twinoid_sites::{RESTRICTED_SITES, SITES};
pub use crate::generated::twinoid_stats::STATS;
pub use crate::generated::{ConstFullSite, ConstSiteIcon, ConstSiteInfo, ConstTwinoidAchievement, ConstTwinoidStat};
