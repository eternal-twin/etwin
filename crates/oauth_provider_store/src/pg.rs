use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::auth::EtwinOauthAccessTokenKey;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, SecretString};
use eternaltwin_core::oauth::{
  CreateStoredAccessTokenOptions, GetOauthAccessTokenOptions, GetOauthClientOptions, OauthClientDisplayName,
  OauthClientId, OauthClientKey, OauthClientRef, OauthProviderStore, RawCreateAccessTokenError, RawGetAccessTokenError,
  RawGetOauthClientError, RawGetOauthClientWithSecretError, RawUpsertSystemOauthClientError, SimpleOauthClient,
  SimpleOauthClientWithSecret, StoredOauthAccessToken, UpsertSystemClientOptions,
};
use eternaltwin_core::password::{PasswordHash, PasswordService, PasswordServiceRef};
use eternaltwin_core::user::{UserId, UserIdRef};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use sqlx::PgPool;
use url::Url;

pub struct PgOauthProviderStore<TyClock, TyDatabase, TyPassword, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyPassword: PasswordServiceRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  password: TyPassword,
  uuid_generator: TyUuidGenerator,
  database_secret: SecretString,
}

impl<TyClock, TyDatabase, TyPassword, TyUuidGenerator>
  PgOauthProviderStore<TyClock, TyDatabase, TyPassword, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyPassword: PasswordServiceRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(
    clock: TyClock,
    database: TyDatabase,
    password: TyPassword,
    uuid_generator: TyUuidGenerator,
    database_secret: SecretString,
  ) -> Self {
    Self {
      clock,
      database,
      password,
      uuid_generator,
      database_secret,
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyPassword, TyUuidGenerator> OauthProviderStore
  for PgOauthProviderStore<TyClock, TyDatabase, TyPassword, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyPassword: PasswordServiceRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn upsert_system_client(
    &self,
    options: &UpsertSystemClientOptions,
  ) -> Result<SimpleOauthClient, RawUpsertSystemOauthClientError> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      oauth_client_id: OauthClientId,
      key: OauthClientKey,
      // ctime: Instant,
      display_name: OauthClientDisplayName,
      app_uri: String,
      callback_uri: String,
      secret: PasswordHash,
      // owner_id: Option<UserId>,
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r"
      SELECT oauth_client_id, key, ctime,
          display_name, display_name_mtime,
          app_uri, app_uri_mtime,
          callback_uri, callback_uri_mtime,
          pgp_sym_decrypt_bytea(secret, $1::TEXT) AS secret, secret_mtime,
          owner_id
        FROM oauth_clients
        WHERE key = $2::VARCHAR;
      ",
    )
    .bind(self.database_secret.as_str())
    .bind(options.key.as_str())
    .fetch_optional(&*self.database)
    .await
    .map_err(RawUpsertSystemOauthClientError::other)?;

    match row {
      None => {
        let oauth_client_id = OauthClientId::from_uuid(self.uuid_generator.uuid_generator().next());
        let password_hash = self.password.password_service().hash(options.secret.clone());

        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          oauth_client_id: OauthClientId,
        }

        // language=PostgreSQL
        let row: Row = sqlx::query_as::<_, Row>(
          r"
          INSERT INTO oauth_clients(
            oauth_client_id, key, ctime,
            display_name, display_name_mtime,
            app_uri, app_uri_mtime,
            callback_uri, callback_uri_mtime,
            secret, secret_mtime,
            owner_id
          )
           VALUES (
             $2::OAUTH_CLIENT_ID, $3::VARCHAR, $4::INSTANT,
             $5::VARCHAR, $4::INSTANT,
             $6::VARCHAR, $4::INSTANT,
             $7::VARCHAR, $4::INSTANT,
             pgp_sym_encrypt_bytea($8::BYTEA, $1::TEXT), $4::INSTANT,
             NULL
           )
           RETURNING oauth_client_id;
      ",
        )
        .bind(self.database_secret.as_str())
        .bind(oauth_client_id)
        .bind(options.key.as_str())
        .bind(now)
        .bind(options.display_name.as_str())
        .bind(options.app_uri.as_str())
        .bind(options.callback_uri.as_str())
        .bind(password_hash)
        .fetch_one(&*self.database)
        .await
        .map_err(RawUpsertSystemOauthClientError::other)?;

        Ok(SimpleOauthClient {
          id: row.oauth_client_id,
          key: Some(options.key.clone()),
          display_name: options.display_name.clone(),
          app_uri: options.app_uri.clone(),
          callback_uri: options.callback_uri.clone(),
          owner: None,
        })
      }
      Some(row) => {
        debug_assert_eq!(row.key, options.key);
        let oauth_client_id = row.oauth_client_id;
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          oauth_client_id: OauthClientId,
        }

        if row.display_name != options.display_name {
          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            INSERT INTO old_oauth_client_display_names(
              oauth_client_id, start_time, display_name
            )
            SELECT oauth_client_id, display_name_mtime AS start_time, display_name
            FROM oauth_clients
            WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);

          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            UPDATE oauth_clients
            SET display_name = $2::VARCHAR, display_name_mtime = $3::INSTANT
              WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .bind(options.display_name.as_str())
          .bind(now)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);
        }
        if row.app_uri.as_str() != options.app_uri.as_str() {
          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            INSERT INTO old_oauth_client_app_uris(
              oauth_client_id, start_time, app_uri
            )
            SELECT oauth_client_id, app_uri_mtime AS start_time, app_uri
            FROM oauth_clients
            WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);

          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            UPDATE oauth_clients
            SET app_uri = $2::VARCHAR, app_uri_mtime = $3::INSTANT
              WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .bind(options.app_uri.as_str())
          .bind(now)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);
        }
        if row.callback_uri.as_str() != options.callback_uri.as_str() {
          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            INSERT INTO old_oauth_client_callback_uris(
              oauth_client_id, start_time, callback_uri
            )
            SELECT oauth_client_id, callback_uri_mtime AS start_time, callback_uri
            FROM oauth_clients
            WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);

          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            UPDATE oauth_clients
            SET callback_uri = $2::VARCHAR, callback_uri_mtime = $3::INSTANT
              WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .bind(options.callback_uri.as_str())
          .bind(now)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);
        }
        if !self
          .password
          .password_service()
          .verify(row.secret, options.secret.clone())
        {
          let password_hash = self.password.password_service().hash(options.secret.clone());

          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            INSERT INTO old_oauth_client_secrets(
              oauth_client_id, start_time, secret
            )
            SELECT oauth_client_id, secret_mtime AS start_time, secret
            FROM oauth_clients
            WHERE oauth_client_id = $1::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(oauth_client_id)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);

          // language=PostgreSQL
          let row: Row = sqlx::query_as::<_, Row>(
            r"
            UPDATE oauth_clients
            SET secret = pgp_sym_encrypt_bytea($3::BYTEA, $1::TEXT), secret_mtime = $4::INSTANT
              WHERE oauth_client_id = $2::UUID
            RETURNING oauth_client_id;
          ",
          )
          .bind(self.database_secret.as_str())
          .bind(oauth_client_id)
          .bind(password_hash)
          .bind(now)
          .fetch_one(&*self.database)
          .await
          .map_err(RawUpsertSystemOauthClientError::other)?;
          debug_assert_eq!(row.oauth_client_id, oauth_client_id);
        }

        Ok(SimpleOauthClient {
          id: row.oauth_client_id,
          key: Some(options.key.clone()),
          display_name: options.display_name.clone(),
          app_uri: options.app_uri.clone(),
          callback_uri: options.callback_uri.clone(),
          owner: None,
        })
      }
    }
  }

  async fn get_client(&self, options: &GetOauthClientOptions) -> Result<SimpleOauthClient, RawGetOauthClientError> {
    let mut ref_id: Option<OauthClientId> = None;
    let mut ref_key: Option<OauthClientKey> = None;
    match &options.r#ref {
      OauthClientRef::Id(r) => ref_id = Some(r.id),
      OauthClientRef::Key(r) => ref_key = Some(r.key.clone()),
    }

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      oauth_client_id: OauthClientId,
      key: Option<OauthClientKey>,
      // ctime: Instant,
      display_name: OauthClientDisplayName,
      app_uri: String,
      callback_uri: String,
      owner_id: Option<UserId>,
    }

    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT oauth_client_id, key, ctime, display_name, app_uri, callback_uri, owner_id
      FROM oauth_clients
      WHERE oauth_client_id = $1::OAUTH_CLIENT_ID OR key = $2::OAUTH_CLIENT_KEY;
      ",
    )
    .bind(ref_id)
    .bind(ref_key)
    .fetch_optional(&*self.database)
    .await
    .map_err(RawGetOauthClientError::other)?;

    let row: Row = row.ok_or_else(|| RawGetOauthClientError::NotFound(options.r#ref.clone()))?;

    Ok(SimpleOauthClient {
      id: row.oauth_client_id,
      key: row.key,
      display_name: row.display_name,
      app_uri: Url::parse(row.app_uri.as_str()).map_err(RawGetOauthClientError::other)?,
      callback_uri: Url::parse(row.callback_uri.as_str()).map_err(RawGetOauthClientError::other)?,
      owner: row.owner_id.map(UserIdRef::from),
    })
  }

  async fn get_client_with_secret(
    &self,
    options: &GetOauthClientOptions,
  ) -> Result<SimpleOauthClientWithSecret, RawGetOauthClientWithSecretError> {
    let mut ref_id: Option<OauthClientId> = None;
    let mut ref_key: Option<OauthClientKey> = None;
    match &options.r#ref {
      OauthClientRef::Id(r) => ref_id = Some(r.id),
      OauthClientRef::Key(r) => ref_key = Some(r.key.clone()),
    }

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      oauth_client_id: OauthClientId,
      key: Option<OauthClientKey>,
      // ctime: Instant,
      display_name: OauthClientDisplayName,
      app_uri: String,
      callback_uri: String,
      owner_id: Option<UserId>,
      secret: Vec<u8>,
    }

    let row = sqlx::query_as::<_, Row>(
      r"
      SELECT oauth_client_id, key, ctime, display_name, app_uri, callback_uri, owner_id, pgp_sym_decrypt_bytea(secret, $1::TEXT) AS secret
      FROM oauth_clients
      WHERE oauth_client_id = $2::OAUTH_CLIENT_ID OR key = $3::OAUTH_CLIENT_KEY;
      ",
    )
      .bind(self.database_secret.as_str())
      .bind(ref_id)
      .bind(ref_key)
      .fetch_optional(&*self.database)
      .await.map_err(RawGetOauthClientWithSecretError::other)?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(RawGetOauthClientWithSecretError::NotFound(options.r#ref.clone()));
    };

    Ok(SimpleOauthClientWithSecret {
      id: row.oauth_client_id,
      key: row.key,
      display_name: row.display_name,
      app_uri: Url::parse(row.app_uri.as_str()).map_err(RawGetOauthClientWithSecretError::other)?,
      callback_uri: Url::parse(row.callback_uri.as_str()).map_err(RawGetOauthClientWithSecretError::other)?,
      owner: row.owner_id.map(UserIdRef::from),
      secret: PasswordHash(row.secret),
    })
  }

  async fn create_access_token(
    &self,
    options: &CreateStoredAccessTokenOptions,
  ) -> Result<StoredOauthAccessToken, RawCreateAccessTokenError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      oauth_access_token_id: EtwinOauthAccessTokenKey,
      oauth_client_id: OauthClientId,
      user_id: UserId,
      ctime: Instant,
      atime: Instant,
    }

    let row = sqlx::query_as::<_, Row>(
      r"
      INSERT INTO oauth_access_tokens(
            oauth_access_token_id, oauth_client_id, user_id, ctime, atime
          )
          VALUES (
            $1::UUID, $2::OAUTH_CLIENT_ID, $3::USER_ID, $4::INSTANT, $4::INSTANT
          )
          RETURNING oauth_access_token_id, oauth_client_id, user_id, ctime, atime;
      ",
    )
    .bind(options.key.into_uuid())
    .bind(options.client.id)
    .bind(options.user.id)
    .bind(options.ctime)
    .fetch_one(&*self.database)
    .await
    .map_err(RawCreateAccessTokenError::other)?;

    Ok(StoredOauthAccessToken {
      key: row.oauth_access_token_id,
      created_at: row.ctime,
      accessed_at: row.atime,
      expires_at: row.atime,
      user: row.user_id.into(),
      client: row.oauth_client_id.into(),
    })
  }

  async fn get_access_token(
    &self,
    options: &GetOauthAccessTokenOptions,
  ) -> Result<StoredOauthAccessToken, RawGetAccessTokenError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      oauth_access_token_id: EtwinOauthAccessTokenKey,
      oauth_client_id: OauthClientId,
      user_id: UserId,
      ctime: Instant,
      atime: Instant,
    }

    let row: Option<Row> = if options.touch_accessed_at {
      sqlx::query_as::<_, Row>(
        r"
        UPDATE oauth_access_tokens
        SET atime = NOW()
        WHERE oauth_access_token_id = $1::ETWIN_OAUTH_ACCESS_TOKEN_ID
        RETURNING oauth_access_token_id, oauth_client_id, user_id, ctime, atime;
      ",
      )
      .bind(options.key.into_uuid())
      .fetch_optional(&*self.database)
      .await
      .map_err(RawGetAccessTokenError::other)?
    } else {
      sqlx::query_as::<_, Row>(
        r"
        SELECT oauth_access_token_id, oauth_client_id, user_id, ctime, atime
        FROM oauth_access_tokens
        WHERE oauth_access_token_id = $1::OAUTH_ACCESS_TOKEN_ID;
       ",
      )
      .bind(options.key.into_uuid())
      .fetch_optional(&*self.database)
      .await
      .map_err(RawGetAccessTokenError::other)?
    };

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(RawGetAccessTokenError::NotFound);
    };

    Ok(StoredOauthAccessToken {
      key: row.oauth_access_token_id,
      created_at: row.ctime,
      accessed_at: row.atime,
      expires_at: row.atime,
      user: row.user_id.into(),
      client: row.oauth_client_id.into(),
    })
  }
}

#[cfg(test)]
mod test {
  use super::PgOauthProviderStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::oauth::OauthProviderStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_password::scrypt::ScryptPasswordService;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn OauthProviderStore>> {
    let config = eternaltwin_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let database_secret = SecretString::new("dev_secret".to_string());
    let password = Arc::new(ScryptPasswordService::with_os_rng(
      config.scrypt.max_time.value,
      config.scrypt.max_mem_frac.value,
    ));
    let uuid_generator = Arc::new(Uuid4Generator);
    let oauth_provider_store: Arc<dyn OauthProviderStore> = Arc::new(PgOauthProviderStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      password,
      uuid_generator,
      database_secret,
    ));

    TestApi {
      clock,
      oauth_provider_store,
    }
  }

  test_oauth_provider_store!(
    #[serial]
    || make_test_api().await
  );
}
