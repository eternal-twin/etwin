use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef, VirtualClock};
use eternaltwin_core::core::{Duration, FinitePeriod, IdempotencyKey, Instant};
use eternaltwin_core::email::EmailAddress;
use eternaltwin_core::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use eternaltwin_core::mailer::store::{
  EmailContentPayload, EmailContentSummary, EmailPayloadKind, EmailRequestStatus, MailerStore, MailerStoreRef,
  OutboundEmail, OutboundEmailIdRef, OutboundEmailRequest, OutboundEmailRequestIdRef,
};
use eternaltwin_core::user::{CreateUserOptions, UserIdRef, UserStore, UserStoreRef};
use sqlx::types::JsonValue;
use std::str::FromStr;

macro_rules! test_mailer_store {
  ($(#[$meta:meta])* || $api:expr) => {
    crate::store::test::register_test!($(#[$meta])*, $api, create_oubound_email);
    crate::store::test::register_test!($(#[$meta])*, $api, create_oubound_email_deadline_now);
    crate::store::test::register_test!($(#[$meta])*, $api, outbound_request_acquire_release);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::store::test::$test_name($api).await;
    }
  };
}

// macro_rules! assert_ok {
//   ($result:expr $(,)?) => {{
//     match &$result {
//       Err(_) => {
//         panic!("assertion failed: `result.is_ok()`: {:?}", &$result)
//       }
//       Ok(()) => {}
//     }
//   }};
// }

pub(crate) struct TestApi<TyClock, TyMailerStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyUserStore: UserStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) mailer_store: TyMailerStore,
  pub(crate) user_store: TyUserStore,
}

impl<TyClock, TyMailerStore, TyUserStore> ClockRef for TestApi<TyClock, TyMailerStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyUserStore: UserStoreRef,
{
  type Clock = VirtualClock;

  fn clock(&self) -> &Self::Clock {
    self.clock.clock()
  }
}

impl<TyClock, TyMailerStore, TyUserStore> MailerStoreRef<JsonValue> for TestApi<TyClock, TyMailerStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyUserStore: UserStoreRef,
{
  type MailerStore = TyMailerStore::MailerStore;

  fn mailer_store(&self) -> &Self::MailerStore {
    self.mailer_store.mailer_store()
  }
}

impl<TyClock, TyMailerStore, TyUserStore> UserStoreRef for TestApi<TyClock, TyMailerStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyUserStore: UserStoreRef,
{
  type UserStore = TyUserStore::UserStore;

  fn user_store(&self) -> &Self::UserStore {
    self.user_store.user_store()
  }
}

pub(crate) async fn create_oubound_email<Api>(api: Api)
where
  Api: ClockRef<Clock = VirtualClock> + MailerStoreRef<JsonValue> + UserStoreRef,
{
  api.clock().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  let actual = api
    .mailer_store()
    .create_outbound_email(CreateOutboundEmail {
      idempotency_key: IdempotencyKey::from_u128(1),
      now: api.clock().now(),
      actor: UserIdRef::new(alice.id),
      deadline: api.clock().now() + Duration::from_millis(5000),
      sender: EmailAddress::from_str("support@eternaltwin.localhost").expect("support email is valid"),
      recipient: EmailAddress::from_str("alice@example.localhost").expect("recipient email is valid"),
      payload: EmailContentPayload {
        kind: EmailPayloadKind::Marktwin,
        version: 1,
        data: JsonValue::String(String::new()),
        text: EmailContentSummary::EMPTY,
        html: EmailContentSummary::EMPTY,
      },
    })
    .await;
  let expected = Ok(OutboundEmail {
    id: actual.as_ref().map(|email| email.id).unwrap(),
    submitted_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    created_by: UserIdRef::new(alice.id),
    deadline: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
    sender: EmailAddress::from_str("support@eternaltwin.localhost").unwrap(),
    recipient: EmailAddress::from_str("alice@example.localhost").unwrap(),
    payload: EmailContentPayload {
      kind: EmailPayloadKind::Marktwin,
      version: 1,
      data: JsonValue::String(String::new()),
      text: EmailContentSummary::EMPTY,
      html: EmailContentSummary::EMPTY,
    },
    read_at: None,
  });
  assert_eq!(actual, expected);
}

pub(crate) async fn create_oubound_email_deadline_now<Api>(api: Api)
where
  Api: ClockRef<Clock = VirtualClock> + MailerStoreRef<JsonValue> + UserStoreRef,
{
  api.clock().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  let actual = api
    .mailer_store()
    .create_outbound_email(CreateOutboundEmail {
      idempotency_key: IdempotencyKey::from_u128(1),
      now: api.clock().now(),
      actor: UserIdRef::new(alice.id),
      deadline: api.clock().now(),
      sender: EmailAddress::from_str("support@eternaltwin.localhost").expect("support email is valid"),
      recipient: EmailAddress::from_str("alice@example.localhost").expect("recipient email is valid"),
      payload: EmailContentPayload {
        kind: EmailPayloadKind::Marktwin,
        version: 1,
        data: JsonValue::String(String::new()),
        text: EmailContentSummary::EMPTY,
        html: EmailContentSummary::EMPTY,
      },
    })
    .await;
  let expected = Err(CreateOutboundEmailError::InvalidDeadline(
    Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
  ));
  assert_eq!(actual, expected);
}

pub(crate) async fn outbound_request_acquire_release<Api>(api: Api)
where
  Api: ClockRef<Clock = VirtualClock> + MailerStoreRef<JsonValue> + UserStoreRef,
{
  api.clock().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  let email: OutboundEmail<JsonValue> = api
    .mailer_store()
    .create_outbound_email(CreateOutboundEmail {
      idempotency_key: IdempotencyKey::from_u128(1),
      now: api.clock().now(),
      actor: UserIdRef::new(alice.id),
      deadline: api.clock().now() + Duration::from_millis(5000),
      sender: EmailAddress::from_str("support@eternaltwin.localhost").expect("support email is valid"),
      recipient: EmailAddress::from_str("alice@example.localhost").expect("recipient email is valid"),
      payload: EmailContentPayload {
        kind: EmailPayloadKind::Marktwin,
        version: 1,
        data: JsonValue::String(String::new()),
        text: EmailContentSummary::EMPTY,
        html: EmailContentSummary::EMPTY,
      },
    })
    .await
    .unwrap();

  api.clock().advance_by(Duration::from_seconds(1));

  let req_token: OutboundEmailRequest = {
    let actual = api
      .mailer_store()
      .acquire_outbound_email_request(AcquireOutboundEmailRequest {
        now: api.clock().now(),
        outbound_email: email.id,
      })
      .await;

    let expected = Ok(OutboundEmailRequest {
      id: actual.as_ref().map(|req| req.id).unwrap(),
      email: OutboundEmailIdRef::new(email.id),
      period: FinitePeriod {
        start: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
        end: Instant::ymd_hms(2021, 1, 1, 1, 0, 1),
      },
      status: EmailRequestStatus::Pending,
    });
    assert_eq!(actual.as_ref(), expected.as_ref());
    actual.unwrap()
  };

  api.clock().advance_by(Duration::from_seconds(1));

  {
    let actual = api
      .mailer_store()
      .release_outbound_email_request(ReleaseOutboundEmailRequest {
        now: api.clock().now(),
        outbound_email_request: OutboundEmailRequestIdRef::new(req_token.id),
        result: Some(Ok(())),
      })
      .await;

    let expected = Ok(OutboundEmailRequest {
      id: actual.as_ref().map(|req| req.id).unwrap(),
      email: OutboundEmailIdRef::new(email.id),
      period: FinitePeriod {
        start: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
        end: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      },
      status: EmailRequestStatus::Ok,
    });
    assert_eq!(actual.as_ref(), expected.as_ref());
  }
}

use eternaltwin_core::mailer::store::acquire_outbound_email_request::AcquireOutboundEmailRequest;
use eternaltwin_core::mailer::store::release_outbound_email_request::ReleaseOutboundEmailRequest;
pub(crate) use {register_test, test_mailer_store};
