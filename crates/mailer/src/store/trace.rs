use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Handler, Request};
use eternaltwin_core::mailer::store::acquire_outbound_email_request::AcquireOutboundEmailRequest;
use eternaltwin_core::mailer::store::create_outbound_email::CreateOutboundEmail;
use eternaltwin_core::mailer::store::get_outbound_email_requests::GetOutboundEmailRequests;
use eternaltwin_core::mailer::store::get_outbound_emails::GetOutboundEmails;
use eternaltwin_core::mailer::store::release_outbound_email_request::ReleaseOutboundEmailRequest;
use eternaltwin_core::types::DisplayErrorChain;
use opentelemetry::trace::{FutureExt, Status, TraceContextExt, Tracer};
use opentelemetry::{Context, KeyValue};
use std::borrow::Cow;
use std::fmt::Debug;

pub struct TraceMailerStore<TyClock, TyMailerStore, TyTracer> {
  clock: TyClock,
  inner: TyMailerStore,
  tracer: TyTracer,
}

impl<TyClock, TyMailerStore, TyTracer> TraceMailerStore<TyClock, TyMailerStore, TyTracer> {
  pub fn new(clock: TyClock, inner: TyMailerStore, tracer: TyTracer) -> Self {
    Self { clock, inner, tracer }
  }
}

#[async_trait]
impl<Opaque, TyClock, TyMailerStore, TyTracer> Handler<CreateOutboundEmail<Opaque>>
  for TraceMailerStore<TyClock, TyMailerStore, TyTracer>
where
  Opaque: Send + Debug + 'static,
  TyClock: ClockRef,
  TyMailerStore: Handler<CreateOutboundEmail<Opaque>> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, cmd: CreateOutboundEmail<Opaque>) -> <CreateOutboundEmail<Opaque> as Request>::Response {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("MailerStore::CreateOutboundEmail")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new("submitted_at", v.submitted_at.to_string()));
        span.set_attribute(KeyValue::new("deadline", v.deadline.to_string()));
        span.set_attribute(KeyValue::new("payload.kind", v.payload.kind.to_string()));
        span.set_attribute(KeyValue::new("payload.version", i64::from(v.payload.version)));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyMailerStore, TyTracer> Handler<AcquireOutboundEmailRequest>
  for TraceMailerStore<TyClock, TyMailerStore, TyTracer>
where
  TyClock: ClockRef,
  TyMailerStore: Handler<AcquireOutboundEmailRequest> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, cmd: AcquireOutboundEmailRequest) -> <AcquireOutboundEmailRequest as Request>::Response {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("MailerStore::AcquireOutboundEmailRequest")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyMailerStore, TyTracer> Handler<ReleaseOutboundEmailRequest>
  for TraceMailerStore<TyClock, TyMailerStore, TyTracer>
where
  TyClock: ClockRef,
  TyMailerStore: Handler<ReleaseOutboundEmailRequest> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, cmd: ReleaseOutboundEmailRequest) -> <ReleaseOutboundEmailRequest as Request>::Response {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("MailerStore::ReleaseOutboundEmailRequest")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<Opaque, TyClock, TyMailerStore, TyTracer> Handler<GetOutboundEmails<Opaque>>
  for TraceMailerStore<TyClock, TyMailerStore, TyTracer>
where
  Opaque: Send + 'static,
  TyClock: ClockRef,
  TyMailerStore: Handler<GetOutboundEmails<Opaque>> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, cmd: GetOutboundEmails<Opaque>) -> <GetOutboundEmails<Opaque> as Request>::Response {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("MailerStore::GetOutboundEmails")
        .with_start_time(now.into_system()),
    );
    let cx = Context::current_with_span(span);
    let res = self.inner.handle(cmd).with_context(cx.clone()).await;
    let span = cx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyMailerStore, TyTracer> Handler<GetOutboundEmailRequests>
  for TraceMailerStore<TyClock, TyMailerStore, TyTracer>
where
  TyClock: ClockRef,
  TyMailerStore: Handler<GetOutboundEmailRequests> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, cmd: GetOutboundEmailRequests) -> <GetOutboundEmailRequests as Request>::Response {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("MailerStore::GetOutboundEmailRequests")
        .with_start_time(now.into_system()),
    );
    let cx = Context::current_with_span(span);
    let res = self.inner.handle(cmd).with_context(cx.clone()).await;
    let span = cx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
