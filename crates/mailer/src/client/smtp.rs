use async_trait::async_trait;
use compact_str::CompactString;
use eternaltwin_core::email::{EmailAddress, EmailContent, Mailer, MailerRef};
use eternaltwin_core::types::WeakError;
use lettre::message::header;
use lettre::message::header::Header;
pub use lettre::message::header::HeaderName;
use lettre::message::{Mailbox, MultiPart};
use lettre::transport::smtp::authentication::Credentials;
use lettre::{AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor};

#[derive(Clone, PartialEq, Debug)]
pub struct RawHeader {
  name: HeaderName,
  value: String,
}

impl RawHeader {
  pub fn new(name: HeaderName, value: String) -> Self {
    Self { name, value }
  }
}

enum KnownHeader {
  From(header::From),
  PmMessageStream(PmMessageStream),
  ReplyTo(header::ReplyTo),
  // ...
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum KnownHeaderFromRawError {
  #[error("invalid `From` header value")]
  From(#[source] WeakError),
  #[error("invalid `X-PM-Message-Stream` header value")]
  PmMessageStream(#[source] WeakError),
  #[error("invalid `Reply-To` header value")]
  ReplyTo(#[source] WeakError),
  #[error("unknown header name {0:?}")]
  Unknown(CompactString),
}

impl KnownHeader {
  pub fn from_raw(raw: RawHeader) -> Result<Self, KnownHeaderFromRawError> {
    Ok(match raw.name {
      name if name == header::From::name() => KnownHeader::From(
        header::From::parse(&raw.value).map_err(|e| KnownHeaderFromRawError::From(WeakError::wrap_ref(&*e)))?,
      ),
      name if name == PmMessageStream::name() => KnownHeader::PmMessageStream(
        PmMessageStream::parse(&raw.value)
          .map_err(|e| KnownHeaderFromRawError::PmMessageStream(WeakError::wrap_ref(&*e)))?,
      ),
      name if name == header::ReplyTo::name() => KnownHeader::ReplyTo(
        header::ReplyTo::parse(&raw.value).map_err(|e| KnownHeaderFromRawError::ReplyTo(WeakError::wrap_ref(&*e)))?,
      ),
      name => return Err(KnownHeaderFromRawError::Unknown(CompactString::new(name))),
    })
  }
}

#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Debug, Hash)]
struct PmMessageStream(String);

impl Header for PmMessageStream {
  fn name() -> HeaderName {
    HeaderName::new_from_ascii_str("X-PM-Message-Stream")
  }

  fn parse(s: &str) -> Result<Self, Box<dyn std::error::Error + Send + Sync>> {
    Ok(Self(s.into()))
  }

  fn display(&self) -> lettre::message::header::HeaderValue {
    lettre::message::header::HeaderValue::new(Self::name(), self.0.clone())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("invalid header name {name:?}")]
pub struct InvalidHeaderNameError {
  pub name: String,
}

pub struct SmtpMailerBuilder {
  pub relay: String,
  pub username: String,
  pub password: String,
  pub sender: Mailbox,
  pub headers: Vec<RawHeader>,
}

impl SmtpMailerBuilder {
  pub fn new(relay: String, username: String, password: String, sender: String) -> Self {
    Self {
      relay,
      username,
      password,
      sender: sender.parse().unwrap(),
      headers: Vec::new(),
    }
  }

  pub fn header(&mut self, header: RawHeader) {
    self.headers.push(header);
  }

  pub fn ascii_header(&mut self, name: &str, value: String) -> Result<(), InvalidHeaderNameError> {
    self.headers.push(RawHeader::new(
      HeaderName::new_from_ascii(name.to_string()).map_err(|_| InvalidHeaderNameError { name: name.to_string() })?,
      value,
    ));
    Ok(())
  }

  pub fn build(self) -> SmtpMailer {
    SmtpMailer::new(
      self.relay.as_str(),
      self.username,
      self.password,
      self.sender,
      self.headers,
    )
  }
}

pub struct SmtpMailer {
  transport: AsyncSmtpTransport<Tokio1Executor>,
  sender: Mailbox,
  headers: Vec<RawHeader>,
}

impl SmtpMailer {
  pub fn builder(relay: String, username: String, password: String, sender: String) -> SmtpMailerBuilder {
    SmtpMailerBuilder::new(relay, username, password, sender)
  }

  pub fn new(relay: &str, username: String, password: String, sender: Mailbox, headers: Vec<RawHeader>) -> Self {
    Self {
      transport: AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(relay)
        .unwrap()
        .credentials(Credentials::new(username, password))
        .build(),
      headers,
      sender,
    }
  }
}

impl MailerRef for SmtpMailer {
  type Mailer = Self;

  fn mailer(&self) -> &Self::Mailer {
    self
  }
}

#[async_trait]
impl Mailer for SmtpMailer {
  async fn send_email(&self, recipient: &EmailAddress, content: &EmailContent) -> Result<(), WeakError> {
    let mut email = Message::builder()
      .from(self.sender.clone())
      .reply_to(self.sender.clone())
      .to(recipient.as_str().parse().unwrap());

    for header in self.headers.iter() {
      let header = KnownHeader::from_raw(header.clone()).map_err(WeakError::wrap)?;
      email = match header {
        KnownHeader::From(header) => email.header(header),
        KnownHeader::PmMessageStream(header) => email.header(header),
        KnownHeader::ReplyTo(header) => email.header(header),
      };
    }

    let email = email.subject(content.subject.as_str());

    let email = match content.body_html.as_ref() {
      None => email.body(content.body_text.to_string()).unwrap(),
      Some(body_html) => email
        .multipart(MultiPart::alternative_plain_html(
          content.body_text.to_string(),
          body_html.to_string(),
        ))
        .unwrap(),
    };

    self.transport.send(email).await.unwrap();

    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::client::smtp::{RawHeader, SmtpMailer};
  use eternaltwin_config::MailerType;
  use eternaltwin_core::email::{EmailAddress, EmailContent, Mailer};
  use lettre::message::header::HeaderName;

  #[tokio::test]
  async fn verify_registration_en() {
    let config = eternaltwin_config::Config::for_test();

    if !matches!(&config.backend.mailer.value, MailerType::Network) {
      eprintln!("Mailer type is not `Network`, skipping SMTP test");
      return;
    }

    let config = dbg!(config.mailer);

    let mut mailer = SmtpMailer::builder(
      config.host.value,
      config.username.value,
      config.password.value,
      config.sender.value,
    );
    for header in config.headers.value.into_iter() {
      mailer.header(RawHeader::new(
        HeaderName::new_from_ascii(header.name).unwrap(),
        header.value,
      ));
    }
    let mailer = mailer.build();

    let alice: EmailAddress = "demurgos@demurgos.net".parse().unwrap();

    mailer
      .send_email(
        &alice,
        &EmailContent {
          subject: "Eternaltwin registration".parse().unwrap(),
          body_text: "Hi, complete the registration by going to <https://eternaltwin.org>."
            .parse()
            .unwrap(),
          body_html: Some(
            "Hi, complete the registration by going to <a href=https://eternaltwin.org>Eternaltwin</a>."
              .parse()
              .unwrap(),
          ),
        },
      )
      .await
      .unwrap();
  }
}
