use crate::error::{
  InternalError, InvalidClientIdError, InvalidResponseTypeError, InvalidScopeError, MissingAuthenticationError,
  MissingClientIdError, MissingOauthClientAuthenticationError, MissingResponseTypeError, OauthClientNotFoundError,
  OauthCodeFormatError, OauthCodeMissingError, OauthCodeTimeError, RedirectUriMismatchError,
  UnsupportedResponseTypeError, WrongOauthClientError,
};
use crate::helpers::link_resolver::{LinkResolver, ResolveLinksError};
use base64::Engine;
use eternaltwin_core::auth::{
  AccessTokenAuthContext, AuthContext, AuthScope, AuthStore, AuthStoreRef, ClaimAccessTokenCommand,
  CreateSessionOptions, CreateValidatedEmailVerificationOptions, Credentials, EtwinOauthAccessTokenKey,
  GrantOauthAuthorizationOptions, Login, OauthClientAuthContext, RawCredentials, RawUserCredentials,
  RegisterOrLoginWithEmailOptions, RegisterWithUsernameOptions, RegisterWithVerifiedEmailOptions, SessionId,
  UserAndSession, UserAuthContext, UserCredentials, UserLogin, UserLoginParseError,
};
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Duration, FinitePeriod, Instant, LocaleId};
use eternaltwin_core::dinoparc::{DinoparcStore, DinoparcStoreRef, ShortDinoparcUser};
use eternaltwin_core::email::{
  EmailAddress, EmailFormatter, EmailFormatterRef, Mailer, MailerRef, VerifyRegistrationEmail,
};
use eternaltwin_core::hammerfest::{
  HammerfestClientCreateSessionError, HammerfestStore, HammerfestStoreRef, HammerfestUserIdRef, ShortHammerfestUser,
};
use eternaltwin_core::link::store::{LinkStore, LinkStoreRef};
use eternaltwin_core::link::{GetLinksFromEtwinOptions, TouchLinkError, VersionedLinks};
use eternaltwin_core::oauth::{
  CreateStoredAccessTokenOptions, EtwinOauthScopes, EtwinOauthStateClaims, GetOauthAccessTokenOptions,
  GetOauthClientOptions, OauthAccessToken, OauthClientId, OauthClientKey, OauthClientRef, OauthProviderStore,
  OauthProviderStoreRef, RawGetOauthClientError, RfcOauthResponseType, RfcOauthTokenType, ShortOauthClient,
  SimpleOauthClient,
};
use eternaltwin_core::opentelemetry::{DynTracer, TraceParent};
use eternaltwin_core::password::{Password, PasswordService, PasswordServiceRef};
use eternaltwin_core::twinoid::store::TwinoidStoreRef;
use eternaltwin_core::twinoid::{ShortTwinoidUser, TwinoidCredentials, TwinoidStore};
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use eternaltwin_core::user::{
  CreateUserOptions, GetShortUserOptions, GetUserOptions, RawGetShortUserError, RawGetUser, RawGetUserError,
  RawGetUserResult, ShortUser, ShortUserWithPassword, SimpleUser, User, UserDisplayName, UserEmailRef, UserFields,
  UserId, UserIdRef, UserRef, UserStore, UserStoreRef, UserUsernameRef,
};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use opentelemetry::trace::{FutureExt, Link, Span, SpanKind, Status, TraceContextExt, Tracer};
use opentelemetry::KeyValue;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use std::borrow::Cow;
use std::collections::HashSet;
use std::str::FromStr;
use std::sync::Arc;
use url::Url;

#[derive(Debug, Serialize, Deserialize)]
struct EmailJwtClaims {
  /// Expiration time (Unix timestamp)
  exp: i64,
  /// Issued at (Unix timestamp)
  iat: i64,
  /// Custom: Email address to validate
  email: EmailAddress,
}

#[derive(Debug, Serialize, Deserialize)]
struct OauthCodeJwtClaims {
  /// The recipients that the JWT is intended for
  aud: Vec<String>,
  /// Expiration time (Unix timestamp)
  exp: i64,
  /// Issued at (Unix timestamp)
  iat: i64,
  /// Issuer
  iss: String,
  /// Not before (Unix timestamp)
  nbf: i64,
  /// Subject
  sub: UserId,
  /// OpenTelemetry Trace Parent
  ///
  /// Refers to the scope that created the span,
  trp: TraceParent,
  /// Custom: Authorization scopes
  scopes: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OauthCodeGrant {
  code: String,
  state: Option<String>,
  callback_uri: Url,
}

impl OauthCodeGrant {
  pub fn redirect_uri(&self) -> Url {
    let mut redirect_uri = self.callback_uri.clone();
    {
      let mut query_pairs = redirect_uri.query_pairs_mut();
      query_pairs.append_pair("code", self.code.as_str());
      if let Some(state) = self.state.as_deref() {
        query_pairs.append_pair("state", state);
      }
    }
    redirect_uri
  }
}

#[derive(Debug, thiserror::Error)]
pub enum GrantOauthAuthorizationError {
  #[error(transparent)]
  MissingClientId(MissingClientIdError),
  #[error(transparent)]
  InvalidClientId(InvalidClientIdError),
  #[error(transparent)]
  RedirectUriMismatch(RedirectUriMismatchError),
  #[error(transparent)]
  OauthClientNotFound(OauthClientNotFoundError),
  #[error(transparent)]
  MissingResponseType(MissingResponseTypeError),
  #[error(transparent)]
  InvalidResponseType(InvalidResponseTypeError),
  #[error(transparent)]
  UnsupportedResponseType(UnsupportedResponseTypeError),
  #[error(transparent)]
  InvalidScope(InvalidScopeError),
  #[error(transparent)]
  MissingAuthentication(MissingAuthenticationError),
  #[error(transparent)]
  Internal(InternalError),
}

#[derive(thiserror::Error, Debug)]
pub enum ClaimAccessTokenError {
  #[error(transparent)]
  OauthCodeMissing(OauthCodeMissingError),
  #[error(transparent)]
  OauthCodeFormat(OauthCodeFormatError),
  #[error(transparent)]
  OauthCodeTime(OauthCodeTimeError),
  #[error(transparent)]
  MissingOauthClientAuthentication(MissingOauthClientAuthenticationError),
  #[error(transparent)]
  WrongOauthClient(WrongOauthClientError),
  #[error(transparent)]
  Internal(InternalError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawLoginWithCredentialsError {
  #[error("invalid login format")]
  LoginFormat(#[from] UserLoginParseError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum LoginWithHammerfestError {
  #[error("failed to create Hammerfest session")]
  HammerfestSession(#[source] HammerfestClientCreateSessionError),
  #[error("(internal) failed to retrieve links for Hammerfest user")]
  HammerfestLink(#[source] WeakError),
  #[error("(internal) failed to auto-create Eternaltwin user")]
  CreateEtwinUser(#[source] WeakError),
  #[error("(internal) failed to touch Hammerfest user")]
  CreateHammerfestUser(#[source] WeakError),
  #[error("(internal) failed to touch Eternaltwin-Hammerfest link")]
  TouchLink(#[source] TouchLinkError<HammerfestUserIdRef>),
  #[error("(internal) failed to retrieve user data")]
  GetUser(#[source] WeakError),
  #[error("(internal) failed to create session")]
  CreateSession(#[source] WeakError),
  #[error("(internal) failed to get Eternaltwin user raw links")]
  GetLinks(#[source] WeakError),
  #[error("(internal) failed to resolve user links")]
  ResolveLinks(#[source] ResolveLinksError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum ReadOauthStateError {
  #[error("failed to decode tiny state")]
  Decode(#[from] DecodeTinyStateError),
  #[error("invalid JWT")]
  Jwt(#[from] jsonwebtoken::errors::Error),
  #[error("expired token: now = {now}, iat = {issued_at}, exp = {expiration_time}")]
  Time {
    now: Instant,
    issued_at: Instant,
    expiration_time: Instant,
  },
}

pub struct AuthService<
  TyAuthStore,
  TyClock,
  TyDinoparcStore,
  TyEmailFormatter,
  TyHammerfestStore,
  TyLinkStore,
  TyMailer,
  TyOauthProviderStore,
  TyPasswordService,
  TyTracer,
  TyTwinoidStore,
  TyUserStore,
  TyUuidGenerator,
> where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyEmailFormatter: EmailFormatterRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyMailer: MailerRef,
  TyOauthProviderStore: OauthProviderStoreRef,
  TyPasswordService: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  auth_store: TyAuthStore,
  clock: TyClock,
  dinoparc_store: TyDinoparcStore,
  email_formatter: TyEmailFormatter,
  hammerfest_store: TyHammerfestStore,
  link_store: TyLinkStore,
  mailer: TyMailer,
  oauth_provider_store: TyOauthProviderStore,
  password_service: TyPasswordService,
  tracer: TyTracer,
  twinoid_store: TyTwinoidStore,
  user_store: TyUserStore,
  uuid_generator: TyUuidGenerator,
  internal_auth_key: Vec<u8>,
  jwt_secret_key: Vec<u8>,
  default_locale: LocaleId,
  email_verification_validity: Duration,
  authorization_code_validity: Duration,
  access_token_validity: Duration,
}

pub type DynAuthService = AuthService<
  Arc<dyn AuthStore>,
  Arc<dyn Clock>,
  Arc<dyn DinoparcStore>,
  Arc<dyn EmailFormatter>,
  Arc<dyn HammerfestStore>,
  Arc<dyn LinkStore>,
  Arc<dyn Mailer>,
  Arc<dyn OauthProviderStore>,
  Arc<dyn PasswordService>,
  DynTracer,
  Arc<dyn TwinoidStore>,
  Arc<dyn UserStore>,
  Arc<dyn UuidGenerator>,
>;

impl<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator: UuidGeneratorRef,
  >
  AuthService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyEmailFormatter: EmailFormatterRef,
  TyHammerfestStore: HammerfestStoreRef + Clone + 'static,
  TyLinkStore: LinkStoreRef,
  TyMailer: MailerRef,
  TyOauthProviderStore: OauthProviderStoreRef,
  TyPasswordService: PasswordServiceRef,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  #[allow(clippy::too_many_arguments)]
  pub fn new(
    auth_store: TyAuthStore,
    clock: TyClock,
    dinoparc_store: TyDinoparcStore,
    email_formatter: TyEmailFormatter,
    hammerfest_store: TyHammerfestStore,
    link_store: TyLinkStore,
    mailer: TyMailer,
    oauth_provider_store: TyOauthProviderStore,
    password_service: TyPasswordService,
    user_store: TyUserStore,
    tracer: TyTracer,
    twinoid_store: TyTwinoidStore,
    uuid_generator: TyUuidGenerator,
    internal_auth_key: Vec<u8>,
    secret: Vec<u8>,
  ) -> Self {
    Self {
      auth_store,
      clock,
      dinoparc_store,
      email_formatter,
      hammerfest_store,
      link_store,
      mailer,
      oauth_provider_store,
      password_service,
      tracer,
      twinoid_store,
      user_store,
      uuid_generator,
      internal_auth_key,
      jwt_secret_key: secret,
      default_locale: LocaleId::EnUs,
      email_verification_validity: Duration::from_days(1),
      authorization_code_validity: Duration::from_minutes(10),
      // TODO: Make it expire!
      access_token_validity: Duration::from_seconds(1_000_000_000),
    }
  }

  // TODO: Return enum codeGrant/tokenGrant
  /// Grant an Eternaltwin OAuth authorization token to a third-party client.
  ///
  ///
  pub async fn grant_oauth_authorization(
    &self,
    acx: &AuthContext,
    cmd: &GrantOauthAuthorizationOptions,
  ) -> Result<OauthCodeGrant, GrantOauthAuthorizationError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthService::grant_oauth_authorization")
        .with_kind(SpanKind::Internal)
        .with_attributes([KeyValue::new("acx", format!("{acx:?}"))])
        .with_start_time(now.into_system()),
    );
    let trace_parent = TraceParent::from_span_context(span.span_context());
    let ocx = opentelemetry::Context::current_with_span(span);
    let res = self
      .grant_oauth_authorization_inner(acx, cmd, trace_parent)
      .with_context(ocx.clone())
      .await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  // inner implementation of `grant_oauth_authorization`
  async fn grant_oauth_authorization_inner(
    &self,
    acx: &AuthContext,
    options: &GrantOauthAuthorizationOptions,
    trace_parent: TraceParent,
  ) -> Result<OauthCodeGrant, GrantOauthAuthorizationError> {
    // We start by checking for early errors that may correspond to malicious queries.
    // If the client id is missing, the client does not exist or there is a
    // mismatch on the `redirect_uri`, then we treat it as a
    // malicious request and display the error on our own website (we do not
    // redirect it to the client).
    let client_ref = options
      .client_ref
      .as_ref()
      .ok_or(GrantOauthAuthorizationError::MissingClientId(MissingClientIdError))?;
    let client_ref: OauthClientRef = client_ref
      .parse()
      .map_err(|()| GrantOauthAuthorizationError::InvalidClientId(InvalidClientIdError))?;
    let client = self
      .oauth_provider_store
      .oauth_provider_store()
      .get_client(&GetOauthClientOptions { r#ref: client_ref })
      .await
      .map_err(|e| match e {
        RawGetOauthClientError::NotFound(r) => {
          GrantOauthAuthorizationError::OauthClientNotFound(OauthClientNotFoundError { oauth_client: r })
        }
        RawGetOauthClientError::Other(e) => GrantOauthAuthorizationError::Internal(InternalError(e)),
      })?;
    if let Some(redirect_uri) = options.redirect_uri.as_ref() {
      if redirect_uri.as_str() != client.callback_uri.as_str() {
        return Err(GrantOauthAuthorizationError::RedirectUriMismatch(
          RedirectUriMismatchError {
            provided: redirect_uri.clone(),
            registered: client.callback_uri,
          },
        ));
      }
    }

    // We now trust the query enough to report errors to the corresponding
    // client. If an error occurs, we now redirect to the client.

    let scope = options.scope.as_ref();

    let response_type = options
      .response_type
      .as_ref()
      .ok_or(GrantOauthAuthorizationError::MissingResponseType(
        MissingResponseTypeError,
      ))?;
    let response_type: RfcOauthResponseType = response_type
      .parse()
      .map_err(|_| GrantOauthAuthorizationError::InvalidResponseType(InvalidResponseTypeError))?;

    match response_type {
      RfcOauthResponseType::Token => Err(GrantOauthAuthorizationError::UnsupportedResponseType(
        UnsupportedResponseTypeError,
      )),
      RfcOauthResponseType::Code => {
        let scopes = match scope {
          Some(scope) => EtwinOauthScopes::from_str(scope)
            .map_err(|()| GrantOauthAuthorizationError::InvalidScope(InvalidScopeError))?,
          None => EtwinOauthScopes::default(),
        };
        let acx = match acx {
          AuthContext::User(acx) => acx,
          _ => {
            return Err(GrantOauthAuthorizationError::MissingAuthentication(
              MissingAuthenticationError,
            ))
          }
        };
        let code = self
          .create_authorization_code(acx.user.id.into(), &client, &scopes, trace_parent)
          .map_err(|e| GrantOauthAuthorizationError::Internal(InternalError(e)))?;
        Ok(OauthCodeGrant {
          code,
          state: options.state.clone(),
          callback_uri: client.callback_uri,
        })
      }
    }
  }

  /// Create an Eternaltwin Oauth access token for a third-party client
  ///
  /// The access token is claimed by providing a valid oauth code token.
  pub async fn claim_access_token(
    &self,
    acx: &AuthContext,
    cmd: &ClaimAccessTokenCommand,
  ) -> Result<OauthAccessToken, ClaimAccessTokenError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthService::claim_access_token")
        .with_start_time(now.into_system())
        // temporary: log code to verify auth issues
        .with_attributes(cmd.code.as_ref().map(|c| KeyValue::new("code", c.to_string()))),
    );
    let ocx = opentelemetry::Context::current_with_span(span);
    let res = self.claim_access_token_inner(acx, cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("token_type", v.token_type.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn claim_access_token_inner(
    &self,
    acx: &AuthContext,
    cmd: &ClaimAccessTokenCommand,
  ) -> Result<OauthAccessToken, ClaimAccessTokenError> {
    let now = self.clock.clock().now();
    let code = cmd
      .code
      .as_ref()
      .ok_or(ClaimAccessTokenError::OauthCodeMissing(OauthCodeMissingError))?;
    let client = match acx {
      AuthContext::OauthClient(ref acx) => &acx.client,
      _ => {
        return Err(ClaimAccessTokenError::MissingOauthClientAuthentication(
          MissingOauthClientAuthenticationError,
        ))
      }
    };
    let claims = self.read_oauth_code_token(code).map_err(|e| match e {
      ReadOauthCodeError::Format(e) => ClaimAccessTokenError::OauthCodeFormat(OauthCodeFormatError(e)),
      ReadOauthCodeError::ValidityTime { now, period } => {
        ClaimAccessTokenError::OauthCodeTime(OauthCodeTimeError { now, period })
      }
      ReadOauthCodeError::Other(e) => ClaimAccessTokenError::Internal(InternalError(e)),
    })?;
    // todo: move this log before the validity time check
    let mut span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthService::grant_oauth_authorization::claims")
        .with_kind(SpanKind::Internal)
        .with_start_time(now.into_system())
        .with_links(vec![Link::with_context(claims.trp.into_span_context())])
        .with_attributes([KeyValue::new("trp", format!("{:?}", claims.trp))]),
    );
    span.end_with_timestamp(now.into_system());
    // TODO: Check if `redirect_uri` matches
    let client_id_str = client.id.to_string();
    if !claims.aud.iter().any(|aud| aud.as_str() == client_id_str.as_str()) {
      return Err(ClaimAccessTokenError::WrongOauthClient(WrongOauthClientError {
        registered: client_id_str,
      }));
    }
    // TODO: Use actually secure random (split into id + secret)
    let key: EtwinOauthAccessTokenKey = self.uuid_generator.uuid_generator().next().to_string().parse().unwrap();
    let token = self
      .oauth_provider_store
      .oauth_provider_store()
      .create_access_token(&CreateStoredAccessTokenOptions {
        key,
        ctime: now,
        expiration_time: now + self.access_token_validity,
        user: claims.sub.into(),
        client: client.id.into(),
      })
      .await
      .map_err(|e| ClaimAccessTokenError::Internal(InternalError(WeakError::wrap(e))))?;
    Ok(OauthAccessToken {
      token_type: RfcOauthTokenType::Bearer,
      access_token: token.key,
      expires_in: self.access_token_validity.seconds(),
      refresh_token: None,
    })
  }

  /// Authenticate an internal request
  pub fn authenticate_internal(&self, key: &[u8]) -> bool {
    key == self.internal_auth_key.as_slice()
  }

  /// Authenticate a third-party client through an Eternaltwin access token.
  pub async fn authenticate_access_token(&self, token: &str) -> Result<AuthContext, WeakError> {
    if token.starts_with('[') {
      // JSON token used for internal auth, should be replaced eventually by a JWT.
      let (key, acx) = serde_json::from_str::<(Cow<str>, AuthContext)>(token).map_err(WeakError::wrap)?;
      if key.as_ref().as_bytes() == self.internal_auth_key.as_slice() {
        return Ok(acx);
      } else {
        return Err(WeakError::new("invalid internal auth key"));
      }
    }

    let token: EtwinOauthAccessTokenKey = token.parse().map_err(WeakError::wrap)?;

    let token = self
      .oauth_provider_store
      .oauth_provider_store()
      .get_access_token(&GetOauthAccessTokenOptions {
        key: token,
        touch_accessed_at: true,
      })
      .await
      .map_err(WeakError::wrap)?;

    let client = self
      .oauth_provider_store
      .oauth_provider_store()
      .get_client(&GetOauthClientOptions {
        r#ref: OauthClientRef::Id(token.client),
      })
      .await
      .map_err(WeakError::wrap)?;

    let user: ShortUser = self
      .user_store
      .user_store()
      .get_short_user(&GetShortUserOptions {
        r#ref: UserRef::Id(token.user),
        time: None,
        skip_deleted: true,
      })
      .await
      .map_err(WeakError::wrap)?;

    let client = ShortOauthClient {
      id: client.id,
      key: client.key,
      display_name: client.display_name,
    };

    Ok(AuthContext::AccessToken(AccessTokenAuthContext {
      scope: AuthScope::Default,
      client,
      user,
    }))
  }

  pub async fn register_or_login_with_email(&self, options: &RegisterOrLoginWithEmailOptions) -> Result<(), WeakError> {
    let token = self.create_email_verification_token(&options.email)?;
    let locale = options.locale.unwrap_or(self.default_locale);
    let email_content = self
      .email_formatter
      .email_formatter()
      .verify_registration_email(locale, &VerifyRegistrationEmail { token })
      .await?;
    self.mailer.mailer().send_email(&options.email, &email_content).await?;
    Ok(())
  }

  pub async fn register_with_verified_email(
    &self,
    options: &RegisterWithVerifiedEmailOptions,
  ) -> Result<UserAndSession, WeakError> {
    let email_jwt = self.read_email_verification_token(options.email_token.as_str())?;
    let email = email_jwt.email;

    let old_user: Option<ShortUser> = match self
      .user_store
      .user_store()
      .get_short_user(&GetShortUserOptions {
        r#ref: UserRef::Email(UserEmailRef { email: email.clone() }),
        time: None,
        skip_deleted: true,
      })
      .await
    {
      Ok(u) => Some(u),
      Err(RawGetShortUserError::NotFound(_)) => None,
      Err(e) => return Err(WeakError::wrap(e)),
    };
    if old_user.is_some() {
      return Err(WeakError::new("Conflict: EmailAddressAlreadyInUser"));
    }
    let display_name = options.display_name.clone();
    let password_hash = self.password_service.password_service().hash(options.password.clone());

    let user = self
      .user_store
      .user_store()
      .create_user(&CreateUserOptions {
        display_name,
        email: Some(email.clone()),
        username: None,
        password: Some(password_hash),
      })
      .await
      .map_err(WeakError::wrap)?;

    self
      .auth_store
      .auth_store()
      .create_validated_email_verification(&CreateValidatedEmailVerificationOptions {
        user: user.id.into(),
        email,
        token_issued_at: Instant::from_posix_timestamp(email_jwt.iat),
      })
      .await?;

    let session = self
      .auth_store
      .auth_store()
      .create_session(&CreateSessionOptions { user: user.id.into() })
      .await
      .map_err(WeakError::wrap)?
      .into_session(user.display_name.clone());

    let is_administrator = user.is_administrator;

    let user = User {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted_at,
      display_name: user.display_name,
      is_administrator: user.is_administrator,
      // Hardcoding to `default` is fine: new users don't have links
      links: VersionedLinks::default(),
    };

    Ok(UserAndSession {
      user,
      is_administrator,
      session,
    })
  }

  pub async fn register_with_username(
    &self,
    options: &RegisterWithUsernameOptions,
  ) -> Result<UserAndSession, WeakError> {
    let old_user = match self
      .user_store
      .user_store()
      .get_short_user(&GetShortUserOptions {
        r#ref: UserRef::Username(UserUsernameRef {
          username: options.username.clone(),
        }),
        time: None,
        skip_deleted: true,
      })
      .await
    {
      Ok(u) => Some(u),
      Err(RawGetShortUserError::NotFound(_)) => None,
      Err(e) => return Err(WeakError::wrap(e)),
    };
    if old_user.is_some() {
      return Err(WeakError::new("Conflict: UsernameAlreadyInUse"));
    }
    let display_name = options.display_name.clone();
    let password_hash = self.password_service.password_service().hash(options.password.clone());

    let user = self
      .user_store
      .user_store()
      .create_user(&CreateUserOptions {
        display_name,
        email: None,
        username: Some(options.username.clone()),
        password: Some(password_hash),
      })
      .await
      .map_err(WeakError::wrap)?;

    let session = self
      .auth_store
      .auth_store()
      .create_session(&CreateSessionOptions { user: user.id.into() })
      .await?
      .into_session(user.display_name.clone());

    let is_administrator = user.is_administrator;
    let user = User {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted_at,
      display_name: user.display_name,
      is_administrator: user.is_administrator,
      // Hardcoding to `default` is fine: new users don't have links
      links: VersionedLinks::default(),
    };

    Ok(UserAndSession {
      user,
      is_administrator,
      session,
    })
  }

  /// Create a session from an untyped login/password pair
  ///
  /// "Raw login" means that the type (email or username) is not yet known and will be detected automatically.
  pub async fn raw_login_with_credentials(
    &self,
    credentials: &RawUserCredentials,
  ) -> Result<UserAndSession, RawLoginWithCredentialsError> {
    let credentials = UserCredentials {
      login: credentials.login.parse()?,
      password: credentials.password.clone(),
    };
    self
      .login_with_credentials(&credentials)
      .await
      .map_err(RawLoginWithCredentialsError::Other)
  }

  pub async fn login_with_credentials(&self, credentials: &UserCredentials) -> Result<UserAndSession, WeakError> {
    let user_ref = match credentials.login.clone() {
      UserLogin::EmailAddress(email) => UserRef::Email(UserEmailRef { email }),
      UserLogin::Username(username) => UserRef::Username(UserUsernameRef { username }),
    };
    let user = self
      .authenticate_user_with_password(user_ref, credentials.password.clone())
      .await?;

    let session = self
      .auth_store
      .auth_store()
      .create_session(&CreateSessionOptions { user: user.id.into() })
      .await?
      .into_session(user.display_name.clone());

    let is_administrator = user.is_administrator;
    let time = self.clock.clock().now();
    let links = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: user.id.into(),
        time: Some(time),
      })
      .await?;
    let links = self.resolve_links(links, time).await.map_err(WeakError::wrap)?;
    let user = User {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted_at,
      display_name: user.display_name,
      is_administrator: user.is_administrator,
      links,
    };

    Ok(UserAndSession {
      user,
      is_administrator,
      session,
    })
  }

  pub async fn register_or_login_with_twinoid(
    &self,
    _credentials: &TwinoidCredentials,
  ) -> Result<UserAndSession, WeakError> {
    Err(WeakError::new("TwinoidCredentialsAreNotSupportedYet"))
  }

  // pub async fn request_twinoid_auth(&self, action: EtwinOauthStateAction) -> Result<Url, WeakError> {
  //   const ALL_TWINOID_SCOPES: [&str; 17] = [
  //     "contacts",
  //     "groups",
  //     "applications",
  //     "www.hordes.fr",
  //     "www.die2nite.com",
  //     "www.dieverdammten.de",
  //     "www.zombinoia.com",
  //     "mush.vg",
  //     "mush_ship_data",
  //     "arkadeo.com",
  //     "arkadeo_plays",
  //     "mush.twinoid.es",
  //     "mush.twinoid.com",
  //     "rockfaller.com",
  //     "www.dinorpg.com",
  //     "es.dinorpg.com",
  //     "en.dinorpg.com",
  //   ];
  //
  //   /// Max duration of the JWT (15 min)
  //   static TOKEN_LIFETIME: Duration = Duration::from_minutes(15);
  //
  //   let now = self.clock.clock().now();
  //   let key = jsonwebtoken::EncodingKey::from_secret(self.jwt_secret_key.as_slice());
  //
  //   let jwt_header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256);
  //   let claims = EtwinOauthStateClaims {
  //     request_forgery_protection: "TODO".to_string(),
  //     action,
  //     issued_at: now,
  //     authorization_server: self.twinoid_oauth_client.authorization_server().to_string(),
  //     expiration_time: now + TOKEN_LIFETIME,
  //   };
  //
  //   let state = jsonwebtoken::encode(&jwt_header, &claims, &key)?;
  //   let state = encode_tiny_state(&state)?;
  //
  //   Ok(
  //     self
  //       .twinoid_oauth_client
  //       .authorization_uri(&ALL_TWINOID_SCOPES.join(","), &state),
  //   )
  // }

  // pub async fn get_twinoid_access_token(&self, rfc_oauth_code: &str) -> Result<RfcOauthAccessToken, WeakError> {
  //   Ok(self.twinoid_oauth_client.get_access_token(rfc_oauth_code).await?)
  // }

  pub async fn authenticate_session(&self, cmd: SessionId) -> Result<Option<UserAndSession>, WeakError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("AuthService::authenticate_session")
        .with_start_time(now.into_system()),
    );
    let ocx = opentelemetry::Context::current_with_span(span);
    let res = self.authenticate_session_inner(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        match v.as_ref() {
          Some(v) => {
            span.set_attribute(KeyValue::new("user.id", v.user.id.to_string()));
          }
          None => {
            span.set_attribute(KeyValue::new("user.id", false));
          }
        }
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  pub async fn authenticate_session_inner(&self, session: SessionId) -> Result<Option<UserAndSession>, WeakError> {
    let session = self.auth_store.auth_store().get_and_touch_session(session).await?;
    let session = match session {
      Some(s) => s,
      None => return Ok(None),
    };

    let user = self
      .user_store
      .user_store()
      .get_user(&GetUserOptions {
        r#ref: UserRef::Id(UserIdRef { id: session.user.id }),
        fields: UserFields::Default,
        time: None,
        skip_deleted: true,
      })
      .await
      .map_err(WeakError::wrap)?;

    let user: SimpleUser = match user {
      RawGetUserResult::Complete(u) => u.into(),
      RawGetUserResult::Default(u) => u,
      RawGetUserResult::Short(_) => {
        unreachable!("AssertionError: Requested `UserFields::Default` but got short response")
      }
    };

    let session = session.into_session(user.display_name.clone());
    let is_administrator = user.is_administrator;
    let time = self.clock.clock().now();
    let links = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: user.id.into(),
        time: Some(time),
      })
      .await
      .map_err(WeakError::wrap)?;
    let links = self.resolve_links(links, time).await.map_err(WeakError::wrap)?;
    let user = User {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted_at,
      display_name: user.display_name,
      is_administrator: user.is_administrator,
      links,
    };

    Ok(Some(UserAndSession {
      user,
      is_administrator,
      session,
    }))
  }

  pub async fn raw_authenticate_credentials(&self, credentials: &RawCredentials) -> Result<AuthContext, WeakError> {
    let credentials = Credentials {
      login: credentials.login.parse().map_err(|()| WeakError::new("BadLogin"))?,
      password: credentials.password.clone(),
    };
    self.authenticate_credentials(credentials).await
  }

  pub async fn authenticate_credentials(&self, credentials: Credentials) -> Result<AuthContext, WeakError> {
    fn from_user(user: SimpleUser) -> AuthContext {
      let is_administrator = user.is_administrator;
      AuthContext::User(UserAuthContext {
        scope: AuthScope::Default,
        user: user.into(),
        is_administrator,
      })
    }

    fn from_client(client: SimpleOauthClient) -> AuthContext {
      AuthContext::OauthClient(OauthClientAuthContext {
        scope: AuthScope::Default,
        client: client.into(),
      })
    }

    match credentials.login {
      Login::EmailAddress(email) => {
        let user = self
          .authenticate_user_with_password(UserRef::Email(UserEmailRef { email }), credentials.password)
          .await?;
        Ok(from_user(user))
      }
      Login::Username(username) => {
        let user = self
          .authenticate_user_with_password(UserRef::Username(UserUsernameRef { username }), credentials.password)
          .await?;
        Ok(from_user(user))
      }
      Login::UserId(user_id) => {
        let user = self
          .authenticate_user_with_password(UserRef::Id(user_id.into()), credentials.password)
          .await?;
        Ok(from_user(user))
      }
      Login::OauthClientId(client_id) => {
        let client = self
          .authenticate_oauth_client(OauthClientRef::Id(client_id.into()), credentials.password)
          .await?;
        Ok(from_client(client))
      }
      Login::OauthClientKey(client_key) => {
        let client = self
          .authenticate_oauth_client(OauthClientRef::Key(client_key.into()), credentials.password)
          .await?;
        Ok(from_client(client))
      }
      Login::UntypedUuid(id) => {
        let user_id = UserId::from(id);
        let client_id = OauthClientId::from(id);
        // TODO: Race both
        let user = self
          .authenticate_user_with_password(UserRef::Id(user_id.into()), credentials.password.clone())
          .await;
        let client = self
          .authenticate_oauth_client(OauthClientRef::Id(client_id.into()), credentials.password)
          .await;
        match (user, client) {
          (Ok(user), _) => Ok(from_user(user)),
          (_, Ok(client)) => Ok(from_client(client)),
          (Err(_), Err(_)) => Err(WeakError::new("UserOrClientNotFound")),
        }
      }
    }
  }

  async fn authenticate_user_with_password(
    &self,
    user_ref: UserRef,
    password: Password,
  ) -> Result<SimpleUser, WeakError> {
    let now = self.clock.clock().now();
    let user_with_password: ShortUserWithPassword = self
      .user_store
      .user_store()
      .get_user_with_password(RawGetUser {
        r#ref: user_ref,
        fields: UserFields::Complete,
        time: now,
        skip_deleted: false,
      })
      .await
      .map_err(WeakError::wrap)?;
    if user_with_password.deleted.is_some() {
      return Err(WeakError::new("Gone"));
    }
    let password_hash = match user_with_password.password {
      Some(password_hash) => password_hash,
      None => return Err(WeakError::new("NoPassword")),
    };
    let is_match = self.password_service.password_service().verify(password_hash, password);
    if !is_match {
      return Err(WeakError::new("WrongPassword"));
    }

    let user: Option<RawGetUserResult> = match self
      .user_store
      .user_store()
      .get_user(&GetUserOptions {
        r#ref: UserRef::Id(UserIdRef {
          id: user_with_password.id,
        }),
        fields: UserFields::Default,
        time: None,
        skip_deleted: true,
      })
      .await
    {
      Ok(u) => Some(u),
      Err(RawGetUserError::NotFound) => None,
      Err(e) => return Err(WeakError::wrap(e)),
    };
    let user = user.expect("UserShouldStillExistAfterPasswordWasVerified");

    let user: SimpleUser = match user {
      RawGetUserResult::Complete(u) => u.into(),
      RawGetUserResult::Default(u) => u,
      RawGetUserResult::Short(_) => {
        unreachable!("AssertionError: Requested `UserFields::Default` but got short response")
      }
    };

    Ok(user)
  }

  async fn authenticate_oauth_client(
    &self,
    oauth_client_ref: OauthClientRef,
    secret: Password,
  ) -> Result<SimpleOauthClient, WeakError> {
    let client_with_secret = self
      .oauth_provider_store
      .oauth_provider_store()
      .get_client_with_secret(&GetOauthClientOptions {
        r#ref: oauth_client_ref,
      })
      .await
      .map_err(WeakError::wrap)?;
    let is_match = self
      .password_service
      .password_service()
      .verify(client_with_secret.secret, secret);
    if !is_match {
      return Err(WeakError::new("WrongSecret"));
    }

    Ok(SimpleOauthClient {
      id: client_with_secret.id,
      key: client_with_secret.key,
      display_name: client_with_secret.display_name,
      app_uri: client_with_secret.app_uri,
      callback_uri: client_with_secret.callback_uri,
      owner: client_with_secret.owner,
    })
  }

  fn create_email_verification_token(&self, email: &EmailAddress) -> Result<String, WeakError> {
    let now = self.clock.clock().now();
    let expires_at = now + self.email_verification_validity;

    let claims = EmailJwtClaims {
      exp: expires_at.into_posix_timestamp(),
      iat: now.into_posix_timestamp(),
      email: email.clone(),
    };

    let key = jsonwebtoken::EncodingKey::from_secret(self.jwt_secret_key.as_slice());

    let token = jsonwebtoken::encode(
      &jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256),
      &claims,
      &key,
    )
    .map_err(WeakError::wrap)?;
    Ok(token)
  }

  /// Read the eternaltwin client state (round-tripped through a third-party provider)
  pub fn read_oauth_state(&self, state: &str) -> Result<EtwinOauthStateClaims, ReadOauthStateError> {
    let state = decode_tiny_state(state)?;
    let state = state.as_str();
    let now = self.clock.clock().now();
    let key = jsonwebtoken::DecodingKey::from_secret(self.jwt_secret_key.as_slice());
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    // TODO: validate expiration (the `validation` updates below disable it)!
    validation.required_spec_claims = HashSet::new();
    validation.leeway = 0;
    validation.validate_exp = false;
    let token = jsonwebtoken::decode::<EtwinOauthStateClaims>(state, &key, &validation)?;
    if !(token.claims.issued_at <= now && now < token.claims.expiration_time) {
      return Err(ReadOauthStateError::Time {
        now,
        issued_at: token.claims.issued_at,
        expiration_time: token.claims.expiration_time,
      });
    }

    Ok(token.claims)
  }

  fn read_email_verification_token(&self, token: &str) -> Result<EmailJwtClaims, WeakError> {
    let now = self.clock.clock().now().into_posix_timestamp();
    let key = jsonwebtoken::DecodingKey::from_secret(self.jwt_secret_key.as_slice());
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    // TODO: validate expiration (the `validation` updates below disable it)!
    validation.required_spec_claims = HashSet::new();
    validation.leeway = 0;
    validation.validate_exp = false;
    let token = jsonwebtoken::decode::<EmailJwtClaims>(token, &key, &validation).map_err(WeakError::wrap)?;
    if !(token.claims.iat <= now && now < token.claims.exp) {
      return Err(WeakError::new("TokenIsNotValidAtThisTime"));
    }

    Ok(token.claims)
  }

  /// Create an OAuth authorization code.
  fn create_authorization_code(
    &self,
    user: UserIdRef,
    client: &SimpleOauthClient,
    scopes: &EtwinOauthScopes,
    trace_parent: TraceParent,
  ) -> Result<String, WeakError> {
    // let mut missing_scopes: HashSet<String> = HashSet::new();
    // let EtwinOauthScopes { base: base_scope } = scopes;
    // TODO: Check for missing scopes and prompt user...
    self.create_code_token(client.id, client.key.as_ref(), user.id, scopes, trace_parent)
  }

  fn create_code_token(
    &self,
    client_id: OauthClientId,
    client_key: Option<&OauthClientKey>,
    user_id: UserId,
    scopes: &EtwinOauthScopes,
    trace_parent: TraceParent,
  ) -> Result<String, WeakError> {
    let now = self.clock.clock().now();
    let expires_at = now + self.authorization_code_validity;

    let mut aud: Vec<String> = Vec::with_capacity(2);
    aud.push(client_id.to_string());
    if let Some(client_key) = client_key {
      aud.push(client_key.to_string());
    }

    let claims = OauthCodeJwtClaims {
      aud,
      exp: expires_at.into_posix_timestamp(),
      iat: now.into_posix_timestamp(),
      nbf: now.into_posix_timestamp(),
      iss: "eternaltwin".to_string(),
      sub: user_id,
      trp: trace_parent,
      scopes: scopes.strings(),
    };

    let key = jsonwebtoken::EncodingKey::from_secret(self.jwt_secret_key.as_slice());

    let token = jsonwebtoken::encode(
      &jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256),
      &claims,
      &key,
    )
    .map_err(WeakError::wrap)?;
    Ok(token)
  }

  /// Read the oauth code received during the "access token" response step
  fn read_oauth_code_token(&self, oauth_code: &str) -> Result<OauthCodeJwtClaims, ReadOauthCodeError> {
    read_oauth_code_token(self.clock.clock().now(), self.jwt_secret_key.as_slice(), oauth_code)
  }
}

#[derive(
  Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize, thiserror::Error,
)]
pub enum ReadOauthCodeError {
  #[error("invalid oauth code format")]
  Format(#[source] WeakError),
  #[error("code expired: now = {now}, period.start = {start}, period.end = {end}", start = period.start, end = period.end)]
  ValidityTime { now: Instant, period: FinitePeriod },
  #[error(transparent)]
  Other(WeakError),
}

/// Read oauth code, expected to be a JWT generated by `create_code_token`.
///
/// The oauth code is generated by Eternaltwin when a user grants authorization to an OAuth
/// client. The user is redirected to the client oauth callback with the code passed as a query string.
/// The client can then exchange this code to claim an OAuth token. This function reads this code
/// during this last step.
///
/// The code is generated by `create_code_token` and should not be modified by either the user or oauth
/// client (but we can't trust them, so full checks are performed).
fn read_oauth_code_token(
  now: Instant,
  jwt_secret_key: &[u8],
  oauth_code: &str,
) -> Result<OauthCodeJwtClaims, ReadOauthCodeError> {
  let key = jsonwebtoken::DecodingKey::from_secret(jwt_secret_key);
  let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
  // Disable builtin expiration validation of the `jsonwebtoken` lib.
  // The reason is that it directly uses the system time instead of `Clock`
  // instance attached to the service. We do our own checks a bit below.
  {
    validation.required_spec_claims = HashSet::new();
    validation.leeway = 0;
    validation.validate_exp = false;
  }

  let token = match jsonwebtoken::decode::<OauthCodeJwtClaims>(oauth_code, &key, &validation) {
    Ok(token) => token,
    Err(e) => {
      use jsonwebtoken::errors::ErrorKind as JwtErrorKind;
      return Err(match e.kind() {
        JwtErrorKind::Base64(_)
        | JwtErrorKind::InvalidAlgorithm
        | JwtErrorKind::InvalidKeyFormat
        | JwtErrorKind::InvalidToken
        | JwtErrorKind::InvalidSignature
        | JwtErrorKind::Json(_)
        | JwtErrorKind::MissingAlgorithm
        | JwtErrorKind::Utf8(_) => ReadOauthCodeError::Format(WeakError::wrap(e)),
        _ => ReadOauthCodeError::Other(WeakError::wrap(e)),
      });
    }
  };

  let nbf = Instant::from_posix_timestamp(token.claims.nbf);
  let exp = Instant::from_posix_timestamp(token.claims.exp);
  let period = FinitePeriod::new(nbf, exp);
  if !period.contains(now) {
    return Err(ReadOauthCodeError::ValidityTime { now, period });
  }

  Ok(token.claims)
}

impl<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  > HammerfestStoreRef
  for AuthService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyEmailFormatter: EmailFormatterRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyMailer: MailerRef,
  TyOauthProviderStore: OauthProviderStoreRef,
  TyPasswordService: PasswordServiceRef,
  TyTracer: Send + Sync,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  type HammerfestStore = TyHammerfestStore::HammerfestStore;

  fn hammerfest_store(&self) -> &Self::HammerfestStore {
    self.hammerfest_store.hammerfest_store()
  }
}

impl<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  > TwinoidStoreRef
  for AuthService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyEmailFormatter: EmailFormatterRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyMailer: MailerRef,
  TyOauthProviderStore: OauthProviderStoreRef,
  TyPasswordService: PasswordServiceRef,
  TyTracer: Send + Sync,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  type TwinoidStore = TyTwinoidStore::TwinoidStore;

  fn twinoid_store(&self) -> &Self::TwinoidStore {
    self.twinoid_store.twinoid_store()
  }
}

impl<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  > LinkResolver
  for AuthService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyEmailFormatter,
    TyHammerfestStore,
    TyLinkStore,
    TyMailer,
    TyOauthProviderStore,
    TyPasswordService,
    TyTracer,
    TyTwinoidStore,
    TyUserStore,
    TyUuidGenerator,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyEmailFormatter: EmailFormatterRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyMailer: MailerRef,
  TyOauthProviderStore: OauthProviderStoreRef,
  TyPasswordService: PasswordServiceRef,
  TyTracer: Send + Sync,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  type DinoparcStore = TyDinoparcStore::DinoparcStore;
  type UserStore = TyUserStore::UserStore;

  fn dinoparc_store(&self) -> &Self::DinoparcStore {
    self.dinoparc_store.dinoparc_store()
  }

  fn user_store(&self) -> &Self::UserStore {
    self.user_store.user_store()
  }
}

#[allow(unused)]
trait DeriveUserDisplayName {
  fn derive_user_display_name(&self) -> UserDisplayName;
}

impl DeriveUserDisplayName for ShortDinoparcUser {
  fn derive_user_display_name(&self) -> UserDisplayName {
    if let Ok(name) = UserDisplayName::from_str(self.username.as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("dparc_{}", &self.username).as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("dparc_{}", &self.id).as_str()) {
      return name;
    }
    UserDisplayName::from_str("dparcPlayer").expect("`dparcPlayer` should be a valid `UserDisplayName`")
  }
}

impl DeriveUserDisplayName for ShortHammerfestUser {
  fn derive_user_display_name(&self) -> UserDisplayName {
    if let Ok(name) = UserDisplayName::from_str(self.username.as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("hf_{}", &self.username).as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("hf_{}", &self.id).as_str()) {
      return name;
    }
    UserDisplayName::from_str("hfPlayer").expect("`hfPlayer` should be a valid `UserDisplayName`")
  }
}

impl DeriveUserDisplayName for ShortTwinoidUser {
  fn derive_user_display_name(&self) -> UserDisplayName {
    if let Ok(name) = UserDisplayName::from_str(self.display_name.as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("tid_{}", &self.display_name).as_str()) {
      return name;
    }
    if let Ok(name) = UserDisplayName::from_str(format!("tid_{}", &self.id).as_str()) {
      return name;
    }
    UserDisplayName::from_str("tidPlayer").expect("`tidPlayer` should be a valid `UserDisplayName`")
  }
}

#[derive(Debug, thiserror::Error)]
pub enum DecodeTinyStateError {
  #[error("invalid state format, expected [object, object, string]")]
  Format(serde_json::Error),
  #[error("invalid header payload")]
  Header(serde_json::Error),
  #[error("invalid token payload")]
  Payload(serde_json::Error),
  #[error("(BUG) failed to re-encode header")]
  EncodeHeader(serde_json::Error),
  #[error("(BUG) failed to re-encode payload")]
  EncodePayload(serde_json::Error),
}

fn decode_tiny_state(tiny: &str) -> Result<String, DecodeTinyStateError> {
  let (header, payload, signature) =
    serde_json::from_str::<(JsonValue, JsonValue, String)>(tiny).map_err(DecodeTinyStateError::Format)?;
  let header: jsonwebtoken::Header = serde_json::from_value(header).map_err(DecodeTinyStateError::Header)?;
  let payload: EtwinOauthStateClaims = serde_json::from_value(payload).map_err(DecodeTinyStateError::Payload)?;
  let mut result = String::new();
  base64::engine::general_purpose::URL_SAFE_NO_PAD.encode_string(
    serde_json::to_vec(&header).map_err(DecodeTinyStateError::EncodeHeader)?,
    &mut result,
  );
  result.push('.');
  base64::engine::general_purpose::URL_SAFE_NO_PAD.encode_string(
    serde_json::to_vec(&payload).map_err(DecodeTinyStateError::EncodePayload)?,
    &mut result,
  );
  result.push('.');
  result.push_str(signature.as_str());
  Ok(result)
}

// fn encode_tiny_state(full: &str) -> Result<String, WeakError> {
//   let mut parts = full.split('.');
//   let header = parts
//     .next()
//     .ok_or_else(|| WeakError::from("failed to read JWT header"))?;
//   let payload = parts
//     .next()
//     .ok_or_else(|| WeakError::from("failed to read JWT payload"))?;
//   let signature = parts
//     .next()
//     .ok_or_else(|| WeakError::from("failed to read JWT signature"))?;
//   if parts.next().is_some() {
//     return Err(WeakError::from("failed to split JWT, got 4 parts"));
//   }
//
//   let header = base64::engine::general_purpose::URL_SAFE_NO_PAD.decode(header)?;
//   let header: jsonwebtoken::Header = serde_json::from_slice(&header)?;
//   let payload = base64::engine::general_purpose::URL_SAFE_NO_PAD.decode(payload)?;
//   let payload: EtwinOauthStateClaims = serde_json::from_slice(&payload)?;
//
//   Ok(serde_json::to_string(&(header, payload, signature))?)
// }
//
// #[cfg(test)]
// mod tests {
//   use crate::auth::{decode_tiny_state, encode_tiny_state};
//
//   #[test]
//   fn test_decode_tiny_state() {
//     let input = r#"[{"typ":"JWT","alg":"HS256"},{"rfp":"TODO","a":{"type":"Login"},"iat":1662586004,"as":"twinoid.com","exp":1662586904},"67ePoTz30T5ZRNfs72c1BpWQc3TbO3LKTRJCeMGJYwM"]"#;
//     let actual = decode_tiny_state(input).unwrap();
//     let expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZnAiOiJUT0RPIiwiYSI6eyJ0eXBlIjoiTG9naW4ifSwiaWF0IjoxNjYyNTg2MDA0LCJhcyI6InR3aW5vaWQuY29tIiwiZXhwIjoxNjYyNTg2OTA0fQ.67ePoTz30T5ZRNfs72c1BpWQc3TbO3LKTRJCeMGJYwM";
//     assert_eq!(actual.as_str(), expected);
//   }
//
//   #[test]
//   fn test_encode_tiny_state() {
//     let input = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZnAiOiJUT0RPIiwiYSI6eyJ0eXBlIjoiTG9naW4ifSwiaWF0IjoxNjYyNTg2MDA0LCJhcyI6InR3aW5vaWQuY29tIiwiZXhwIjoxNjYyNTg2OTA0fQ.67ePoTz30T5ZRNfs72c1BpWQc3TbO3LKTRJCeMGJYwM";
//     let actual = encode_tiny_state(input).unwrap();
//     let expected = r#"[{"typ":"JWT","alg":"HS256"},{"rfp":"TODO","a":{"type":"Login"},"iat":1662586004,"as":"twinoid.com","exp":1662586904},"67ePoTz30T5ZRNfs72c1BpWQc3TbO3LKTRJCeMGJYwM"]"#;
//     assert_eq!(actual.as_str(), expected);
//   }
// }
