use crate::helpers::link_resolver::{
  LinkResolver, ResolveDinoparcLinkError, ResolveHammerfestLinkError, ResolveLinksError, ResolveTwinoidLinkError,
};
use eternaltwin_core::auth::{AuthContext, AuthStore, AuthStoreRef, RawDeleteAllSessions};
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::dinoparc::{DinoparcStore, DinoparcStoreRef, DinoparcUserIdRef, ShortDinoparcUser};
use eternaltwin_core::hammerfest::{
  GetHammerfestUserOptions, HammerfestClientCreateSessionError, HammerfestStore, HammerfestStoreRef,
  HammerfestUserIdRef, RawGetShortHammerfestUserError, ShortHammerfestUser, TouchHammerfestSessionError,
};
use eternaltwin_core::link::store::{LinkStore, LinkStoreRef};
use eternaltwin_core::link::{
  DeleteLinkError, DeleteLinkOptions, GetLinksFromEtwinOptions, RawDeleteAllLinks, TouchLinkError, TouchLinkOptions,
  VersionedLink, VersionedRawLinks,
};
use eternaltwin_core::password::{PasswordService, PasswordServiceRef};
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::twinoid::client::get_me::GetMeShortError;
use eternaltwin_core::twinoid::store::{TouchTwinoidOauthError, TwinoidStoreRef};
use eternaltwin_core::twinoid::{ShortTwinoidUser, TwinoidStore, TwinoidUserIdRef};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::store::enable_user::{EnableUser as RawEnableUser, EnableUserError as RawEnableUserError};
pub use eternaltwin_core::user::RawDeleteUserError;
use eternaltwin_core::user::{
  CompleteSimpleUser, CompleteUser, DeleteUserOptions, EnableUser, EnableUserOut, GetUserOptions, GetUserResult,
  GetUserTos, GetUserTosOptions, LinkToDinoparcOptions, LinkToHammerfestOptions, LinkToTwinoidOptions, RawDeleteUser,
  RawGetUser, RawGetUserError, RawGetUserResult, RawGetUserTosError, RawGetUserTosOptions, RawGetUserWithPasswordError,
  RawUpdateUserError, RawUpdateUserOptions, RawUpdateUserPatch, RawUpdateUserTosError, RawUpdateUserTosOptions,
  RawUpsertSeedUserError, ShortUser, UnlinkFromDinoparcOptions, UnlinkFromHammerfestOptions, UnlinkFromTwinoidOptions,
  UpdateUserOptions, UpsertSeedUser, UpsertSeedUserOptions, User, UserFields, UserIdRef, UserStore, UserStoreRef,
};
use std::sync::Arc;

pub struct UserService<
  TyAuthStore,
  TyClock,
  TyDinoparcStore,
  TyHammerfestStore,
  TyLinkStore,
  TyPassword,
  TyTwinoidStore,
  TyUserStore,
> where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  auth_store: TyAuthStore,
  clock: TyClock,
  dinoparc_store: TyDinoparcStore,
  hammerfest_store: TyHammerfestStore,
  link_store: TyLinkStore,
  password: TyPassword,
  twinoid_store: TyTwinoidStore,
  user_store: TyUserStore,
}

pub type DynUserService = UserService<
  Arc<dyn AuthStore>,
  Arc<dyn Clock>,
  Arc<dyn DinoparcStore>,
  Arc<dyn HammerfestStore>,
  Arc<dyn LinkStore>,
  Arc<dyn PasswordService>,
  Arc<dyn TwinoidStore>,
  Arc<dyn UserStore>,
>;

#[derive(Debug, thiserror::Error)]
pub enum GetUserError {
  #[error("user not found")]
  NotFound,
  #[error("internal store get_user error")]
  InternalStoreGetUser(#[source] RawGetUserError),
  #[error("internal store get_links_from_etwin error")]
  InternalStoreGetLinksFromEtwin(#[source] WeakError),
  #[error("internal resolve_links error")]
  InternalResolveLinks(#[source] ResolveLinksError),
  #[error("internal store get_user_with_password error")]
  InternalStoreGetUserWithPassword(#[source] RawGetUserWithPasswordError),
}

#[derive(Debug, thiserror::Error)]
pub enum UpdateUserError {
  #[error("forbidden")]
  Forbidden,
  #[error("raw update error")]
  Raw(#[from] RawUpdateUserError),
  #[error("internal resolve_links error")]
  InternalResolveLinks(#[source] ResolveLinksError),
  #[error("internal get_user_with_password error")]
  InternalGetUserWithPassword(#[source] RawGetUserWithPasswordError),
  #[error("internal error")]
  Other(#[source] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum LinkToDinoparcError {
  #[error("dinoparc linking forbidden for current actor")]
  Forbidden,
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<DinoparcUserIdRef>),
  #[error("internal resolve_dinoparc_link error")]
  InternalResolveDinoparcLink(#[source] ResolveDinoparcLinkError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum UnlinkFromDinoparcError {
  #[error("dinoparc unlinking forbidden for current actor")]
  Forbidden,
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<DinoparcUserIdRef>),
  #[error("internal resolve_dinoparc_link error")]
  InternalResolveDinoparcLink(#[source] ResolveDinoparcLinkError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum LinkToHammerfestError {
  #[error("hammerfest linking forbidden for current actor")]
  Forbidden,
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<HammerfestUserIdRef>),
  #[error("remote user not found")]
  RemoteNotFound(#[from] RawGetShortHammerfestUserError),
  #[error("internal resolve_hammerfest_link error")]
  InternalResolveHammerfestLink(#[source] ResolveHammerfestLinkError),
  #[error("wrong credentials")]
  WrongCredentials,
  #[error("internal touch hammerfest session error")]
  TouchHammerfestSession(#[from] TouchHammerfestSessionError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

impl From<HammerfestClientCreateSessionError> for LinkToHammerfestError {
  fn from(value: HammerfestClientCreateSessionError) -> Self {
    use HammerfestClientCreateSessionError::*;
    match value {
      WrongCredentials => Self::WrongCredentials,
      Other(inner) => Self::Internal(WeakError::wrap(inner)),
    }
  }
}

#[derive(Debug, thiserror::Error)]
pub enum UnlinkFromHammerfestError {
  #[error("hammerfest unlinking forbidden for current actor")]
  Forbidden,
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<HammerfestUserIdRef>),
  #[error("internal resolve_hammerfest_link error")]
  InternalResolveHammerfestLink(#[source] ResolveHammerfestLinkError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum UpsertSeedUserError {
  #[error("internal upsert seed user error")]
  Internal(#[source] RawUpsertSeedUserError),
}

#[derive(Debug, thiserror::Error)]
pub enum LinkToTwinoidError {
  #[error("twinoid linking forbidden for current actor")]
  Forbidden,
  #[error("internal resolve_twinoid_link error")]
  InternalResolveTwinoidLink(#[source] ResolveTwinoidLinkError),
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<TwinoidUserIdRef>),
  #[error("internal touch twinoid oauth error")]
  TouchTwinoidOauth(#[from] TouchTwinoidOauthError),
  #[error("failed to query Twinoid for user details")]
  TwinoidGetMe(#[from] GetMeShortError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum UnlinkFromTwinoidError {
  #[error("twinoid unlinking forbidden for current actor")]
  Forbidden,
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<TwinoidUserIdRef>),
  #[error("internal resolve_twinoid_link error")]
  InternalResolveTwinoidLink(#[source] ResolveTwinoidLinkError),
  #[error("internal error")]
  Internal(#[source] WeakError),
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum DeleteUserError {
  #[error("insufficient permission to delete user")]
  Forbidden,
  #[error("failed to fetch user links")]
  InternalGetLinksFromEtwin(#[source] WeakError),
  #[error("failed to resolve user links")]
  InternalResolveLinks(#[source] ResolveLinksError),
  #[error("failed to find user to delete for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error("user is already deleted for ref: {:?}", .0)]
  Gone(UserIdRef),
  #[error("unexpected delete_user error")]
  InternalDeleteUser(#[source] WeakError),
  #[error("failed to delete user links")]
  InternalDeleteAllLinks(#[source] WeakError),
  #[error("failed to delete user sessions")]
  InternalDeleteAllSessions(#[source] WeakError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum GetUserTosError {
  #[error("failed to find user for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error("insufficient permission to access user's Terms of Services")]
  Forbidden,
  #[error("failed to get user Terms of Services")]
  InternalGetUserTos(#[source] RawGetUserTosError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum UpdateUserTosError {
  #[error("failed to find user for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error("insufficient permission to update user's Terms of Services")]
  Forbidden,
  #[error("ToS are already accepted")]
  AlreadyAccepted,
  #[error("Failed to update user Terms of Services")]
  InternalUpdateUserTos(#[source] RawUpdateUserTosError),
  #[error(transparent)]
  Other(WeakError),
}

impl<TyAuthStore, TyClock, TyDinoparcStore, TyHammerfestStore, TyLinkStore, TyPassword, TyTwinoidStore, TyUserStore>
  UserService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  #[allow(clippy::too_many_arguments)]
  pub fn new(
    auth_store: TyAuthStore,
    clock: TyClock,
    dinoparc_store: TyDinoparcStore,
    hammerfest_store: TyHammerfestStore,
    link_store: TyLinkStore,
    password: TyPassword,
    twinoid_store: TyTwinoidStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      auth_store,
      clock,
      dinoparc_store,
      hammerfest_store,
      link_store,
      password,
      twinoid_store,
      user_store,
    }
  }

  pub async fn upsert_seed_user(&self, cmd: UpsertSeedUser) -> Result<ShortUser, UpsertSeedUserError> {
    let now = self.clock.clock().now();
    let password = cmd.password.map(|p| self.password.password_service().hash(p));
    let raw_user = self
      .user_store
      .user_store()
      .upsert_seed_user(&UpsertSeedUserOptions {
        now,
        r#ref: cmd.r#ref,
        display_name: cmd.display_name,
        username: cmd.username,
        email: cmd.email,
        password,
        is_administrator: cmd.is_administrator,
      })
      .await
      .map_err(UpsertSeedUserError::Internal)?;
    Ok(ShortUser::from(raw_user))
  }

  pub async fn get_user(&self, acx: &AuthContext, options: &GetUserOptions) -> Result<GetUserResult, GetUserError> {
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());
    let fields: UserFields = match acx {
      AuthContext::User(acx) => {
        if acx.is_administrator {
          UserFields::Complete
        } else {
          UserFields::CompleteIfSelf {
            self_user_id: acx.user.id,
          }
        }
      }
      _ => UserFields::Default,
    };
    let get_user_options = GetUserOptions {
      r#ref: options.r#ref.clone(),
      fields,
      time: Some(time),
      skip_deleted: options.skip_deleted,
    };
    let raw_user = self.user_store.user_store().get_user(&get_user_options).await;
    let raw_user: RawGetUserResult = match raw_user {
      Ok(u) => u,
      Err(RawGetUserError::NotFound) => return Err(GetUserError::NotFound),
      Err(e) => return Err(GetUserError::InternalStoreGetUser(e)),
    };
    let raw_links: VersionedRawLinks = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: raw_user.id().into(),
        time: Some(time),
      })
      .await
      .map_err(GetUserError::InternalStoreGetLinksFromEtwin)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(GetUserError::InternalResolveLinks)?;
    Ok(match raw_user {
      RawGetUserResult::Short(_) => unreachable!("expected `default` or `complete` user"),
      RawGetUserResult::Default(raw_user) => GetUserResult::Default(User {
        id: raw_user.id,
        created_at: raw_user.created_at,
        deleted_at: raw_user.deleted_at,
        display_name: raw_user.display_name,
        is_administrator: raw_user.is_administrator,
        links,
      }),
      RawGetUserResult::Complete(raw_user) => {
        let user_with_password = self
          .user_store
          .user_store()
          .get_user_with_password(RawGetUser {
            r#ref: get_user_options.r#ref,
            fields: get_user_options.fields,
            time: get_user_options.time.unwrap_or_else(|| self.clock.clock().now()),
            skip_deleted: get_user_options.skip_deleted,
          })
          .await
          .map_err(GetUserError::InternalStoreGetUserWithPassword)?;
        let has_password = user_with_password.password.is_some();
        GetUserResult::Complete(CompleteUser {
          id: raw_user.id,
          created_at: raw_user.created_at,
          deleted_at: raw_user.deleted_at,
          display_name: raw_user.display_name,
          is_administrator: raw_user.is_administrator,
          links,
          username: raw_user.username,
          email_address: raw_user.email_address,
          has_password,
        })
      }
    })
  }

  pub async fn update_user(
    &self,
    acx: AuthContext,
    options: UpdateUserOptions,
  ) -> Result<CompleteUser, UpdateUserError> {
    let time = self.clock.clock().now();
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(UpdateUserError::Forbidden),
    };
    if !(actor.is_administrator || actor.user.as_ref() == options.r#ref) {
      return Err(UpdateUserError::Forbidden);
    }
    let raw_patch = RawUpdateUserPatch {
      display_name: options.patch.display_name.clone(),
      username: options.patch.username.clone(),
      password: match options.patch.password {
        SimplePatch::Set(Some(new_password)) => {
          let new_hash = self.password.password_service().hash(new_password);
          SimplePatch::Set(Some(new_hash))
        }
        SimplePatch::Set(None) => SimplePatch::Set(None),
        SimplePatch::Skip => SimplePatch::Skip,
      },
    };
    let raw_user = self
      .user_store
      .user_store()
      .update_user(&RawUpdateUserOptions {
        actor: actor.user.as_ref(),
        r#ref: options.r#ref,
        skip_rate_limit: actor.is_administrator,
        patch: raw_patch,
      })
      .await?;
    let raw_links: VersionedRawLinks = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: options.r#ref,
        time: Some(time),
      })
      .await
      .map_err(UpdateUserError::Other)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(UpdateUserError::InternalResolveLinks)?;
    let user_with_password = self
      .user_store
      .user_store()
      .get_user_with_password(RawGetUser {
        r#ref: options.r#ref.into(),
        fields: UserFields::Default,
        time,
        skip_deleted: false,
      })
      .await
      .map_err(UpdateUserError::InternalGetUserWithPassword)?;
    let has_password = user_with_password.password.is_some();
    Ok(CompleteUser {
      id: raw_user.id,
      created_at: raw_user.created_at,
      deleted_at: raw_user.deleted_at,
      display_name: raw_user.display_name,
      is_administrator: raw_user.is_administrator,
      links,
      username: raw_user.username,
      email_address: raw_user.email_address,
      has_password,
    })
  }

  pub async fn delete_user(
    &self,
    acx: AuthContext,
    options: DeleteUserOptions,
  ) -> Result<CompleteUser, DeleteUserError> {
    let time = self.clock.clock().now();
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(DeleteUserError::Forbidden),
    };
    if !(actor.is_administrator || actor.user.as_ref() == options.r#ref) {
      return Err(DeleteUserError::Forbidden);
    }
    let raw_links: VersionedRawLinks = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: options.r#ref,
        time: Some(time),
      })
      .await
      .map_err(DeleteUserError::InternalGetLinksFromEtwin)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(DeleteUserError::InternalResolveLinks)?;
    let raw_user: CompleteSimpleUser = match self
      .user_store
      .user_store()
      .delete_user(&RawDeleteUser {
        actor: actor.user.as_ref(),
        r#user: options.r#ref,
        now: time,
      })
      .await
    {
      Ok(raw_user) => raw_user,
      Err(e) => {
        return Err(match e {
          RawDeleteUserError::NotFound(r) => DeleteUserError::NotFound(r),
          RawDeleteUserError::Gone(r) => DeleteUserError::Gone(r),
          RawDeleteUserError::Forbidden => DeleteUserError::Forbidden,
          RawDeleteUserError::Other(e) => DeleteUserError::InternalDeleteUser(e),
        })
      }
    };
    self
      .link_store
      .link_store()
      .delete_all_links(RawDeleteAllLinks {
        etwin: options.r#ref,
        now: time,
        unlinked_by: actor.user.as_ref(),
      })
      .await
      .map_err(DeleteUserError::InternalDeleteAllLinks)?;

    self
      .auth_store
      .auth_store()
      .delete_all_sessions(&RawDeleteAllSessions { user: options.r#ref })
      .await
      .map_err(DeleteUserError::InternalDeleteAllSessions)?;

    Ok(CompleteUser {
      id: raw_user.id,
      created_at: raw_user.created_at,
      deleted_at: raw_user.deleted_at,
      display_name: raw_user.display_name,
      is_administrator: raw_user.is_administrator,
      links,
      username: raw_user.username,
      email_address: raw_user.email_address,
      has_password: raw_user.has_password,
    })
  }

  pub async fn link_to_dinoparc(
    &self,
    acx: AuthContext,
    options: LinkToDinoparcOptions,
  ) -> Result<VersionedLink<ShortDinoparcUser>, LinkToDinoparcError> {
    let time = self.clock.clock().now();
    let (acx, etwin, remote) = match options {
      LinkToDinoparcOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToDinoparcError::Forbidden),
        };
        let remote = DinoparcUserIdRef {
          server: options.dinoparc_server,
          id: options.dinoparc_user_id,
        };
        (acx, options.user_id, remote)
      }
    };
    let raw = self
      .link_store
      .link_store()
      .touch_dinoparc_link(TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_dinoparc_link(raw, time)
      .await
      .map_err(LinkToDinoparcError::InternalResolveDinoparcLink)
  }

  pub async fn unlink_from_dinoparc(
    &self,
    acx: AuthContext,
    options: UnlinkFromDinoparcOptions,
  ) -> Result<VersionedLink<ShortDinoparcUser>, UnlinkFromDinoparcError> {
    let time = self.clock.clock().now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromDinoparcError::Forbidden),
    };
    let raw = self
      .link_store
      .link_store()
      .delete_dinoparc_link(DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: DinoparcUserIdRef {
          server: options.dinoparc_server,
          id: options.dinoparc_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_dinoparc_link(raw, time)
      .await
      .map_err(UnlinkFromDinoparcError::InternalResolveDinoparcLink)
  }

  pub async fn link_to_hammerfest(
    &self,
    acx: AuthContext,
    options: LinkToHammerfestOptions,
  ) -> Result<VersionedLink<ShortHammerfestUser>, LinkToHammerfestError> {
    let time = self.clock.clock().now();
    let (acx, etwin, remote) = match options {
      LinkToHammerfestOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToHammerfestError::Forbidden),
        };
        let remote_user = self
          .hammerfest_store
          .hammerfest_store()
          .get_short_user(&GetHammerfestUserOptions {
            server: options.hammerfest_server,
            id: options.hammerfest_user_id,
            time: None,
          })
          .await?;
        (acx, options.user_id, remote_user.as_ref())
      }
    };
    let raw = self
      .link_store
      .link_store()
      .touch_hammerfest_link(TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_hammerfest_link(raw, time)
      .await
      .map_err(LinkToHammerfestError::InternalResolveHammerfestLink)
  }

  pub async fn unlink_from_hammerfest(
    &self,
    acx: AuthContext,
    options: UnlinkFromHammerfestOptions,
  ) -> Result<VersionedLink<ShortHammerfestUser>, UnlinkFromHammerfestError> {
    let time = self.clock.clock().now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromHammerfestError::Forbidden),
    };
    let raw = self
      .link_store
      .link_store()
      .delete_hammerfest_link(DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: HammerfestUserIdRef {
          server: options.hammerfest_server,
          id: options.hammerfest_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_hammerfest_link(raw, time)
      .await
      .map_err(UnlinkFromHammerfestError::InternalResolveHammerfestLink)
  }

  pub async fn link_to_twinoid(
    &self,
    acx: AuthContext,
    options: LinkToTwinoidOptions,
  ) -> Result<VersionedLink<ShortTwinoidUser>, LinkToTwinoidError> {
    let time = self.clock.clock().now();
    let (acx, etwin, remote) = match options {
      LinkToTwinoidOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToTwinoidError::Forbidden),
        };
        // TODO: Use scraping client to touch user.
        let remote = TwinoidUserIdRef {
          id: options.twinoid_user_id,
        };
        (acx, options.user_id, remote)
      }
    };
    let raw = self
      .link_store
      .link_store()
      .touch_twinoid_link(TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_twinoid_link(raw, time)
      .await
      .map_err(LinkToTwinoidError::InternalResolveTwinoidLink)
  }

  pub async fn unlink_from_twinoid(
    &self,
    acx: AuthContext,
    options: UnlinkFromTwinoidOptions,
  ) -> Result<VersionedLink<ShortTwinoidUser>, UnlinkFromTwinoidError> {
    let time = self.clock.clock().now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromTwinoidError::Forbidden),
    };
    let raw = self
      .link_store
      .link_store()
      .delete_twinoid_link(DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: TwinoidUserIdRef {
          id: options.twinoid_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_twinoid_link(raw, time)
      .await
      .map_err(UnlinkFromTwinoidError::InternalResolveTwinoidLink)
  }

  pub async fn get_tos(&self, acx: &AuthContext, options: &GetUserTosOptions) -> Result<GetUserTos, GetUserTosError> {
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(GetUserTosError::Forbidden),
    };

    if !actor.is_administrator && actor.user.id != options.r#ref.id {
      return Err(GetUserTosError::Forbidden);
    }

    let options = RawGetUserTosOptions {
      r#ref: options.r#ref,
      time: options.time.unwrap_or_else(|| self.clock.clock().now()),
    };

    match self.user_store.user_store().get_user_tos(&options).await {
      Ok(res) => Ok(res),
      Err(e) => Err(GetUserTosError::InternalGetUserTos(e)),
    }
  }

  pub async fn create_user_tos(&self, acx: &AuthContext, user: &UserIdRef) -> Result<(), UpdateUserTosError> {
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(UpdateUserTosError::Forbidden),
    };

    if actor.user.id != user.id {
      return Err(UpdateUserTosError::Forbidden);
    }

    let options = RawGetUserTosOptions {
      r#ref: *user,
      time: self.clock.clock().now(),
    };

    let tos_answer = match self.user_store.user_store().get_user_tos(&options).await {
      Ok(res) => res,
      Err(_) => return Err(UpdateUserTosError::NotFound(options.r#ref)),
    };

    if tos_answer.is_tos_accepted {
      return Err(UpdateUserTosError::AlreadyAccepted);
    }

    let options = RawUpdateUserTosOptions {
      r#ref: *user,
      now: self.clock.clock().now(),
      is_tos_accepted: true,
    };

    self
      .user_store
      .user_store()
      .create_user_tos(&options)
      .await
      .map_err(UpdateUserTosError::InternalUpdateUserTos)
  }
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum EnableUserError {
  #[error("insufficient permission to enable user")]
  Forbidden,
  #[error("failed to enable base user data")]
  InternalEnableUser(#[source] RawEnableUserError),
  #[error("failed to read all old user links")]
  ReadOldLinks(#[source] WeakError),
  #[error("failed to restore dinoparc.com link")]
  RestoreDinoparcCom(#[source] TouchLinkError<DinoparcUserIdRef>),
  #[error("failed to restore en.dinoparc.com link")]
  RestoreEnDinoparcCom(#[source] TouchLinkError<DinoparcUserIdRef>),
  #[error("failed to restore hammerfest.es link")]
  RestoreHammerfestEs(#[source] TouchLinkError<HammerfestUserIdRef>),
  #[error("failed to restore hammerfest.fr link")]
  RestoreHammerfestFr(#[source] TouchLinkError<HammerfestUserIdRef>),
  #[error("failed to restore hfest.net link")]
  RestoreHfestNet(#[source] TouchLinkError<HammerfestUserIdRef>),
  #[error("failed to restore sp.dinoparc.com link")]
  RestoreSpDinoparcCom(#[source] TouchLinkError<DinoparcUserIdRef>),
  #[error("failed to restore twinoid.com link")]
  RestoreTwinoid(#[source] TouchLinkError<TwinoidUserIdRef>),
  #[error("failed to read all current user links")]
  ReadCurrentLinks(#[source] WeakError),
  #[error("failed to resolve user links")]
  InternalResolveLinks(#[source] ResolveLinksError),
}

impl<TyAuthStore, TyClock, TyDinoparcStore, TyHammerfestStore, TyLinkStore, TyPassword, TyTwinoidStore, TyUserStore>
  UserService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  /// Re-enable a disabled user (no-op if not disabled)
  pub async fn enable_user(&self, acx: &AuthContext, cmd: &EnableUser) -> Result<EnableUserOut, EnableUserError> {
    let actor = match acx {
      AuthContext::User(acx) if acx.is_administrator => acx,
      _ => return Err(EnableUserError::Forbidden),
    };

    let now = self.clock.clock().now();
    let actor_ref = actor.user.as_ref();

    let raw_enable_out = self
      .user_store
      .user_store()
      .enable_user(RawEnableUser {
        now,
        r#ref: UserIdRef::new(cmd.r#ref),
        actor: actor_ref,
        span: None, // span will be set by store itself
      })
      .await
      .map_err(EnableUserError::InternalEnableUser)?;
    let raw_user = raw_enable_out.current;

    let target_ref = UserIdRef::new(raw_user.id);

    let mut dinoparc_com_res = Ok(());
    let mut en_dinoparc_com_res = Ok(());
    let mut hammerfest_es_res = Ok(());
    let mut hammerfest_fr_res = Ok(());
    let mut hfest_net_res = Ok(());
    let mut sp_dinoparc_com_res = Ok(());
    let mut twinoid_res = Ok(());

    if let Some(deleted_at) = raw_enable_out.deleted_at {
      let old_links: VersionedRawLinks = self
        .link_store
        .link_store()
        .get_links_from_etwin(GetLinksFromEtwinOptions {
          etwin: target_ref,
          time: Some(deleted_at),
        })
        .await
        .map_err(EnableUserError::ReadOldLinks)?;

      if let Some(dinoparc_com) = old_links.dinoparc_com.current {
        match self
          .link_store
          .link_store()
          .touch_dinoparc_link(TouchLinkOptions {
            etwin: target_ref,
            remote: dinoparc_com.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            dinoparc_com_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreDinoparcCom(e)),
        };
      }

      if let Some(en_dinoparc_com) = old_links.en_dinoparc_com.current {
        match self
          .link_store
          .link_store()
          .touch_dinoparc_link(TouchLinkOptions {
            etwin: target_ref,
            remote: en_dinoparc_com.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            en_dinoparc_com_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreEnDinoparcCom(e)),
        };
      }

      if let Some(hammerfest_es) = old_links.hammerfest_es.current {
        match self
          .link_store
          .link_store()
          .touch_hammerfest_link(TouchLinkOptions {
            etwin: target_ref,
            remote: hammerfest_es.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            hammerfest_es_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreHammerfestEs(e)),
        };
      }

      if let Some(hammerfest_fr) = old_links.hammerfest_fr.current {
        match self
          .link_store
          .link_store()
          .touch_hammerfest_link(TouchLinkOptions {
            etwin: target_ref,
            remote: hammerfest_fr.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            hammerfest_fr_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreHammerfestFr(e)),
        };
      }

      if let Some(hfest_net) = old_links.hfest_net.current {
        match self
          .link_store
          .link_store()
          .touch_hammerfest_link(TouchLinkOptions {
            etwin: target_ref,
            remote: hfest_net.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            hfest_net_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreHfestNet(e)),
        };
      }

      if let Some(sp_dinoparc_com) = old_links.sp_dinoparc_com.current {
        match self
          .link_store
          .link_store()
          .touch_dinoparc_link(TouchLinkOptions {
            etwin: target_ref,
            remote: sp_dinoparc_com.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            sp_dinoparc_com_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreSpDinoparcCom(e)),
        };
      }

      if let Some(twinoid) = old_links.twinoid.current {
        match self
          .link_store
          .link_store()
          .touch_twinoid_link(TouchLinkOptions {
            etwin: target_ref,
            remote: twinoid.remote,
            linked_by: actor_ref,
          })
          .await
        {
          Ok(_) => {}
          Err(TouchLinkError::ConflictEtwin(other) | TouchLinkError::ConflictBoth(other, _)) => {
            twinoid_res = Err(other)
          }
          Err(e) => return Err(EnableUserError::RestoreTwinoid(e)),
        };
      }
    }

    let raw_links: VersionedRawLinks = self
      .link_store
      .link_store()
      .get_links_from_etwin(GetLinksFromEtwinOptions {
        etwin: target_ref,
        time: Some(now),
      })
      .await
      .map_err(EnableUserError::ReadCurrentLinks)?;
    let links = self
      .resolve_links(raw_links, now)
      .await
      .map_err(EnableUserError::InternalResolveLinks)?;

    Ok(EnableUserOut {
      user: CompleteUser {
        id: raw_user.id,
        created_at: raw_user.created_at,
        deleted_at: raw_user.deleted_at,
        display_name: raw_user.display_name,
        is_administrator: raw_user.is_administrator,
        links,
        username: raw_user.username,
        email_address: raw_user.email_address,
        has_password: raw_user.has_password,
      },
      deleted_at: raw_enable_out.deleted_at,
      username: raw_enable_out.username,
      dinoparc_com: dinoparc_com_res,
      en_dinoparc_com: en_dinoparc_com_res,
      hammerfest_es: hammerfest_es_res,
      hammerfest_fr: hammerfest_fr_res,
      hfest_net: hfest_net_res,
      sp_dinoparc_com: sp_dinoparc_com_res,
      twinoid: twinoid_res,
    })
  }
}

impl<TyAuthStore, TyClock, TyDinoparcStore, TyHammerfestStore, TyLinkStore, TyPassword, TyTwinoidStore, TyUserStore>
  HammerfestStoreRef
  for UserService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  type HammerfestStore = TyHammerfestStore::HammerfestStore;

  fn hammerfest_store(&self) -> &Self::HammerfestStore {
    self.hammerfest_store.hammerfest_store()
  }
}

impl<TyAuthStore, TyClock, TyDinoparcStore, TyHammerfestStore, TyLinkStore, TyPassword, TyTwinoidStore, TyUserStore>
  TwinoidStoreRef
  for UserService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  type TwinoidStore = TyTwinoidStore::TwinoidStore;

  fn twinoid_store(&self) -> &Self::TwinoidStore {
    self.twinoid_store.twinoid_store()
  }
}

impl<TyAuthStore, TyClock, TyDinoparcStore, TyHammerfestStore, TyLinkStore, TyPassword, TyTwinoidStore, TyUserStore>
  LinkResolver
  for UserService<
    TyAuthStore,
    TyClock,
    TyDinoparcStore,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyDinoparcStore: DinoparcStoreRef,
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyPassword: PasswordServiceRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  type DinoparcStore = TyDinoparcStore::DinoparcStore;
  type UserStore = TyUserStore::UserStore;

  fn dinoparc_store(&self) -> &Self::DinoparcStore {
    self.dinoparc_store.dinoparc_store()
  }

  fn user_store(&self) -> &Self::UserStore {
    self.user_store.user_store()
  }
}
