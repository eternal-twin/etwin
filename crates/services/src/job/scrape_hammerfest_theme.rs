use crate::job::reliable_hammerfest_acquire_session::ReliableHammerfestAcquireSession;
use crate::job::scrape_hammerfest_thread_list::{ScrapeHammerfestThreadList, ScrapeHammerfestThreadListError};
use crate::job::{error_sleep, GLOBAL_HAMMEREFEST_RATE_LIMITER};
use async_trait::async_trait;
use eternaltwin_core::hammerfest::{
  HammerfestClient, HammerfestClientCreateSessionError, HammerfestClientGetThemePageError, HammerfestClientRef,
  HammerfestCredentials, HammerfestForumThemeId, HammerfestForumThreadId, HammerfestSession, HammerfestStore,
  HammerfestStoreRef, TouchSession,
};
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::rate_limiter::RateLimiterError;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::num::NonZeroU16;
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ScrapeHammerfestTheme {
  Start {
    credentials: HammerfestCredentials,
    theme_id: HammerfestForumThemeId,
  },
  Theme {
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
    session_task: ReliableHammerfestAcquireSession,
    theme_id: HammerfestForumThemeId,
    page1: NonZeroU16,
    /// Number of times the session was lost consecutively
    session_loss: u32,
    /// Number of times the fetch request failed
    fetch_failure: u32,
    threads: BTreeSet<(HammerfestForumThreadId, NonZeroU16)>,
  },
  Threads(Box<ScrapeHammerfestThreadList>),
  Ready(Result<(), Box<ScrapeHammerfestThemeError>>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum ScrapeHammerfestThemeError {
  #[error("wrong credentials")]
  WrongCredentials,
  #[error("theme not found")]
  NotFound,
  #[error("theme access is forbidden")]
  Forbidden,
  #[error("error while scraping thread list")]
  ThreadList(#[from] ScrapeHammerfestThreadListError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<HammerfestClientCreateSessionError> for ScrapeHammerfestThemeError {
  fn from(value: HammerfestClientCreateSessionError) -> Self {
    match value {
      HammerfestClientCreateSessionError::WrongCredentials => Self::WrongCredentials,
      HammerfestClientCreateSessionError::Other(e) => Self::Other(e),
    }
  }
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ScrapeHammerfestTheme
where
  Cx: TaskCx,
  Cx::Extra: HammerfestClientRef + HammerfestStoreRef,
{
  const NAME: &'static str = "ScrapeHammerfestTheme";
  const VERSION: u32 = 1;
  type Output = Result<(), Box<ScrapeHammerfestThemeError>>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    // Scraping pages in descending order to avoid missing threads due to deletions
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start { credentials, theme_id } => {
          *self = Self::Theme {
            timeout: None,
            credentials: credentials.clone(),
            session_task: ReliableHammerfestAcquireSession::new(credentials.clone()),
            theme_id: *theme_id,
            page1: NonZeroU16::new(1).unwrap(),
            session_loss: 0,
            fetch_failure: 0,
            threads: BTreeSet::new(),
          }
        }
        Self::Theme {
          timeout,
          credentials,
          session_task,
          theme_id,
          page1,
          session_loss,
          fetch_failure,
          threads,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          match GLOBAL_HAMMEREFEST_RATE_LIMITER
            .lock()
            .expect("failed to acquire Hammerfest RateLimiter")
            .acquire(cx.now(), 1)
          {
            Ok(()) => {}
            Err(RateLimiterError::Wait(t)) => {
              cx.inc_starvation();
              *timeout = Some(cx.sleep_until(t));
              continue;
            }
            Err(RateLimiterError::OverLimit) => unreachable!("acquiring 1 token is never over the limit"),
          };
          let session: HammerfestSession = match session_task.poll(cx).await {
            TaskPoll::Pending => return TaskPoll::Pending,
            TaskPoll::Ready(Err(e)) => {
              *self = Self::Ready(Err(Box::new(ScrapeHammerfestThemeError::from(e))));
              continue;
            }
            TaskPoll::Ready(Ok(session)) => session,
          };
          *session_loss = 0;
          match cx
            .extra()
            .hammerfest_client()
            .get_forum_theme_page(Some(&session), credentials.server, *theme_id, *page1)
            .await
          {
            Err(e) => {
              match e {
                HammerfestClientGetThemePageError::NotFound => {
                  *self = Self::Ready(Err(Box::new(ScrapeHammerfestThemeError::NotFound)));
                }
                HammerfestClientGetThemePageError::Forbidden => {
                  // May be caused by a session loss, check if the session is still active
                  match cx
                    .extra()
                    .hammerfest_client()
                    .test_session(credentials.server, &session.key)
                    .await
                  {
                    Ok(Some(sess)) if sess.key == session.key && sess.user.id == session.user.id => {
                      *self = Self::Ready(Err(Box::new(ScrapeHammerfestThemeError::Forbidden)));
                    }
                    Ok(_) => {
                      // No session or different session
                      eprintln!("[ScrapeHammerfestTheme::session_loss_redirect]");
                      if let Err(e) = cx
                        .extra()
                        .hammerfest_store()
                        .touch_session(TouchSession {
                          now: cx.now(),
                          server: session.user.server,
                          key: session.key.clone(),
                          user: None,
                        })
                        .await
                      {
                        eprintln!("[ScrapeHammerfestTheme::touch_session_error] {e:#}");
                      }
                      *session_task = ReliableHammerfestAcquireSession::new(credentials.clone());
                      *session_loss += 1;
                      *timeout = Some(cx.sleep(error_sleep(*session_loss)));
                      continue;
                    }
                    Err(e) => {
                      eprintln!("[ScrapeHammerfestTheme::forbidden] {e:#}");
                      *fetch_failure += 1;
                      *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
                      continue;
                    }
                  }
                }
                HammerfestClientGetThemePageError::Other(e) => {
                  eprintln!("[ScrapeHammerfestTheme::fetch] {e:#}");
                  *fetch_failure += 1;
                  *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
                  continue;
                }
              }
            }
            Ok(response) => {
              if response.session.is_none() {
                *session_loss += 1;
                if let Err(e) = cx
                  .extra()
                  .hammerfest_store()
                  .touch_session(TouchSession {
                    now: cx.now(),
                    server: session.user.server,
                    key: session.key.clone(),
                    user: None,
                  })
                  .await
                {
                  eprintln!("[ScrapeHammerfestTheme::missing_session] {e:#}");
                }
                *session_task = ReliableHammerfestAcquireSession::new(credentials.clone());
                *timeout = Some(cx.sleep(error_sleep(*session_loss)));
                continue;
              }
              match cx.extra().hammerfest_store().touch_theme_page(&response).await {
                Err(e) => {
                  eprintln!("[ScrapeHammerfestTheme::write] {e:#}");
                  *fetch_failure += 1;
                  *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
                  continue;
                }
                Ok(()) => {
                  *fetch_failure = 0;
                  for sticky in response.page.sticky {
                    threads.insert((sticky.short.id, NonZeroU16::new(1 + sticky.reply_count).unwrap()));
                  }
                  for thread in response.page.threads.items {
                    threads.insert((thread.short.id, NonZeroU16::new(1 + thread.reply_count).unwrap()));
                  }
                  let next_page = NonZeroU16::new(1 + page1.get()).unwrap();
                  if next_page.get() <= response.page.threads.pages.get() {
                    *page1 = next_page;
                    continue;
                  } else {
                    *self = Self::Threads(Box::new(ScrapeHammerfestThreadList::Start {
                      credentials: credentials.clone(),
                      threads: threads.iter().copied().collect(),
                    }))
                  }
                }
              }
            }
          }
        }
        Self::Threads(threads) => match threads.poll(cx).await {
          TaskPoll::Pending => return TaskPoll::Pending,
          TaskPoll::Ready(r) => *self = Self::Ready(r.map_err(|e| Box::new(ScrapeHammerfestThemeError::ThreadList(e)))),
        },
        Self::Ready(result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
