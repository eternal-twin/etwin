use async_trait::async_trait;
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::oauth::RfcOauthAccessTokenKey;
use eternaltwin_core::twinoid::client::{FullUser, SafeUser};
use eternaltwin_core::twinoid::store::TwinoidStoreRef;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::ops::Range;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ArchiveTwinoidUsersSingleToken(pub ArchiveTwinoidUsersWorker);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum ArchiveTwinoidUsersError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ArchiveTwinoidUsersSingleToken
where
  Cx: TaskCx,
  Cx::Extra: TwinoidStoreRef,
{
  const NAME: &'static str = "ArchiveTwinoidUsersSingleToken";
  const VERSION: u32 = 1;
  type Output = Result<(), ArchiveTwinoidUsersError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    self.0.poll(cx).await
  }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum ArchiveTwinoidUsersWorker {
  Start {
    token: RfcOauthAccessTokenKey,
    range: Range<u32>,
  },
  Read {
    timeout: Option<Sleep>,
    token: RfcOauthAccessTokenKey,
    range: Range<u32>,
    scope: Scope,
    read_errors: u32,
    last_error: Option<WeakError>,
  },
  Write {
    timeout: Option<Sleep>,
    token: RfcOauthAccessTokenKey,
    range: Range<u32>,
    result: WriteScope,
    write_errors: u32,
    last_error: Option<WeakError>,
  },
  Ready(Result<(), ArchiveTwinoidUsersError>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Scope {
  Full1,
  Safe1,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum WriteScope {
  Full1(Vec<FullUser>),
  Safe1(Vec<SafeUser>),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ArchiveTwinoidUsersWorker
where
  Cx: TaskCx,
  Cx::Extra: TwinoidStoreRef,
{
  const NAME: &'static str = "ArchiveTwinoidUsersWorker";
  const VERSION: u32 = 1;
  type Output = Result<(), ArchiveTwinoidUsersError>;

  async fn poll(&mut self, _cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    todo!()
  }
}
