use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::clock::{Clock, ClockRef, Scheduler, SchedulerRef};
use eternaltwin_core::core::{Duration, Listing};
use eternaltwin_core::email::{Mailer, MailerRef};
pub use eternaltwin_core::forum::{CreatePostError, UpdatePostError};
use eternaltwin_core::hammerfest::{HammerfestStore, HammerfestStoreRef};
use eternaltwin_core::job::{
  store, ApiTask, Job, JobId, JobRuntime, JobStore, JobStoreRef, OpaqueTask, OpaqueValue, ShortTask, StoreJob,
  StoreTask, TaskId, TaskKind, TaskStatus,
};
use eternaltwin_core::mailer::store::{MailerStore, MailerStoreRef};
use eternaltwin_core::opentelemetry::DynTracer;
use eternaltwin_core::rate_limiter::{LeakyBucketRateLimiter, LeakyBucketRateLimiterConfig};
use eternaltwin_core::twinoid::store::TwinoidStoreRef;
use eternaltwin_core::twinoid::TwinoidStore;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{
  GetShortUserOptions, RawGetShortUserError, ShortUser, UserId, UserIdRef, UserStore, UserStoreRef,
};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::future::Future;
use std::num::NonZeroU64;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub mod archive_twinoid_users;
pub mod archive_twinoid_users_single_token;
pub mod reliable_hammerfest_acquire_session;
pub mod scrape_all_hammerfest_profiles;
pub mod scrape_hammerfest_theme;
pub mod scrape_hammerfest_thread;
pub mod scrape_hammerfest_thread_list;
pub mod with_twinoid_access_token;

static GLOBAL_HAMMEREFEST_RATE_LIMITER: Lazy<Mutex<LeakyBucketRateLimiter>> = Lazy::new(|| {
  let limiter = LeakyBucketRateLimiter::new(LeakyBucketRateLimiterConfig {
    limit: NonZeroU64::new(50).unwrap(),
    tokens_per_period: NonZeroU64::new(5).unwrap(),
    period_duration: Duration::from_millis(500),
  });
  Mutex::new(limiter)
});

pub fn error_sleep(failures: u32) -> Duration {
  const FIRST: Duration = Duration::from_seconds(2);
  const BASE: i32 = 2;
  const MAX_EXP: u32 = 12;
  let exp: u32 = core::cmp::min(MAX_EXP, failures);
  let factor = BASE.checked_pow(exp).unwrap_or(4096);
  FIRST * factor
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct CreateJob {
  pub kind: TaskKind,
  pub kind_version: u32,
  pub state: OpaqueTask,
}

#[derive(Error, Debug)]
pub enum CreateJobError {
  #[error("current actor does not have the permission to create a job")]
  Forbidden,
  #[error("unknown task kind {0:?}")]
  UnknownKind(TaskKind),
  #[error("failed to read input opaque task")]
  ReadTask(#[source] WeakError),
  #[error(transparent)]
  Other(WeakError),
}

impl CreateJobError {
  pub fn read_task<E: std::error::Error>(e: E) -> Self {
    Self::ReadTask(WeakError::wrap(e))
  }

  pub fn other<E: std::error::Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GetJobs {
  pub offset: u32,
  pub limit: u32,
  pub status: Option<TaskStatus>,
  pub creator: Option<UserIdRef>,
}

#[derive(Error, Debug)]
pub enum GetJobsError {
  #[error("current actor does not have the permission to get jobs")]
  Forbidden,
  #[error("failed to retrieve creator")]
  InternalGetShortUser(RawGetShortUserError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GetJob {
  pub id: JobId,
}

#[derive(Error, Debug)]
pub enum GetJobError {
  #[error("job not found: {0}")]
  NotFound(JobId),
  #[error("current actor does not have the permission to get this job")]
  Forbidden,
  #[error("failed to retrieve creator")]
  InternalGetShortUser(RawGetShortUserError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GetTask {
  pub id: TaskId,
}

#[derive(Error, Debug)]
pub enum GetTaskError {
  #[error("job not found: {0}")]
  NotFound(TaskId),
  #[error("current actor does not have the permission to get this task")]
  Forbidden,
  #[error(transparent)]
  Other(WeakError),
}

pub struct JobRuntimeExtra<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore> {
  pub hammerfest_store: TyHammerfestStore,
  pub mailer_client: TyMailerClient,
  pub mailer_store: TyMailerStore,
  pub twinoid_store: TyTwinoidStore,
}

impl<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore> HammerfestStoreRef
  for JobRuntimeExtra<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore>
where
  Self: Send + Sync,
  TyHammerfestStore: HammerfestStoreRef,
{
  type HammerfestStore = TyHammerfestStore::HammerfestStore;

  fn hammerfest_store(&self) -> &Self::HammerfestStore {
    self.hammerfest_store.hammerfest_store()
  }
}

impl<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore> MailerRef
  for JobRuntimeExtra<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore>
where
  Self: Send + Sync,
  TyMailerClient: MailerRef,
{
  type Mailer = TyMailerClient::Mailer;

  fn mailer(&self) -> &Self::Mailer {
    self.mailer_client.mailer()
  }
}

impl<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore> MailerStoreRef<JsonValue>
  for JobRuntimeExtra<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore>
where
  Self: Send + Sync,
  TyMailerStore: MailerStoreRef<JsonValue>,
{
  type MailerStore = TyMailerStore::MailerStore;

  fn mailer_store(&self) -> &Self::MailerStore {
    self.mailer_store.mailer_store()
  }
}

impl<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore> TwinoidStoreRef
  for JobRuntimeExtra<TyHammerfestStore, TyMailerClient, TyMailerStore, TyTwinoidStore>
where
  Self: Send + Sync,
  TyTwinoidStore: TwinoidStoreRef,
{
  type TwinoidStore = TyTwinoidStore::TwinoidStore;

  fn twinoid_store(&self) -> &Self::TwinoidStore {
    self.twinoid_store.twinoid_store()
  }
}

pub type DynJobRuntimeExtra =
  JobRuntimeExtra<Arc<dyn HammerfestStore>, Arc<dyn Mailer>, Arc<dyn MailerStore<JsonValue>>, Arc<dyn TwinoidStore>>;

pub struct JobService<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyTracer, TyUserStore>
where
  TyClock: ClockRef,
  TyJobStore: JobStoreRef<OpaqueTask, OpaqueValue>,
{
  clock: TyClock,
  job_store: TyJobStore,
  #[allow(unused)]
  job_runtime: Arc<JobRuntime<'reg, TyClock, TyJobStore, TyTracer, TyJobRuntimeExtra>>,
  user_store: TyUserStore,
}

pub type DynJobService = JobService<
  'static,
  Arc<dyn Scheduler<Timer = Pin<Box<dyn Future<Output = ()> + Send>>>>,
  DynJobRuntimeExtra,
  Arc<dyn JobStore<OpaqueTask, OpaqueValue>>,
  DynTracer,
  Arc<dyn UserStore>,
>;

impl<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyTracer, TyUserStore>
  JobService<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyTracer, TyUserStore>
where
  TyClock: SchedulerRef,
  TyJobStore: JobStoreRef<OpaqueTask, OpaqueValue>,
  TyUserStore: UserStoreRef,
{
  #[allow(clippy::too_many_arguments)]
  pub fn new(
    clock: TyClock,
    job_runtime: Arc<JobRuntime<'reg, TyClock, TyJobStore, TyTracer, TyJobRuntimeExtra>>,
    job_store: TyJobStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      clock,
      job_store,
      job_runtime,
      user_store,
    }
  }

  pub async fn create_job(&self, acx: &AuthContext, cmd: &CreateJob) -> Result<Job, CreateJobError> {
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(CreateJobError::Forbidden),
    };

    Err(CreateJobError::UnknownKind(cmd.kind.clone()))
  }

  pub async fn get_jobs(&self, acx: &AuthContext, query: &GetJobs) -> Result<Listing<Job>, GetJobsError> {
    let now = self.clock.clock().now();
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(GetJobsError::Forbidden),
    };

    let jobs: Listing<StoreJob> = self
      .job_store
      .job_store()
      .get_jobs(store::GetJobs {
        status: query.status,
        creator: None,
        offset: query.offset,
        limit: query.limit,
      })
      .await
      .map_err(|e| GetJobsError::Other(WeakError::wrap(e)))?;

    let mut users: HashMap<UserId, ShortUser> = HashMap::new();
    for j in &jobs.items {
      let c = if let Some(c) = j.created_by { c } else { continue };
      let e = if let Entry::Vacant(e) = users.entry(c.id) {
        e
      } else {
        continue;
      };
      let u = self
        .user_store
        .user_store()
        .get_short_user(&GetShortUserOptions {
          r#ref: c.id.into(),
          time: Some(now),
          skip_deleted: false,
        })
        .await
        .map_err(GetJobsError::InternalGetShortUser)?;
      e.insert(u);
    }

    let jobs = jobs.map(|job| -> Job {
      Job {
        id: job.id,
        created_at: job.created_at,
        created_by: job
          .created_by
          .map(|c| users.get(&c.id).cloned().expect("user is resolved")),
        task: ShortTask {
          id: job.task.id,
          revision: job.task.revision,
          polled_at: job.task.polled_at.map(|tick| tick.time),
          status: job.task.status,
          kind: job.task.kind,
          kind_version: job.task.kind_version,
          starvation: job.task.starvation,
        },
      }
    });

    Ok(jobs)
  }

  pub async fn get_job(&self, acx: &AuthContext, query: &GetJob) -> Result<Job, GetJobError> {
    let now = self.clock.clock().now();
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(GetJobError::Forbidden),
    };

    let job: StoreJob = self
      .job_store
      .job_store()
      .get_job(store::GetJob { id: query.id })
      .await
      .map_err(|e| GetJobError::Other(WeakError::wrap(e)))?;

    let job = Job {
      id: job.id,
      created_at: job.created_at,
      created_by: match job.created_by {
        None => None,
        Some(user) => Some({
          self
            .user_store
            .user_store()
            .get_short_user(&GetShortUserOptions {
              r#ref: user.into(),
              time: Some(now),
              skip_deleted: false,
            })
            .await
            .map_err(GetJobError::InternalGetShortUser)?
        }),
      },
      task: ShortTask {
        id: job.task.id,
        revision: job.task.revision,
        polled_at: job.task.polled_at.map(|tick| tick.time),
        status: job.task.status,
        starvation: job.task.starvation,
        kind: job.task.kind,
        kind_version: job.task.kind_version,
      },
    };
    Ok(job)
  }

  pub async fn get_task(&self, acx: &AuthContext, query: &GetTask) -> Result<ApiTask, GetTaskError> {
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(GetTaskError::Forbidden),
    };

    let task: StoreTask<OpaqueTask, OpaqueValue> = self
      .job_store
      .job_store()
      .get_task(store::GetTask { id: query.id })
      .await
      .map_err(|e| GetTaskError::Other(WeakError::wrap(e)))?;

    let task = ApiTask {
      id: task.id,
      revision: task.revision,
      polled_at: task.polled_at.map(|tick| tick.time),
      status: task.status,
      starvation: task.starvation,
      kind: task.kind,
      kind_version: task.kind_version,
      state: task.state,
      output: task.output,
    };
    Ok(task)
  }
}
