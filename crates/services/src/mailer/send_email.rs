use async_trait::async_trait;
use eternaltwin_core::core::{IdempotencyKey, Instant};
use eternaltwin_core::email::{EmailAddress, EmailBody, EmailContent, EmailSubject, Mailer, MailerRef};
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::mailer::store::acquire_outbound_email_request::{
  AcquireOutboundEmailRequest, AcquireOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use eternaltwin_core::mailer::store::release_outbound_email_request::{
  ReleaseOutboundEmailRequest, ReleaseOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::{
  EmailContentPayload, EmailContentSummary, EmailPayloadKind, MailerStore, MailerStoreRef, OutboundEmail,
  OutboundEmailRequestIdRef,
};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserIdRef;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SendEmail(pub SendEmailWorker);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum SendEmailError {
  #[error("failed to store outbound email")]
  StoreEmail(#[source] CreateOutboundEmailError),
  #[error("failed to acquire request permit for outbound email")]
  AcquireRequest(#[source] AcquireOutboundEmailRequestError),
  #[error("mailer client encountered error while sending email")]
  MailerClient(#[source] WeakError),
  #[error("failed to release request permit for outbound email")]
  ReleaseRequest(#[source] ReleaseOutboundEmailRequestError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for SendEmail
where
  Cx: TaskCx,
  Cx::Extra: MailerStoreRef<JsonValue> + MailerRef,
{
  const NAME: &'static str = "SendEmail";
  const VERSION: u32 = 1;
  type Output = Result<(), SendEmailError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    self.0.poll(cx).await.map(|r| r.map(drop))
  }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum SendEmailWorker {
  Start {
    idempotency_key: IdempotencyKey,
    now: Instant,
    actor: UserIdRef,
    deadline: Instant,
    sender: EmailAddress,
    recipient: EmailAddress,
  },
  Send {
    timeout: Option<Sleep>,
    email: OutboundEmail<JsonValue>,
    content: EmailContent,
  },
  Ready(Result<OutboundEmail<JsonValue>, SendEmailError>),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for SendEmailWorker
where
  Cx: TaskCx,
  Cx::Extra: MailerStoreRef<JsonValue> + MailerRef,
{
  const NAME: &'static str = "SendEmailWorker";
  const VERSION: u32 = 1;
  type Output = Result<OutboundEmail<JsonValue>, SendEmailError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start {
          idempotency_key,
          now,
          actor,
          deadline,
          sender,
          recipient,
        } => {
          // todo: use email formatter trait
          let content = EmailContent {
            subject: EmailSubject::from_str("Hello!").expect("subject is OK"),
            body_text: EmailBody::from_str("Hey there, welcome to Eternaltwin!").expect("body is OK"),
            body_html: None,
          };
          let email = cx
            .extra()
            .mailer_store()
            .create_outbound_email(CreateOutboundEmail {
              idempotency_key: *idempotency_key,
              now: *now,
              actor: *actor,
              deadline: *deadline,
              sender: sender.clone(),
              recipient: recipient.clone(),
              payload: EmailContentPayload {
                kind: EmailPayloadKind::Marktwin,
                version: 1,
                data: JsonValue::Null,
                text: EmailContentSummary::EMPTY,
                html: EmailContentSummary::EMPTY,
              },
            })
            .await;
          match email {
            Ok(email) => {
              *self = Self::Send {
                timeout: None,
                email,
                content,
              }
            }
            Err(e) => *self = Self::Ready(Err(SendEmailError::StoreEmail(e))),
          }
        }
        Self::Send {
          timeout,
          email,
          content,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          match cx
            .extra()
            .mailer_store()
            .acquire_outbound_email_request(AcquireOutboundEmailRequest {
              now: cx.now(),
              outbound_email: email.id,
            })
            .await
          {
            Err(AcquireOutboundEmailRequestError::Limit(retry_at)) => {
              *timeout = Some(cx.sleep_until(retry_at));
              return TaskPoll::Pending;
            }
            Err(e) => {
              *self = Self::Ready(Err(SendEmailError::AcquireRequest(e)));
            }
            Ok(req_permit) => {
              // todo: apply timeout
              let send_res = cx.extra().mailer().send_email(&email.recipient, content).await;
              let release_res = cx
                .extra()
                .mailer_store()
                .release_outbound_email_request(ReleaseOutboundEmailRequest {
                  now: cx.now(),
                  outbound_email_request: OutboundEmailRequestIdRef::new(req_permit.id),
                  result: Some(send_res.as_ref().map(drop).map_err(drop)),
                })
                .await;
              *self = match (send_res, release_res) {
                (Err(e), _) => Self::Ready(Err(SendEmailError::MailerClient(e))),
                (Ok(()), Err(e)) => Self::Ready(Err(SendEmailError::ReleaseRequest(e))),
                (Ok(()), Ok(_)) => Self::Ready(Ok(email.clone())),
              }
            }
          }
        }
        Self::Ready(result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
