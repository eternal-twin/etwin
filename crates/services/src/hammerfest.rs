use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::core::UserDot;
use eternaltwin_core::hammerfest::{
  GetHammerfestUserOptions, HammerfestStore, HammerfestStoreRef, HammerfestUser, HammerfestUserIdRef,
  StoredHammerfestUser,
};
use eternaltwin_core::link::store::{LinkStore, LinkStoreRef};
use eternaltwin_core::link::{EtwinLink, GetLinkOptions, VersionedEtwinLink, VersionedRawLink};
use eternaltwin_core::user::{GetShortUserOptions, ShortUser, UserRef, UserStore, UserStoreRef};
use std::error::Error;
use std::sync::Arc;

pub struct HammerfestService<TyHammerfestStore, TyLinkStore, TyUserStore>
where
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyUserStore: UserStoreRef,
{
  hammerfest_store: TyHammerfestStore,
  link_store: TyLinkStore,
  user_store: TyUserStore,
}

pub type DynHammerfestService = HammerfestService<Arc<dyn HammerfestStore>, Arc<dyn LinkStore>, Arc<dyn UserStore>>;

impl<TyHammerfestStore, TyLinkStore, TyUserStore> HammerfestService<TyHammerfestStore, TyLinkStore, TyUserStore>
where
  TyHammerfestStore: HammerfestStoreRef,
  TyLinkStore: LinkStoreRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(hammerfest_store: TyHammerfestStore, link_store: TyLinkStore, user_store: TyUserStore) -> Self {
    Self {
      hammerfest_store,
      link_store,
      user_store,
    }
  }

  pub async fn get_user(
    &self,
    _acx: &AuthContext,
    options: &GetHammerfestUserOptions,
  ) -> Result<Option<HammerfestUser>, Box<dyn Error + Send + Sync + 'static>> {
    let user: Option<StoredHammerfestUser> = self.hammerfest_store.hammerfest_store().get_user(options).await?;
    let user: StoredHammerfestUser = match user {
      Some(user) => user,
      None => return Ok(None),
    };
    let etwin_link: VersionedRawLink<HammerfestUserIdRef> = {
      let options: GetLinkOptions<HammerfestUserIdRef> = GetLinkOptions {
        remote: HammerfestUserIdRef {
          server: user.server,
          id: user.id,
        },
        time: None,
      };
      self.link_store.link_store().get_link_from_hammerfest(options).await?
    };
    let etwin_link: VersionedEtwinLink = {
      let current = match etwin_link.current {
        None => None,
        Some(l) => {
          let user: ShortUser = self
            .user_store
            .user_store()
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.link.user),
              time: options.time,
              skip_deleted: false,
            })
            .await?;
          let etwin: ShortUser = self
            .user_store
            .user_store()
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.etwin),
              time: options.time,
              skip_deleted: false,
            })
            .await?;
          Some(EtwinLink {
            link: UserDot {
              time: l.link.time,
              user,
            },
            unlink: (),
            etwin,
          })
        }
      };
      VersionedEtwinLink { current, old: vec![] }
    };
    let hf_user = HammerfestUser {
      server: user.server,
      id: user.id,
      username: user.username,
      archived_at: user.archived_at,
      profile: user.profile,
      inventory: user.inventory,
      etwin: etwin_link,
    };
    Ok(Some(hf_user))
  }
}
